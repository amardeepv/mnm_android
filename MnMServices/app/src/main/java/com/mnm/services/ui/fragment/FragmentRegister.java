package com.mnm.services.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.kelltontech.ui.IScreen;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.StringUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.Constant;
import com.mnm.services.controller.ControllerRegister;
import com.mnm.services.model.request.RegisterRequest;
import com.mnm.services.model.response.RegisterResponse;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.activity.ActivityConfirmMobile;
import com.mnm.services.ui.activity.ActivityHome;
import com.mnm.services.ui.activity.ActivityVerifyMobile;
import com.mnm.services.utils.GlobalUtils;

import static com.mnm.services.constants.Constant.MOBILE_NUMBER;
import static com.mnm.services.constants.Constant.USER_ID;

public class FragmentRegister extends FragmentBase implements View.OnClickListener,IScreen {
    private EditText edtUserName, edtuserMobile, edtUserEmail, edtUserPassword,editReferalCode;
    private ProgressBar progressBar, mg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_register, container, false);
        edtUserName = (EditText)rootview. findViewById(R.id.user_name);
        edtuserMobile= (EditText)rootview. findViewById(R.id.user_mobile);
        edtUserEmail = (EditText)rootview. findViewById(R.id.user_email);
        edtUserPassword = (EditText)rootview. findViewById(R.id.user_password);
        editReferalCode=(EditText)rootview.findViewById(R.id.user_referal_code);
        rootview. findViewById(R.id.root_relay).setOnClickListener(this);
        rootview. findViewById(R.id.btn_signup).setOnClickListener(this);
        progressBar = (ProgressBar) rootview.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
        return rootview;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_signup:
                if (validation()) {
                    callRegisterApi();
                }
                break;
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(getActivity(), view);
                break;
            default:
                break;
        }
    }

    void callRegisterApi() {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            ControllerRegister registerController = new ControllerRegister(getActivity(), this);
            RegisterRequest registerRequest = getRegisterRequest();
            showProgressDialog("Please wait...");
            registerController.getData(AppSettings.REGISTER_REQUEST, registerRequest);
        }
    }

    private RegisterRequest getRegisterRequest() {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setFirstName(edtUserName.getText().toString());
        registerRequest.setLastName("");
        registerRequest.setEmailAddress(edtUserEmail.getText().toString());
        registerRequest.setMobilePrimary(edtuserMobile.getText().toString());
        registerRequest.setPassword(edtUserPassword.getText().toString());
        registerRequest.setReferalCode(editReferalCode.getText().toString());
        return registerRequest;
    }

 
    
    
    
    boolean validation() {
        if (edtUserName.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Please enter user name", Toast.LENGTH_SHORT).show();
            return false;
        }else if (edtUserName.getText().toString().length() <= 2) {
            Toast.makeText(getActivity(), "Name must be be greater than two character", Toast.LENGTH_SHORT).show();
            return false;
        } else if (edtuserMobile.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Please enter mobile number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (edtuserMobile.getText().toString().length() <= 9) {
            Toast.makeText(getActivity(), "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (edtUserEmail.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Please enter email id", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!StringUtils.isValidEmail(edtUserEmail.getText().toString())){
            Toast.makeText(getActivity(), "Please enter valid email id", Toast.LENGTH_SHORT).show();
            return false;
        } else if (edtUserPassword.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Please enter password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (edtUserPassword.getText().toString().length() <= 5) {
            Toast.makeText(getActivity(), "Your password must be atleast 6 characters long", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    @Override
    public void updateUi(final com.kelltontech.network.Response response) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                if (response == null) {
                    ToastUtils.showToast(getActivity(), getString(R.string.server_is_down_please_try_later));
                } else switch (response.getDataType()) {
                    case AppSettings.REGISTER_REQUEST:
                        removeProgressDialog();
                        RegisterResponse registerResponse = (RegisterResponse) response.getResponseObject();
                        if(!registerResponse.isHasError()&&registerResponse.getData()!=null){
                                if(registerResponse.getData().getAccessToken()!=null)
                                    GlobalUtils.saveAccessToken(registerResponse.getData().getAccessToken().getToken(),getActivity());
                                if(registerResponse.getData().getAccess()!=null)
                                    GlobalUtils.saveAccessToken(registerResponse.getData().getAccess().getToken(),getActivity());
                            AppSharedPreference.putString(PrefConstants.USER_ID,""+registerResponse.getData().getUserId(),getActivity());
                            AppSharedPreference.putString(PrefConstants.USER_NAME,""+registerResponse.getData().getUserName(),getActivity());
                            if(!StringUtils.isNullOrEmpty(registerResponse.getData().getStatus())){
                                if(registerResponse.getData().getStatus().equalsIgnoreCase("1")) {
                                    AppSharedPreference.putBoolean(PrefConstants.IS_LOGIN_TRUE, true, getActivity());
                                    if(getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER,false)){
                                        Intent mIntent = new Intent(getActivity(), ActivityHome.class);
                                        mIntent.putExtra(USER_ID, "" + registerResponse.getData().getUserId());
                                        mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(mIntent);
                                    }
                                    getActivity().finish();
                                }else  if(registerResponse.getData().getStatus().equalsIgnoreCase("2")) {
                                    Intent mIntent = new Intent(getActivity(), ActivityConfirmMobile.class);
                                    mIntent.putExtra(USER_ID, "" + registerResponse.getData().getUserId());
                                    mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                    mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER,getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER,false));
                                    startActivity(mIntent);
                                    getActivity().finish();
                                } else  if(registerResponse.getData().getStatus().equalsIgnoreCase("3")) {
                                    Intent mIntent = new Intent(getActivity(), ActivityVerifyMobile.class);
                                    mIntent.putExtra(USER_ID, "" + registerResponse.getData().getUserId());
                                    mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                    mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER,getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER,false));
                                    startActivity(mIntent);
                                    getActivity().finish();
                                }
                            }else if (!StringUtils.isNullOrEmpty(registerResponse.getData().getAccontStatus())){
                                if(registerResponse.getData().getAccontStatus().equalsIgnoreCase("1")) {
                                    AppSharedPreference.putBoolean(PrefConstants.IS_LOGIN_TRUE, true, getActivity());
                                    if(getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER,false)){
                                        Intent mIntent = new Intent(getActivity(), ActivityHome.class);
                                        mIntent.putExtra(USER_ID, "" + registerResponse.getData().getUserId());
                                        mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(mIntent);
                                    }
                                    getActivity().finish();
                                }else  if(registerResponse.getData().getAccontStatus().equalsIgnoreCase("2")) {
                                    Intent mIntent = new Intent(getActivity(), ActivityConfirmMobile.class);
                                    mIntent.putExtra(USER_ID, "" + registerResponse.getData().getUserId());
                                    mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                    mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER,getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER,false));
                                    startActivity(mIntent);
                                    getActivity().finish();
                                } else  if(registerResponse.getData().getAccontStatus().equalsIgnoreCase("3")) {
                                    Intent mIntent = new Intent(getActivity(), ActivityVerifyMobile.class);
                                    mIntent.putExtra(USER_ID, "" + registerResponse.getData().getUserId());
                                    mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                    mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER,getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER,false));
                                    startActivity(mIntent);
                                    getActivity().finish();
                                }
                            }else if(!StringUtils.isNullOrEmpty(registerResponse.getData().getAccountStatus())){
                                if(registerResponse.getData().getAccountStatus().equalsIgnoreCase("1")) {
                                    AppSharedPreference.putBoolean(PrefConstants.IS_LOGIN_TRUE, true, getActivity());
                                    if(getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER,false)){
                                        Intent mIntent = new Intent(getActivity(), ActivityHome.class);
                                        mIntent.putExtra(USER_ID, "" + registerResponse.getData().getUserId());
                                        mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(mIntent);
                                    }
                                    getActivity().finish();
                                }else  if(registerResponse.getData().getAccountStatus().equalsIgnoreCase("2")) {
                                    Intent mIntent = new Intent(getActivity(), ActivityConfirmMobile.class);
                                    mIntent.putExtra(USER_ID, "" + registerResponse.getData().getUserId());
                                    mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                    mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER,getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER,false));
                                    startActivity(mIntent);
                                    getActivity(). finish();
                                } else  if(registerResponse.getData().getAccountStatus().equalsIgnoreCase("3")) {
                                    Intent mIntent = new Intent(getActivity(), ActivityVerifyMobile.class);
                                    mIntent.putExtra(USER_ID, "" + registerResponse.getData().getUserId());
                                    mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                    mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER,getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER,false));
                                    startActivity(mIntent);
                                    getActivity().finish();
                                }
                            }
                            ToastUtils.showToast(getActivity(), "Hello "+edtUserName.getText().toString()+" Welcome to Make n Make." );
                        }else {
                            ToastUtils.showToast(getActivity(), "" + registerResponse.getMessage());
                        }

                        break;
                    default:
                        break;
                }
            }
        });
    }

}



