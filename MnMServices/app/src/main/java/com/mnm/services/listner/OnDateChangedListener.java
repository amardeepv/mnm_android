package com.mnm.services.listner;

import java.text.ParseException;

/**
 * Created by saten on 5/2/17.
 */

public interface OnDateChangedListener {
    public void onDateChanged(int month, int day, int year) throws ParseException;
}
