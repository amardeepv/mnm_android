package com.mnm.services.model.response.myorder;

/**
 * Created by saten on 4/25/17.
 */

public class PlanOrder {
    private String PlanId;
    private String PlanName;
    private String TotalCost;
    private String Created;
    private String OrderID;
    private String Expirydate;
    private String adddressId;
    private String address;

    public String getPlanDesc() {
        return PlanDesc;
    }

    public void setPlanDesc(String planDesc) {
        PlanDesc = planDesc;
    }

    private String PlanDesc;

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    private String paymentStatus;

    public String getPlanId() {
        return PlanId;
    }

    public void setPlanId(String planId) {
        PlanId = planId;
    }

    public String getPlanName() {
        return PlanName;
    }

    public void setPlanName(String planName) {
        PlanName = planName;
    }

    public String getTotalCost() {
        return TotalCost;
    }

    public void setTotalCost(String totalCost) {
        TotalCost = totalCost;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public String getOrderID() {
        return OrderID;
    }

    public void setOrderID(String orderID) {
        OrderID = orderID;
    }

    public String getExpirydate() {
        return Expirydate;
    }

    public void setExpirydate(String expirydate) {
        Expirydate = expirydate;
    }

    public String getAdddressId() {
        return adddressId;
    }

    public void setAdddressId(String adddressId) {
        this.adddressId = adddressId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
