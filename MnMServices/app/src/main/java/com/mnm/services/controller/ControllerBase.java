package com.mnm.services.controller;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.kelltontech.controller.BaseController;
import com.kelltontech.network.ServiceRequest;
import com.kelltontech.ui.IScreen;
import com.mnm.services.constants.ArgumentsKeys;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.prefrence.SharedPreferenceAssist;

public abstract class ControllerBase extends BaseController {
Context mContext;
    public ControllerBase(Activity activity, IScreen screen) {
        super(activity, screen);
        mContext=activity;
    }


    protected void setHttpsHeaders(ServiceRequest serviceRequest) {
        String[] params = {"Content-Type",
                "Authorization"};

            String[] values = {"application/json",
                    "bearer "+ AppSharedPreference.getString(PrefConstants.AUTHORIZATION_TOKEN,"",mContext)};
           //if(AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE,false,mContext))
            serviceRequest.setHttpHeaders(params, values);
        }
    }


