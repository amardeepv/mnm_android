package com.mnm.services.model.response.profile;

/**
 * Created by satendra.singh on 02-05-2017.
 */

public class ConsumerContact {
    private String alterNateContactNumber;

    public String getAlterNateContactNumber() {
        return alterNateContactNumber;
    }

    public void setAlterNateContactNumber(String alterNateContactNumber) {
        this.alterNateContactNumber = alterNateContactNumber;
    }
}
