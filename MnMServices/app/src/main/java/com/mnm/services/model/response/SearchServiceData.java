package com.mnm.services.model.response;

import java.util.List;

/**
 * Created by saten on 3/11/17.
 */

public class SearchServiceData {
    private List<ServiceCategoryModel> services = null;

    public List<ServiceCategoryModel> getServices() {
        return services;
    }

    public void setServices(List<ServiceCategoryModel> services) {
        this.services = services;
    }
}