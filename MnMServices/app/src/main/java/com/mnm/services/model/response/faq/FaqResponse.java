package com.mnm.services.model.response.faq;

import java.util.List;

/**
 * Created by satendra.singh on 10-05-2017.
 */

public class FaqResponse{
    private List<FaqData> data = null;

        public List<FaqData> getData() {
            return data;
        }

        public void setData(List<FaqData> data) {
            this.data = data;
        }
}
