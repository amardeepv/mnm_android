package com.mnm.services.ui.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.kelltontech.utils.StringUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.listner.AddFeedBackListner;
import com.mnm.services.model.request.AddFeedbcakRequest;
import com.mnm.services.model.response.myorder.ServiceData;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.widgets.NonScrollListView;
import com.mnm.services.utils.GlobalUtils;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Adapter to manage list of properties
 */
public class AdapterMyServiceOrder extends RecyclerView.Adapter<AdapterMyServiceOrder.ViewHolder> {
    List<ServiceData>  mList;
    private Context mContext;
    private View mItemLayoutView;
    private ViewHolder mViewHolder;
    private LinearLayoutManager linearLayoutManager;
  AddFeedBackListner addFeedBackListner;

    public AdapterMyServiceOrder(Context ctx, List<ServiceData>  mList, RecyclerView recyclerView,AddFeedBackListner addFeedBackListner) {
        this.mList = mList;
        this.mContext = ctx;
        this.addFeedBackListner=addFeedBackListner;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                }
            });
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("create","create view holder****");
        mItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_service_order_history, null);
        mViewHolder = new ViewHolder(mItemLayoutView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        final ServiceData serviceModel = mList.get(position);
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if (serviceModel != null) {
            viewHolder.txtTicketId.setText("" + serviceModel.getTicketID());
            viewHolder.txtServiceName.setText(serviceModel.getServiceParentName() + "->" + serviceModel.getServiceName());
            viewHolder.txtPaymentStatus.setText(serviceModel.getPaymentStatus());
            viewHolder.txtOrderId.setText("" + serviceModel.getOrderID());
            String date = "";
            if (!StringUtils.isNullOrEmpty(serviceModel.getCreated())) {
                String mdatenTime = serviceModel.getCreated();
                try {
                    date = mdatenTime.substring(0, mdatenTime.indexOf("T"));
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }


            Picasso.with(mContext)
                    .load(AppSettings.BASE_URL + serviceModel.getEngineerImagePath())
                    .placeholder(R.mipmap.user_image) //this is optional the image to display while the url image is downloading
                    .error(R.mipmap.user_image)         //this is also optional if some error has occurred in downloading the image this image would be displayed
                    .into(viewHolder.imgUser);
            viewHolder.txtDateTime.setText(GlobalUtils.getDateString(date));
            viewHolder.txtRate.setText("" + serviceModel.getAmount());
            viewHolder.txtStatus.setText(serviceModel.getTicketStatus());
            viewHolder.txtRating.setText(StringUtils.isNullOrEmpty(serviceModel.getRating()) ? "" : "" + serviceModel.getRating());
            viewHolder.txtUserName.setText(StringUtils.isNullOrEmpty(serviceModel.getEngineerName()) ? "" : serviceModel.getEngineerName());
            viewHolder.textDescription.setText(StringUtils.isNullOrEmpty(serviceModel.getTicketDescription()) ? "" : serviceModel.getTicketDescription());

            if(!StringUtils.isNullOrEmpty(""+serviceModel.getFeedbackRating()))
            viewHolder.ratingBarFeedback.setRating(serviceModel.getFeedbackRating());

            if(StringUtils.isNullOrEmpty(serviceModel.getFeedBacktxtMesaage()))
            viewHolder.edtFeedbackTxt.setText(serviceModel.getFeedBacktxtMesaage());

            if (serviceModel.getTicketHistoryList() != null && serviceModel.getTicketHistoryList().size() > 0) {
                AdapterTicketHistory adapterTicketHistory = new AdapterTicketHistory(mContext, serviceModel.getTicketHistoryList());
                viewHolder.historyList.setAdapter(adapterTicketHistory);
            }else {
                AdapterTicketHistory adapterTicketHistory = new AdapterTicketHistory(mContext, serviceModel.getTicketHistoryList());
                viewHolder.historyList.setAdapter(adapterTicketHistory);
            }
            if (serviceModel.getFeedbackHistoryList() != null && serviceModel.getFeedbackHistoryList().size() > 0) {
                AdapterFeedbackHistory adapterFeedbackHistory = new AdapterFeedbackHistory(mContext, serviceModel.getFeedbackHistoryList());
                viewHolder.feedbackList.setAdapter(adapterFeedbackHistory);
            }else{
                AdapterFeedbackHistory adapterFeedbackHistory = new AdapterFeedbackHistory(mContext, serviceModel.getFeedbackHistoryList());
                viewHolder.feedbackList.setAdapter(adapterFeedbackHistory);
            }
            if (serviceModel.isDetailBtnClicked()) {
                viewHolder.detailLayout.setVisibility(View.VISIBLE);
                viewHolder.btnDetail.setText("Hide");
            } else {
                viewHolder.detailLayout.setVisibility(View.GONE);
                viewHolder.btnDetail.setText("Details");
            }

            if (serviceModel.isDescriptioonClicked()) {
                viewHolder.layoutDescription.setVisibility(View.VISIBLE);
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    viewHolder.btnDescription.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.arrow_down));
                } else {
                    viewHolder.btnDescription.setImageDrawable(mContext.getResources().getDrawable(R.drawable.arrow_down));
                }
            } else {
                viewHolder.layoutDescription.setVisibility(View.GONE);
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    viewHolder.btnDescription.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.arrow_up));
                } else {
                    viewHolder.btnDescription.setImageDrawable(mContext.getResources().getDrawable(R.drawable.arrow_up));
                }
            }
            if (serviceModel.isFeedBackClicked()) {
                viewHolder.layoutFeedback.setVisibility(View.VISIBLE);
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    viewHolder.btnFeedback.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.arrow_down));
                } else {
                    viewHolder.btnFeedback.setImageDrawable(mContext.getResources().getDrawable(R.drawable.arrow_down));
                }
            } else {
                viewHolder.layoutFeedback.setVisibility(View.GONE);
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    viewHolder.btnFeedback.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.arrow_up));
                } else {
                    viewHolder.btnFeedback.setImageDrawable(mContext.getResources().getDrawable(R.drawable.arrow_up));
                }
            }
            if (serviceModel.isHistoryClicked()) {
                viewHolder.layoutHistory.setVisibility(View.VISIBLE);
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    viewHolder.btnHistory.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.arrow_down));
                } else {
                    viewHolder.btnHistory.setImageDrawable(mContext.getResources().getDrawable(R.drawable.arrow_down));
                }
            } else {
                viewHolder.layoutHistory.setVisibility(View.GONE);
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    viewHolder.btnHistory.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.arrow_up));
                } else {
                    viewHolder.btnHistory.setImageDrawable(mContext.getResources().getDrawable(R.drawable.arrow_up));
                }
            }
        }
        if (serviceModel.isFeedbackDisplayClicked()) {
            viewHolder.feedbackDisplayLayout.setVisibility(View.VISIBLE);
        } else {
            viewHolder.feedbackDisplayLayout.setVisibility(View.GONE);
        }
        viewHolder.btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (serviceModel.isDetailBtnClicked()) {
                    viewHolder.detailLayout.setVisibility(View.GONE);
                    serviceModel.setDetailBtnClicked(false);
                    viewHolder.btnDetail.setText("Details");

                } else {
                    viewHolder.detailLayout.setVisibility(View.VISIBLE);
                    viewHolder.btnDetail.setText("Hide");
                    serviceModel.setDetailBtnClicked(true);
                }
            }
        });
        viewHolder.btnDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!StringUtils.isNullOrEmpty(serviceModel.getServiceDescription())) {
                    if (serviceModel.isDescriptioonClicked()) {
                        viewHolder.layoutDescription.setVisibility(View.GONE);
                        serviceModel.setDescriptioonClicked(false);
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            viewHolder.btnDescription.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.arrow_up));
                        } else {
                            viewHolder.btnDescription.setImageDrawable(mContext.getResources().getDrawable(R.drawable.arrow_up));
                        }
                    } else {
                        viewHolder.layoutDescription.setVisibility(View.VISIBLE);
                        serviceModel.setDescriptioonClicked(true);
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            viewHolder.btnDescription.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.arrow_down));
                        } else {
                            viewHolder.btnDescription.setImageDrawable(mContext.getResources().getDrawable(R.drawable.arrow_down));
                        }
                    }
                } else {
                    ToastUtils.showToast(mContext, "No data available for description");
                }
            }
        });
        viewHolder.btnFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  if (serviceModel.getFeedbackHistoryList() != null && serviceModel.getFeedbackHistoryList().size() > 0) {
                if (serviceModel.isFeedBackClicked()) {
                    viewHolder.layoutFeedback.setVisibility(View.GONE);
                    serviceModel.setFeedBackClicked(false);
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        viewHolder.btnFeedback.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.arrow_up));
                    } else {
                        viewHolder.btnFeedback.setImageDrawable(mContext.getResources().getDrawable(R.drawable.arrow_up));
                    }
                } else {
                    viewHolder.layoutFeedback.setVisibility(View.VISIBLE);
                    serviceModel.setFeedBackClicked(true);
                    if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        viewHolder.btnFeedback.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.arrow_down));
                    } else {
                        viewHolder.btnFeedback.setImageDrawable(mContext.getResources().getDrawable(R.drawable.arrow_down));
                    }
                }
                /*} else {
                   ToastUtils.showToast(mContext,"No data available for feedback history");
                }*/
            }
        });
        viewHolder.btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (serviceModel.getTicketHistoryList() != null && serviceModel.getTicketHistoryList().size() > 0) {
                    if (serviceModel.isHistoryClicked()) {
                        viewHolder.layoutHistory.setVisibility(View.GONE);
                        serviceModel.setHistoryClicked(false);
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            viewHolder.btnHistory.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.arrow_up));
                        } else {
                            viewHolder.btnHistory.setImageDrawable(mContext.getResources().getDrawable(R.drawable.arrow_up));
                        }
                    } else {
                        viewHolder.layoutHistory.setVisibility(View.VISIBLE);
                        serviceModel.setHistoryClicked(true);
                        if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            viewHolder.btnHistory.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.arrow_down));
                        } else {
                            viewHolder.btnHistory.setImageDrawable(mContext.getResources().getDrawable(R.drawable.arrow_down));
                        }
                    }
                } else {
                    ToastUtils.showToast(mContext, "No data available for ticket history");
                }
            }
        });

        viewHolder.relayFeedbackLbl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (serviceModel.isFeedbackDisplayClicked()) {
                    viewHolder.feedbackDisplayLayout.setVisibility(View.GONE);
                    serviceModel.setFeedbackDisplayClicked(false);
                } else {
                    viewHolder.feedbackDisplayLayout.setVisibility(View.VISIBLE);
                    serviceModel.setFeedbackDisplayClicked(true);
                }
            }
        });

        viewHolder.btnFeedbackSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation(position)) {
                    AddFeedbcakRequest addFeedbcakRequest=new AddFeedbcakRequest();
                    addFeedbcakRequest.setFeedBackID("0");
                    addFeedbcakRequest.setFeedbackMsg(mList.get(position).getFeedBacktxtMesaage());
                    addFeedbcakRequest.setRating(""+mList.get(position).getFeedbackRating());
                    addFeedbcakRequest.setTicketID(""+mList.get(position).getTicketID());
                    addFeedbcakRequest.setUserID(AppSharedPreference.getString(PrefConstants.USER_ID,"",mContext));
                    addFeedBackListner.addFeedBack(addFeedbcakRequest);
                }
            }
        });
        viewHolder.edtFeedbackTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                serviceModel.setFeedBacktxtMesaage(charSequence.toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        viewHolder.ratingBarFeedback.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
              int rating=  (int)v*2;
                serviceModel.setFeedbackRating(rating);
            }
        });

    }


        public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgUser;
        private TextView txtTicketId;
        private TextView txtServiceName;
        private TextView txtOrderId;
        private TextView txtDateTime;
        private TextView txtRate;
        private TextView txtStatus;
        private TextView txtRating;
        private TextView txtUserName;
        private TextView btnDetail;
        private LinearLayout detailLayout;

        private  ImageView btnDescription;
        private RelativeLayout layoutDescription;
        private TextView textDescription;

        private  ImageView btnFeedback;
        private RelativeLayout layoutFeedback;
        private NonScrollListView feedbackList;

        private  ImageView btnHistory;
        private RelativeLayout layoutHistory;
        private NonScrollListView historyList;

        private TextView txtPaymentStatus;

        //feedback relay
        RelativeLayout relayFeedbackLbl;
        LinearLayout feedbackDisplayLayout;
        EditText edtFeedbackTxt;
        RatingBar ratingBarFeedback;
        Button btnFeedbackSubmit;




        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            imgUser = (ImageView) itemLayoutView.findViewById(R.id.img_user);
            txtTicketId = (TextView) itemLayoutView.findViewById(R.id.txt_ticket);
            txtServiceName=(TextView) itemLayoutView.findViewById(R.id.txt_service_name);
            txtOrderId=(TextView) itemLayoutView.findViewById(R.id.txt_order_id);
            txtRate=(TextView) itemLayoutView.findViewById(R.id.txt_rate);
            txtStatus=(TextView) itemLayoutView.findViewById(R.id.txt_status);
            txtDateTime=(TextView)itemLayoutView.findViewById(R.id.txt_time_date) ;
            txtRating=(TextView)itemLayoutView.findViewById(R.id.txt_rating) ;
            txtUserName=(TextView)itemLayoutView.findViewById(R.id.txt_user_name);
            btnDetail=(TextView)itemLayoutView.findViewById(R.id.btn_detail);
            detailLayout=(LinearLayout)itemLayoutView.findViewById(R.id.detail_layout);

            btnDescription=(ImageView)itemLayoutView.findViewById(R.id.btn_description);
            textDescription=(TextView)itemLayoutView.findViewById(R.id.text_description);
            layoutDescription=(RelativeLayout)itemLayoutView.findViewById(R.id.layout_description);

            btnFeedback=(ImageView)itemLayoutView.findViewById(R.id.btn_feedback);
            layoutFeedback=(RelativeLayout)itemLayoutView.findViewById(R.id.layout_feedback);
            feedbackList=(NonScrollListView)itemLayoutView.findViewById(R.id.feeback_list);

            btnHistory=(ImageView)itemLayoutView.findViewById(R.id.btn_history);
            historyList=(NonScrollListView) itemLayoutView.findViewById(R.id.history_list);
            layoutHistory=(RelativeLayout)itemLayoutView.findViewById(R.id.layout_history);
            txtPaymentStatus=(TextView)itemLayoutView.findViewById(R.id.txt_payment_status);

            relayFeedbackLbl=(RelativeLayout)itemLayoutView.findViewById(R.id.relay_feedback);
            edtFeedbackTxt=(EditText)itemLayoutView.findViewById(R.id.edt_feedback);
            ratingBarFeedback=(RatingBar)itemLayoutView.findViewById(R.id.rating_bar);
            btnFeedbackSubmit=(Button)itemLayoutView.findViewById(R.id.feedback_submit_btn);
            feedbackDisplayLayout=(LinearLayout)itemLayoutView.findViewById(R.id.feedback_display_relay);

        }}

    @Override
    public int getItemCount() {
        return mList.size();
    }

    boolean validation(int position){
        boolean isValid=true;
        if(StringUtils.isNullOrEmpty(mList.get(position).getFeedBacktxtMesaage())){
            ToastUtils.showToast(mContext,"Please enter Feedback");
            isValid= false;
        }else if(mList.get(position).getFeedbackRating()==0){
            ToastUtils.showToast(mContext,"Please select rating");
            isValid=false;
        }
        return isValid;
    }

}