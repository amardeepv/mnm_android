package com.mnm.services.ui.activity;

import android.os.Bundle;
import android.view.View;

import com.mnm.services.R;
import com.mnm.services.utils.GlobalUtils;


public class ActivityRateUs extends MyDrawerBaseActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_us);
        findViewById(R.id.root_relay).setOnClickListener(this);
         initCustomActionBarView(ActivityRateUs.this,"Rate Us",true);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityRateUs.this, v);
                break;
        }
    }
}