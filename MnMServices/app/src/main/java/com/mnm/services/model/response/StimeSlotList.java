package com.mnm.services.model.response;

import java.io.Serializable;

/**
 * Created by saten on 3/7/17.
 */

public class StimeSlotList implements Serializable {
    private int serviceTimeSlotId;
    private int serviceId;
    private String timeFrom;
    private String timeTo;
    private String shift;

    public int getServiceTimeSlotId() {
        return serviceTimeSlotId;
    }

    public void setServiceTimeSlotId(int serviceTimeSlotId) {
        this.serviceTimeSlotId = serviceTimeSlotId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getTimeFrom() {
        return timeFrom;
    }

    public void setTimeFrom(String timeFrom) {
        this.timeFrom = timeFrom;
    }

    public String getTimeTo() {
        return timeTo;
    }

    public void setTimeTo(String timeTo) {
        this.timeTo = timeTo;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }
}
