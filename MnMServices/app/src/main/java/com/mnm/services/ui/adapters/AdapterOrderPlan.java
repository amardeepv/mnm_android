package com.mnm.services.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mnm.services.R;
import com.mnm.services.model.response.ServiceModel;
import com.mnm.services.model.response.plan.PlanService;

import java.util.ArrayList;
import java.util.List;

public class AdapterOrderPlan extends BaseAdapter {
    private Context context;
    List<PlanService> serviceList;

    public AdapterOrderPlan(Context context, ArrayList<PlanService> serviceList) {
        this.context = context;
        this.serviceList = serviceList;
    }

    @Override
    public int getCount() {
        return serviceList.size();
    }

    @Override
    public Object getItem(int position) {
        return serviceList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_service_order, null);
            holder = new ViewHolder();
            holder.txtServiceName=(TextView) convertView.findViewById(R.id.service_name);
            holder.txtServicePrice=(TextView) convertView.findViewById(R.id.service_price);
            holder.imgBtnDeleteService=(ImageView) convertView.findViewById(R.id.delete_service);
            holder.imgEditService=(ImageView) convertView.findViewById(R.id.edit_service);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final PlanService serviceModel=serviceList.get(position);
        holder.txtServiceName.setText(serviceModel.getServiceGroupName());
        holder.txtServicePrice.setText("Rs ");
        holder.imgBtnDeleteService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.imgEditService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        return convertView;
    }

    class ViewHolder {
        private TextView txtServiceName;
        private TextView txtServicePrice;
        private ImageView imgBtnDeleteService;
        private ImageView imgEditService;
    }


}
