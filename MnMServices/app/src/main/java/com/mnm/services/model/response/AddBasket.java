package com.mnm.services.model.response;

/**
 * Created by saten on 4/23/17.
 */

public class AddBasket {
    String referenceId;

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }
}
