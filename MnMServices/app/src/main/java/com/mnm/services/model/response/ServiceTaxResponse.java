package com.mnm.services.model.response;

import java.util.List;

/**
 * Created by satendra.singh on 05-04-2017.
 */

public class ServiceTaxResponse {
    private Boolean hasError;
    private String messageCode;
    private String message;
    private TaxModel data;
    private List<String> messages = null;

    public Boolean getHasError() {
        return hasError;
    }

    public void setHasError(Boolean hasError) {
        this.hasError = hasError;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TaxModel getData() {
        return data;
    }

    public void setData(TaxModel data) {
        this.data = data;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}
