package com.mnm.services.ui.adapters;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mnm.services.R;
import com.mnm.services.model.response.plan.PlanService;
import com.mnm.services.ui.fragment.FragmentUnlimitedPlan;
import java.util.List;

public class AdapterUnlimitedPlan extends BaseAdapter {
    private Context context;
    private List<PlanService> addressList;
    FragmentUnlimitedPlan unlimitedPlanFragment;
    boolean needToshowCheckBox=false;


    public AdapterUnlimitedPlan(Context context, List<PlanService> addressList, FragmentUnlimitedPlan unlimitedPlanFragment) {
        this.context = context;
        this.addressList = addressList;
        this.unlimitedPlanFragment=unlimitedPlanFragment;

    }

    @Override
    public int getCount() {
        return addressList.size();
    }

    @Override
    public Object getItem(int position) {
        return addressList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_unlimited_plan, null);
            holder = new ViewHolder();
            holder.btnCheckBox = (CheckBox) convertView.findViewById(R.id.checkbox_plan);
            holder.txtPlanName = (TextView) convertView.findViewById(R.id.txt_plan_name);
            holder.txtPlanType = (TextView) convertView.findViewById(R.id.txt_unlimited);
            holder.relayCheckBox=(RelativeLayout)convertView.findViewById(R.id.relay_check_box);
            holder.viewCheckBox=(View)convertView.findViewById(R.id.view_check_box);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final PlanService addressModel = addressList.get(position);
        holder.txtPlanName.setText(addressModel.getServiceGroupName());
        holder.txtPlanType.setText(addressModel.getNoofCalls());
        if(!needToshowCheckBox){
            holder.viewCheckBox.setVisibility(View.GONE);
            holder.relayCheckBox.setVisibility(View.GONE);
        }else{
            holder.viewCheckBox.setVisibility(View.VISIBLE);
            holder.relayCheckBox.setVisibility(View.VISIBLE);
        }


            holder.txtPlanType.setText(addressModel.getNoofCalls());
            if (!addressModel.getNoofCalls().equalsIgnoreCase("Unlimited")) {
                holder.btnCheckBox.setChecked(true);
                holder.btnCheckBox.setEnabled(false);
            } else {
                holder.btnCheckBox.setEnabled(true);
            }
            if (addressModel.isCheckVisible()) {
                holder.btnCheckBox.setVisibility(View.VISIBLE);
            } else {
                holder.btnCheckBox.setVisibility(View.INVISIBLE);
            }
            if (addressModel.isChecked()) {
                holder.btnCheckBox.setChecked(true);
            } else {
                holder.btnCheckBox.setChecked(false);
            }
            holder.btnCheckBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PlanService mItem = addressList.get(position);
                    mItem.setChecked(mItem.isChecked() ? false : true);
                    notifyDataSetChanged();
                    unlimitedPlanFragment.addAndRemovePlan();

                }
            });


            return convertView;

    }


    public  List<PlanService> getPlaList(){
      return addressList;
    }


    public void setPlanList(List<PlanService> addressList){
        this.addressList=addressList;
        notifyDataSetChanged();
    }
    public  int getUnlimitedServiceSize(){
        int size=0;
        for (PlanService planService: addressList) {
            if(planService.getNoofCalls().equalsIgnoreCase("Unlimited")){
                size++;
            }

        }
        return size;

    }
    public void setCustomiseStatus(boolean isTrue){
        needToshowCheckBox=isTrue;
    }
    class ViewHolder {
        private CheckBox btnCheckBox;
        private TextView txtPlanName;
        private TextView txtPlanType;
        private RelativeLayout relayCheckBox;
        private View viewCheckBox;
    }
}
