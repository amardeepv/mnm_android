package com.mnm.services.controller;

import android.app.Activity;

import android.content.Context;
import android.util.Log;
import com.google.gson.Gson;
import com.kelltontech.network.HttpClientConnection;
import com.kelltontech.network.Response;
import com.kelltontech.network.ServiceRequest;
import com.kelltontech.ui.IScreen;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.ArgumentsKeys;
import com.mnm.services.model.request.AddAddressModel;
import com.mnm.services.model.response.AddAddressResponse;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

public class ControllerAddAddress extends ControllerBase {
    Context mContext;

    public ControllerAddAddress(Activity activity, IScreen screen) {
        super(activity, screen);
        mContext=activity;

    }


    /*
     * (non-Javadoc)
     *
     * @see com.kelltontech.controller.BaseController#getData(int,
     * java.lang.Object)
     */
    @Override
    public ServiceRequest getData(int requestType, Object requestData) {
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setHttpMethod(HttpClientConnection.HTTP_METHOD.POST);
        serviceRequest.setDataType(requestType);
        setHttpsHeaders(serviceRequest);
        serviceRequest.setRequestData(requestData);
        serviceRequest.setPostData(setRequestParameters((AddAddressModel) requestData));
        serviceRequest.setResponseController(this);
        serviceRequest.setPriority(HttpClientConnection.PRIORITY.HIGH);

        String serviceAddAdressUrl=AppSettings.SERVICE_ADD_ADDRESS+ AppSharedPreference.getString(PrefConstants.USER_ID,"",mContext)+"/address";
        serviceRequest.setUrl(serviceAddAdressUrl);
        HttpClientConnection connection = HttpClientConnection.getInstance();
        connection.addRequest(serviceRequest);
        Log.d("url", serviceRequest.toString());
        return serviceRequest;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kelltontech.controller.IController#handleResponse(com.kelltontech
     * .network.Response)
     */
    @Override
    public void handleResponse(Response response) {
        try {
            String responseData = new String(response.getResponseData());
            Log.d(ArgumentsKeys.RESPONSE, responseData);
            AddAddressResponse addAddressResponse = new Gson().fromJson(responseData, AddAddressResponse.class);
            response.setResponseObject(addAddressResponse);
            sendResponseToScreen(response);
        } catch (Exception e) {
            e.printStackTrace();
            sendResponseToScreen(null);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kelltontech.controller.IController#parseResponse(com.kelltontech.
     * network.Response)
     */
    @Override
    public void parseResponse(Response response) {

    }

    /**
     * this method creates login request
     * <p>
     * param requestData
     * return
     */
    private HttpEntity setRequestParameters(AddAddressModel requestData) {
        try {
            String loginRequest = new Gson().toJson(requestData);
            Log.d(ArgumentsKeys.REQUEST, loginRequest);
            return new StringEntity(loginRequest);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
