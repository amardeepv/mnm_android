package com.mnm.services.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mnm.services.R;
import com.mnm.services.constants.Constant;
import com.mnm.services.model.response.plan.PlanModel;
import com.mnm.services.ui.activity.ActivityPlan;
import com.mnm.services.ui.tabs.SlidingTabLayout;
import java.util.ArrayList;
import java.util.List;


public class FragmentPlanDescription extends Fragment  implements View.OnClickListener{
    private   List<PlanModel> mPanelist;
    private   ViewPager  viewPager;
    private SlidingTabLayout tabLayout;
    int planType=0;
    TextView lblLetUsKnowHomeApartmentSize;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview =  inflater.inflate(R.layout.fragment_plan_description, container, false);
        viewPager = (ViewPager)rootview.findViewById(R.id.mpager);
        tabLayout = (SlidingTabLayout)rootview.findViewById(R.id.tabLayout);
        lblLetUsKnowHomeApartmentSize=(TextView)rootview.findViewById(R.id.lbl_let_us_know_ur_home_apartment_size);
        lblLetUsKnowHomeApartmentSize.setVisibility(View.VISIBLE);
        tabLayout.setDistributeEvenly(true);
        Bundle bundle = this.getArguments();
        mPanelist = ( List<PlanModel>) bundle.getSerializable(Constant.PLAN_SERVICE_LIST);
        planType=bundle.getInt(Constant.PLAN_TYPE);

        //pager for unlimited plans
        initViewPagerAndTabs(mPanelist);
        return rootview;
    }


    private void initViewPagerAndTabs(List<PlanModel> mPlanList) {
        final PagerAdapter pagerAdapter = new PagerAdapter(getActivity().getSupportFragmentManager());
        for (PlanModel planModel:mPlanList){
            if(planModel!=null) {
                if(planType==Constant.PLAN_TYPE_UNLIMITED){
                  lblLetUsKnowHomeApartmentSize.setText("Choose Your Home/Apartment Size?");
                    if(!planModel.isOnlyForAdmin()) {
                        FragmentUnlimitedPlan unlimitedPlanFragment = new FragmentUnlimitedPlan();
                        Bundle mBundle = new Bundle();
                        mBundle.putSerializable(Constant.UNLIMITED_PLAN_MODEL, planModel);
                        unlimitedPlanFragment.setArguments(mBundle);
                        pagerAdapter.addFragment(unlimitedPlanFragment, planModel.getPlanName()+"\n"+planModel.getDescription());

                    }
                }else if(planType==Constant.PLAN_TYPE_FIXED){
                    lblLetUsKnowHomeApartmentSize.setText("Choose the no. of Visits:");
                    if(!planModel.isOnlyForAdmin()) {
                        FragmentFixedPlan unlimitedPlanFragment = new FragmentFixedPlan();
                        Bundle mBundle = new Bundle();
                        mBundle.putSerializable(Constant.UNLIMITED_PLAN_MODEL, planModel);
                        unlimitedPlanFragment.setArguments(mBundle);
                        pagerAdapter.addFragment(unlimitedPlanFragment, planModel.getPlanName());
                    }
                }else if(planType==Constant.PLAN_TYPE_ADD_ON){
                   /* lblLetUsKnowHomeApartmentSize.setText("");*/
                    if(!planModel.isOnlyForAdmin()) {
                        FragmentAddOn unlimitedPlanFragment = new FragmentAddOn();
                        Bundle mBundle = new Bundle();
                        mBundle.putSerializable(Constant.UNLIMITED_PLAN_MODEL, planModel);
                        unlimitedPlanFragment.setArguments(mBundle);
                        pagerAdapter.addFragment(unlimitedPlanFragment, planModel.getPlanName()+"\n"+planModel.getDescription());
                    }
                }

            }
        }
        ((ActivityPlan)getActivity()).removeProgressDialog();
        viewPager.setOffscreenPageLimit(mPlanList.size());
        viewPager.setAdapter(pagerAdapter);

        tabLayout.setViewPager(viewPager);
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white_color);          }

               /* @Override
                public int getDividerColor(int position) {
                    return getResources().getColor(R.color.color_primary_red);
                }*/
        });

        tabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                Fragment frag = pagerAdapter.getItem(position);
                if(planType==Constant.PLAN_TYPE_UNLIMITED){
                    if(frag!=null)
                    ((FragmentUnlimitedPlan)frag).callApi();

                }else if(planType==Constant.PLAN_TYPE_FIXED){
                    if(frag!=null)
                        ((FragmentFixedPlan)frag).callApi();

                }else if(planType==Constant.PLAN_TYPE_ADD_ON){
                    if(frag!=null)
                    ((FragmentAddOn)frag).callApi();
                }



            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });


    }

    @Override
    public void onClick(View view) {

    }

    class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

}



