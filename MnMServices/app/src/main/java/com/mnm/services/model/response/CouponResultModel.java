package com.mnm.services.model.response;

/**
 * Created by saten on 4/6/17.
 */

public class CouponResultModel {
    private String serviceId;
    private String couponCode;
    private double couponDiscount;
    private double discountedAmount;
    private int messageId;
    private boolean ByDefault;
    private int couponId;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public double getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(double couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public double getDiscountedAmount() {
        return discountedAmount;
    }

    public void setDiscountedAmount(double discountedAmount) {
        this.discountedAmount = discountedAmount;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public boolean isByDefault() {
        return ByDefault;
    }

    public void setByDefault(boolean byDefault) {
        ByDefault = byDefault;
    }

    public int getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }
}
