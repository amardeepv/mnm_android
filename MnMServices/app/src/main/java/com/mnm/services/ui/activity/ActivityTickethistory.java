package com.mnm.services.ui.activity;

import android.os.Bundle;
import android.view.View;

import com.mnm.services.R;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.utils.GlobalUtils;


public class ActivityTickethistory extends MyDrawerBaseActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_ticket_plans);
        findViewById(R.id.root_relay).setOnClickListener(this);
        initCustomActionBarView(ActivityTickethistory.this,"Ticket History",true);
        AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivityTickethistory.this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityTickethistory.this, v);
                break;
        }
    }
}