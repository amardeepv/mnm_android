package com.mnm.services.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.gson.Gson;
import com.kelltontech.utils.StringUtils;
import com.mnm.services.R;
import com.mnm.services.constants.Constant;
import com.mnm.services.model.response.ServiceCategoryModel;
import com.mnm.services.model.response.faq.FaqResponse;
import com.mnm.services.ui.adapters.Adapterfaq;
import com.mnm.services.utils.GlobalUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import static com.mnm.services.constants.Constant.AC_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.CARPENTRY_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.CAR_SPA_FAQ;
import static com.mnm.services.constants.Constant.CCTV_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.ELECTRICAL_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.GARDENING_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.HOME_APPLIANCE_FAQ;
import static com.mnm.services.constants.Constant.HOME_CLEANING_FAQ;
import static com.mnm.services.constants.Constant.INTERIOR_DESIGN_FAQ;
import static com.mnm.services.constants.Constant.PAINT_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.PEST_CONTROL_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.PLUMBING_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.RO_SERVICE_FAQ;
import static com.mnm.services.constants.Constant.SECURITY_SERVICE_FAQ;
import static com.mnm.services.constants.Constant.UPHOLESTREY_CLEANING_FAQ;
import static com.mnm.services.constants.Constant.WATER_TANK_CLEANING_SERVICE_FAQ;


public class FragmentFaq extends Fragment  {
    private LinearLayoutManager mLayoutManager;
    RecyclerView recycleViewFaq;
    TextView lblNoDataFound;
    ProgressBar progreesBar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview =  inflater.inflate(R.layout.fragment_faq, container, false);
        recycleViewFaq=(RecyclerView) rootview.findViewById(R.id.list_view_service_faq);
        recycleViewFaq.setLayoutManager(new LinearLayoutManager(getContext()));
        lblNoDataFound=(TextView)rootview.findViewById(R.id.lbl_no_data_found);
        progreesBar=(ProgressBar)rootview.findViewById(R.id.progrees_bar);

        FaqResponse faqResponse = null;
        Bundle bundle = this.getArguments();
        String planName="";

        if(bundle!=null)
         planName = bundle.getString(Constant.PLAN_NAME);

        if(StringUtils.isNullOrEmpty(planName)) {
            ServiceCategoryModel categoryModel = (ServiceCategoryModel) getActivity().getIntent().getSerializableExtra(Constant.CATEGORY_MODEL_DATA);
            try {
                JSONArray mJsonString = GlobalUtils.getFaq(getServiceName(categoryModel.getServiceGroupName()));
                Log.e("mJsonString ",mJsonString.toString());
                JSONObject jsonData = new JSONObject();
                jsonData.put("data", mJsonString);
                Log.e("jsonData ",jsonData.toString());
                faqResponse = new Gson().fromJson(jsonData.toString(), FaqResponse.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            try {
                JSONArray mJsonString = GlobalUtils.getFaqForPlan(planName);
                JSONObject jsonData = new JSONObject();
                jsonData.put("data", mJsonString);
                faqResponse = new Gson().fromJson(jsonData.toString(), FaqResponse.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(faqResponse!=null&&faqResponse.getData()!=null&&faqResponse.getData().size()>0)
        {
            lblNoDataFound.setVisibility(View.GONE);
            recycleViewFaq.setVisibility(View.VISIBLE);
            Adapterfaq adapterFaq = new Adapterfaq(getActivity(), faqResponse.getData(),recycleViewFaq);
            recycleViewFaq.setAdapter(adapterFaq);
        }else{
            lblNoDataFound.setVisibility(View.VISIBLE);
            recycleViewFaq.setVisibility(View.GONE);
        }
        return rootview;
    }

    String getServiceName(String serviceNamee){
        String serviceName=serviceNamee.toLowerCase();
        String serviceNameForFaq="";

        if(serviceName.contains("electrical")){
            serviceNameForFaq=ELECTRICAL_SERVICES_FAQ;
        } else if(serviceName.contains("plumbing")){
            serviceNameForFaq= PLUMBING_SERVICES_FAQ;

        }else if(serviceName.contains("carpentry")){
            serviceNameForFaq= CARPENTRY_SERVICES_FAQ;
        }else if(serviceName.contains("pest control")){
            serviceNameForFaq= PEST_CONTROL_SERVICES_FAQ;

        }else if(serviceName.contains("ac repair")||serviceName.contains("ac")){
            serviceNameForFaq= AC_SERVICES_FAQ;

        }else if(serviceName.contains("ro")){
            serviceNameForFaq= RO_SERVICE_FAQ;

        }else if(serviceName.contains("cctv")){
            serviceNameForFaq= CCTV_SERVICES_FAQ;
        }else if(serviceName.contains("gardening")){
            serviceNameForFaq= GARDENING_SERVICES_FAQ;
        }else if(serviceName.contains("security services")||serviceName.contains("security")){
            serviceNameForFaq=SECURITY_SERVICE_FAQ;
        }else if(serviceName.contains("water tank cleaning")||serviceName.contains("watertank cleaning")){
            serviceNameForFaq= WATER_TANK_CLEANING_SERVICE_FAQ;
        }else if(serviceName.contains("home cleaning")||serviceName.contains("Home Cleaning")){
            serviceNameForFaq= HOME_CLEANING_FAQ;

        }else if(serviceName.contains("home appliances")||serviceName.contains("Home Appliances")){
            serviceNameForFaq= HOME_APPLIANCE_FAQ;

        }else if(serviceName.contains("Paint Services")||serviceName.contains("paint services")){
            serviceNameForFaq= PAINT_SERVICES_FAQ;

        }else if(serviceName.contains("car spa")){
            serviceNameForFaq= CAR_SPA_FAQ;

        }else if(serviceName.contains("Upholstery Cleaning")||serviceName.contains("upholstry")||serviceName.contains("upholstery cleaning")){
            serviceNameForFaq= UPHOLESTREY_CLEANING_FAQ;

        }else if(serviceName.contains("Interior Design")||serviceName.contains("interior")||serviceName.contains("interior design")){
            serviceNameForFaq= INTERIOR_DESIGN_FAQ;
        }
        return serviceNameForFaq;
    }

}

