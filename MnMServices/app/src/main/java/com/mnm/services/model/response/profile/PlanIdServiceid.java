package com.mnm.services.model.response.profile;

import java.io.Serializable;

/**
 * Created by saten on 6/10/17.
 */

public class PlanIdServiceid implements Serializable{

    int planId;

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    int serviceId;
}
