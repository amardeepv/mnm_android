package com.mnm.services.model.response;

import java.util.List;

/**
 * Created by satendra.singh on 04-04-2017.
 */

public class LocationSearchResponse {
    public List<Result> results = null;
    public List<Result> getResults() {
        return results;
    }
    public void setResults(List<Result> results) {
        this.results = results;
    }
}
