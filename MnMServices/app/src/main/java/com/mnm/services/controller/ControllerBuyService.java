package com.mnm.services.controller;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.kelltontech.network.HttpClientConnection;
import com.kelltontech.network.Response;
import com.kelltontech.network.ServiceRequest;
import com.kelltontech.ui.IScreen;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.ArgumentsKeys;
import com.mnm.services.model.request.BuyServiceRequest;
import com.mnm.services.model.response.BuyServiceResponse;
import com.mnm.services.model.response.plan.BuyPlanResponse;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

public class ControllerBuyService extends ControllerBase {
boolean isPlan;

    public ControllerBuyService(Activity activity, IScreen screen, boolean isPlan) {
        super(activity, screen);
        this.isPlan=isPlan;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kelltontech.controller.BaseController#getData(int,
     * java.lang.Object)
     */
    @Override
    public ServiceRequest getData(int requestType, Object requestData) {
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setHttpMethod(HttpClientConnection.HTTP_METHOD.POST);
        serviceRequest.setDataType(requestType);
        setHttpsHeaders(serviceRequest);
        serviceRequest.setRequestData(requestData);
        serviceRequest.setPostData(setRequestParameters((BuyServiceRequest) requestData));
        serviceRequest.setResponseController(this);
        serviceRequest.setPriority(HttpClientConnection.PRIORITY.HIGH);
        if(isPlan){
            serviceRequest.setUrl(AppSettings.PLAN_BUY);
        }else {
            serviceRequest.setUrl(AppSettings.SERVICE_BUY);
        }
        HttpClientConnection connection = HttpClientConnection.getInstance();
        connection.addRequest(serviceRequest);
        Log.d("req login", serviceRequest.toString());
        return serviceRequest;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kelltontech.controller.IController#handleResponse(com.kelltontech
     * .network.Response)
     */
    @Override
    public void handleResponse(Response response) {
        try {
            String responseData = new String(response.getResponseData());
            Log.d(ArgumentsKeys.RESPONSE, responseData);
            if(isPlan){
                BuyPlanResponse buyPlanResponse = new Gson().fromJson(responseData, BuyPlanResponse.class);
                response.setResponseObject(buyPlanResponse);
                sendResponseToScreen(response);
            }else{
                BuyServiceResponse buyServiceResponse = new Gson().fromJson(responseData, BuyServiceResponse.class);
                response.setResponseObject(buyServiceResponse);
                sendResponseToScreen(response);
            }

        } catch (Exception e) {
            e.printStackTrace();
            sendResponseToScreen(null);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kelltontech.controller.IController#parseResponse(com.kelltontech.
     * network.Response)
     */
    @Override
    public void parseResponse(Response response) {

    }

    /**
     * this method creates login request
     * <p>
     * param requestData
     * return
     */
    private HttpEntity setRequestParameters(BuyServiceRequest requestData) {
        try {
            String loginRequest = new Gson().toJson(requestData);
            Log.d(ArgumentsKeys.REQUEST, loginRequest);
            return new StringEntity(loginRequest);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
