package com.mnm.services.model.response.address;

/**
 * Created by satendra.singh on 31-03-2017.
 */

public class AddressModel {
    private boolean isChecked;
    private boolean lineOneEditable;
    private boolean lineTwoEditable;
    private boolean lineThreeEditable;
    private boolean isEditButtonClicked;
    private String id;
    private String userId;
    private String title;
    private String line1;
    private String line2;
    private String city;
    private String state;
    private String pinCode;
    private String area;
    private String longitude;
    private String latitude;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isLineOneEditable() {
        return lineOneEditable;
    }

    public void setLineOneEditable(boolean lineOneEditable) {
        this.lineOneEditable = lineOneEditable;
    }

    public boolean isLineTwoEditable() {
        return lineTwoEditable;
    }

    public void setLineTwoEditable(boolean lineTwoEditable) {
        this.lineTwoEditable = lineTwoEditable;
    }

    public boolean isLineThreeEditable() {
        return lineThreeEditable;
    }

    public void setLineThreeEditable(boolean lineThreeEditable) {
        this.lineThreeEditable = lineThreeEditable;
    }

    public boolean isEditButtonClicked() {
        return isEditButtonClicked;
    }

    public void setEditButtonClicked(boolean editButtonClicked) {
        isEditButtonClicked = editButtonClicked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
}
