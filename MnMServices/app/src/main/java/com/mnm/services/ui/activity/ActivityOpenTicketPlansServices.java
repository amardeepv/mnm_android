package com.mnm.services.ui.activity;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.mnm.services.R;
import com.mnm.services.constants.Constant;
import com.mnm.services.model.response.oepnticket.Plan;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.adapters.AdapterOpenTicketServices;
import com.mnm.services.utils.GlobalUtils;


public class ActivityOpenTicketPlansServices extends MyDrawerBaseActivity implements View.OnClickListener {
    private RecyclerView recycleView;
    AdapterOpenTicketServices adapterOpenTicketServices;
    TextView lblNoDataFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_ticket_plans_services);
        AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivityOpenTicketPlansServices.this);
        recycleView=(RecyclerView)findViewById(R.id.recycle_view);
        recycleView.setLayoutManager(new LinearLayoutManager(ActivityOpenTicketPlansServices.this));
        lblNoDataFound=(TextView)findViewById(R.id.lbl_no_data_found);
        lblNoDataFound.setVisibility(View.GONE);
        findViewById(R.id.root_relay).setOnClickListener(this);
        initCustomActionBarView(ActivityOpenTicketPlansServices.this,getResources().getString(R.string.purchased_service),true);
        AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivityOpenTicketPlansServices.this);
        setAdapter((Plan)getIntent().getSerializableExtra(Constant.SELECTED_PLAN_DATA));
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityOpenTicketPlansServices.this, v);
                break;
        }
    }




    void setAdapter(Plan parentServices){
        if(parentServices!=null&&parentServices.getServiceList().size()>0) {
            adapterOpenTicketServices = new AdapterOpenTicketServices(ActivityOpenTicketPlansServices.this, parentServices.getServiceList(), recycleView,""+parentServices.getPlanId(),getIntent().getStringExtra(Constant.ADDRESS_ID),parentServices.getExpirydate());
            recycleView.setAdapter(adapterOpenTicketServices);
            recycleView.setVisibility(View.VISIBLE);
            lblNoDataFound.setVisibility(View.GONE);
        }else{
            recycleView.setVisibility(View.GONE);
            lblNoDataFound.setVisibility(View.VISIBLE);
        }

    }
}