package com.mnm.services.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.Constant;
import com.mnm.services.controller.ControllerAddress;
import com.mnm.services.listner.AddressInterface;
import com.mnm.services.model.response.address.AddressModel;
import com.mnm.services.model.response.address.AddressResponse;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.adapters.AddressAdapter;
import com.mnm.services.ui.widgets.NonScrollListView;
import com.mnm.services.utils.GlobalUtils;
import java.util.ArrayList;


public class ActivityOpenticketAddress extends MyDrawerBaseActivity implements View.OnClickListener,AddressInterface {
    NonScrollListView nonScrollAddressListview;
    AddressAdapter addressAdapter;
    TextView lblNoDataFound;
    private boolean isAddressApiCalled;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_ticket_address);
        findViewById(R.id.root_relay).setOnClickListener(this);
        initCustomActionBarView(ActivityOpenticketAddress.this,"Select Address",true);
        nonScrollAddressListview=(NonScrollListView)findViewById(R.id.non_scroll_address_listview);
        lblNoDataFound=(TextView)findViewById(R.id.lbl_no_data_found);
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(!isAddressApiCalled) {
            callAddressApi();
        }
    }

    private void callAddressApi() {
      if(AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE,false,ActivityOpenticketAddress.this))  {
        if (!ConnectivityUtils.isNetworkEnabled(ActivityOpenticketAddress.this)) {
            ToastUtils.showToast(ActivityOpenticketAddress.this, getResources().getString(R.string.network_error));
        } else {
            showProgressDialog(getString(R.string.please_wait));
            ControllerAddress addressController = new ControllerAddress(ActivityOpenticketAddress.this, this);
            addressController.getData(AppSettings.SERVICE_ADDRESS_REQUEST, null);
        }
      }
      else{
            ToastUtils.showToast(ActivityOpenticketAddress.this,"Please login first");
            Intent mLoginIntent=new Intent(ActivityOpenticketAddress.this,ActivityLogin.class);
            startActivity(mLoginIntent);
        }
    }

    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        ActivityOpenticketAddress.this.runOnUiThread(new Runnable() {
            public void run() {
                removeProgressDialog();
                if (response == null) {
                    // ToastUtils.showToast(ActivityHome.this, getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.SERVICE_ADDRESS_REQUEST:
                            removeProgressDialog();
                            isAddressApiCalled=true;
                            AddressResponse addressResponse = (AddressResponse) response.getResponseObject();
                            if (!addressResponse.getHasError()) {
                                if (addressResponse.getData().getLstAddressResult().size() > 0) {
                                    addressAdapter = new AddressAdapter(ActivityOpenticketAddress.this, (ArrayList<AddressModel>) addressResponse.getData().getLstAddressResult(), ActivityOpenticketAddress.this);
                                    nonScrollAddressListview.setAdapter(addressAdapter);
                                    nonScrollAddressListview.setVisibility(View.VISIBLE);
                                    lblNoDataFound.setVisibility(View.GONE);
                                } else {
                                    nonScrollAddressListview.setVisibility(View.GONE);
                                    lblNoDataFound.setVisibility(View.VISIBLE);

                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityOpenticketAddress.this, v);
                break;
        }
    }

    @Override
    public void setAddress(AddressModel addAddressModel) {
        Intent mIntent=new Intent(ActivityOpenticketAddress.this,ActivityOpenTicketPlans.class);
        mIntent.putExtra(Constant.ADDRESS_ID,addAddressModel.getId());
        startActivity(mIntent);

    }
}