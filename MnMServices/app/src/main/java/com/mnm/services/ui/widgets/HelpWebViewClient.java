package com.mnm.services.ui.widgets;

import android.graphics.Bitmap;
import android.net.http.SslError;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

/**
 * Created by satendra.singh on 12-05-2017.
 */

public class HelpWebViewClient extends WebViewClient {
    ProgressBar bar;

    public HelpWebViewClient(ProgressBar bar) {
        this.bar = bar;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        bar.setVisibility(View.GONE);
        view.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (!url.contains(url)) {
            view.loadUrl(url);
            return false;
        } else
            return super.shouldOverrideUrlLoading(view, url);
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        String summary = "<html><body style='background: #fbf9f5; border-style: solid; border-width: 3px;'><div style='text-align:center; padding:15% 0 0 0;'><b><h2><font color='#685f53'>Video not available</font></h2></b><a href='" +failingUrl+ "'><img src='file:///android_asset/reload.png'></a></div></body></html>";
        view.loadDataWithBaseURL("file:///android_asset/", summary, "text/html", "utf-8", null);
        super.onReceivedError(view, errorCode, description, failingUrl);
    }

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        super.onReceivedSslError(view, handler, error);
    }
}
