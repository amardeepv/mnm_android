package com.mnm.services.model.response.profile;

import java.util.List;

/**
 * Created by satendra.singh on 02-05-2017.
 */

public class Data {
    private List<Customer> customers = null;
    private List<ConsumerDetail> ConsumerDetails = null;
    private List<ConsumerContact> ConsumerContacts = null;
    private String userID;
    private String firstName;
    private String lastName;
    private String emailId;
    private String password;
    private String roleID;
    private String zoneID;
    private String subZoneID;
    private String created;
    private String status;
    private String roleParentPages;
    private String roleChildPages;
    private String iswebuser;
    private String mobileNumber;

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    public List<ConsumerDetail> getConsumerDetails() {
        return ConsumerDetails;
    }

    public void setConsumerDetails(List<ConsumerDetail> consumerDetails) {
        ConsumerDetails = consumerDetails;
    }

    public List<ConsumerContact> getConsumerContacts() {
        return ConsumerContacts;
    }

    public void setConsumerContacts(List<ConsumerContact> consumerContacts) {
        ConsumerContacts = consumerContacts;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRoleID() {
        return roleID;
    }

    public void setRoleID(String roleID) {
        this.roleID = roleID;
    }

    public String getZoneID() {
        return zoneID;
    }

    public void setZoneID(String zoneID) {
        this.zoneID = zoneID;
    }

    public String getSubZoneID() {
        return subZoneID;
    }

    public void setSubZoneID(String subZoneID) {
        this.subZoneID = subZoneID;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRoleParentPages() {
        return roleParentPages;
    }

    public void setRoleParentPages(String roleParentPages) {
        this.roleParentPages = roleParentPages;
    }

    public String getRoleChildPages() {
        return roleChildPages;
    }

    public void setRoleChildPages(String roleChildPages) {
        this.roleChildPages = roleChildPages;
    }

    public String getIswebuser() {
        return iswebuser;
    }

    public void setIswebuser(String iswebuser) {
        this.iswebuser = iswebuser;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
