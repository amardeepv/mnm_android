package com.mnm.services.model.response.myorder;

/**
 * Created by saten on 4/25/17.
 */

public class TicketHistory {

    private String serviceDate;
    private String checkIn;
    private String checkOut;
    private String nextShceduleDate;

    public String getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(String serviceDate) {
        this.serviceDate = serviceDate;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public String getNextShceduleDate() {
        return nextShceduleDate;
    }

    public void setNextShceduleDate(String nextShceduleDate) {
        this.nextShceduleDate = nextShceduleDate;
    }
}
