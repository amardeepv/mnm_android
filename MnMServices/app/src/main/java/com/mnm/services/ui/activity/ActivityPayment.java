package com.mnm.services.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.Constant;
import com.mnm.services.controller.ControllerBuyService;
import com.mnm.services.controller.ControllerWallet;
import com.mnm.services.model.request.BuyServiceRequest;
import com.mnm.services.model.response.BuyServiceResponse;
import com.mnm.services.model.response.ServiceModel;
import com.mnm.services.model.response.WalletResponse;
import com.mnm.services.model.response.plan.BuyPlanResponse;
import com.mnm.services.model.response.plan.PlanService;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.adapters.AdapterOrderService;
import com.mnm.services.ui.adapters.AdapterPlan;
import com.mnm.services.ui.widgets.NonScrollListView;
import com.mnm.services.utils.GlobalUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static com.mnm.services.constants.Constant.IS_FIXED_PLAN;


public class ActivityPayment extends MyDrawerBaseActivity implements View.OnClickListener {
    private TextView txtAmountMnmWallet;
    RelativeLayout relayWallet,relayCitrus,relayDebitCard,relayCod,relayCodPlusWallet,walletPlusNetbanking;
    TextView txtTotalPriceOfService,txtServiceName;
    TextView txtType,txtMnmWalletAmount;
    double walletAmount;
    NonScrollListView orderDetailListView;//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        findViewById(R.id.relay_continue).setOnClickListener(this);
        txtAmountMnmWallet=(TextView)findViewById(R.id.txt_amount_mnm_wallet);
        txtMnmWalletAmount=(TextView)findViewById(R.id.txt_mnm_wallet_amount);
        orderDetailListView=(NonScrollListView)findViewById(R.id.nonscrolllistview_order_detail) ;
        txtType=(TextView)findViewById(R.id.type);
        relayWallet=(RelativeLayout)findViewById(R.id.wallet);
        relayWallet.setOnClickListener(this);
        relayCitrus=(RelativeLayout)findViewById(R.id.citrus);
        relayCitrus.setOnClickListener(this);
        relayDebitCard=(RelativeLayout)findViewById(R.id.debit_card);
        txtTotalPriceOfService=(TextView)findViewById(R.id.txt_total_price_of_service);
        txtServiceName=(TextView)findViewById(R.id.service_name_e);
        relayDebitCard.setOnClickListener(this);
        relayCod=(RelativeLayout)findViewById(R.id.cod);
        relayCod.setOnClickListener(this);
        relayCodPlusWallet=(RelativeLayout)findViewById(R.id.wallet_plus_cod);
        relayCodPlusWallet.setOnClickListener(this);
        walletPlusNetbanking=(RelativeLayout)findViewById(R.id.wallet_plus_netbanking);
       RelativeLayout serviceHeader=(RelativeLayout)findViewById(R.id.service_header);
        LinearLayout planHeader=(LinearLayout)findViewById(R.id.plan_header);
        walletPlusNetbanking.setOnClickListener(this);
        initCustomActionBarView(ActivityPayment.this,"Payment",true);
        txtTotalPriceOfService.setText(""+getIntent().getStringExtra(Constant.TOTAL_BILL_AFTER_AMOUNT));
        if(getIntent().getBooleanExtra(Constant.IS_PLAN,false)){
            txtType.setText("Plan name");
            serviceHeader.setVisibility(View.GONE);
            planHeader.setVisibility(View.VISIBLE);
        }else{
            txtType.setText("Service name");
            serviceHeader.setVisibility(View.VISIBLE);
            planHeader.setVisibility(View.GONE);
        }
        txtServiceName.setText(getIntent().getStringExtra(Constant.SERVICE_NAME));
        List<ServiceModel> mList=null;
        if(getIntent().getBooleanExtra(Constant.IS_PLAN,false)){
            ArrayList<PlanService> serviceList= (ArrayList<PlanService>) getIntent().getSerializableExtra(Constant.SERVICE_SELECTED_DATA);
            AdapterPlan orderServiceAdapter=new AdapterPlan(ActivityPayment.this,serviceList,getIntent().getBooleanExtra(Constant.IS_FIXED_PLAN,false));
            orderDetailListView.setAdapter(orderServiceAdapter);
        }else{
            ArrayList<ServiceModel> serviceList= (ArrayList<ServiceModel>) getIntent().getSerializableExtra(Constant.SERVICE_SELECTED_DATA);
            AdapterOrderService orderServiceAdapter=new AdapterOrderService(ActivityPayment.this,serviceList);
            orderDetailListView.setAdapter(orderServiceAdapter);
        }
        callWalletApi();
    }



    private void callWalletApi() {
        if (!ConnectivityUtils.isNetworkEnabled(ActivityPayment.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerWallet walletController = new ControllerWallet(this, this);
            showProgressDialog("Getting wallet detail");
            walletController.getData(AppSettings.SERVICE_WALLET_EVENT, null);
        }
    }
    private void callBuyServiceApi(int paymentMethod) {
        if (!ConnectivityUtils.isNetworkEnabled(ActivityPayment.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerBuyService buyServiceController = new ControllerBuyService(this, this,getIntent().getBooleanExtra(Constant.IS_PLAN,false));
            BuyServiceRequest buyServiceRequest=getBuyServiceRequest(paymentMethod);
            showProgressDialog("Please wait...");
            buyServiceController.getData(AppSettings.SERVICE_BUY_EVENT, buyServiceRequest);
        }
    }
    BuyServiceRequest getBuyServiceRequest(int paymentMethod){
        BuyServiceRequest buyServiceRequest=new BuyServiceRequest();
        buyServiceRequest.setUserId(AppSharedPreference.getString(PrefConstants.USER_ID,"",ActivityPayment.this));
        buyServiceRequest.setPaymentMethod(""+paymentMethod);
        buyServiceRequest.setTotalAmount(""+getIntent().getStringExtra(Constant.TOTAL_BILL_AFTER_AMOUNT));
        buyServiceRequest.setServiceTaxAmount(""+getIntent().getStringExtra(Constant.TAX_AMOUNT));
        buyServiceRequest.setTaxRate(""+getIntent().getStringExtra(Constant.TAX_RATE));
        buyServiceRequest.setChannel("MA");
        buyServiceRequest.setDiscount("0.0");
        buyServiceRequest.setUniqueRefId(getIntent().getStringExtra(Constant.ADD_TO_BASKET_REFERENCE_ID));
        return buyServiceRequest;
    }


    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        ActivityPayment.this.runOnUiThread(new Runnable() {
            public void run() {
                removeProgressDialog();
                Log.e("Response", response.toString());
                if (response == null) {
                    ToastUtils.showToast(ActivityPayment.this, getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.SERVICE_WALLET_EVENT:
                            WalletResponse walletResponse = (WalletResponse) response.getResponseObject();
                            if(!walletResponse.getHasError()){
                                walletAmount=walletResponse.getData().getAmount();
                                txtAmountMnmWallet.setText(""+getString(R.string.rupees_symbol)+" "+walletAmount);;
                                txtMnmWalletAmount.setText(""+getString(R.string.rupees_symbol)+" "+walletAmount);
                            }else{
                                walletAmount=000;
                                txtAmountMnmWallet.setText(""+walletAmount);
                            }

                            break;
                        case AppSettings.SERVICE_BUY_EVENT:
                            if(getIntent().getBooleanExtra(Constant.IS_PLAN,false)){
                                BuyPlanResponse buyServiceResponse = (BuyPlanResponse) response.getResponseObject();
                                if(!buyServiceResponse.getHasError()){
                                    ToastUtils.showToast(ActivityPayment.this,""+buyServiceResponse.getMessage());
                                    Intent mIntent=new Intent(ActivityPayment.this,ActivityOrderConfirmation.class);
                                    mIntent.putExtra(Constant.IS_FIXED_PLAN,getIntent().getBooleanExtra(Constant.IS_FIXED_PLAN,false));
                                    mIntent.putExtra(Constant.SERVICE_NAME,getIntent().getStringExtra(Constant.SERVICE_NAME));
                                    mIntent.putExtra(Constant.TOTAL_BILL_AFTER_AMOUNT,getIntent().getStringExtra(Constant.TOTAL_BILL_AFTER_AMOUNT));
                                    mIntent.putExtra(Constant.IS_PLAN,getIntent().getBooleanExtra(Constant.IS_PLAN,false));
                                     mIntent.putExtra(Constant.SERVICE_SELECTED_DATA,  (Serializable)  (List<PlanService>) getIntent().getSerializableExtra(Constant.SERVICE_SELECTED_DATA));
                                    mIntent.putExtra(Constant.INVOICE_NUMBER,buyServiceResponse.getData().getInvoiceNumber());
                                    startActivity(mIntent);
                                    finish();
                                }else{
                                    ToastUtils.showToast(ActivityPayment.this,""+buyServiceResponse.getMessage());
                                }
                            }else{
                                BuyServiceResponse buyServiceResponse = (BuyServiceResponse) response.getResponseObject();
                                if(!buyServiceResponse.isHasError()){
                                    AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivityPayment.this);
                                    ToastUtils.showToast(ActivityPayment.this,""+buyServiceResponse.getMessage());
                                    Intent mIntent=new Intent(ActivityPayment.this,ActivityOrderConfirmation.class);
                                    mIntent.putExtra(Constant.IS_FIXED_PLAN,getIntent().getBooleanExtra(Constant.IS_FIXED_PLAN,false));
                                    mIntent.putExtra(Constant.SERVICE_NAME,getIntent().getStringExtra(Constant.SERVICE_NAME));
                                    mIntent.putExtra(Constant.TOTAL_BILL_AFTER_AMOUNT,getIntent().getStringExtra(Constant.TOTAL_BILL_AFTER_AMOUNT));
                                    mIntent.putExtra(Constant.SERVICE_SELECTED_DATA,  (Serializable)  (List<ServiceModel>) getIntent().getSerializableExtra(Constant.SERVICE_SELECTED_DATA));
                                    mIntent.putExtra(Constant.IS_PLAN,getIntent().getBooleanExtra(Constant.IS_PLAN,false));
                                    mIntent.putExtra(Constant.INVOICE_NUMBER,buyServiceResponse.getData().getBuyResult().get(0).getInvoiceNumber());
                                    startActivity(mIntent);
                                    finish();
                                }else{
                                    ToastUtils.showToast(ActivityPayment.this,""+buyServiceResponse.getMessage());
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityPayment.this, v);
                break;
            case R.id.relay_continue:

                break;
            case R.id.wallet:
               // callBuyServiceApi(1);
                ToastUtils.showToast(ActivityPayment.this,"WalletData feature is Comming Soon");
                break;
            case R.id.citrus:
                //callBuyServiceApi(2);
                ToastUtils.showToast(ActivityPayment.this,"Citrus wallet feature is Comming Soon");
                break;
            case R.id.debit_card:
               // callBuyServiceApi(3);
                ToastUtils.showToast(ActivityPayment.this,"Debit card feature is Comming Soon");
                break;
            case R.id.cod:
                callBuyServiceApi(4);
                break;
            case R.id.wallet_plus_cod:
                //callBuyServiceApi(5);
                ToastUtils.showToast(ActivityPayment.this,"This feature is Comming Soon");
                break;
            case R.id.wallet_plus_netbanking:
                //callBuyServiceApi(7);
                ToastUtils.showToast(ActivityPayment.this,"This feature is Comming Soon");
                break;
        }
    }
}