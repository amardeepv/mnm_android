package com.mnm.services.model.response;

import java.util.List;

/**
 * Created by saten on 4/6/17.
 */

public class CoupanResponseData {
    private List<CouponResultModel> lstCouponResult = null;

    public List<CouponResultModel> getLstCouponResult() {
        return lstCouponResult;
    }

    public void setLstCouponResult(List<CouponResultModel> lstCouponResult) {
        this.lstCouponResult = lstCouponResult;
    }
}
