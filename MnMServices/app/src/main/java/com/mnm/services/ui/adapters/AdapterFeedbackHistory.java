package com.mnm.services.ui.adapters;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kelltontech.utils.StringUtils;
import com.mnm.services.R;
import com.mnm.services.model.response.myorder.FeedbackHistory;
import com.mnm.services.utils.GlobalUtils;

import java.util.List;

public class AdapterFeedbackHistory extends BaseAdapter {
    private Context context;
    private List<FeedbackHistory> feedbackHistories;

    public AdapterFeedbackHistory(Context context,List<FeedbackHistory> feedbackHistories) {
        this.context = context;
        this.feedbackHistories = feedbackHistories;

    }

    @Override
    public int getCount() {
        return feedbackHistories.size();
    }

    @Override
    public Object getItem(int position) {
        return feedbackHistories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_feedback_history, null);
            holder = new ViewHolder();
            holder.txtDate = (TextView) convertView.findViewById(R.id.txt_date_feedback);
            holder.txtBy=(TextView) convertView.findViewById(R.id.txt_by);
            holder.txtratingfeedback=(TextView) convertView.findViewById(R.id.txt_feedback_rating);
            holder.txtRating=(TextView)convertView.findViewById(R.id.txt_rating);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final FeedbackHistory txtNextSchedule=feedbackHistories.get(position);
        String feedbackDate="";
        if(!StringUtils.isNullOrEmpty(txtNextSchedule.getFeedbackDate())){
            String mFeedBackDate=txtNextSchedule.getFeedbackDate();
            feedbackDate=mFeedBackDate.substring(0,mFeedBackDate.indexOf("T"));
        }
        holder.txtDate.setText(GlobalUtils.getDateString(feedbackDate));
        holder.txtBy.setText(StringUtils.isNullOrEmpty(txtNextSchedule.getFeedbackBy())?"":txtNextSchedule.getFeedbackBy());
        holder.txtratingfeedback.setText(StringUtils.isNullOrEmpty(txtNextSchedule.getFeebackMsg())?"":txtNextSchedule.getFeebackMsg());
        holder.txtRating.setText(StringUtils.isNullOrEmpty(""+txtNextSchedule.getRating())?"":""+txtNextSchedule.getRating());
          return convertView;
    }

    class ViewHolder {
        private TextView txtDate;
        private TextView txtBy;
        private TextView txtratingfeedback;
        private TextView txtRating;

    }

}
