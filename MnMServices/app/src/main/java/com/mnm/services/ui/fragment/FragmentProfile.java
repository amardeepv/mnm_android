package com.mnm.services.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Toast;
import com.kelltontech.ui.IScreen;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.controller.ControllerGetProfile;
import com.mnm.services.controller.ControllerUpdateProfile;
import com.mnm.services.model.request.ConsumerContactReq;
import com.mnm.services.model.request.UpdateConsumerRequest;
import com.mnm.services.model.response.profile.Data;
import com.mnm.services.model.response.profile.ProfileResponse;
import com.mnm.services.model.response.profile.Profileupdateresponse;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.dialog.SelectDateFragment;
import com.mnm.services.utils.GlobalUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FragmentProfile extends Fragment  implements View.OnClickListener,IScreen {
    private ProgressBar progressBar;
    EditText edtFistName, edtlastName,edtMobile, edtAlternateMobileOne,edtAlternateMobileTwo,edtAlternateMobileThree,edtAlternateMobileFour, edtEmail, edtDOB;
    RadioButton radioMale, radioFemale;
    String gender="";
    View rootview;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_profile, container, false);
        progressBar = (ProgressBar) rootview.findViewById(R.id.progrees_bar);
        progressBar.setVisibility(View.GONE);
        edtFistName = (EditText) rootview.findViewById(R.id.input_first_name);
        edtlastName = (EditText) rootview.findViewById(R.id.input_last_name);
        edtMobile = (EditText) rootview.findViewById(R.id.input_mobile);
        edtAlternateMobileOne = (EditText) rootview.findViewById(R.id.input_alternate_mobile_first);
        edtAlternateMobileTwo = (EditText) rootview.findViewById(R.id.input_alternate_mobile_two);
        edtAlternateMobileThree = (EditText) rootview.findViewById(R.id.input_alternate_mobile_third);
        edtAlternateMobileFour = (EditText) rootview.findViewById(R.id.input_alternate_mobile_fourth);
        edtEmail = (EditText) rootview.findViewById(R.id.input_email_id);
        edtDOB = (EditText) rootview.findViewById(R.id.input_calender);
        edtDOB.setOnClickListener(this);
        radioMale = (RadioButton) rootview.findViewById(R.id.radio_male);
        radioFemale = (RadioButton) rootview.findViewById(R.id.radio_female);
        rootview.findViewById(R.id.btn_update_profile).setOnClickListener(this);
        radioMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                gender=b?"M":"F";
                Log.e("gedner ",gender);
            }
        });
        radioFemale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                gender=b?"F":"M";
                Log.e("gedner ",gender);
            }
        });

        getProfileDataApi();
        return rootview;
    }

    void getProfileDataApi(){
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            ControllerGetProfile controllerGetProfile = new ControllerGetProfile(getActivity(), this);
            progressBar.setVisibility(View.VISIBLE);
            controllerGetProfile.getData(AppSettings.PROFILE_GET_DATA_EVENT, null);
        }

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_update_profile:
                if(validation()){
                    updateConsumerProfileApi();
                }
                break;
            case R.id.input_calender:
                DialogFragment newFragment = new SelectDateFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
                break;

            default:
                break;
        }

    }


    void updateConsumerProfileApi() {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            ControllerUpdateProfile controllerUpdateProfile = new ControllerUpdateProfile(getActivity(), this);
            progressBar.setVisibility(View.VISIBLE);
            UpdateConsumerRequest updateConsumerRequest=getConsumerRequest();
            controllerUpdateProfile.getData(AppSettings.UPDATE_CONSUMER_PROFILE_EVENT, updateConsumerRequest);
        }
    }
    private UpdateConsumerRequest getConsumerRequest() {
        UpdateConsumerRequest registerRequest = new UpdateConsumerRequest();
        registerRequest.setUserId(AppSharedPreference.getString(PrefConstants.USER_ID,"",getActivity()));
        registerRequest.setFirstName(edtFistName.getText().toString());
        registerRequest.setLastName(edtlastName.getText().toString());
        registerRequest.setMobileNumber(edtMobile.getText().toString());
        List<ConsumerContactReq> mlIst=new ArrayList<>();
        ConsumerContactReq consumerContactOne=new ConsumerContactReq();
        consumerContactOne.setAlterNateContactNumber(edtAlternateMobileOne.getText().toString());
        mlIst.add(consumerContactOne);

        ConsumerContactReq consumerContactTwo=new ConsumerContactReq();
        consumerContactTwo.setAlterNateContactNumber(edtAlternateMobileTwo.getText().toString());
        mlIst.add(consumerContactTwo);

        ConsumerContactReq consumerContactThree=new ConsumerContactReq();
        consumerContactThree.setAlterNateContactNumber(edtAlternateMobileThree.getText().toString());
        mlIst.add(consumerContactThree);

        ConsumerContactReq consumerContactFour=new ConsumerContactReq();
        consumerContactFour.setAlterNateContactNumber(edtAlternateMobileFour.getText().toString());
        mlIst.add(consumerContactFour);

        registerRequest.setConsumerContact(mlIst);
        registerRequest.setEmailId(edtEmail.getText().toString());

        registerRequest.setDob(GlobalUtils.getDatejdjString(edtDOB.getText().toString()));
        registerRequest.setGender(gender);
        return registerRequest;
    }


    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Log.e("Response", response.toString());
                progressBar.setVisibility(View.GONE);
                if (response == null) {
                    ToastUtils.showToast(getActivity(), getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.UPDATE_CONSUMER_PROFILE_EVENT:
                            Profileupdateresponse serviceResponse = (Profileupdateresponse) response.getResponseObject();
                            if (!serviceResponse.isHasError()) {
                                getProfileDataApi();
                                ToastUtils.showToast(getActivity(),""+serviceResponse.getMessage());
                            }else{
                                ToastUtils.showToast(getActivity(),""+serviceResponse.getMessage());
                            }
                            break;
                        case AppSettings.PROFILE_GET_DATA_EVENT:
                            ProfileResponse profileResponse = (ProfileResponse) response.getResponseObject();
                            if (!profileResponse.isHasError()) {
                                setProfileData(profileResponse.getData());
                            }else{
                                ToastUtils.showToast(getActivity(),""+profileResponse.getMessage());
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        });
    }

    void setProfileData(Data profileData){
        edtFistName.setText(profileData.getFirstName());
        edtlastName.setText(profileData.getLastName());
        edtMobile.setText(profileData.getMobileNumber());
        edtEmail.setText(profileData.getEmailId());
        edtDOB.setText(GlobalUtils.getdate(profileData.getCustomers().get(0).getDob()));
        gender=profileData.getCustomers().get(0).getGender();
        if(gender.equalsIgnoreCase("male")||gender.equalsIgnoreCase("m")){
            radioMale.setChecked(true);
        }else{
            radioFemale.setChecked(true);
        }

        if(profileData.getConsumerContacts().size()>0)
        edtAlternateMobileOne.setText(profileData.getConsumerContacts().get(0).getAlterNateContactNumber());
        if(profileData.getConsumerContacts().size()>1)
        edtAlternateMobileTwo.setText(profileData.getConsumerContacts().get(1).getAlterNateContactNumber());
        if(profileData.getConsumerContacts().size()>2)
        edtAlternateMobileThree.setText(profileData.getConsumerContacts().get(2).getAlterNateContactNumber());
        if(profileData.getConsumerContacts().size()>3)
        edtAlternateMobileFour.setText(profileData.getConsumerContacts().get(3).getAlterNateContactNumber());
    }

    public void setDateString(int month, int day, int year) throws ParseException {
        String userDob="";
        String currentDate = GlobalUtils.giveDate();
        Log.e("currentDate ", "" + currentDate);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDatee = sdf.parse(currentDate);
        String myMonth, myday;
        if (month < 10)
            myMonth = "0" + month;
        else
            myMonth = "" + month;
        if (day < 10)
            myday = "0" + day;
        else
            myday = "" + day;

        Date calendaDAte = sdf.parse(year + "-" + myMonth + "-" + myday);
        if (calendaDAte.before(currentDatee)) {
            userDob = year + "-" + myMonth + "-" + myday;
            setDateOfBirth(userDob);
            Log.e("if userDob", "" + userDob);
        } else {
            userDob = year + "-" + myMonth + "-" + myday;
            Log.e("else userDob", "" + userDob);
            Toast.makeText(getActivity(), getResources().getString(R.string.select_a_valid_date), Toast.LENGTH_SHORT).show();
        }

    }

    private void setDateOfBirth(String userDob) {
        edtDOB.setText(userDob.substring(8, 10) + " " + GlobalUtils.getMonth(Integer.parseInt(userDob.substring(5, 7))) + " " + userDob.substring(0, 4));
        userDob = userDob.substring(0, 4) + "-" + userDob.substring(5, 7) + "-" + userDob.substring(8, 10);
        Log.e("userDob ", "" + userDob);

        edtDOB.setText(GlobalUtils.getDateString(userDob).replaceAll("-","/"));
    }

    boolean validation(){
        boolean isValid=true;
      /*  if(edtFistName.getText().toString().length()==0){
            ToastUtils.showToast(getActivity(),"Please enter name");
            isValid=false;
        }else if(edtFistName.getText().toString().length()<=2){
            ToastUtils.showToast(getActivity(),"Name sould be greater then 2 letters");
            isValid=false;
        }else if(edtMobile.getText().toString().length()==0){
            ToastUtils.showToast(getActivity(),"Please enter mobile number");
            isValid=false;
        }else if(edtMobile.getText().toString().length()<=9){
            ToastUtils.showToast(getActivity(),"Please enter valid mobile number");
            isValid=false;
        }else if(edtAlternateMobile.getText().toString().length()==0){
            ToastUtils.showToast(getActivity(),"Please enter Alternate  mobile number");
            isValid=false;
        }else if(edtAlternateMobile.getText().toString().length()<=9){
            ToastUtils.showToast(getActivity(),"Please enter valid Alternate mobile number");
            isValid=false;
        }else */if(edtEmail.getText().toString().length()==0){
            ToastUtils.showToast(getActivity(),"Please enter email id");
            isValid=false;
        }else if(!edtEmail.getText().toString().contains("@")&&!edtEmail.getText().toString().contains(".com")){
            ToastUtils.showToast(getActivity(),"please enter valid email id");
            isValid=false;
        }/*else if(edtDOB.getText().toString().length()==0){
            ToastUtils.showToast(getActivity(),"Please enter Date Of Birth");
            isValid=false;
        }else if(gender.length()==0){
            ToastUtils.showToast(getActivity(),"Please select gender");
            isValid=false;
        }*/
        return isValid;

    }

    @Override
    public void onResume() {
        super.onResume();
        GlobalUtils.hideKeyboardFrom(getActivity(),rootview);
    }
}



