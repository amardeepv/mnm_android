package com.mnm.services.model.response;

/**
 * Created by saten on 5/9/17.
 */

public class ReferAndEarn {
    private String referalCode;
    private String referalMessage;

    public String getReferalCode() {
        return referalCode;
    }

    public void setReferalCode(String referalCode) {
        this.referalCode = referalCode;
    }

    public String getReferalMessage() {
        return referalMessage;
    }

    public void setReferalMessage(String referalMessage) {
        this.referalMessage = referalMessage;
    }
}
