package com.mnm.services.model.response.myorder;

import java.util.List;

/**
 * Created by saten on 4/25/17.
 */

public class PlanOrderData {
    private List<PlanOrder> PlanOrders = null;

    public List<PlanOrder> getPlanOrders() {
        return PlanOrders;
    }

    public void setPlanOrders(List<PlanOrder> PlanOrders) {
        this.PlanOrders = PlanOrders;
    }

}
