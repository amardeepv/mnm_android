package com.mnm.services.controller;

import android.app.Activity;
import android.util.Log;
import com.google.gson.Gson;
import com.kelltontech.network.HttpClientConnection;
import com.kelltontech.network.Response;
import com.kelltontech.network.ServiceRequest;
import com.kelltontech.ui.IScreen;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.ArgumentsKeys;
import com.mnm.services.model.request.VerifyMobileRequest;
import com.mnm.services.model.response.LocationSearchResponse;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

public class ControllerSearchLocation extends ControllerBase {
String searchTxt;

    public ControllerSearchLocation(Activity activity, IScreen screen, String searchTxt) {
        super(activity, screen);
        this.searchTxt=searchTxt;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kelltontech.controller.BaseController#getData(int,
     * java.lang.Object)
     */
    @Override
    public ServiceRequest getData(int requestType, Object requestData) {
        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setHttpMethod(HttpClientConnection.HTTP_METHOD.GET);
        serviceRequest.setDataType(requestType);
        setHttpsHeaders(serviceRequest);
        serviceRequest.setRequestData(requestData);
        serviceRequest.setResponseController(this);
        serviceRequest.setPriority(HttpClientConnection.PRIORITY.HIGH);
        String mSearchtxt=searchTxt.replaceAll(" ","%20");
        serviceRequest.setUrl(AppSettings.SERVICE_LOCATION_API_FIRST_HALF+mSearchtxt+AppSettings.SERVICE_LOCATION_SECOND_HALF);
        HttpClientConnection connection = HttpClientConnection.getInstance();
        connection.addRequest(serviceRequest);
        Log.d("req service category", serviceRequest.toString());
        return serviceRequest;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kelltontech.controller.IController#handleResponse(com.kelltontech
     * .network.Response)
     */
    @Override
    public void handleResponse(Response response) {
        try {
            String responseData = new String(response.getResponseData());
            Log.d(ArgumentsKeys.RESPONSE, responseData);
            LocationSearchResponse verifymobileResponse = new Gson().fromJson(responseData, LocationSearchResponse.class);
            response.setResponseObject(verifymobileResponse);
            sendResponseToScreen(response);
        } catch (Exception e) {
            e.printStackTrace();
            sendResponseToScreen(null);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kelltontech.controller.IController#parseResponse(com.kelltontech.
     * network.Response)
     */
    @Override
    public void parseResponse(Response response) {

    }

    /**
     * this method creates login request
     * <p>
     * param requestData
     * return
     */
    private HttpEntity setRequestParameters(VerifyMobileRequest requestData) {
        try {
            String loginRequest = new Gson().toJson(requestData);
            Log.d(ArgumentsKeys.REQUEST, loginRequest);
            return new StringEntity(loginRequest);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
