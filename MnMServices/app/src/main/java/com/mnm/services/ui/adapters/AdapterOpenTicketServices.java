package com.mnm.services.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.kelltontech.utils.StringUtils;
import com.mnm.services.R;
import com.mnm.services.constants.Constant;
import com.mnm.services.model.response.ServiceCategoryModel;
import com.mnm.services.model.response.ServiceCategoryResponse;
import com.mnm.services.model.response.oepnticket.ParentService;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.activity.ActivityOpenTicketPlansServices;
import com.mnm.services.ui.activity.ActivityServiceGroup;
import com.mnm.services.ui.fragment.FragmentService;
import com.mnm.services.utils.GlobalUtils;
import java.io.Serializable;
import java.util.List;

/**
 * Adapter to manage list of properties
 */
public class AdapterOpenTicketServices extends RecyclerView.Adapter<AdapterOpenTicketServices.ViewHolder> {
    private List<ParentService> planList;
    private Context mContext;
    private View mItemLayoutView;
    private ViewHolder mViewHolder;
    FragmentService serviceFragment;
    private LinearLayoutManager linearLayoutManager;
    private RelativeLayout relayButtons;
    private String mPlanId;
    String categoryFileName = "category.json";
    private String addressId;
    private String expiryDate;
    private  boolean isPlanUnlimited;
    public AdapterOpenTicketServices(Context ctx, List<ParentService> planList, RecyclerView recyclerView,String PLAN_ID,String addressId,String expiryDate) {
        this.planList = planList;
        this.mContext = ctx;
        this.mPlanId=PLAN_ID;
        this.addressId=addressId;
        this.expiryDate=expiryDate;
        this.serviceFragment = serviceFragment;
        for (ParentService parentService:planList) {
            if(parentService.getTotalCalls()==99){
                isPlanUnlimited=true;
            }

        }
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                }
            });
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("create","create view holder****");
        mItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_open_ticket, null);
        mViewHolder = new ViewHolder(mItemLayoutView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        Log.e("create","bind view holder ++++ "+position);
        final ParentService  service = planList.get(position);
        viewHolder.txtServiceName.setText(service.getServiceName());
        viewHolder.txtServiceName.setVisibility(View.GONE);
        viewHolder.txtServiceDescription.setVisibility(View.VISIBLE);
        viewHolder.txtServiceDescription.setText(service.getServiceName());
        viewHolder.imgService.setImageDrawable(getImage(service.getServiceName(),""));
        viewHolder.txtServiceDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String numberOfCalls="";
                String expiryDated="";
                if(!StringUtils.isNullOrEmpty(expiryDate)){
                    String mFeedBackDate=expiryDate;
                    String  feedbackDate=mFeedBackDate.substring(0,mFeedBackDate.indexOf("T"));
                    expiryDated=GlobalUtils.getDateString(feedbackDate);
                }
                if(planList.get(position).getTotalCalls()==99){
                    numberOfCalls="Unlimited";
                }else{
                    numberOfCalls=""+planList.get(position).getTotalCalls();
                }
    if(isPlanUnlimited) {
    GlobalUtils.displayDialog(mContext, "Info", "Total calls: " + numberOfCalls + "\nExpiry date: " + expiryDated);
   }else {
        GlobalUtils.displayDialog(mContext, "Info", "Total calls: " + numberOfCalls + "\nUsed calls: "+planList.get(position).getUsedCalls()+"\nRemaining calls: "+planList.get(position).getRemainingCalls()+"\nExpiry date: " + expiryDated);
    }
            }
        });
        viewHolder.mainLinlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                moveToNextScreen(planList.get(position),position);
            }
        });


    }

    private void moveToNextScreen(ParentService plan,int position) {
        ServiceCategoryResponse serviceCategoryResponse = new Gson().fromJson(GlobalUtils.getJsonData(mContext, categoryFileName), ServiceCategoryResponse.class);
        for (ServiceCategoryModel serviceCategoryModel:serviceCategoryResponse.getData()){
            int planId=plan.getServiceId();
            int  servicegrpId=serviceCategoryModel.getServiceGroupId();
            if(planId==servicegrpId){
                moveToServiceScreen(serviceCategoryModel,position);
                break;
            }
        }

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgService;
        private RelativeLayout btnRightArrow;
        private TextView txtServiceName;
        private TextView txtServiceDescription;
        private LinearLayout mainLinlay;
        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            imgService = (ImageView) itemLayoutView.findViewById(R.id.img_service);
            btnRightArrow = (RelativeLayout) itemLayoutView.findViewById(R.id.relay_arrow_right);
            txtServiceDescription=(TextView) itemLayoutView.findViewById(R.id.txt_service_description);
            txtServiceName=(TextView)itemLayoutView.findViewById(R.id.txt_service_name);
            mainLinlay=(LinearLayout)itemLayoutView.findViewById(R.id.main_linlay);
        }
    }
    void moveToServiceScreen(ServiceCategoryModel mCategoryModel,int position) {
        AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", mContext);
        Intent mIntent=new Intent(mContext,ActivityServiceGroup.class);
        mIntent.putExtra(Constant.CATEGORY_MODEL_DATA,mCategoryModel);
        mIntent.putExtra(Constant.OPEN_TICKET_SERVICE_DATA,planList.get(position));
        mIntent.putExtra(Constant.IS_OPEN_TICKET_TRUE,true);
        mIntent.putExtra(Constant.ADDRESS_ID,addressId);
        mIntent.putExtra(Constant.PLAN_ID,mPlanId);
        mIntent.putExtra(Constant.TIME_SLOT_DATA,  (Serializable)mCategoryModel.getStimeSlotList());
        mContext.startActivity(mIntent);

    }
    @Override
    public int getItemCount() {
        return planList.size();
    }

    Drawable getImage(String serviceNamee,String planType){
        // Log.e("serviceNamee ",serviceNamee);
        String serviceName=serviceNamee.toLowerCase();
        // Log.e("serviceName ",serviceName);
        Drawable icon=null;
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(planType.equalsIgnoreCase("U")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_unlimited);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_unlimited);
            }
        }else if(planType.equalsIgnoreCase("F")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_fixed_lock);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_fixed_lock);
            }
        }else if(planType.equalsIgnoreCase("A")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_addon);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_addon);
            }
        }
        else if(serviceName.contains("electrical")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_electrical);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_electrical);
            }
        } else if(serviceName.contains("plumbing")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_plumbing);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_plumbing);
            }

        }else if(serviceName.contains("carpentry")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_carpentry);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_carpentry);
            }

        }else if(serviceName.contains("pest control")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_pest_control);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_pest_control);
            }

        }else if(serviceName.contains("ac repair")||serviceName.contains("ac")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_acrepair);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_acrepair);
            }

        }else if(serviceName.contains("watertank cleaning")||serviceName.contains("Watertank Cleaning")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_water_tank);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_water_tank);
            }

        }else if(serviceName.contains("security services")||serviceName.contains("security")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_security);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_security);
            }

        }else if(serviceName.contains("gardening")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_gardening);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_gardening);
            }

        }else if(serviceName.contains("cctv")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_cctv);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_cctv);
            }

        }else if(serviceName.contains("ro")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_ro);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_ro);
            }

        }else if(serviceName.contains("home cleaning")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_home_cleaning);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_home_cleaning);
            }

        }else if(serviceName.contains("home appliances")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_home_appliances);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_home_appliances);
            }

        }else if(serviceName.contains("upholstery cleaning")||serviceName.contains("Upholstery")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_uphole_history);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_uphole_history);
            }

        }else if(serviceName.contains("interior design")||serviceName.contains("interior")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_interier_design);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_interier_design);
            }

        }else if(serviceName.contains("Car Spa")||serviceName.contains("car")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_car_cleaning);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_car_cleaning);
            }

        }


        return icon;
    }

}