package com.mnm.services.model.response;

import java.util.List;

/**
 * Created by satendra.singh on 07-03-2017.
 */

public class CategoryServiceResponse {
    private boolean hasError;
    private String messageCode;
    private String message;
    private List<ServiceCategory> data = null;
    private List<String> messages = null;


    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ServiceCategory> getData() {
        return data;
    }

    public void setData(List<ServiceCategory> data) {
        this.data = data;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }
}
