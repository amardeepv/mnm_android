package com.mnm.services.ui.adapters;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;

import com.kelltontech.utils.StringUtils;
import com.mnm.services.R;
import com.mnm.services.listner.AddressInterface;
import com.mnm.services.model.response.address.AddressModel;
import com.mnm.services.ui.activity.ActivityPlan;

import java.util.ArrayList;

public class AddressAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<AddressModel> addressList;
    ActivityPlan planActivity;
    AddressInterface addressInterface;


    public AddressAdapter(Context context, ArrayList<AddressModel> addressList,AddressInterface addressInterface) {
        this.context = context;
        this.addressList = addressList;
        this.addressInterface=addressInterface;
        this.planActivity=planActivity;
    }

    @Override
    public int getCount() {
        return addressList.size();
    }

    @Override
    public Object getItem(int position) {
        return addressList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_address, null);
            holder = new ViewHolder();
            holder.selectAddressBtn = (RadioButton) convertView.findViewById(R.id.address_radio);
            holder.txtAddressOne=(TextView) convertView.findViewById(R.id.address_line_one);
            holder.txtAddressTwo=(TextView) convertView.findViewById(R.id.address_line_two);
            holder.txtAddressThree=(TextView) convertView.findViewById(R.id.address_line_three);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final AddressModel addressModel=addressList.get(position);


        holder.txtAddressOne.setText(addressModel.getTitle());
        int addressCount=position+1;

        if(addressModel.isChecked()){
            holder.selectAddressBtn.setChecked(true);
        }else{
            holder.selectAddressBtn.setChecked(false);
        }
        holder.selectAddressBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setRadioCheck(position,addressList.get(position).isChecked()?false:true);
            }
        });
        /*holder.selectAddressBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                setRadioCheck(position,b);

            }
        });*/
        return convertView;
    }

    class ViewHolder {
        private RadioButton selectAddressBtn;
        private TextView txtAddressOne;
        private TextView txtAddressTwo;
        private TextView txtAddressThree;
    }

    void setRadioCheck(int position,Boolean ischeckedtrue){
        for (int i=0;i<addressList.size();i++){
            if(i==position) {
                addressList.get(i).setChecked(ischeckedtrue);
                addressInterface.setAddress(addressList.get(i));
            }else{
                addressList.get(i).setChecked(false);
            }
        }
        notifyDataSetChanged();

    }
    public ArrayList<AddressModel>  getAddressList(){
        return addressList;
    }
}
