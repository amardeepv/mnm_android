package com.mnm.services.model.request;

import java.util.List;

/**
 * Created by saten on 4/4/17.
 */

public class ApplyCouponRequest {
    private List<SimpleCouponRequestModel> lstCouponRequest = null;

    public List<SimpleCouponRequestModel> getLstCouponRequest() {
        return lstCouponRequest;
    }

    public void setLstCouponRequest(List<SimpleCouponRequestModel> lstCouponRequest) {
        this.lstCouponRequest = lstCouponRequest;
    }
}