package com.mnm.services.model.request;

/**
 * Created by saten on 4/6/17.
 */

public class AutoCouponRequest {
  String  userID;
    String  serviceGroupID;
    double  serviceAmount;
    String    channel;

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getServiceGroupID() {
        return serviceGroupID;
    }

    public void setServiceGroupID(String serviceGroupID) {
        this.serviceGroupID = serviceGroupID;
    }

    public double getServiceAmount() {
        return serviceAmount;
    }

    public void setServiceAmount(double serviceAmount) {
        this.serviceAmount = serviceAmount;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
