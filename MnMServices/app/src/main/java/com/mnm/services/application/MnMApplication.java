package com.mnm.services.application;

import android.app.Application;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.facebook.FacebookSdk;

public class MnMApplication extends MultiDexApplication {

    public static MnMApplication instance;

    public void onCreate() {
        super.onCreate();

        MultiDex.install(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        instance = this;

        //This is for Firebase crash analytics
        /*Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                FirebaseCrash.report(ex);
            }
        });*/

    }

    public static synchronized MnMApplication getInstance() {
        if (instance != null)
            return instance;
        return null;
    }
}
