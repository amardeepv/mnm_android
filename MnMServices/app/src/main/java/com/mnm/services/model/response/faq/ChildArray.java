package com.mnm.services.model.response.faq;

/**
 * Created by satendra.singh on 10-05-2017.
 */

public class ChildArray {
    private String point;

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }
}
