package com.mnm.services.constants;



public interface AppSettings {
    //Production URL
   String BASE_URL= "https://www.makenmake.in/mnmapi/";
    //String BASE_URL= "http://103.25.131.238/MnMapiNew/";
    //UAT URL
    //String BASE_URL = "http://103.25.131.238/mnmapi/";
    String LOGIN_URL_EXTERNAL = BASE_URL + "Account/Login";
    //String SERVICE_LOCATION_API="https://maps.googleapis.com/maps/api/place/autocomplete/json?=types=geocode&sensor=false&components=country:IN&key=AIzaSyC2Z2qRrXy_WJLHSyyrfBMTXbYObgwhbys&input=gurgaon";

 String SERVICE_LOCATION_API_FIRST_HALF="https://maps.googleapis.com/maps/api/geocode/json?address=";
   String SERVICE_LOCATION_SECOND_HALF="&components=country:IN&key=AIzaSyC2Z2qRrXy_WJLHSyyrfBMTXbYObgwhbys";
 String SERVICE_BY_CITY_NAME=BASE_URL+"SearchServiceByCity?CityName=";
 //https://www.makenmake.in/mnmapi/earchServiceByCity?CityName=

    String VERIFY_OTP_URL = BASE_URL + "Account/VerifyMobile";
    String CONFIRM_MOBILE_URL = BASE_URL + "Account/AddMobile";
    String REGISTER = BASE_URL + "Account/Register";
    String SOCIAL_LOGIN_URL = BASE_URL + "Account/RegisterSocial/Mobile";
    String SERVICE_CATEGORY_URL = BASE_URL + "ServiceCategory/serviceCategories";
    String SERVICE_GROUP_URL=BASE_URL+"ServiceGroup/ServiceGroups?serviceGroupId=";
    String SERVICE_URL=BASE_URL + "Services?parentID=";
    String SERVICE_SEARCH_URL=BASE_URL+"SearchService?SearchText=";
    String SERVICE_GET_ADDRESS=BASE_URL+"user/";
    String SERVICE_ADD_ADDRESS=BASE_URL+"user/";

    String SERVICE_APPLYING_TAX=BASE_URL+"ApplyServiceTax?totalAmount=";
    String SERVICE_ADD_TO_BASKET=BASE_URL+"AddToBasket";
    String SERVICE_WALLET=BASE_URL+"WalletDetailsById?UserId=";
    String SERVICE_COUPAN=BASE_URL+"ApplyCoupon";
    String SERVICE_DEFAULT_COUPAN=BASE_URL+"DefaultApplyCoupon";
    String API_UNLIMITED_PLAN=BASE_URL+"getUnlimtedPlanDetail";
    String API_FIXED_PLAN=BASE_URL+"GetFixedPlanDetail";
    String API_AddOn_PLAN=BASE_URL+"GetAddOnPlanDetail";
    String API_UNLIMITED_DURATION=BASE_URL+"getPlanDuration?planid=";
    String API_PLAN_COUPAN=BASE_URL+"ApplyCouponForPlan";
    String API_ADD_TO_PLAN_BASKET=BASE_URL+"AddToPlanBasket";
    String SERVICE_BUY=BASE_URL+"BuyService";
    String PLAN_BUY=BASE_URL+"BuyPlan";

    String OPEN_TICKET_URL=BASE_URL+"GetPlanForOpenTicket?UserId=";
    String BUY_OPEN_TICKET_URL=BASE_URL+"OpenTicket";
    String CURRENT_SERVICE_ORDER_API=BASE_URL+"ServiceDetailsCurrentList?UserID=";
    String PAST_SERVICE_ORDER_API=BASE_URL+"ServiceDetailsList?UserID=";
    String PLAN_ORDER_API=BASE_URL+"PlanDetailsList?UserID=";
    String UPDATE_CONSUMER_PROFILE=BASE_URL+"UpdateConsumerProfile";
    String GET_CONSUMER_PROFILE=BASE_URL+"ConsumerByUserId?UserId=";
    String RFER_AND_EARN=BASE_URL+"GetReferralcode?UserId=";
    String WALLET_HISTORY_URL=BASE_URL+"WalletHistory?UserId=";
    String GENERATE_OTP_URL=BASE_URL+"Account/ForgotPasswordOTP?mobile=";
    String CREATE_PASSWORD_URL=BASE_URL+"Account/ForgotPasswordOTPConfrim?mobile=";
   String ADD_FEEDBACK_URL=BASE_URL+"AddFeedback";



    //API CONSTANT
    int REGISTER_REQUEST=1;
    int LOGIN_REQUEST=2;
    int VERIFY_OTP_REQUEST=3;
    int CONFIRM_MOBILE_REQUEST=4;
    int SOCIAL_LOGIN_REQUEST=5;
    int SERVICE_CATEGORY_REQUEST=6;
    int SERVICE_GROUP_REQUEST=7;
    int SERVICE_REQUEST=8;
    int SERVICE_SEARCH_REQUEST=9;
    int SERVICE_ADDRESS_REQUEST=10;
    int LOCATION_SEARCH_REQUEST=11;
    int SERVICE_BY_CITY_NAME_EVENT=12;
    int SERVICE_ADD_ADDRESS_EVENT=13;
    int SERVICE_APPLY_TAX_EVENT=14;
    int SERVICE_ADD_TO_BASKET_EVENT=15;
    int SERVICE_WALLET_EVENT=16;
    int SERVICE_BUY_EVENT=17;
    int SERVICE_COUPAN_EVENT=18;
    int SERVICE_DEFAULT_COUPAN_EVENT=19;
    int PLAN_EVENT=20;
    int UNLIMITED_DURATION_EVENT=21;
    int COUPAN_PLAN_EVENT=22;
    int ADD_TO_PLAN_BASKET=23;
    int OPEN_TICKET_EVENT=24;
    int BUY_OPEN_TICKET_EVENT=25;
    int SERVICE_ORDER_EVENT=26;
    int PLAN_ORDER_EVENT=27;
    int UPDATE_CONSUMER_PROFILE_EVENT=28;
    int EDIT_ADDRESS_EVENT=29;
    int DELETE_ADDRESS_EVENT=30;
    int PROFILE_GET_DATA_EVENT=31;
    int REFER_AND_EARN_EVENT=32;
    int WALLET_HISTORY_EVENT=33;
    int GENERATE_OTP=35;
    int CREATE_PASSWORD_EVENT=36;
   int ADD_FEEDBACK=37;
}
