package com.mnm.services.model.response.profile;

/**
 * Created by satendra.singh on 02-05-2017.
 */

public class ConsumerDetail {
    private String registrationDate;
    private String sourcedBy;
    private String serviceType;
    private String reason;

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getSourcedBy() {
        return sourcedBy;
    }

    public void setSourcedBy(String sourcedBy) {
        this.sourcedBy = sourcedBy;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
