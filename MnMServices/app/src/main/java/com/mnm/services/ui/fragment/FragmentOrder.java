package com.mnm.services.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.mnm.services.R;
import com.mnm.services.constants.Constant;
import com.mnm.services.ui.tabs.SlidingTabLayout;
import java.util.ArrayList;
import java.util.List;


public class FragmentOrder extends Fragment  implements View.OnClickListener{
    private   ViewPager  viewPager;
    private SlidingTabLayout tabLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview =  inflater.inflate(R.layout.fragment_plan_description, container, false);
        viewPager = (ViewPager)rootview.findViewById(R.id.mpager);
        tabLayout = (SlidingTabLayout)rootview.findViewById(R.id.tabLayout);
        tabLayout.setDistributeEvenly(true);
        initViewPagerAndTabs();
        return rootview;
    }


    private void initViewPagerAndTabs() {
        PagerAdapter pagerAdapter = new PagerAdapter(getActivity().getSupportFragmentManager());
        Bundle mcurrentOrderbundle=new Bundle();
        mcurrentOrderbundle.putInt(Constant.ORDER_TYPE,Constant.CURRENT_ORDER);
        Bundle mPastOrder=new Bundle();
        mPastOrder.putInt(Constant.ORDER_TYPE,Constant.PAST_ORDER);
        FragmnetServiceOrder currentOrder = new FragmnetServiceOrder();
        currentOrder.setArguments(mcurrentOrderbundle);
        FragmnetServiceOrder pastOrder = new FragmnetServiceOrder();
        pastOrder.setArguments(mPastOrder);
        String[] tabs = getResources().getStringArray(R.array.tabsMyOrder);
        pagerAdapter.addFragment(currentOrder, tabs[0]);
        pagerAdapter.addFragment(pastOrder, tabs[1]);
        viewPager.setOffscreenPageLimit(2);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setViewPager(viewPager);

        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white_color);  }

        });

        tabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });
    }

    @Override
    public void onClick(View view) {

    }

    class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

}



