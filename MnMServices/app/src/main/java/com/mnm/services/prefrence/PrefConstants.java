package com.mnm.services.prefrence;

/**
 * Created by satender.s on 16-08-2016.
 */
public class PrefConstants {
    public static final String MNM_PREF_NAME = "mnm_prefernce_name";
    public static String PREF_MOBILE = "mobile";
    public static String IS_LOGIN_TRUE = "is_login_true";
    public static String CATEGORY_DATA_UPDATE_TIME="service_data_update_time";
    public static String IS_GET_STARTED_CLICKED = "is_get_started_clicked";
    public static  String USER_ID="user_id";
    public static  String CITY_NAME="city_name";
    public static  String USER_NAME="user_name";
    public static  String USER_PHONE="user_phone";
    public static  String SELECTED_SERVICE_DATA="selected_service_data";
    public static  String ADDRESS_COUNT="ADDRESS_COUNT";
    public static  String AUTHORIZATION_TOKEN="authorization_token_value";



}
