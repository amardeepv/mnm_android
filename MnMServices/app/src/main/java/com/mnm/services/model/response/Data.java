package com.mnm.services.model.response;

import android.content.Intent;

/**
 * Created by bluepi on 16/2/17.
 */

public class Data {
    String status;
    String accontStatus;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    String name;
    private String accountStatus;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccontStatus() {
        return accontStatus;
    }

    public void setAccontStatus(String accontStatus) {
        this.accontStatus = accontStatus;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    private Integer userId;



    private String userName;

    private AccessToken access,accessToken;


    public AccessToken getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(AccessToken accessToken) {
        this.accessToken = accessToken;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public AccessToken getAccess() {
        return access;
    }

    public void setAccess(AccessToken access) {
        this.access = access;
    }
}
