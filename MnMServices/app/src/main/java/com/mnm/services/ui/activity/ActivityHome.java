package com.mnm.services.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.android.volley.Response;
import com.google.gson.Gson;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.StringUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.Constant;
import com.mnm.services.controller.ControllerAddress;
import com.mnm.services.controller.ControllerBuyOpenTicket;
import com.mnm.services.controller.ControllerOpenTicket;
import com.mnm.services.controller.ControllerSearchLocation;
import com.mnm.services.controller.ControllerServiceByCity;
import com.mnm.services.controller.ControllerServiceSearch;
import com.mnm.services.model.request.openticketrequest.OpenTicketRequest;
import com.mnm.services.model.response.AddressComponent;
import com.mnm.services.model.response.LocationSearchResponse;
import com.mnm.services.model.response.ServiceCategoryModel;
import com.mnm.services.model.response.ServiceCategoryResponse;
import com.mnm.services.model.response.ServiceModel;
import com.mnm.services.model.response.address.AddressResponse;
import com.mnm.services.model.response.oepnticket.ChildService;
import com.mnm.services.model.response.oepnticket.OpenTicketResponse;
import com.mnm.services.model.response.oepnticket.ParentService;
import com.mnm.services.model.response.oepnticket.Plan;
import com.mnm.services.model.response.profile.PlanIdServiceid;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.adapters.AdapterAddress;
import com.mnm.services.ui.adapters.AdapterBannerHome;
import com.mnm.services.ui.fragment.FragmentAddress;
import com.mnm.services.ui.widgets.CirclePageIndicator;
import com.mnm.services.utils.GlobalUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.mnm.services.constants.AppSettings.BUY_OPEN_TICKET_EVENT;
import static com.mnm.services.constants.Constant.AC_SERVICE;
import static com.mnm.services.constants.Constant.CARPENTRY_SERVICE;
import static com.mnm.services.constants.Constant.CAR_SPA_SERVICE;
import static com.mnm.services.constants.Constant.CCTV_SERVICE;
import static com.mnm.services.constants.Constant.ELECTRICAL_SERVICE;
import static com.mnm.services.constants.Constant.GARDENING_SERVICE;
import static com.mnm.services.constants.Constant.HOME_APPLIANCE_SERVICE;
import static com.mnm.services.constants.Constant.HOME_CLEANING_SERVICE;
import static com.mnm.services.constants.Constant.INTERIER_DESIGN_SERVICE;
import static com.mnm.services.constants.Constant.PAINT_SERVICE;
import static com.mnm.services.constants.Constant.PEST_CONTROL_SERVICE;
import static com.mnm.services.constants.Constant.PLUMBING_SERVICE;
import static com.mnm.services.constants.Constant.ROO_SERVICE;
import static com.mnm.services.constants.Constant.SECURITY_SERVICE;
import static com.mnm.services.constants.Constant.UPHOLESTRY_SERVICE;
import static com.mnm.services.constants.Constant.WATER_TANK_SERVICE;
import static com.mnm.services.constants.Constant.categoryFileName;

public class ActivityHome extends MyDrawerBaseActivity implements ViewPager.OnPageChangeListener, TextWatcher, View.OnClickListener {
    private ProgressBar progreesBar;
    private ViewPager bannerViewPager;
    private CirclePageIndicator pagerIndicater;
    boolean isElectricalClick, isPlumbingClick, isCarpentryClick, isPestControlClick, isACRepairClick, isWaterTankClick,
            isGardeningClick, isCCTVClick, isROClick, isHomeCleaningClick, isHomeAppliancesClick, isPaintServicesClick,
            isCarCleaningClick, isUPHOLESTRYClick, isINTERIERDESIGNClick, isSECurityServiceClick;
    private ServiceCategoryResponse serviceCategoryResponse;
    private RelativeLayout service_electrical, service_plumbing, service_carpentry,
            service_pest_control, service_ac_repair, service_water_tank_category, service_security_services,
            service_gardening, service_cctv, service_ro, service_home_cleaning, service_home_appliances, service_paint_services,
            service_car_cleaning, service_uphole_history, service_interior_design;
    private RelativeLayout relayServices, relaySearchServices;
    private AutoCompleteTextView edtSearch;
    private AutoCompleteTextView edtSearchlocation;
    private ListView listViewSearchServices;
    private ArrayAdapter<String> adapter;
    private ArrayAdapter<String> adapterLocation;
    private RelativeLayout relayServiceSearch;
    private TextView lblNoDataFound;
    // private CardView relaySearchServicess;
    // Listview Data
    private ArrayList<String> serviceSearchList;
    private List<ServiceCategoryModel> searchservices = null;
    private RelativeLayout locationRelay;
    private TextView lblLocNoDataFound;
    private ProgressBar progressBarLoc;
    ArrayList<String> mLocationlist;
    private ScrollView scrollView;
    ArrayAdapter<String> adapterSearchLoction;
    ArrayAdapter<String> adapterSearchService;
    TextView userName;
    private LinearLayout searchLayut;
    private ImageView btnCross,imgCrossBtnSearch;
private boolean isSingleAddressTrue=false;
    private  ArrayList<PlanIdServiceid> alreadyPurchasedServices=new ArrayList<>();
    private String addressIdSingle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        initUi();
    }


    private void initUi() {
        initCustomActionBarView(this, "Home", false);
        initializeDrawer();
        userName = (TextView) findViewById(R.id.user_name);
        btnCross=(ImageView)findViewById(R.id.img_cross);
        imgCrossBtnSearch=(ImageView)findViewById(R.id.img_cross_search);
        btnCross.setVisibility(View.GONE);
        imgCrossBtnSearch.setVisibility(View.GONE);
        findViewById(R.id.root_relay_home).setOnClickListener(this);
        edtSearchlocation = (AutoCompleteTextView) findViewById(R.id.edt_search);
        edtSearchlocation.setText(AppSharedPreference.getString(PrefConstants.CITY_NAME, "", this));
        edtSearchlocation.setSelection(edtSearchlocation.getText().length());
        edtSearchlocation.setOnClickListener(this);
        edtSearchlocation.bringToFront();
        edtSearch = (AutoCompleteTextView) findViewById(R.id.edt_service_search);
        progreesBar = (ProgressBar) findViewById(R.id.mprogrees_bar);
        progressBarLoc = (ProgressBar) findViewById(R.id.progrees_bar_location_serach);
        lblLocNoDataFound = (TextView) findViewById(R.id.lbl_no_data_found_loc);
        locationRelay = (RelativeLayout) findViewById(R.id.location_relay_out);
        locationRelay.setVisibility(View.GONE);
        lblLocNoDataFound.setVisibility(View.GONE);
        progressBarLoc.setVisibility(View.GONE);
        bannerViewPager = (ViewPager) findViewById(R.id.mpager);
        pagerIndicater = (CirclePageIndicator) findViewById(R.id.pager_indicater);
        progreesBar.setVisibility(View.GONE);
        findViewById(R.id.service_electrical).setOnClickListener(this);
        findViewById(R.id.service_plumbing).setOnClickListener(this);
        findViewById(R.id.service_carpentry).setOnClickListener(this);
        findViewById(R.id.service_pest_control).setOnClickListener(this);
        findViewById(R.id.service_ac_repair).setOnClickListener(this);
        findViewById(R.id.service_water_tank_category).setOnClickListener(this);
        findViewById(R.id.service_security_services).setOnClickListener(this);
        findViewById(R.id.service_gardening).setOnClickListener(this);
        findViewById(R.id.service_cctv).setOnClickListener(this);
        findViewById(R.id.service_ro).setOnClickListener(this);
        findViewById(R.id.service_home_cleaning).setOnClickListener(this);
        findViewById(R.id.service_home_appliances).setOnClickListener(this);
        findViewById(R.id.service_paint_services).setOnClickListener(this);
        findViewById(R.id.service_car_cleaning).setOnClickListener(this);
        findViewById(R.id.service_uphole_history).setOnClickListener(this);
        findViewById(R.id.service_interior_design).setOnClickListener(this);

        relayServices = (RelativeLayout) findViewById(R.id.relay_our_services);
        relaySearchServices = (RelativeLayout) findViewById(R.id.relay_search_services);
        listViewSearchServices = (ListView) findViewById(R.id.listview_search_services);
        // relaySearchServicess = (CardView) findViewById(R.id.relay_search);
        lblNoDataFound = (TextView) findViewById(R.id.lbl_no_data_found);
        findViewById(R.id.relay_unlimited_plan).setOnClickListener(this);
        findViewById(R.id.relay_fixed_plan).setOnClickListener(this);
        findViewById(R.id.relay_addon_plan).setOnClickListener(this);
        searchLayut = (LinearLayout) findViewById(R.id.search_layoutt);
        searchLayut.bringToFront();
        //  relaySearchServicess.setVisibility(View.GONE);
        // relaySearchServicess.bringToFront();
        scrollView = (ScrollView) findViewById(R.id.mscrollview);
        findViewById(R.id.linear_lay_grid).setOnClickListener(this);
        AdapterBannerHome bannerAdapterHome = new AdapterBannerHome(ActivityHome.this);
        bannerViewPager.setAdapter(bannerAdapterHome);
        pagerIndicater.setRadius(7);
        pagerIndicater.setViewPager(bannerViewPager);
        pageSwitcher(3);
        btnCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("cross btn clicked ","clicked");
                edtSearchlocation.setText("");
            }
        });
        imgCrossBtnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtSearch.setText("");
                relayServices.setVisibility(View.VISIBLE);
                imgCrossBtnSearch.setVisibility(View.GONE);
                listViewSearchServices.setVisibility(View.GONE);
                GlobalUtils.hideKeyboardFrom(ActivityHome.this,view);
               // edtSearch.setAdapter(adapterSearchService);
                //adapterSearchService.notifyDataSetChanged();

            }
        });
        edtSearchlocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence cs, int i, int i1, int i2) {
                if (!StringUtils.isNullOrEmpty(cs.toString())) {
                    searchLocationApi(cs.toString());
                    btnCross.setVisibility(View.VISIBLE);
                    //addressLinetwo.setEnabled(false);
                }else {
                    btnCross.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        edtSearchlocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {
                String cityName = (String) parent.getItemAtPosition(pos);
                Log.e("itemSelected ", cityName);
                String mCityname = cityName;//cityName.substring(0, cityName.indexOf(","));
                Log.e("cityname ", mCityname);
                edtSearchlocation.setText(mCityname);
                edtSearchlocation.setSelection(edtSearchlocation.getText().length());
                serviceByCityNameApi(mCityname);
                serviceByCityNameApi(mCityname);
                GlobalUtils.hideKeyboardFrom(ActivityHome.this, arg1);

            }
        });

        edtSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                relayServices.setVisibility(View.GONE);
                //relaySearchServicess.setVisibility(View.VISIBLE);
                return false;
            }
        });
        edtSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                if (!StringUtils.isNullOrEmpty(cs.toString())) {
                    serviceSearchApi(cs.toString());
                    imgCrossBtnSearch.setVisibility(View.VISIBLE);
                }else {
                    imgCrossBtnSearch.setVisibility(View.GONE);

                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        edtSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int pos, long id) {
                GlobalUtils.hideKeyboardFrom(ActivityHome.this, arg1);
                moveToServiceScreen(searchservices.get(pos));

            }
        });

        enableServiceUi(null);
    }

    private void enableServiceUi(ServiceCategoryResponse mserviceCategoryResponse) {
        if (mserviceCategoryResponse == null) {
            serviceCategoryResponse = new Gson().fromJson(GlobalUtils.getJsonData(ActivityHome.this, categoryFileName), ServiceCategoryResponse.class);

        } else {
            serviceCategoryResponse = mserviceCategoryResponse;
        }

        if (serviceCategoryResponse!=null&&serviceCategoryResponse.getData() != null && serviceCategoryResponse.getData().size() > 0) {
            for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                if (obj.getServiceGroupId() == ELECTRICAL_SERVICE) {
                    isElectricalClick = true;
                } else if (obj.getServiceGroupId() == PLUMBING_SERVICE) {
                    isPlumbingClick = true;
                } else if (obj.getServiceGroupId() == CARPENTRY_SERVICE) {
                    isCarpentryClick = true;
                } else if (obj.getServiceGroupId() == PEST_CONTROL_SERVICE) {
                    isPestControlClick = true;
                } else if (obj.getServiceGroupId() == AC_SERVICE) {
                    isACRepairClick = true;
                } else if (obj.getServiceGroupId() == WATER_TANK_SERVICE) {
                    isWaterTankClick = true;
                } else if (obj.getServiceGroupId() == GARDENING_SERVICE) {
                    isGardeningClick = true;
                } else if (obj.getServiceGroupId() == CCTV_SERVICE) {
                    isCCTVClick = true;
                } else if (obj.getServiceGroupId() == ROO_SERVICE) {
                    isROClick = true;
                } else if (obj.getServiceGroupId() == HOME_CLEANING_SERVICE) {
                    isHomeCleaningClick = true;
                } else if (obj.getServiceGroupId() == HOME_APPLIANCE_SERVICE) {
                    isHomeAppliancesClick = true;
                } else if (obj.getServiceGroupId() == PAINT_SERVICE) {
                    isPaintServicesClick = true;
                } else if (obj.getServiceGroupId() == CAR_SPA_SERVICE) {
                    isCarCleaningClick = true;
                } else if (obj.getServiceGroupId() == UPHOLESTRY_SERVICE) {
                    isUPHOLESTRYClick = true;
                } else if (obj.getServiceGroupId() == INTERIER_DESIGN_SERVICE) {
                    isINTERIERDESIGNClick = true;
                } else if (obj.getServiceGroupId() == SECURITY_SERVICE) {
                    isSECurityServiceClick = true;
                }
            }
        } else {
            if (!StringUtils.isNullOrEmpty("" + serviceCategoryResponse) && serviceCategoryResponse != null)
                ToastUtils.showToast(ActivityHome.this, "" + serviceCategoryResponse.getMessage());
        }
    }

    void moveToServiceScreen(ServiceCategoryModel mCategoryModel) {
        Intent mIntent = new Intent(ActivityHome.this, ActivityService.class);
        mIntent.putExtra(Constant.CATEGORY_MODEL_DATA, mCategoryModel);
        mIntent.putExtra(Constant.SINGLE_ADDRESS_OPEN_TICKET_DATA,alreadyPurchasedServices);
        startActivity(mIntent);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.relay_unlimited_plan:
                if (!ConnectivityUtils.isNetworkEnabled(ActivityHome.this)) {
                    ToastUtils.showToast(this, getResources().getString(R.string.network_error));
                } else {
                    Intent mUnlimitedIntent = new Intent(ActivityHome.this, ActivityPlan.class);
                    mUnlimitedIntent.putExtra(Constant.PLAN_TYPE, Constant.PLAN_TYPE_UNLIMITED);
                    startActivity(mUnlimitedIntent);
                }
                break;
            case R.id.relay_fixed_plan:
                if (!ConnectivityUtils.isNetworkEnabled(ActivityHome.this)) {
                    ToastUtils.showToast(this, getResources().getString(R.string.network_error));
                } else {
                    Intent mIntentFixedPlan = new Intent(ActivityHome.this, ActivityPlan.class);
                    mIntentFixedPlan.putExtra(Constant.PLAN_TYPE, Constant.PLAN_TYPE_FIXED);
                    startActivity(mIntentFixedPlan);
                }
                break;
            case R.id.relay_addon_plan:
                if (!ConnectivityUtils.isNetworkEnabled(ActivityHome.this)) {
                    ToastUtils.showToast(this, getResources().getString(R.string.network_error));
                } else {
                    Intent mIntentAddOnPlan = new Intent(ActivityHome.this, ActivityPlan.class);
                    mIntentAddOnPlan.putExtra(Constant.PLAN_TYPE, Constant.PLAN_TYPE_ADD_ON);
                    startActivity(mIntentAddOnPlan);
                }

                break;
            case R.id.service_electrical:

                if (isElectricalClick) {
                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == ELECTRICAL_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }
                break;
            case R.id.service_plumbing:

                if (isPlumbingClick) {
                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == PLUMBING_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }
                break;
            case R.id.service_carpentry:
                if (isCarpentryClick) {

                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == CARPENTRY_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }

                break;
            case R.id.service_pest_control:
                if (isPestControlClick) {

                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == PEST_CONTROL_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }
                break;
            case R.id.service_ac_repair:
                if (isACRepairClick) {

                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == AC_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }
                break;
            case R.id.service_water_tank_category:
                if (isWaterTankClick) {

                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == WATER_TANK_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }
                break;
            case R.id.service_security_services:
                if (isSECurityServiceClick) {

                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == SECURITY_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }
                break;
            case R.id.service_gardening:
                if (isGardeningClick) {

                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == GARDENING_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }
                break;
            case R.id.service_cctv:
                if (isCCTVClick) {

                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == CCTV_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }
                break;
            case R.id.service_ro:
                if (isROClick) {

                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == ROO_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }
                break;
            case R.id.service_home_cleaning:
                if (isHomeCleaningClick) {

                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == HOME_CLEANING_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }
                break;
            case R.id.service_home_appliances:
                if (isHomeAppliancesClick) {

                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == HOME_APPLIANCE_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }
                break;
            case R.id.service_paint_services:
                if (isPaintServicesClick) {


                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == PAINT_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }
                break;
            case R.id.service_car_cleaning:
                if (isCarCleaningClick) {

                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == CAR_SPA_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }
                break;
            case R.id.service_uphole_history:
                if (isUPHOLESTRYClick) {

                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == UPHOLESTRY_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }
                break;
            case R.id.service_interior_design:
                if (isINTERIERDESIGNClick) {
                    for (ServiceCategoryModel obj : serviceCategoryResponse.getData()) {
                        if (obj.getServiceGroupId() == INTERIER_DESIGN_SERVICE) {
                            moveToServiceScreen(obj);
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivityHome.this, "This service coming soon in your city");
                }
                break;
            case R.id.root_relay_home:
                resetUi(v);
                break;
            case R.id.edt_service_search:
                break;
            case R.id.edt_search:
                resetUi(v);
            case R.id.linear_lay_grid:
                resetUi(v);
            default:
                break;
        }
    }


    @Override
    protected void updateUi(Response response) {
        super.updateUi(response);
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


    }

    @Override
    public void afterTextChanged(Editable editable) {

    }


    @Override
    protected void onResume() {
        super.onResume();
        if(AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE,false,ActivityHome.this)) {
            if (!StringUtils.isNullOrEmpty(AppSharedPreference.getString(PrefConstants.USER_NAME, "", ActivityHome.this))) {
                userName.setText(AppSharedPreference.getString(PrefConstants.USER_NAME, "", ActivityHome.this));
            } else {
                userName.setText("GUEST");
            }
        if(StringUtils.isNullOrEmpty(userName.getText().toString())||userName.getText().toString().equalsIgnoreCase("NULL")){
            userName.setText("GUEST");
        }
        relayServices.setVisibility(View.VISIBLE);
        edtSearch.setText("");

        }
        if(AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE,false,ActivityHome.this)) {
            if (AppSharedPreference.getInt(PrefConstants.ADDRESS_COUNT, 0, ActivityHome.this) == 0 || AppSharedPreference.getInt(PrefConstants.ADDRESS_COUNT, 0, ActivityHome.this) == 1) {
                callAddressApi();
            }
        }
       // userName.setText(AppSharedPreference.getString(PrefConstants.USER_NAME, "GUEST", ActivityHome.this));
    }
    private void resetUi(View v) {
        if (relayServices != null)
            relayServices.setVisibility(View.VISIBLE);
        //if (relaySearchServicess != null)
        //relaySearchServicess.setVisibility(View.GONE);
        if (edtSearch != null)
            edtSearch.setText("");
        //if (v != null)
        //GlobalUtils.hideKeyboardFrom(this, v);
    }

    void searchLocationApi(String searchTxt) {
        if (!ConnectivityUtils.isNetworkEnabled(ActivityHome.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerSearchLocation searchLocationController = new ControllerSearchLocation(this, this, searchTxt);
            progreesBar.setVisibility(View.VISIBLE);
            searchLocationController.getData(AppSettings.LOCATION_SEARCH_REQUEST, "");
        }
    }

    void serviceSearchApi(String searchTxt) {
        if (!ConnectivityUtils.isNetworkEnabled(ActivityHome.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerServiceSearch loginController = new ControllerServiceSearch(this, this, searchTxt);
            progreesBar.setVisibility(View.VISIBLE);
            loginController.getData(AppSettings.SERVICE_SEARCH_REQUEST, "");
        }
    }

    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        ActivityHome.this.runOnUiThread(new Runnable() {
            public void run() {
                progreesBar.setVisibility(View.GONE);
                progressBarLoc.setVisibility(View.GONE);

                //Log.e("Response", response.toString());
                if (response == null) {
                    removeProgressDialog();
                    // ToastUtils.showToast(ActivityHome.this, getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.LOCATION_SEARCH_REQUEST:
                            removeProgressDialog();
                            LocationSearchResponse locationSearchResponse = (LocationSearchResponse) response.getResponseObject();
                            ArrayList<String> mAddresslist = new ArrayList<String>();
                            if (locationSearchResponse.getResults() != null && locationSearchResponse.getResults().size() > 0 && locationSearchResponse.getResults().get(0).getAddress_components().size() > 0) {
                                for (AddressComponent obj : locationSearchResponse.getResults().get(0).getAddress_components()) {
                                    mAddresslist.add(obj.getLong_name());
                                }

                            } else {
                                mAddresslist.clear();
                            }
                            adapterSearchLoction = new ArrayAdapter<String>(ActivityHome.this, android.R.layout.select_dialog_item, mAddresslist);
                            edtSearchlocation.setThreshold(1);//will start working from first character
                            edtSearchlocation.setAdapter(adapterSearchLoction);//setting the adapter data into the AutoCompleteTextView
                            break;
                        case AppSettings.SERVICE_SEARCH_REQUEST:
                            removeProgressDialog();
                            // relaySearchServicess.setVisibility(View.VISIBLE);
                            ServiceCategoryResponse serviceGroupResponse = (ServiceCategoryResponse) response.getResponseObject();
                            if (!serviceGroupResponse.getHasError()) {
                                serviceSearchList = null;
                                serviceSearchList = new ArrayList<String>();
                                searchservices = serviceGroupResponse.getData();

                                for (ServiceCategoryModel obj : searchservices) {
                                    serviceSearchList.add(obj.getServiceGroupName());
                                }
                                if (serviceSearchList.size() > 0) {
                                    /*adapter = new ArrayAdapter<String>(ActivityHome.this, R.layout.row_search_service, R.id.product_name, serviceSearchList);
                                    listViewSearchServices.setAdapter(adapter);*/
                                    adapterSearchService = new ArrayAdapter<String>(ActivityHome.this, android.R.layout.select_dialog_item, serviceSearchList);
                                    edtSearch.setThreshold(1);//will start working from first character
                                    edtSearch.setAdapter(adapterSearchService);//setting the adapter data into the AutoCompleteTextView


                                } else {
                                   /* listViewSearchServices.setVisibility(View.GONE);
                                    lblNoDataFound.setVisibility(View.VISIBLE);
                                    lblNoDataFound.setText("No Data Found");*/
                                }

                                // moveToScreen();
                            } else {
                                listViewSearchServices.setVisibility(View.GONE);
                                lblNoDataFound.setVisibility(View.VISIBLE);
                                lblNoDataFound.setText("" + serviceGroupResponse.getMessage());
                            }
                            break;
                        case AppSettings.SERVICE_BY_CITY_NAME_EVENT:
                            removeProgressDialog();
                            ServiceCategoryResponse msServiceGroupResponse = (ServiceCategoryResponse) response.getResponseObject();
                            if (!msServiceGroupResponse.getHasError()) {
                                enableServiceUi(msServiceGroupResponse);
                            } else {
                                isElectricalClick = false;
                                isPlumbingClick = false;
                                isCarpentryClick = false;
                                isPestControlClick = false;
                                isACRepairClick = false;
                                isWaterTankClick = false;
                                isGardeningClick = false;
                                isCCTVClick = false;
                                isROClick = false;
                                isHomeCleaningClick = false;
                                isHomeAppliancesClick = false;
                                isPaintServicesClick = false;
                                isCarCleaningClick = false;
                                isUPHOLESTRYClick = false;
                                isINTERIERDESIGNClick = false;
                                isSECurityServiceClick = false;
                                ToastUtils.showToast(ActivityHome.this, msServiceGroupResponse.getMessage());
                            }
                            break;
                        case AppSettings.SERVICE_ADDRESS_REQUEST:
                            removeProgressDialog();
                            AddressResponse addressResponse = (AddressResponse) response.getResponseObject();
                            if (!addressResponse.getHasError()) {
                                if (addressResponse.getData().getLstAddressResult().size() == 1) {
                                    AppSharedPreference.putInt(PrefConstants.ADDRESS_COUNT, 1, ActivityHome.this);
                                    addressIdSingle=addressResponse.getData().getLstAddressResult().get(0).getId();
                                    getPlansApi(addressIdSingle);

                                } else {

                                    AppSharedPreference.putInt(PrefConstants.ADDRESS_COUNT, 2, ActivityHome.this);

                                }
                            } else {


                            }
                        case AppSettings.OPEN_TICKET_EVENT:
                            removeProgressDialog();
                            OpenTicketResponse openTicketResponse = (OpenTicketResponse) response.getResponseObject();
                            if (!openTicketResponse.getHasError()) {
                                if (openTicketResponse.getData() != null && openTicketResponse.getData().getPlans() != null && openTicketResponse.getData().getPlans().size() > 0) {
                                    for (Plan plan : openTicketResponse.getData().getPlans()) {
                                        if (plan != null && plan.getServiceList() != null && plan.getServiceList().size() > 0) {
                                            for (ParentService parentService : plan.getServiceList()) {
                                                if (parentService != null && parentService.getServices() != null && parentService.getServices().size() > 0) {
                                                    for (ChildService childService : parentService.getServices()) {
                                                        PlanIdServiceid planIdServiceid = new PlanIdServiceid();
                                                        planIdServiceid.setPlanId(plan.getPlanId());
                                                        planIdServiceid.setServiceId(Integer.parseInt(childService.getServiceId()));
                                                        alreadyPurchasedServices.add(planIdServiceid);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                } else {
                                    /*lblNoDataFound.setVisibility(View.VISIBLE);
                                    recycleView.setVisibility(View.GONE);*/
                                }

                            } else {
                               /* lblNoDataFound.setVisibility(View.VISIBLE);*/
                                //ToastUtils.showToast(ActivitySheduleBooking.this,""+openTicketResponse.getMessage());
                            }

                            break;
                        default:
                            break;
                    }
                }
            }
        });
    }


    void serviceByCityNameApi(String cityName) {
        if (!ConnectivityUtils.isNetworkEnabled(ActivityHome.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {

            ControllerServiceByCity loginController = new ControllerServiceByCity(this, this, cityName);
            showProgressDialog("Please wait...");
            loginController.getData(AppSettings.SERVICE_BY_CITY_NAME_EVENT, "");
        }
    }

    Timer timer;
    int page = 1;

    public void pageSwitcher(int seconds) {
        try {
            timer = new Timer();
            timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000); // delay
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    // this is an inner class...
    class RemindTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                public void run() {

                    if (page > 4) {
                        page = 0;// In my case the number of pages are 5
                        //timer.cancel();
                        // Showing a toast for just testing purpose
                       /* Toast.makeText(getApplicationContext(), "Timer stoped",
                                Toast.LENGTH_LONG).show();*/
                    } else {
                        bannerViewPager.setCurrentItem(page++);
                    }
                }
            });

        }
    }

    private void callAddressApi() {
        if (!ConnectivityUtils.isNetworkEnabled(ActivityHome.this)) {
            ToastUtils.showToast(ActivityHome.this, getResources().getString(R.string.network_error));
        } else {
            showProgressDialog("Please wait...");
            ControllerAddress addressController = new ControllerAddress(ActivityHome.this, this);
            addressController.getData(AppSettings.SERVICE_ADDRESS_REQUEST, null);
        }
    }

    void  getPlansApi(String addressId){
        if (!ConnectivityUtils.isNetworkEnabled(ActivityHome.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerOpenTicket openTicketController = new ControllerOpenTicket(this, this, addressId);
            if(alreadyPurchasedServices==null)
            showProgressDialog("Please wait...");
            openTicketController.getData(AppSettings.OPEN_TICKET_EVENT, null);
        }

    }


}