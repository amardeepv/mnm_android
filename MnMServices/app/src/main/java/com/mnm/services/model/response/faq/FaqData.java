package com.mnm.services.model.response.faq;

import java.util.List;

/**
 * Created by satendra.singh on 10-05-2017.
 */

public class FaqData {
private boolean isArrowExpand;

    public boolean isArrowExpand() {
        return isArrowExpand;
    }

    public void setArrowExpand(boolean arrowExpand) {
        isArrowExpand = arrowExpand;
    }

    private String question;

    private String answer;

    private List<ChildArray> child_array = null;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public List<ChildArray> getChild_array() {
        return child_array;
    }

    public void setChild_array(List<ChildArray> child_array) {
        this.child_array = child_array;
    }
}
