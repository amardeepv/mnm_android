package com.mnm.services.ui.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.kelltontech.utils.StringUtils;
import com.mnm.services.R;
import com.mnm.services.model.response.address.AddressModel;
import com.mnm.services.ui.fragment.FragmentAddress;
import java.util.List;

/**
 * Adapter to manage list of properties
 */
public class AdapterAddress extends RecyclerView.Adapter<AdapterAddress.ViewHolder> {
    private List<AddressModel> addressList;
    private Context mContext;
    private View mItemLayoutView;
    private ViewHolder mViewHolder;
    private LinearLayoutManager linearLayoutManager;
    FragmentAddress fragmentAddress;
    Dialog threeDotDialog;
    public AdapterAddress(Context ctx, List<AddressModel> addressList, RecyclerView recyclerView,FragmentAddress fragmentAddress) {
        this.addressList = addressList;
        this.mContext = ctx;
        this.fragmentAddress = fragmentAddress;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                }
            });
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("create","create view holder****three");
        mItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_address_new, null);
        mViewHolder = new ViewHolder(mItemLayoutView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        final AddressModel  addressModel = addressList.get(position);
        String title="",line1="",line2="",city="",pincode="",state="",area="",lat="0",longi="0";
        if(!StringUtils.isNullOrEmpty(addressModel.getTitle()))
            title=addressModel.getTitle();
        if(!StringUtils.isNullOrEmpty(addressModel.getLine1()))
            line1=addressModel.getTitle();
        if(!StringUtils.isNullOrEmpty(addressModel.getLine2()))
            line2=addressModel.getLine2();
        if(!StringUtils.isNullOrEmpty(addressModel.getCity()))
            city=addressModel.getCity();
        if(!StringUtils.isNullOrEmpty(addressModel.getPinCode()))
            pincode=addressModel.getPinCode();
        if(!StringUtils.isNullOrEmpty(addressModel.getState()))
            state=addressModel.getState();
        if(!StringUtils.isNullOrEmpty(addressModel.getArea()))
            area=addressModel.getArea();
        if(!StringUtils.isNullOrEmpty(addressModel.getLongitude()))
            lat=addressModel.getLongitude();
        if(!StringUtils.isNullOrEmpty(addressModel.getLongitude()))
            longi=addressModel.getLongitude();


        //viewHolder.txtAddressLineOne.setText(addressModel.getTitle());
        viewHolder.txtAddressLineOne.setText(addressModel.getTitle());

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtAddressLineOne;
        private TextView txtAddressLineTwo;
        private TextView txtAddressLineThree;



        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtAddressLineOne=(TextView) itemLayoutView.findViewById(R.id.txt_line_one);
            txtAddressLineTwo=(TextView) itemLayoutView.findViewById(R.id.txt_line_two);
            txtAddressLineThree=(TextView) itemLayoutView.findViewById(R.id.txt_line_three);
        }
    }

    @Override
    public int getItemCount() {
        return addressList.size();
    }

}