package com.mnm.services.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mnm.services.R;
import com.mnm.services.model.response.plan.PlanService;
import com.mnm.services.ui.activity.ActivityPlan;
import com.mnm.services.ui.fragment.FragmentFixedPlan;

import java.util.List;

public class AdapterAddonPlan extends BaseAdapter {
    private Context context;
    private List<PlanService> addressList;
    ActivityPlan planActivity;
    FragmentFixedPlan unlimitedPlanFragment;


    public AdapterAddonPlan(Context context, List<PlanService> addressList) {
        this.context = context;
        this.addressList = addressList;
        this.unlimitedPlanFragment=unlimitedPlanFragment;
        this.planActivity=planActivity;
    }

    @Override
    public int getCount() {
        return addressList.size();
    }

    @Override
    public Object getItem(int position) {
        return addressList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_fixed_plan, null);
            holder = new ViewHolder();
            holder.txtPlanName=(TextView) convertView.findViewById(R.id.txt_plan_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final PlanService addressModel=addressList.get(position);
        holder.txtPlanName.setText(addressModel.getServiceGroupName());

        return convertView;
    }

    class ViewHolder {
        private TextView txtPlanName;


    }
}
