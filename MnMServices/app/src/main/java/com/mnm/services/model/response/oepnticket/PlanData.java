package com.mnm.services.model.response.oepnticket;

import java.io.Serializable;
import java.util.List;

/**
 * Created by saten on 4/22/17.
 */

public class PlanData implements Serializable {

    private List<Plan> Plans = null;

    public List<Plan> getPlans() {
        return Plans;
    }

    public void setPlans(List<Plan> plans) {
        Plans = plans;
    }
}
