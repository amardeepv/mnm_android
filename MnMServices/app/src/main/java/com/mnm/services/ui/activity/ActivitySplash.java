package com.mnm.services.ui.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.StringUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.controller.ControllerServiceByCity;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.utils.GlobalUtils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.mnm.services.constants.Constant.FOUR_DAYS;
import static com.mnm.services.constants.Constant.categoryFileName;

public class ActivitySplash extends MyDrawerBaseActivity {
    private static int SPLASH_TIME_OUT = 3000;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Log.e("jsonData ",GlobalUtils.getData().toString());
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }


        progressBar = (ProgressBar) findViewById(R.id.progrees_bar);
        long lastUpdateTime = AppSharedPreference.getLong(PrefConstants.CATEGORY_DATA_UPDATE_TIME, 0, this);
        long lastUpdateDifference = System.currentTimeMillis() - lastUpdateTime;
        if (lastUpdateTime != 0 && lastUpdateDifference < FOUR_DAYS && !StringUtils.isNullOrEmpty(GlobalUtils.getJsonData(ActivitySplash.this, categoryFileName))) {
            launchSplash();
        } else {
            serviceCategoryApi();
        }
    }

    void serviceCategoryApi() {
        if (!ConnectivityUtils.isNetworkEnabled(ActivitySplash.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
            finish();
        } else {

            ControllerServiceByCity loginController = new ControllerServiceByCity(this, this,AppSharedPreference.getString(PrefConstants.CITY_NAME,"Gurugram",ActivitySplash.this));
            progressBar.setVisibility(View.VISIBLE);
            loginController.getData(AppSettings.SERVICE_CATEGORY_REQUEST, "");
        }
    }

    private void launchSplash() {
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                moveToScreen();
            }
        }, SPLASH_TIME_OUT);
    }


    private void moveToScreen() {
        AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivitySplash.this);
        if (AppSharedPreference.getBoolean(PrefConstants.IS_GET_STARTED_CLICKED, false, ActivitySplash.this)) {
            ActivitySplash.this.startActivity(new Intent(ActivitySplash.this, ActivityHome.class));
            finish();
        } else {
            ActivitySplash.this.startActivity(new Intent(ActivitySplash.this, ActivityViewPager.class));
            finish();
        }
    }

    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        ActivitySplash.this.runOnUiThread(new Runnable() {
            public void run() {
                progressBar.setVisibility(View.GONE);
                Log.e("Response", response.toString());
                if (response == null) {
                    ToastUtils.showToast(ActivitySplash.this, getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.SERVICE_CATEGORY_REQUEST:
                            moveToScreen();
                            break;
                        default:
                            break;
                    }
                }
            }
        });


    }
}
