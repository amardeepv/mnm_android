package com.mnm.services.ui.activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import com.android.volley.Response;
import com.kelltontech.ui.IScreen;

/**
 * @author satendra.singh
 */
public class DrawerBaseActivity extends AppCompatActivity implements IScreen {
    ProgressDialog mProgressDialog=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

    }

    public void showProgressDialog(String bodyText) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(DrawerBaseActivity.this);
            mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setOnKeyListener(new Dialog.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    return keyCode == KeyEvent.KEYCODE_CAMERA || keyCode == KeyEvent.KEYCODE_SEARCH;
                }
            });
        }

        mProgressDialog.setMessage(bodyText);
        //play store crash MOb-612
        if (!this.isFinishing()) {
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        }


    }

    public void removeProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    protected void updateUi(Response response) {
        if (isFinishing()) {
            return;
        }
        try {
            updateUi(response);
        } catch (Exception e) {
            Log.i(getClass().getSimpleName(), "updateUi()", e);
        }
    }

    @Override
    public void handleUiUpdate(com.kelltontech.network.Response response) {
    }
}
