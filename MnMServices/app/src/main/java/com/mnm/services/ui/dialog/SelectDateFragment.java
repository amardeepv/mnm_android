package com.mnm.services.ui.dialog;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.mnm.services.listner.OnDateChangedListener;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by satendra.sigh on 05-05-2015.
 */
public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

OnDateChangedListener onDateChangedListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, yy, mm, dd);
    }

    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        //populateSetDate(yy, mm+1, dd);
        try {
            populateSetDate(yy, mm, dd);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
    public void populateSetDate(int year, int month, int day) throws ParseException {
        onDateChangedListener.onDateChanged(month+1,day,year);
       // dob.setText(month+"/"+day+"/"+year);
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onDateChangedListener = (OnDateChangedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement ToolbarListener");
        }
    }
}