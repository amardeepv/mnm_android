package com.mnm.services.ui.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.kelltontech.ui.IScreen;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.StringUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.Constant;
import com.mnm.services.controller.ControllerLogin;
import com.mnm.services.controller.ControllerSocialLogin;
import com.mnm.services.google.GooglePlusUtils;
import com.mnm.services.google.IPlusClient;
import com.mnm.services.model.request.LoginRequest;
import com.mnm.services.model.request.SocialloginRequest;
import com.mnm.services.model.response.LoginResponse;
import com.mnm.services.model.response.SocialLoginResponse;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.activity.ActivityConfirmMobile;
import com.mnm.services.ui.activity.ActivityHome;
import com.mnm.services.ui.activity.ActivityLogin;
import com.mnm.services.ui.activity.ActivityNeedHelp;
import com.mnm.services.ui.activity.ActivityVerifyMobile;
import com.mnm.services.utils.GlobalUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import static com.mnm.services.constants.Constant.MOBILE_NUMBER;
import static com.mnm.services.constants.Constant.USER_ID;

public class FragmentLogin extends FragmentBase implements View.OnClickListener, IScreen {

    private EditText edtuserMobile, edtUserPassword;
    private ProgressBar progreesBar;
    View rootview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fragment_login, container, false);

        edtuserMobile = (EditText) rootview.findViewById(R.id.user_mobile);
        edtUserPassword = (EditText) rootview.findViewById(R.id.user_password);
        rootview.findViewById(R.id.lbl_help).setOnClickListener(this);
        rootview.findViewById(R.id.btn_signin).setOnClickListener(this);
        rootview.findViewById(R.id.btn_signup).setOnClickListener(this);
        rootview.findViewById(R.id.root_relay).setOnClickListener(this);
        rootview.findViewById(R.id.btn_facebook).setOnClickListener(this);
        rootview.findViewById(R.id.btn_google_plus).setOnClickListener(this);
        progreesBar = (ProgressBar) rootview.findViewById(R.id.progrees_bar);
        progreesBar.setVisibility(View.GONE);
        return rootview;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signin:
                if (validation()) {
                    loginApi();
                }
                break;
            case R.id.btn_signup:
                Intent mIntent = new Intent(getActivity(), ActivityLogin.class);
                mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER, getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER, false));
                startActivity(mIntent);
                getActivity().finish();
                break;
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(getActivity(), rootview);
                break;
            case R.id.btn_facebook:
                //fbLogin();
                ((ActivityLogin) getContext()).fbLogin();
                break;
            case R.id.btn_google_plus:
                ((ActivityLogin) getContext()).googleLogin();
                break;
            case R.id.lbl_help:
                Intent mItntent = new Intent(getActivity(), ActivityConfirmMobile.class);
                mItntent.putExtra(Constant.IS_COMING_FROM_CREATE_PASSWORD, true);
                startActivity(mItntent);
            default:
                break;
        }
    }

    public void loginApi() {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            ControllerLogin loginController = new ControllerLogin(getActivity(), this);
            LoginRequest loginRequest = getLoginRequest();
            showProgressDialog("Please wait..");
            loginController.getData(AppSettings.LOGIN_REQUEST, loginRequest);
        }
    }

    public void socialLoginApi(String provider, String socialId, String firstName, String accessTokenn, String email, String mUserProfilePic) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            ControllerSocialLogin loginController = new ControllerSocialLogin(getActivity(), this);
            SocialloginRequest loginRequest = getSocialLoginRequest(provider, socialId, firstName, accessTokenn, email);
            showProgressDialog("Please wait...");
            loginController.getData(AppSettings.SOCIAL_LOGIN_REQUEST, loginRequest);
        }
    }

    private LoginRequest getLoginRequest() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setPassword(edtUserPassword.getText().toString());
        loginRequest.setUserName(edtuserMobile.getText().toString());
        return loginRequest;
    }

    private SocialloginRequest getSocialLoginRequest(String provider, String socialId, String firstName, String accessTokenn, String email) {
        SocialloginRequest loginRequest = new SocialloginRequest();
        loginRequest.setProvider(provider);
        loginRequest.setSocialId(socialId);
        loginRequest.setFirstName(firstName);
        loginRequest.setAccessToken(accessTokenn);
        loginRequest.setEmail(email);
        return loginRequest;
    }

    @Override
    public void updateUi(final com.kelltontech.network.Response response) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                removeProgressDialog();
                Log.e("Response", response.toString());
                if (response == null) {
                    ToastUtils.showToast(getActivity(), getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.LOGIN_REQUEST: {
                            LoginResponse loginResponse = (LoginResponse) response.getResponseObject();
                            if (!loginResponse.isHasError() && loginResponse.getData() != null) {
                                if(loginResponse.getData().getAccessToken()!=null)
                                    GlobalUtils.saveAccessToken(loginResponse.getData().getAccessToken().getToken(),getActivity());
                                if(loginResponse.getData().getAccess()!=null)
                                    GlobalUtils.saveAccessToken(loginResponse.getData().getAccess().getToken(),getActivity());
                                AppSharedPreference.putString(PrefConstants.USER_ID, "" + loginResponse.getData().getUserId(), getActivity());
                                AppSharedPreference.putString(PrefConstants.USER_NAME, "" + loginResponse.getData().getName(), getActivity());
                                if(loginResponse.getMessageCode().equalsIgnoreCase("ACT20")){
                                    Intent mIntent=new Intent(getActivity(),ActivityConfirmMobile.class);
                                    mIntent.putExtra(Constant.IS_COMING_FROM_CREATE_PASSWORD,true);
                                    mIntent.putExtra(Constant.IS_OLD_USER,true);
                                    startActivity(mIntent);
                                }
                               else if (!StringUtils.isNullOrEmpty(loginResponse.getData().getStatus())) {
                                    if (loginResponse.getData().getStatus().equalsIgnoreCase("1")) {
                                        AppSharedPreference.putBoolean(PrefConstants.IS_LOGIN_TRUE, true, getActivity());
                                        if (getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER, false)) {
                                            Intent mIntent = new Intent(getActivity(), ActivityHome.class);
                                            mIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                            mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(mIntent);
                                        }
                                        getActivity().finish();
                                    } else if (loginResponse.getData().getStatus().equalsIgnoreCase("2")) {
                                        Intent mIntent = new Intent(getActivity(), ActivityConfirmMobile.class);
                                        mIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                        mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                        startActivity(mIntent);
                                        getActivity().finish();
                                    } else if (loginResponse.getData().getStatus().equalsIgnoreCase("3")) {
                                        Intent mIntent = new Intent(getActivity(), ActivityVerifyMobile.class);
                                        mIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                        mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                        startActivity(mIntent);
                                        getActivity().finish();
                                    }
                                } else if (!StringUtils.isNullOrEmpty(loginResponse.getData().getAccontStatus())) {
                                    if (loginResponse.getData().getAccontStatus().equalsIgnoreCase("1")) {
                                        AppSharedPreference.putBoolean(PrefConstants.IS_LOGIN_TRUE, true, getActivity());
                                        if (getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER, false)) {
                                            Intent mIntent = new Intent(getActivity(), ActivityHome.class);
                                            mIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                            mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(mIntent);
                                        }
                                        getActivity().finish();
                                    } else if (loginResponse.getData().getAccontStatus().equalsIgnoreCase("2")) {
                                        Intent mIntent = new Intent(getActivity(), ActivityConfirmMobile.class);
                                        mIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                        mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                        startActivity(mIntent);
                                        getActivity().finish();
                                    } else if (loginResponse.getData().getAccontStatus().equalsIgnoreCase("3")) {
                                        Intent mIntent = new Intent(getActivity(), ActivityVerifyMobile.class);
                                        mIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                        mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                        startActivity(mIntent);
                                        getActivity().finish();
                                    }
                                } else if (!StringUtils.isNullOrEmpty(loginResponse.getData().getAccountStatus())) {
                                    if (loginResponse.getData().getAccountStatus().equalsIgnoreCase("1")) {
                                        AppSharedPreference.putBoolean(PrefConstants.IS_LOGIN_TRUE, true, getActivity());
                                        if (getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER, false)) {
                                            Intent mIntent = new Intent(getActivity(), ActivityHome.class);
                                            mIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                            mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(mIntent);
                                        }
                                        getActivity().finish();
                                    } else if (loginResponse.getData().getAccountStatus().equalsIgnoreCase("2")) {
                                        Intent mIntent = new Intent(getActivity(), ActivityConfirmMobile.class);
                                        mIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                        mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                        startActivity(mIntent);
                                        getActivity().finish();
                                    } else if (loginResponse.getData().getAccountStatus().equalsIgnoreCase("3")) {
                                        Intent mIntent = new Intent(getActivity(), ActivityVerifyMobile.class);
                                        mIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                        mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                        startActivity(mIntent);
                                        getActivity().finish();
                                    }
                                }

                            } else {
                                ToastUtils.showToast(getActivity(), "" + loginResponse.getMessage());
                            }
                        }
                        break;
                        case AppSettings.SOCIAL_LOGIN_REQUEST: {
                            SocialLoginResponse loginResponse = (SocialLoginResponse) response.getResponseObject();
                            if (!loginResponse.getHasError() && loginResponse.getData() != null) {
                                if(loginResponse.getData().getAccessToken()!=null)
                                    GlobalUtils.saveAccessToken(loginResponse.getData().getAccessToken().getToken(),getActivity());
                                if(loginResponse.getData().getAccess()!=null)
                                    GlobalUtils.saveAccessToken(loginResponse.getData().getAccess().getToken(),getActivity());
                                AppSharedPreference.putString(PrefConstants.USER_ID, "" + loginResponse.getData().getUserId(), getActivity());
                                AppSharedPreference.putString(PrefConstants.USER_NAME, "" + loginResponse.getData().getName(), getActivity());
                                if(loginResponse.getMessageCode().equalsIgnoreCase("ACT20")){
                                    Intent mIntent=new Intent(getActivity(),ActivityConfirmMobile.class);
                                    mIntent.putExtra(Constant.IS_COMING_FROM_CREATE_PASSWORD,true);
                                    startActivity(mIntent);
                                }
                               else if (!StringUtils.isNullOrEmpty(loginResponse.getData().getStatus())) {
                                    if (loginResponse.getData().getStatus().equalsIgnoreCase("2")) {
                                        Intent mConfirmIntent = new Intent(getActivity(), ActivityConfirmMobile.class);
                                        mConfirmIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                        startActivity(mConfirmIntent);
                                        getActivity().finish();
                                    } else if (loginResponse.getData().getStatus().equalsIgnoreCase("3")) {
                                        Intent mConfirmIntent = new Intent(getActivity(), ActivityVerifyMobile.class);
                                        mConfirmIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                        startActivity(mConfirmIntent);
                                        getActivity().finish();

                                    } else if (loginResponse.getData().getStatus().equalsIgnoreCase("4")) {
                                        AppSharedPreference.putBoolean(PrefConstants.IS_LOGIN_TRUE, true, getActivity());
                                        if (getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER, false)) {
                                            Intent mIntent = new Intent(getActivity(), ActivityHome.class);
                                            mIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                            mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                                            ;
                                            startActivity(mIntent);
                                        }
                                        getActivity().finish();
                                    }
                                } else if (!StringUtils.isNullOrEmpty(loginResponse.getData().getAccontStatus())) {
                                    if (loginResponse.getData().getAccontStatus().equalsIgnoreCase("2")) {
                                        Intent mConfirmIntent = new Intent(getActivity(), ActivityConfirmMobile.class);
                                        mConfirmIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                        startActivity(mConfirmIntent);
                                        getActivity().finish();
                                    } else if (loginResponse.getData().getAccontStatus().equalsIgnoreCase("3")) {
                                        Intent mConfirmIntent = new Intent(getActivity(), ActivityVerifyMobile.class);
                                        mConfirmIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                        startActivity(mConfirmIntent);
                                        getActivity().finish();

                                    } else if (loginResponse.getData().getAccontStatus().equalsIgnoreCase("4")) {
                                        AppSharedPreference.putBoolean(PrefConstants.IS_LOGIN_TRUE, true, getActivity());
                                        if (getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER, false)) {
                                            Intent mIntent = new Intent(getActivity(), ActivityHome.class);
                                            mIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                            mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(mIntent);
                                        }
                                        getActivity().finish();

                                    }
                                } else if (!StringUtils.isNullOrEmpty(loginResponse.getData().getAccountStatus())) {
                                    if (loginResponse.getData().getAccountStatus().equalsIgnoreCase("2")) {
                                        Intent mConfirmIntent = new Intent(getActivity(), ActivityConfirmMobile.class);
                                        mConfirmIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                        startActivity(mConfirmIntent);
                                        getActivity().finish();
                                    } else if (loginResponse.getData().getAccountStatus().equalsIgnoreCase("3")) {
                                        Intent mConfirmIntent = new Intent(getActivity(), ActivityVerifyMobile.class);
                                        mConfirmIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                        startActivity(mConfirmIntent);
                                        getActivity().finish();

                                    } else if (loginResponse.getData().getAccountStatus().equalsIgnoreCase("4")) {
                                        AppSharedPreference.putBoolean(PrefConstants.IS_LOGIN_TRUE, true, getActivity());
                                        if (getActivity().getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER, false)) {
                                            Intent mIntent = new Intent(getActivity(), ActivityHome.class);
                                            mIntent.putExtra(USER_ID, "" + loginResponse.getData().getUserId());
                                            mIntent.putExtra(MOBILE_NUMBER, "" + edtuserMobile.getText().toString());
                                            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(mIntent);
                                        }
                                        getActivity().finish();

                                    }
                                } else {
                                    ToastUtils.showToast(getActivity(), "" + loginResponse.getMessage());
                                }
                            } else {
                                ToastUtils.showToast(getActivity(), "" + loginResponse.getMessage());
                            }
                        }
                        break;

                        default:
                            break;
                    }

                }
            }
        });
    }

    boolean validation() {
        if (edtuserMobile.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Please enter mobile number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (edtuserMobile.getText().toString().length() <= 9) {
            Toast.makeText(getActivity(), "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (edtUserPassword.getText().toString().length() <= 0) {
            Toast.makeText(getActivity(), "Please enter password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (edtUserPassword.getText().toString().length()<= 5) {
            Toast.makeText(getActivity(), "Your password must be atleast 6 characters long", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;


    }
}