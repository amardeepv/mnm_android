package com.mnm.services.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mnm.services.R;
import com.mnm.services.constants.Constant;
import com.mnm.services.model.response.ServiceModel;
import com.mnm.services.model.response.plan.PlanService;
import com.mnm.services.ui.adapters.AdapterOrderPlan;
import com.mnm.services.ui.adapters.AdapterOrderService;
import com.mnm.services.ui.adapters.AdapterPlan;
import com.mnm.services.ui.widgets.NonScrollListView;
import com.mnm.services.utils.GlobalUtils;

import org.w3c.dom.Text;

import java.util.ArrayList;


public class ActivityOrderConfirmation extends MyDrawerBaseActivity implements View.OnClickListener {
    //private ArrayList<ServiceModel> serviceList;
    private TextView txtInvoiceNumber;
    private TextView txtMainServicename;
    private TextView txtTotalAmountWithTax;
    private TextView txtPaymentMethod;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_confirmation);
        initCustomActionBarView(ActivityOrderConfirmation.this,"Order Confirmation",true);
        txtInvoiceNumber=(TextView)findViewById(R.id.txt_invoice_number);
        txtMainServicename=(TextView)findViewById(R.id.main_service_name);
        txtTotalAmountWithTax=(TextView)findViewById(R.id.total_amount_with_tax);
        txtPaymentMethod=(TextView)findViewById(R.id.txt_payment_type) ;
        NonScrollListView nonScrollListView=(NonScrollListView)findViewById(R.id.lisview_non_scroll);
        txtInvoiceNumber.setText(getIntent().getStringExtra(Constant.INVOICE_NUMBER));
        txtMainServicename.setText(getIntent().getStringExtra(Constant.SERVICE_NAME));
        txtTotalAmountWithTax.setText("Total Amount (Including Service Tax)-:  "+getString(R.string.rupees_symbol)+""+getIntent().getStringExtra(Constant.TOTAL_BILL_AFTER_AMOUNT));
        txtPaymentMethod.setText("Payment Type-:  COD(cash on delivery)");
        //  mIntent.putExtra(Constant.INVOICE_NUMBER,
        // LinearLayout planeHeader=(LinearLayout)findViewById(R.id.services_relay);
        RelativeLayout serviceHeader=(RelativeLayout)findViewById(R.id.service_header);
        LinearLayout planHeader=(LinearLayout)findViewById(R.id.plan_header);
        if(getIntent().getBooleanExtra(Constant.IS_PLAN,false)){
            serviceHeader.setVisibility(View.GONE);
            planHeader.setVisibility(View.VISIBLE);
            ArrayList<PlanService> serviceList= (ArrayList<PlanService>) getIntent().getSerializableExtra(Constant.SERVICE_SELECTED_DATA);
            AdapterPlan orderServiceAdapter=new AdapterPlan(ActivityOrderConfirmation.this,serviceList,getIntent().getBooleanExtra(Constant.IS_FIXED_PLAN,false));
            nonScrollListView.setAdapter(orderServiceAdapter);
        }else{
            serviceHeader.setVisibility(View.VISIBLE);
            planHeader.setVisibility(View.GONE);
            ArrayList<ServiceModel> serviceList= (ArrayList<ServiceModel>) getIntent().getSerializableExtra(Constant.SERVICE_SELECTED_DATA);
            AdapterOrderService orderServiceAdapter=new AdapterOrderService(ActivityOrderConfirmation.this,serviceList);
            nonScrollListView.setAdapter(orderServiceAdapter);
        }



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i = new Intent(ActivityOrderConfirmation.this, ActivityHome.class);
// set the new task and clear flags
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityOrderConfirmation.this, v);
                break;
            default:
                break;
        }
    }
}