package com.mnm.services.ui.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.controller.ControllerWalletHistory;
import com.mnm.services.model.response.mywallet.WalletResponse;
import com.mnm.services.ui.adapters.AdapterWalletHistory;
import com.mnm.services.ui.widgets.NonScrollListView;
import com.mnm.services.utils.GlobalUtils;


public class ActivityMyWallet extends MyDrawerBaseActivity implements View.OnClickListener {
    private NonScrollListView listviewWalletHistory;
    private TextView txtWalletPrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);
        findViewById(R.id.root_relay).setOnClickListener(this);
        initCustomActionBarView(ActivityMyWallet.this,"My Wallet",true);
        listviewWalletHistory=(NonScrollListView)findViewById(R.id.listview_wallet_history);
        txtWalletPrice=(TextView)findViewById(R.id.txt_wallet_price);
        walletHistoryApi();
    }

    private void  walletHistoryApi(){
        if (!ConnectivityUtils.isNetworkEnabled(ActivityMyWallet.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerWalletHistory controllerWalletHistory = new ControllerWalletHistory(this, this);
            showProgressDialog("Please wait...");
            controllerWalletHistory.getData(AppSettings.WALLET_HISTORY_EVENT, null);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityMyWallet.this, v);
                break;
        }
    }


    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        removeProgressDialog();
        ActivityMyWallet.this.runOnUiThread(new Runnable() {
            public void run() {
                Log.e("Response", response.toString());
                if (response == null) {
                    ToastUtils.showToast(ActivityMyWallet.this, getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.WALLET_HISTORY_EVENT: {
                            WalletResponse walletResponse = (WalletResponse) response.getResponseObject();
                            if (!walletResponse.getHasError()) {
                                txtWalletPrice.setText(""+walletResponse.getData().getWalletAmount());
                                if(walletResponse.getData().getWalletHistory().size()>0){
                                    AdapterWalletHistory adapterWalletHistory=new AdapterWalletHistory(ActivityMyWallet.this,walletResponse.getData().getWalletHistory());
                                    listviewWalletHistory.setAdapter(adapterWalletHistory);
                                }
                            } else {
                                ToastUtils.showToast(ActivityMyWallet.this, "" + walletResponse.getMessages());
                            }
                        }
                        break;

                        default:
                            break;
                    }

                }
            }
        });
    }
}