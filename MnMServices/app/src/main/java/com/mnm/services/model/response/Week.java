package com.mnm.services.model.response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bluepi on 15/3/17.
 */

public class Week {
    public Week(Integer sequence){
        this.sequence = sequence;
        this.days = new ArrayList<>();
    }

    private Day activeDay;

    private Boolean active = Boolean.FALSE;

    public Day getActiveDay() {
        return activeDay;
    }

    public void setActiveDay(Day activeDay) {
        this.activeDay = activeDay;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    private Integer sequence;

    private List<Day> days;

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public List<Day> getDays() {
        return days;
    }

    public void setDays(List<Day> days) {
        this.days = days;
    }

    public void addDay(Day day){
        this.days.add(day);
    }

    public void toggleActive(Day day){
        active = !active;
        if(active){
            activeDay = day;
        }
        else{
            activeDay = null;
        }
    }

    @Override
    public String toString() {
        return "Week{" +
                "sequence=" + sequence +
                ", days=" + days +
                '}';
    }
}
