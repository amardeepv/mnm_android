package com.mnm.services.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kelltontech.utils.StringUtils;
import com.mnm.services.R;
import com.mnm.services.constants.Constant;
import com.mnm.services.model.response.ServiceCategoryModel;
import com.mnm.services.model.response.ServiceModel;
import com.mnm.services.model.response.oepnticket.ParentService;
import com.mnm.services.model.response.profile.PlanIdServiceid;
import com.mnm.services.ui.activity.ActivityServiceGroup;
import com.mnm.services.ui.adapters.AdapterBannerService;
import com.mnm.services.ui.widgets.CirclePageIndicator;
import com.mnm.services.ui.widgets.HelpWebViewClient;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static com.mnm.services.constants.Constant.AC_SERVICE;
import static com.mnm.services.constants.Constant.CARPENTRY_SERVICE;
import static com.mnm.services.constants.Constant.CAR_SPA_SERVICE;
import static com.mnm.services.constants.Constant.CCTV_SERVICE;
import static com.mnm.services.constants.Constant.ELECTRICAL_SERVICE;
import static com.mnm.services.constants.Constant.GARDENING_SERVICE;
import static com.mnm.services.constants.Constant.HOME_APPLIANCE_SERVICE;
import static com.mnm.services.constants.Constant.HOME_CLEANING_SERVICE;
import static com.mnm.services.constants.Constant.INTERIER_DESIGN_SERVICE;
import static com.mnm.services.constants.Constant.PAINT_SERVICE;
import static com.mnm.services.constants.Constant.PEST_CONTROL_SERVICE;
import static com.mnm.services.constants.Constant.PLUMBING_SERVICE;
import static com.mnm.services.constants.Constant.ROO_SERVICE;
import static com.mnm.services.constants.Constant.SECURITY_SERVICE;
import static com.mnm.services.constants.Constant.UPHOLESTRY_SERVICE;
import static com.mnm.services.constants.Constant.WATER_TANK_SERVICE;


public class FragmentServiceDescription extends Fragment implements View.OnClickListener {
    private LinearLayoutManager mLayoutManager;
    private ServiceCategoryModel categoryModel;
    private ViewPager bannerViewPager;
    private CirclePageIndicator pagerIndicater;
    private TextView txtDescription;
    private RelativeLayout offerRelay;
    private WebView videoView;
    private ImageView imgService;
    private ProgressBar pgBar;
    private TextView lblVideoNotFound;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_service_description, container, false);
        bannerViewPager = (ViewPager) rootview.findViewById(R.id.mpager);
        pagerIndicater = (CirclePageIndicator) rootview.findViewById(R.id.pager_indicater);
        offerRelay = (RelativeLayout) rootview.findViewById(R.id.offer_relay);
        txtDescription = (TextView) rootview.findViewById(R.id.txt_description);
        imgService = (ImageView) rootview.findViewById(R.id.img_service);
        videoView = (WebView) rootview.findViewById(R.id.video_view);
        lblVideoNotFound = (TextView) rootview.findViewById(R.id.lbl_video_not_found);
        pgBar = (ProgressBar) rootview.findViewById(R.id.progress_bar);
        rootview.findViewById(R.id.btn_start_booking).setOnClickListener(this);
        categoryModel = (ServiceCategoryModel) getActivity().getIntent().getSerializableExtra(Constant.CATEGORY_MODEL_DATA);
        AdapterBannerService bannerAdapterService = new AdapterBannerService(getActivity());
        bannerViewPager.setAdapter(bannerAdapterService);
        pagerIndicater.setRadius(7);
        pagerIndicater.setViewPager(bannerViewPager);
        pageSwitcher(3);
        if (!StringUtils.isNullOrEmpty(categoryModel.getDescription()))
            txtDescription.setText(Html.fromHtml(categoryModel.getDescription()));
        if (categoryModel.getShowOffer()) {
            offerRelay.setVisibility(View.VISIBLE);
        } else {
            offerRelay.setVisibility(View.GONE);
        }

        setServiceImage(categoryModel);
        if (!StringUtils.isNullOrEmpty(categoryModel.getVideoUrl())) {
            lblVideoNotFound.setVisibility(View.GONE);
            HelpWebViewClient mWebViewClient = new HelpWebViewClient(pgBar);
            videoView.setWebViewClient(mWebViewClient);
            videoView.getSettings().setJavaScriptEnabled(true);

            videoView.loadUrl(categoryModel.getVideoUrl());
        } else {
            lblVideoNotFound.setVisibility(View.VISIBLE);
        }

        return rootview;
    }

    void setServiceImage(ServiceCategoryModel obj) {

        if (obj.getServiceGroupId() == ELECTRICAL_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.electric_service));
        } else if (obj.getServiceGroupId() == PLUMBING_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.plumbing));
        } else if (obj.getServiceGroupId() == CARPENTRY_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.carpentry_service));
        } else if (obj.getServiceGroupId() == PEST_CONTROL_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.pestcontrol));
        } else if (obj.getServiceGroupId() == AC_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.ac_service));
        } else if (obj.getServiceGroupId() == WATER_TANK_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.watertank));
        } else if (obj.getServiceGroupId() == GARDENING_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.gardening_service));
        } else if (obj.getServiceGroupId() == CCTV_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.cctv));
        } else if (obj.getServiceGroupId() == ROO_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.ro));
        } else if (obj.getServiceGroupId() == HOME_CLEANING_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.homecleaning));
        } else if (obj.getServiceGroupId() == HOME_APPLIANCE_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.home_appliances));
        } else if (obj.getServiceGroupId() == PAINT_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.paint_service));
        } else if (obj.getServiceGroupId() == CAR_SPA_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.carwash));
        } else if (obj.getServiceGroupId() == UPHOLESTRY_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.sofa_cleaning));
        } else if (obj.getServiceGroupId() == INTERIER_DESIGN_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.interiordesign));
        } else if (obj.getServiceGroupId() == SECURITY_SERVICE) {
            imgService.setImageDrawable(getResources().getDrawable(R.drawable.security));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_start_booking:
                Intent mIntent = new Intent(getActivity(), ActivityServiceGroup.class);
                mIntent.putExtra(Constant.CATEGORY_MODEL_DATA, categoryModel);
                mIntent.putExtra(Constant.IS_OPEN_TICKET_TRUE, false);
                mIntent.putExtra(Constant.SINGLE_ADDRESS_OPEN_TICKET_DATA, (ArrayList<PlanIdServiceid>)getActivity().getIntent().getSerializableExtra(Constant.SINGLE_ADDRESS_OPEN_TICKET_DATA));
                startActivity(mIntent);
                break;
        }

    }

    Timer timer;
    int page = 1;

    public void pageSwitcher(int seconds) {
        try {
            timer = new Timer();
            timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000); // delay
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    // this is an inner class...
    class RemindTask extends TimerTask {
        @Override
        public void run() {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {

                        if (page >= 3) {
                            page = 0;// In my case the number of pages are 5
                            //timer.cancel();
                            // Showing a toast for just testing purpose
                       /* Toast.makeText(getApplicationContext(), "Timer stoped",
                                Toast.LENGTH_LONG).show();*/
                        } else {
                            bannerViewPager.setCurrentItem(page++);
                        }
                    }
                });

            }
        }
    }


    @Override
    public void onDestroy() {
        this.videoView.clearHistory();
        this.videoView.clearCache(true);
        this.videoView.loadUrl("about:blank");
        this.videoView.freeMemory();
        super.onDestroy();
    }
}



