package com.mnm.services.model.response;

/**
 * Created by saten on 6/17/17.
 */

public class Geometry {
    LocationM location;

    public LocationM getLocation() {
        return location;
    }

    public void setLocation(LocationM location) {
        this.location = location;
    }
}
