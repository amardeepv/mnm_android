package com.mnm.services.model.response.oepnticket;

import java.io.Serializable;
import java.util.List;

/**
 * Created by saten on 4/22/17.
 */

public class Plan implements Serializable {

    private Integer planId;

    private String planName;

    private String created;

    private String expirydate;

    private String planType;

    private Integer addressId;

    private Object address;

    private List<ParentService> serviceList = null;

    public Integer getPlanId() {
        return planId;
    }

    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public Object getAddress() {
        return address;
    }

    public void setAddress(Object address) {
        this.address = address;
    }

    public List<ParentService> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<ParentService> serviceList) {
        this.serviceList = serviceList;
    }
}