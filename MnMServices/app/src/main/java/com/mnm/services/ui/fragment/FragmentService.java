package com.mnm.services.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.kelltontech.ui.IScreen;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.Constant;
import com.mnm.services.controller.ControllerService;
import com.mnm.services.model.response.ServiceCategoryModel;
import com.mnm.services.model.response.ServiceModel;
import com.mnm.services.model.response.ServiceResponse;
import com.mnm.services.model.response.oepnticket.ChildService;
import com.mnm.services.model.response.oepnticket.ParentService;
import com.mnm.services.model.response.profile.PlanIdServiceid;
import com.mnm.services.ui.activity.ActivityServiceGroup;
import com.mnm.services.ui.adapters.AdapterService;

import java.util.ArrayList;
import java.util.List;


public class FragmentService extends Fragment  implements View.OnClickListener,IScreen{
    private LinearLayoutManager mLayoutManager;
    private ServiceCategoryModel categoryModel;
    private RecyclerView listViewService;
    private AdapterService serviceAdapter;
    private ProgressBar progressBar;
    private TextView lblNoDataFound;
    List<ChildService> childServices = null;
    String serviceTag="";
    ArrayList<PlanIdServiceid> alreadyPurchaseOnSingleAddress;
    String singleAddressId;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview =  inflater.inflate(R.layout.fragment_service, container, false);
        listViewService=(RecyclerView)rootview.findViewById(R.id.list_view_service);
        listViewService.setLayoutManager(new LinearLayoutManager(getContext()));
        progressBar=(ProgressBar)rootview.findViewById(R.id.progrees_bar);
        Bundle bundle = this.getArguments();
        categoryModel =(ServiceCategoryModel) bundle.getSerializable(Constant.SERVICE_MODEL_DATA);
        if(bundle.getBoolean(Constant.IS_OPEN_TICKET_TRUE)){
            ParentService parentService=(ParentService)bundle.getSerializable(Constant.OPEN_TICKET_SERVICE_DATA);
            childServices=parentService.getServices();
        }
        serviceTag=bundle.getString(Constant.SERVICE_GRP_NAME)+"->"+categoryModel.getServiceGroupName();
        lblNoDataFound=(TextView)rootview.findViewById(R.id.lbl_no_data_found);
        lblNoDataFound.setVisibility(View.GONE);
        alreadyPurchaseOnSingleAddress=(ArrayList<PlanIdServiceid>)getActivity().getIntent().getSerializableExtra(Constant.SINGLE_ADDRESS_OPEN_TICKET_DATA);
        serviceApi();
        return rootview;
    }

    void serviceApi() {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            //2&group=6 http://103.25.131.238/mnmapi/Services?category=2&group=6 where is category and group
            int parentId,serviceGrpId;
            if(categoryModel.getServiceGroupName().equalsIgnoreCase("RO")){
                parentId=categoryModel.getServiceGroupId();
                serviceGrpId=categoryModel.getParentID();
            }else{
                parentId=categoryModel.getParentID();
                serviceGrpId=categoryModel.getServiceGroupId();
            }
            ControllerService loginController = new ControllerService(getActivity(), this,parentId,serviceGrpId);
            progressBar.setVisibility(View.VISIBLE);
            loginController.getData(AppSettings.SERVICE_REQUEST, "");
        }
    }
    @Override
    public void onClick(View view) {

    }

    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Log.e("Response", response.toString());
                progressBar.setVisibility(View.GONE);
                if (response == null) {
                    ToastUtils.showToast(getActivity(), getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.SERVICE_REQUEST:
                            ServiceResponse serviceResponse = (ServiceResponse) response.getResponseObject();
                            if(!serviceResponse.getHasError()) {
                                if(serviceResponse.getData().size()>0) {
                                    lblNoDataFound.setVisibility(View.GONE);
                                    listViewService.setVisibility(View.VISIBLE);
                                    ((ActivityServiceGroup)getActivity()).selectedServiceList.addAll(serviceResponse.getData());
                                    setAdapterData(serviceResponse.getData());
                                }else{
                                    lblNoDataFound.setVisibility(View.VISIBLE);
                                    listViewService.setVisibility(View.GONE);
                                }
                            }else{

                            }
                            break;
                        default:
                            break;
                    }
                }}
        });
    }
    private void setAdapterData(List<ServiceModel> servicedata) {
        if(childServices!=null&&childServices.size()>0){
            for (ChildService childService:childServices){
                for(ServiceModel serviceModel:servicedata){
                    if(childService.getServiceId().equalsIgnoreCase(""+serviceModel.getServiceid())){
                        serviceModel.setRate(0);
                    }
                }
            }
        }
     if(alreadyPurchaseOnSingleAddress!=null&&alreadyPurchaseOnSingleAddress.size()>0){
         for (PlanIdServiceid planIdServiceid:alreadyPurchaseOnSingleAddress){
           for(ServiceModel serviceModel:servicedata){
            if(planIdServiceid.getServiceId()==serviceModel.getServiceid()){
                serviceModel.setSingleAddresrate(true);
            }
        }
    }
}
        serviceAdapter = new AdapterService(getActivity(), servicedata, listViewService, FragmentService.this,serviceTag);
        listViewService.setAdapter(serviceAdapter);

    }
  public void addAndRemove(ServiceModel mList){
        ((ActivityServiceGroup)getActivity()).addAndRemoveService(mList);
    }
    public String getServiceTag(){
        return serviceTag;
    }
}



