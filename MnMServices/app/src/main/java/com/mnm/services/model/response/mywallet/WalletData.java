package com.mnm.services.model.response.mywallet;

import java.util.List;

/**
 * Created by saten on 5/9/17.
 */

public class WalletData {
    private Double WalletAmount;
    private List<WalletHistory> WalletHistory = null;

    public Double getWalletAmount() {
        return WalletAmount;
    }

    public void setWalletAmount(Double walletAmount) {
        WalletAmount = walletAmount;
    }

    public List<com.mnm.services.model.response.mywallet.WalletHistory> getWalletHistory() {
        return WalletHistory;
    }

    public void setWalletHistory(List<com.mnm.services.model.response.mywallet.WalletHistory> walletHistory) {
        WalletHistory = walletHistory;
    }
}
