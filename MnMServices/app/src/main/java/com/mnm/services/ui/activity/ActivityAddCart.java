package com.mnm.services.ui.activity;

import android.os.Bundle;
import android.view.View;

import com.mnm.services.R;
import com.mnm.services.utils.GlobalUtils;


public class ActivityAddCart extends MyDrawerBaseActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cart);
        findViewById(R.id.root_relay).setOnClickListener(this);
       initCustomActionBarView(ActivityAddCart.this,"Add cart",true);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityAddCart.this, v);
                break;
        }
    }
}