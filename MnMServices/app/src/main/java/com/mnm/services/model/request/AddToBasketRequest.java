package com.mnm.services.model.request;

import java.util.List;

/**
 * Created by satendra.singh on 05-04-2017.
 */

public class AddToBasketRequest {
    private List<ServicestoAdd> servicestoAdd = null;

    public List<ServicestoAdd> getServicestoAdd() {
        return servicestoAdd;
    }

    public void setServicestoAdd(List<ServicestoAdd> servicestoAdd) {
        this.servicestoAdd = servicestoAdd;
    }
}
