package com.mnm.services.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.mnm.services.R;
import com.mnm.services.constants.Constant;
import com.mnm.services.model.response.ServiceCategoryModel;
import com.mnm.services.ui.fragment.FragmentFaq;
import com.mnm.services.ui.fragment.FragmentServiceDescription;
import com.mnm.services.ui.tabs.SlidingTabLayout;
import com.mnm.services.utils.GlobalUtils;

import java.util.ArrayList;
import java.util.List;


public class ActivityService extends MyDrawerBaseActivity implements View.OnClickListener {
    ServiceCategoryModel categoryModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        intiUi();
    }

    private void intiUi() {
        findViewById(R.id.root_relay).setOnClickListener(this);
        categoryModel = (ServiceCategoryModel) getIntent().getSerializableExtra(Constant.CATEGORY_MODEL_DATA);
        initCustomActionBarView(ActivityService.this, categoryModel.getServiceGroupName(), true);
        initViewPagerAndTabs();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityService.this, v);
                break;
        }
    }

    private void initViewPagerAndTabs() {
        FragmentServiceDescription fragmentServiceDescription = new FragmentServiceDescription();
        Bundle mBudle = new Bundle();
        mBudle.putSerializable(Constant.CATEGORY_MODEL_DATA, categoryModel);
        fragmentServiceDescription.setArguments(mBudle);
        FragmentFaq faqFragment = new FragmentFaq();
        faqFragment.setArguments(mBudle);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        String[] tabs = getResources().getStringArray(R.array.tabs);
        pagerAdapter.addFragment(fragmentServiceDescription, tabs[0]);
        pagerAdapter.addFragment(faqFragment, tabs[1]);
        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(pagerAdapter);
        SlidingTabLayout tabLayout = (SlidingTabLayout) findViewById(R.id.tabLayout);
        tabLayout.setDistributeEvenly(true);
        tabLayout.setViewPager(viewPager);
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white_color);
            }

               /* @Override
                public int getDividerColor(int position) {
                    return getResources().getColor(R.color.color_primary_red);
                }*/
        });

        tabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });


    }


    class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }
}