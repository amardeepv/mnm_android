package com.mnm.services.model.response.myorder;

/**
 * Created by saten on 4/25/17.
 */

public class FeedbackHistory {
    private String feedbackDate;
    private String feebackMsg;
    private Integer rating;
    private String feedbackBy;

    public String getFeedbackDate() {
        return feedbackDate;
    }

    public void setFeedbackDate(String feedbackDate) {
        this.feedbackDate = feedbackDate;
    }

    public String getFeebackMsg() {
        return feebackMsg;
    }

    public void setFeebackMsg(String feebackMsg) {
        this.feebackMsg = feebackMsg;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getFeedbackBy() {
        return feedbackBy;
    }

    public void setFeedbackBy(String feedbackBy) {
        this.feedbackBy = feedbackBy;
    }
}
