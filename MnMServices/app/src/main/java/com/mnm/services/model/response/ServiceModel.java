package com.mnm.services.model.response;

import java.io.Serializable;
import java.util.List;

/**
 * Created by saten on 3/11/17.
 */

public class ServiceModel implements Serializable {
    private boolean isChecked;
    private boolean isExpended;
    private int quantity;
    private Integer serviceid;
    private String servicename;
    private Integer rate;
    private String rateType;
    private int additionalRate;
    private int initialrate;
    private boolean isServiceSelected;
    private int mPlanId;

    public void setAdditionalRate(int additionalRate) {
        this.additionalRate = additionalRate;
    }

    public boolean isSingleAddresrate() {
        return singleAddresrate;
    }

    public void setSingleAddresrate(boolean singleAddresrate) {
        this.singleAddresrate = singleAddresrate;
    }

    private boolean singleAddresrate;

    private String Notes;

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public int getmPlanId() {
        return mPlanId;
    }

    public void setmPlanId(int mPlanId) {
        this.mPlanId = mPlanId;
    }

    public int getInitialrate() {
        return initialrate;
    }

    public void setInitialrate(int initialrate) {
        this.initialrate = initialrate;
    }

    public String getServiceNameToDisplay() {
        return serviceNameToDisplay;
    }

    public void setServiceNameToDisplay(String serviceNameToDisplay) {
        this.serviceNameToDisplay = serviceNameToDisplay;
    }

    private String serviceNameToDisplay;

    public boolean isServiceSelected() {
        return isServiceSelected;
    }

    public void setServiceSelected(boolean serviceSelected) {
        isServiceSelected = serviceSelected;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isExpended() {
        return isExpended;
    }

    public void setExpended(boolean expended) {
        isExpended = expended;
    }


    public Boolean getExpended() {
        return isExpended;
    }

    public void setExpended(Boolean expended) {
        isExpended = expended;
    }

    private String desc;
    private List<ServiceCity> serviceCityRateList = null;
    private Object serviceTimeSlotList;
    private List<ServiceExcludeCity> serviceExcludeCitiesList = null;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Integer getServiceid() {
        return serviceid;
    }

    public void setServiceid(Integer serviceid) {
        this.serviceid = serviceid;
    }

    public String getServicename() {
        return servicename;
    }

    public void setServicename(String servicename) {
        this.servicename = servicename;
    }

    public Integer getRate() {
        if(initialrate==0){
            initialrate=rate;
        }
        if(quantity>0){
            rate=initialrate*quantity;
        }else{
            rate=initialrate;
        }
        return rate;
    }



    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public Integer getAdditionalRate() {
        return additionalRate;
    }

    public void setAdditionalRate(Integer additionalRate) {
        this.additionalRate = additionalRate;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<ServiceCity> getServiceCityRateList() {
        return serviceCityRateList;
    }

    public void setServiceCityRateList(List<ServiceCity> serviceCityRateList) {
        this.serviceCityRateList = serviceCityRateList;
    }

    public Object getServiceTimeSlotList() {
        return serviceTimeSlotList;
    }

    public void setServiceTimeSlotList(Object serviceTimeSlotList) {
        this.serviceTimeSlotList = serviceTimeSlotList;
    }

    public List<ServiceExcludeCity> getServiceExcludeCitiesList() {
        return serviceExcludeCitiesList;
    }

    public void setServiceExcludeCitiesList(List<ServiceExcludeCity> serviceExcludeCitiesList) {
        this.serviceExcludeCitiesList = serviceExcludeCitiesList;
    }
}