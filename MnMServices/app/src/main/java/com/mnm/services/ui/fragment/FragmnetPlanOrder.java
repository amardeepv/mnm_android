package com.mnm.services.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kelltontech.ui.IScreen;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.controller.ControllerPlanOrder;
import com.mnm.services.model.response.myorder.PlanOrderResponse;
import com.mnm.services.model.response.oepnticket.ChildService;
import com.mnm.services.ui.adapters.AdapterMyPlanOrder;
import com.mnm.services.ui.adapters.AdapterService;

import java.util.List;


public class FragmnetPlanOrder extends Fragment  implements View.OnClickListener,IScreen{
    private LinearLayoutManager mLayoutManager;
    private RecyclerView recyclerView;
    private AdapterService serviceAdapter;
    private ProgressBar progressBar;
    private TextView lblNoDataFound;
    List<ChildService> childServices = null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview =  inflater.inflate(R.layout.fragment_service, container, false);
        recyclerView=(RecyclerView)rootview.findViewById(R.id.list_view_service);
        lblNoDataFound=(TextView)rootview.findViewById(R.id.lbl_no_data_found);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        lblNoDataFound.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);
        progressBar=(ProgressBar)rootview.findViewById(R.id.progrees_bar);
        serviceApi();
        return rootview;
    }

    void serviceApi() {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {

           ControllerPlanOrder controllerPlanOrder = new ControllerPlanOrder(getActivity(), this);
            progressBar.setVisibility(View.VISIBLE);
            controllerPlanOrder.getData(AppSettings.PLAN_ORDER_EVENT, "");
        }
    }
    @Override
    public void onClick(View view) {

    }

    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Log.e("Response", response.toString());
                progressBar.setVisibility(View.GONE);
                if (response == null) {
                    ToastUtils.showToast(getActivity(), getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.PLAN_ORDER_EVENT:
                            PlanOrderResponse planOrderResponse = (PlanOrderResponse) response.getResponseObject();
                            if(!planOrderResponse.getHasError()) {
                               if(planOrderResponse.getData().getPlanOrders().size()>0) {
                                    lblNoDataFound.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                   AdapterMyPlanOrder adapterMyPlanOrder=new AdapterMyPlanOrder(getActivity(),planOrderResponse.getData().getPlanOrders(),recyclerView);
                                   recyclerView.setAdapter(adapterMyPlanOrder);

                                }else{
                                    lblNoDataFound.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.GONE);
                                }
                            }else{

                            }
                            break;
                        default:
                            break;
                    }
                }}
        });
    }

}



