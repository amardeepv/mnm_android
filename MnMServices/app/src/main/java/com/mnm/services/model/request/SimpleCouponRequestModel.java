package com.mnm.services.model.request;

/**
 * Created by saten on 4/4/17.
 */

public class SimpleCouponRequestModel {
    private String couponCode;
    private Integer serviceGroupID;
    private double totalAmount;
    private String userID;
    private String channel;


    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    private String addressId;
    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getServiceGroupID() {
        return serviceGroupID;
    }

    public void setServiceGroupID(Integer serviceGroupID) {
        this.serviceGroupID = serviceGroupID;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
