package com.mnm.services.model.response.mywallet;

/**
 * Created by saten on 5/9/17.
 */

public class WalletHistory {

    private String Description;

    private String Date;

    private Double Amount;

    private String Action;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public Double getAmount() {
        return Amount;
    }

    public void setAmount(Double amount) {
        Amount = amount;
    }

    public String getAction() {
        return Action;
    }

    public void setAction(String action) {
        Action = action;
    }
}
