package com.mnm.services.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

import com.mnm.services.R;


public class AdapterBanner extends PagerAdapter {
    private Activity mContext;
    private View view;
    private int[] bannerImages = new int[]{
            R.mipmap.welcome_one,
            R.mipmap.welcome_two,
            R.mipmap.welcome_three,
            R.mipmap.welcome_four};

    public AdapterBanner(final Activity context) {
        mContext = context;
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int resultId = 0;
        resultId = R.layout.banner;
        view = layoutInflater.inflate(resultId, null);
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        if (view instanceof ImageView) {
            return view == object;
        } else {
            return view == object;
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        imageView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        imageView.setImageResource(bannerImages[position]);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if (position == 4) {
            container.removeView((View) object);
        } else {
            container.removeView((ImageView) object);
        }
    }
}
