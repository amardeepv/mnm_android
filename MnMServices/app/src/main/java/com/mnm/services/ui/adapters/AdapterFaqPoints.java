package com.mnm.services.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.mnm.services.R;
import com.mnm.services.model.response.faq.ChildArray;
import java.util.List;

public class AdapterFaqPoints extends BaseAdapter {
    private Context context;
    private List<ChildArray> pointsList;


    public AdapterFaqPoints(Context context, List<ChildArray> pointsList) {
        this.context = context;
        this.pointsList = pointsList;
    }

    @Override
    public int getCount() {
        return pointsList.size();
    }

    @Override
    public ChildArray getItem(int position) {
        return pointsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_faq_point, null);
            holder = new ViewHolder();
            holder.txtPoint=(TextView) convertView.findViewById(R.id.txt_point);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ChildArray childArray=pointsList.get(position);
        holder.txtPoint.setText(childArray.getPoint());
        return convertView;
    }

    class ViewHolder {
        private TextView txtPoint;
    }
}
