package com.mnm.services.model.response;

import java.util.List;

/**
 * Created by saten on 4/6/17.
 */

public class CoupanResponse {
    private boolean hasError;
    private String messageCode;
    private String message;
    private CoupanResponseData data;
    private List<String> messages = null;

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CoupanResponseData getData() {
        return data;
    }

    public void setData(CoupanResponseData data) {
        this.data = data;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}

