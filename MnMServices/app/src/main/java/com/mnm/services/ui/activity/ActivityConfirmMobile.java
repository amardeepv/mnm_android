package com.mnm.services.ui.activity;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Response;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.Constant;
import com.mnm.services.controller.ControllerConfirmMobile;
import com.mnm.services.controller.ControllerGenerateOtp;
import com.mnm.services.model.request.AddMobileRequest;
import com.mnm.services.model.response.AddMobileResponse;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.utils.GlobalUtils;
import static com.mnm.services.constants.Constant.MOBILE_NUMBER;
import static com.mnm.services.constants.Constant.USER_ID;

public class ActivityConfirmMobile extends MyDrawerBaseActivity implements View.OnClickListener {
    private EditText edtuserMobile;
    private ProgressBar progreesBar;
    private boolean isComingFromCP;
TextView txtOtp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_mobile);
        txtOtp=(TextView)findViewById(R.id.txv_top);
        if(getIntent().getBooleanExtra(Constant.IS_COMING_FROM_CREATE_PASSWORD, false)){
            initCustomActionBarView(ActivityConfirmMobile.this, "Reset Password", true);

        }else{
            initCustomActionBarView(ActivityConfirmMobile.this, "Mobile Verification", true);
        }


        edtuserMobile = (EditText) findViewById(R.id.user_mobile);
        findViewById(R.id.root_relay).setOnClickListener(this);
        findViewById(R.id.btn_confirm_mobile).setOnClickListener(this);
        progreesBar = (ProgressBar) findViewById(R.id.progrees_bar);
        progreesBar.setVisibility(View.GONE);
        isComingFromCP = getIntent().getBooleanExtra(Constant.IS_COMING_FROM_CREATE_PASSWORD, false);
        if(getIntent().getBooleanExtra(Constant.IS_OLD_USER,false)){
            txtOtp.setText("We have migrated our system to new platform. It is mandatory to Reset the password.");
        }else{
            txtOtp.setText("Please enter your mobile number, You will receive otp on the entered number");
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm_mobile:
                if (validation()) {
                    if (!isComingFromCP) {
                        confirmMobileApi();
                    } else {
                        confirmMobileCreatePasswordApi();
                    }
                }
                break;
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityConfirmMobile.this, v);
                break;
            default:
                break;
        }
    }

    void confirmMobileApi() {
        if (!ConnectivityUtils.isNetworkEnabled(ActivityConfirmMobile.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerConfirmMobile confrimMobileController = new ControllerConfirmMobile(this, this);
            AddMobileRequest registerRequest = getConfirmMobileRequest();
            showProgressDialog(getString(R.string.please_wait));
            confrimMobileController.getData(AppSettings.CONFIRM_MOBILE_REQUEST, registerRequest);
        }
    }

    private AddMobileRequest getConfirmMobileRequest() {
        AddMobileRequest confirmMobileRequest = new AddMobileRequest();
        confirmMobileRequest.setUserId(getIntent().getStringExtra(USER_ID));
        confirmMobileRequest.setMobile(edtuserMobile.getText().toString());
        AppSharedPreference.putString(PrefConstants.PREF_MOBILE, edtuserMobile.getText().toString(), ActivityConfirmMobile.this);
        return confirmMobileRequest;
    }

    void confirmMobileCreatePasswordApi() {
        if (!ConnectivityUtils.isNetworkEnabled(ActivityConfirmMobile.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerGenerateOtp controllerGenerateOtp = new ControllerGenerateOtp(this, this, edtuserMobile.getText().toString());
            showProgressDialog(getString(R.string.please_wait));
            controllerGenerateOtp.getData(AppSettings.GENERATE_OTP, null);
        }
    }

    @Override
    protected void updateUi(Response response) {
        super.updateUi(response);

    }

    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        removeProgressDialog();
        ActivityConfirmMobile.this.runOnUiThread(new Runnable() {
            public void run() {
                Log.e("Response", response.toString());
                if (response == null) {
                    ToastUtils.showToast(ActivityConfirmMobile.this, getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.CONFIRM_MOBILE_REQUEST: {
                            AddMobileResponse confirmMobileResponse = (AddMobileResponse) response.getResponseObject();
                            if (!confirmMobileResponse.isHasError()) {
                                Intent mIntent = new Intent(ActivityConfirmMobile.this, ActivityVerifyMobile.class);
                                mIntent.putExtra(USER_ID, getIntent().getStringExtra(USER_ID));
                                mIntent.putExtra(MOBILE_NUMBER, edtuserMobile.getText().toString());
                                mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER, getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER, false));
                                startActivity(mIntent);
                                finish();
                                ;
                            } else {
                                ToastUtils.showToast(ActivityConfirmMobile.this, "" + confirmMobileResponse.getMessage());
                            }
                        }
                        break;
                        case AppSettings.GENERATE_OTP:
                            AddMobileResponse confirmMobileResponse = (AddMobileResponse) response.getResponseObject();
                            if (!confirmMobileResponse.isHasError()||confirmMobileResponse.getMessageCode().equalsIgnoreCase("ACT06")) {
                                Intent mIntent = new Intent(ActivityConfirmMobile.this, ActivityVerifyMobile.class);
                                mIntent.putExtra(USER_ID, getIntent().getStringExtra(USER_ID));
                                mIntent.putExtra(Constant.IS_COMING_FROM_CREATE_PASSWORD, isComingFromCP);
                                mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER, true);
                                mIntent.putExtra(MOBILE_NUMBER, edtuserMobile.getText().toString());
                                startActivity(mIntent);
                                finish();
                                ;
                            } else {
                                ToastUtils.showToast(ActivityConfirmMobile.this, "" + confirmMobileResponse.getMessage());
                            }

                            break;
                        default:
                            break;
                    }

                }
            }
        });
    }

    boolean validation() {
        if (edtuserMobile.getText().toString().length() <= 0) {
            Toast.makeText(ActivityConfirmMobile.this, "Please enter phone number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (edtuserMobile.getText().toString().length() <= 9) {
            Toast.makeText(ActivityConfirmMobile.this, "Please enter valid phone number", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
