package com.mnm.services.model.response;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by bluepi on 15/3/17.
 */

public class Day {

    private Calendar date;
    private List<Date> slots;
    private boolean active = false;

    public Day(){
        this.date = null;
    }

    public Day(Calendar date){
        this.date = date;
    }

    public Calendar getDate() {
        return date;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public List<Date> getSlots() {
        return slots;
    }

    public void setSlots(List<Date> slots) {
        this.slots = slots;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Boolean hasSlots(){
        Boolean has = Boolean.FALSE;
        if(slots != null && slots.size()>0){
            has = Boolean.TRUE;
        }
        return has;
    }

    public void toggleActive(){
        active = !active;
    }

    @Override
    public String toString() {
        return "Day{" +
                "date=" + date.get(Calendar.DAY_OF_MONTH) +
                ", slots=" + slots +
                ", active=" + active +
                '}';
    }
}
