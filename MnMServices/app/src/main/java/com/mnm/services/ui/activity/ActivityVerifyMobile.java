package com.mnm.services.ui.activity;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Response;
import com.kelltontech.persistence.SharedPrefsUtils;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.StringUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.Constant;
import com.mnm.services.controller.ControllerCreatePassword;
import com.mnm.services.controller.ControllerGenerateOtp;
import com.mnm.services.controller.ControllerVerifyMobile;
import com.mnm.services.model.request.VerifyMobileRequest;
import com.mnm.services.model.response.AddMobileResponse;
import com.mnm.services.model.response.VerifymobileResponse;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.utils.GlobalUtils;

import static com.mnm.services.constants.Constant.MOBILE_NUMBER;
import static com.mnm.services.constants.Constant.USER_ID;

public class ActivityVerifyMobile extends MyDrawerBaseActivity implements View.OnClickListener {
    private EditText edtOtp;
    private EditText edtPassword;
    private ProgressBar progreesBar;
    private boolean isComingFromCP;
    LinearLayout passwordLinlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_mobile);
        if(getIntent().getBooleanExtra(Constant.IS_COMING_FROM_CREATE_PASSWORD, false)){
            initCustomActionBarView(ActivityVerifyMobile.this, "Forgot Password", true);
        }else{
            initCustomActionBarView(ActivityVerifyMobile.this, "Mobile Verification", true);
        }

        findViewById(R.id.root_relay).setOnClickListener(this);
        registerReceiver(broadcastReceiver, new IntentFilter("INTERNET_LOST"));
        edtOtp = (EditText) findViewById(R.id.user_code);
        edtPassword = (EditText) findViewById(R.id.user_new_password);
        findViewById(R.id.btn_verify_otp).setOnClickListener(this);
        findViewById(R.id.root_relay).setOnClickListener(this);
        progreesBar = (ProgressBar) findViewById(R.id.progrees_bar);
        findViewById(R.id.btn_resend_otp).setOnClickListener(this);
        progreesBar.setVisibility(View.GONE);
        passwordLinlay = (LinearLayout) findViewById(R.id.password_linlay);
        isComingFromCP = getIntent().getBooleanExtra(Constant.IS_COMING_FROM_CREATE_PASSWORD, false);

        if (isComingFromCP) {
            passwordLinlay.setVisibility(View.VISIBLE);
        } else {
            passwordLinlay.setVisibility(View.GONE);
        }
    }


    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // internet lost alert dialog method call from here...
            String otp = SharedPrefsUtils.getSharedPrefString(ActivityVerifyMobile.this, Constant.PREF_FILE_NAME, Constant.PREF_OTP, "");
            edtOtp.setText(otp);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_verify_otp:
                if (validation()) {
                    if (!isComingFromCP) {
                        verifyOtpApi();
                    } else {
                        generatePassword(edtOtp.getText().toString(), edtPassword.getText().toString(), getIntent().getStringExtra(MOBILE_NUMBER));
                    }
                }
                break;
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityVerifyMobile.this, v);
                break;
            case R.id.btn_resend_otp:
                resendOtppi();
                break;
            default:
                break;
        }
    }

    void resendOtppi() {
        if (!ConnectivityUtils.isNetworkEnabled(ActivityVerifyMobile.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerGenerateOtp controllerGenerateOtp = new ControllerGenerateOtp(this, this, getIntent().getStringExtra(Constant.MOBILE_NUMBER));
            showProgressDialog(getString(R.string.please_wait));
            controllerGenerateOtp.getData(AppSettings.GENERATE_OTP, null);
        }
    }

    void verifyOtpApi() {
        if (!ConnectivityUtils.isNetworkEnabled(ActivityVerifyMobile.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerVerifyMobile verifyMobileController = new ControllerVerifyMobile(this, this);
            VerifyMobileRequest verifyMobileRequest = getVerifyMobileRequest();
            showProgressDialog(getString(R.string.please_wait));
            verifyMobileController.getData(AppSettings.VERIFY_OTP_REQUEST, verifyMobileRequest);
        }
    }

    private VerifyMobileRequest getVerifyMobileRequest() {
        VerifyMobileRequest verifyMobileRequest = new VerifyMobileRequest();
        if (StringUtils.isNullOrEmpty(getIntent().getStringExtra(MOBILE_NUMBER))) {
            verifyMobileRequest.setMobile(AppSharedPreference.getString(PrefConstants.PREF_MOBILE, "", ActivityVerifyMobile.this));
        } else {
            verifyMobileRequest.setMobile(getIntent().getStringExtra(MOBILE_NUMBER));
        }
        verifyMobileRequest.setUserId(getIntent().getStringExtra(USER_ID));
        verifyMobileRequest.setOtp(edtOtp.getText().toString());
        return verifyMobileRequest;
    }


    void generatePassword(String otp, String password, String mobile) {
        if (!ConnectivityUtils.isNetworkEnabled(ActivityVerifyMobile.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerCreatePassword verifyMobileController = new ControllerCreatePassword(this, this, otp, password, mobile);
            showProgressDialog(getString(R.string.please_wait));
            verifyMobileController.getData(AppSettings.CREATE_PASSWORD_EVENT, null);
        }
    }

    @Override
    protected void updateUi(Response response) {
        super.updateUi(response);
    }

    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        ActivityVerifyMobile.this.runOnUiThread(new Runnable() {
            public void run() {
                removeProgressDialog();
                Log.e("Response", response.toString());
                if (response == null) {
                    ToastUtils.showToast(ActivityVerifyMobile.this, getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.VERIFY_OTP_REQUEST: {
                            VerifymobileResponse verifymobileResponse = (VerifymobileResponse) response.getResponseObject();
                            if (!verifymobileResponse.getHasError()) {
                                AppSharedPreference.putBoolean(PrefConstants.IS_LOGIN_TRUE, true, ActivityVerifyMobile.this);
                                ToastUtils.showToast(ActivityVerifyMobile.this, "" + verifymobileResponse.getMessage());
                                if (getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER, false)) {
                                    Intent mIntent = new Intent(ActivityVerifyMobile.this, ActivityHome.class);
                                    startActivity(mIntent);
                                    finish();
                                } else {
                                    finish();
                                }
                            } else {
                                ToastUtils.showToast(ActivityVerifyMobile.this, "" + verifymobileResponse.getMessage());
                            }
                        }
                        break;
                        case AppSettings.CREATE_PASSWORD_EVENT:
                            AddMobileResponse verifymobileResponse = (AddMobileResponse) response.getResponseObject();
                            if (!verifymobileResponse.isHasError()) {
                                ToastUtils.showToast(ActivityVerifyMobile.this, "" + verifymobileResponse.getMessage());
                                Intent mIntent = new Intent(ActivityVerifyMobile.this, ActivityLogin.class);
                                mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER, getIntent().getBooleanExtra(Constant.IS_COMING_FROM_DRAWER, false));
                                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(mIntent);
                                finish();
                            } else {
                                ToastUtils.showToast(ActivityVerifyMobile.this, "" + verifymobileResponse.getMessage());
                            }
                            break;
                        case AppSettings.GENERATE_OTP:
                            AddMobileResponse confirmMobileResponse = (AddMobileResponse) response.getResponseObject();
                            if (!confirmMobileResponse.isHasError()) {
                                ToastUtils.showToast(ActivityVerifyMobile.this, "" + confirmMobileResponse.getMessage());
                            } else {
                                ToastUtils.showToast(ActivityVerifyMobile.this, "" + confirmMobileResponse.getMessage());
                            }
                            break;

                        default:
                            break;
                    }

                }
            }
        });
    }

    boolean validation() {
        boolean isValid = true;
        if (edtOtp.getText().toString().length() <= 0) {
            Toast.makeText(ActivityVerifyMobile.this, "Please enter otp", Toast.LENGTH_SHORT).show();
            isValid = false;
        } else if (edtOtp.getText().toString().length() <= 2) {
            Toast.makeText(ActivityVerifyMobile.this, "Please enter valid otp", Toast.LENGTH_SHORT).show();
            isValid = false;
        }
        if (isComingFromCP) {
            if (edtPassword.getText().toString().length() <= 0) {
                Toast.makeText(ActivityVerifyMobile.this, "Please enter new password", Toast.LENGTH_SHORT).show();
                isValid = false;
            } else if (edtPassword.getText().toString().length() <= 5) {
                Toast.makeText(ActivityVerifyMobile.this, "Your password must be atleast 6 characters long", Toast.LENGTH_SHORT).show();
                isValid = false;
            }
        }
        return isValid;
    }
}
