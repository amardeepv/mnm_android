package com.mnm.services.model.response.plan;

import java.io.Serializable;
import java.util.List;

/**
 * Created by saten on 4/10/17.
 */

public class UnlimitedPlanResponse implements Serializable{
    private Boolean hasError;
    private String messageCode;
    private String message;
    private List<PlanModel> data = null;
    private List<String> messages = null;

    public Boolean getHasError() {
        return hasError;
    }

    public void setHasError(Boolean hasError) {
        this.hasError = hasError;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PlanModel> getData() {
        return data;
    }

    public void setData(List<PlanModel> data) {
        this.data = data;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}
