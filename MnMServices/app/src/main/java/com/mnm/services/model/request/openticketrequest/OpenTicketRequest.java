package com.mnm.services.model.request.openticketrequest;

import java.util.List;

/**
 * Created by saten on 4/23/17.
 */

public class OpenTicketRequest {
    private List<LstOpenRequest> lstOpenRequest = null;

    public List<LstOpenRequest> getLstOpenRequest() {
        return lstOpenRequest;
    }

    public void setLstOpenRequest(List<LstOpenRequest> lstOpenRequest) {
        this.lstOpenRequest = lstOpenRequest;
    }
}
