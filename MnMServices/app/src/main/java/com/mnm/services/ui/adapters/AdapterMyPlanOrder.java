package com.mnm.services.ui.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kelltontech.utils.StringUtils;
import com.mnm.services.R;
import com.mnm.services.model.response.myorder.PlanOrder;
import com.mnm.services.ui.fragment.FragmentService;
import com.mnm.services.utils.GlobalUtils;

import java.util.List;

/**
 * Adapter to manage list of properties
 */
public class AdapterMyPlanOrder extends RecyclerView.Adapter<AdapterMyPlanOrder.ViewHolder> {
    List<PlanOrder> PlanOrders;
    private Context mContext;
    private View mItemLayoutView;
    private ViewHolder mViewHolder;
    FragmentService serviceFragment;
    private LinearLayoutManager linearLayoutManager;
    private RelativeLayout relayButtons;

    public AdapterMyPlanOrder(Context ctx, List<PlanOrder> PlanOrders, RecyclerView recyclerView) {
        this.PlanOrders = PlanOrders;
        this.mContext = ctx;
        this.serviceFragment = serviceFragment;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                }
            });
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("create","create view holder****");
        mItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_plan_order_history, null);
        mViewHolder = new ViewHolder(mItemLayoutView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        Log.e("create","bind view holder ++++ "+position);
        final PlanOrder  planOrder = PlanOrders.get(position);
        viewHolder.txtOrder.setText(StringUtils.isNullOrEmpty(planOrder.getOrderID())?"":planOrder.getOrderID());
        viewHolder.txtPlanName.setText(planOrder.getPlanName()+" ("+planOrder.getPlanDesc()+")");
        String createdDate="";
        if(!StringUtils.isNullOrEmpty(planOrder.getCreated())){
            String mCreateddate=planOrder.getCreated();
            createdDate=mCreateddate.substring(0,mCreateddate.indexOf("T"));
        }
        viewHolder.txtDatenTime.setText(GlobalUtils.getDateString(createdDate));
        viewHolder.txtrate.setText(StringUtils.isNullOrEmpty(planOrder.getTotalCost())?"":planOrder.getTotalCost());
       viewHolder.txtStatus.setText(planOrder.getPaymentStatus());
        String expirydDate="";
        if(!StringUtils.isNullOrEmpty(planOrder.getExpirydate())){
            String mCreateddate=planOrder.getExpirydate();
            expirydDate=mCreateddate.substring(0,mCreateddate.indexOf("T"));
        }
        viewHolder.txtExpirydate.setText(GlobalUtils.getDateString(expirydDate));
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtOrder;
        private TextView txtPlanName;
        private TextView txtDatenTime;
        private TextView txtExpirydate;
        private TextView txtrate;
        private TextView txtStatus;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            txtOrder=(TextView) itemLayoutView.findViewById(R.id.txt_oder_id);
            txtPlanName=(TextView) itemLayoutView.findViewById(R.id.txt_plan_name);
            txtDatenTime=(TextView) itemLayoutView.findViewById(R.id.txt_date_n_time);
            txtExpirydate=(TextView) itemLayoutView.findViewById(R.id.txt_expiry_date);
            txtrate=(TextView)itemLayoutView.findViewById(R.id.txt_rate);
            txtStatus=(TextView)itemLayoutView.findViewById(R.id.txt_status);

        }
    }



    @Override
    public int getItemCount() {
        return PlanOrders.size();
    }

}