package com.mnm.services.model.response.oepnticket;

import java.io.Serializable;
import java.util.List;

/**
 * Created by saten on 4/22/17.
 */

public class OpenTicketResponse implements Serializable {

    private boolean hasError;

    private String messageCode;

    private String message;

    private PlanData data;

    private List<String> messages = null;


    public Boolean getHasError() {
        return hasError;
    }

    public void setHasError(Boolean hasError) {
        this.hasError = hasError;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PlanData getData() {
        return data;
    }

    public void setData(PlanData data) {
        this.data = data;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}