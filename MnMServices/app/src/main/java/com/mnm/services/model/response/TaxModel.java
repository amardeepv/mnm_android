package com.mnm.services.model.response;

/**
 * Created by satendra.singh on 05-04-2017.
 */

public class TaxModel {
    private float totalPayableAmount;
    private float taxRate;
    private float taxAmount;
    private float billAfterTax;

    public float getTotalPayableAmount() {
        return totalPayableAmount;
    }

    public void setTotalPayableAmount(float totalPayableAmount) {
        this.totalPayableAmount = totalPayableAmount;
    }

    public float getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(float taxRate) {
        this.taxRate = taxRate;
    }

    public float getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(float taxAmount) {
        this.taxAmount = taxAmount;
    }

    public float getBillAfterTax() {
        return billAfterTax;
    }

    public void setBillAfterTax(float billAfterTax) {
        this.billAfterTax = billAfterTax;
    }
}
