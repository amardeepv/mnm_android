package com.mnm.services.model.response.plan;

import java.io.Serializable;
import java.util.List;

/**
 * Created by saten on 4/10/17.
 */

public class PlanModel implements Serializable{
    private int planID;
    private String planName;
    private String description;
    private Boolean status;
    private float cost;
    private Boolean visitRequired;
    private boolean onlyForAdmin;

    public boolean isOnlyForAdmin() {
        return onlyForAdmin;
    }

    public void setOnlyForAdmin(boolean onlyForAdmin) {
        this.onlyForAdmin = onlyForAdmin;
    }

    private List<PlanService> PlanParentServiceList = null;

    public int getPlanID() {
        return planID;
    }

    public void setPlanID(int planID) {
        this.planID = planID;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public Boolean getVisitRequired() {
        return visitRequired;
    }



    public void setVisitRequired(Boolean visitRequired) {
        this.visitRequired = visitRequired;
    }

    public List<PlanService> getPlanParentServiceList() {
        return PlanParentServiceList;
    }

    public void setPlanParentServiceList(List<PlanService> planParentServiceList) {
        PlanParentServiceList = planParentServiceList;
    }
}
