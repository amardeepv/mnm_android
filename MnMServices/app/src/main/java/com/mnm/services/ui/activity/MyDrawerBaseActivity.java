package com.mnm.services.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.mnm.services.R;
import com.mnm.services.constants.Constant;
import com.mnm.services.model.NavDrawerItem;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.adapters.AdapterNavDrawer;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * @author satendra.singh
 */
public abstract class MyDrawerBaseActivity extends DrawerBaseActivity implements OnClickListener {
    private DrawerLayout mDrawerLayout;
    private LinearLayout mDrawerWindow;
    private ActionBarDrawerToggle mDrawerToggle;
    private ArrayList<String> arrList;
    Toolbar mToolbar;
    String mTitle;
    AdapterNavDrawer drawerItemsAdapter;
    ListView drawerItemsListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    protected void initCustomActionBarView(Context context, String title, boolean isBack) {
        mTitle=title;
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView txvLocation = (TextView) findViewById(R.id.txvLocation);
        ImageView imgWallet=(ImageView)findViewById(R.id.img_wallet);
        ImageView imgLogo=(ImageView)findViewById(R.id.img_logo);
        imgLogo.setVisibility(View.GONE);

        imgWallet.setOnClickListener(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (isBack) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        if(title.equalsIgnoreCase("Home")){
            imgLogo.setVisibility(View.VISIBLE);
            imgWallet.setVisibility(View.VISIBLE);
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayShowTitleEnabled(false);
          //  mToolbar.setLogo(R.drawable.ic_launcher);
        }else{
            imgWallet.setVisibility(View.GONE);
            imgLogo.setVisibility(View.GONE);
            //mToolbar.setLogo(null);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(title);
        }

        //top toolbar values hide and show
        txvLocation.setVisibility(View.GONE);
        imgWallet.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if( AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE, false, MyDrawerBaseActivity.this)) {
                    Intent mIntent = new Intent(MyDrawerBaseActivity.this, ActivityMyWallet.class);
                    startActivity(mIntent);
                }else{
                    Intent mIntent = new Intent(MyDrawerBaseActivity.this,ActivityLogin.class);
                    mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER,true);
                    startActivity(mIntent);
                    //ToastUtils.showToast(MyDrawerBaseActivity.this,"You need to login/register First");

                }
            }
        });
    }

    /**
     * @param
     */
    protected void initializeDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerWindow = (LinearLayout) findViewById(R.id.drawer_window);


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.drawable.black_navigation, 0);
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        setAdapter();
        drawerItemsAdapter.notifyDataSetChanged();
        drawerItemsListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // mDrawerLayout.closeDrawer(mDrawerWindow);
                changeActivityView(position);
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     * @param
     * @return
     */
    private ArrayList<NavDrawerItem> createDrawerItemsList() {
        LinkedHashMap<Integer, String> mListDataChild = new LinkedHashMap<>();
        LinkedHashMap<Integer, String> mListDataExpiry = new LinkedHashMap<>();
        ArrayList<Integer> arrListImageType = new ArrayList<>();
        arrList = new ArrayList<>();
        ArrayList<Integer> arrListImage = new ArrayList<>();


        String[] tittle = getResources().getStringArray(R.array.nav_drawer_items);
        int[] img = {
                R.mipmap.user,
                R.mipmap.myorders,
                R.mipmap.wallet,
                R.mipmap.members,
                R.mipmap.tikets,
                R.mipmap.info,
                R.mipmap.help,
                R.drawable.checkmark,
                R.mipmap.logout};



        for (int i = 0; i < tittle.length; i++) {
            switch (i) {
                default:
                    if(tittle[i].equalsIgnoreCase("LOGOUT")){
                        if(!AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE, false, MyDrawerBaseActivity.this)){
                            arrList.add("LOGIN/REGISTER");
                        }else{
                            arrList.add(tittle[i]);
                        }
                    }else {
                        arrList.add(tittle[i]);
                    }
                    arrListImage.add(img[i]);
                    arrListImageType.add(1);
                    break;
            }
        }

        ArrayList<NavDrawerItem> drawerItemsList = new ArrayList<>();
        NavDrawerItem model;
        for (int i = 0; i < arrList.size(); i++) {
            model = new NavDrawerItem();
            model.setTitle(arrList.get(i));
            model.setIcon(arrListImage.get(i));
            model.setImageType(arrListImageType.get(i));
            drawerItemsList.add(model);
        }
        return drawerItemsList;
    }


    /**
     * @param position
     */
    private void changeActivityView(int position) {
        boolean flag = false;
        Class<?> newActivityClass = null;
        if (arrList.get(position).equals("REFER AND EARN")) {
            if( AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE, false, MyDrawerBaseActivity.this)) {
                Intent mIntent = new Intent(MyDrawerBaseActivity.this, ActivityReferEarn.class);
                startActivity(mIntent);
            }else{
                Intent mIntent = new Intent(MyDrawerBaseActivity.this,ActivityLogin.class);
                mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER,true);
                startActivity(mIntent);
                //ToastUtils.showToast(MyDrawerBaseActivity.this,"You need to login/register First");

            }
            //  newActivityClass = MembershipActivity.class;
        } else if (arrList.get(position).equals("OFFERS")) {
            Intent mIntent = new Intent(MyDrawerBaseActivity.this, ActivityOffer.class);
            startActivity(mIntent);


        } else if (arrList.get(position).equals("PURCHASED SERVICES")) {
            if( AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE, false, MyDrawerBaseActivity.this)) {
                Intent mIntent = new Intent(MyDrawerBaseActivity.this, ActivityOpenticketAddress.class);
                startActivity(mIntent);
            }else{
                Intent mIntent = new Intent(MyDrawerBaseActivity.this,ActivityLogin.class);
                mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER,true);
                startActivity(mIntent);
                // ToastUtils.showToast(MyDrawerBaseActivity.this,"You need to login/register First");

            }

        } else if (arrList.get(position).equals("NOTIFICATIONS")) {
            Intent mIntent = new Intent(MyDrawerBaseActivity.this, ActivityNotification.class);
            startActivity(mIntent);

        } else if (arrList.get(position).equals("ABOUT US")) {
            Intent mIntent = new Intent(MyDrawerBaseActivity.this, ActivityAboutUs.class);
            startActivity(mIntent);

        } else if (arrList.get(position).equals("RATE US")) {
            Intent mIntent = new Intent(MyDrawerBaseActivity.this, ActivityRateUs.class);
            startActivity(mIntent);

        } else if (arrList.get(position).equals("NEED HELP")) {
            Intent mIntent = new Intent(MyDrawerBaseActivity.this, ActivityNeedHelp.class);
            startActivity(mIntent);

        } else if (arrList.get(position).equals("MY PROFILE")) {
            if( AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE, false, MyDrawerBaseActivity.this)) {
                Intent mIntent = new Intent(MyDrawerBaseActivity.this, ActivityMyProfile.class);
                startActivity(mIntent);
            }else{
                Intent mIntent = new Intent(MyDrawerBaseActivity.this,ActivityLogin.class);
                mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER,true);
                startActivity(mIntent);
                // ToastUtils.showToast(MyDrawerBaseActivity.this,"You need to login/register First");

            }


        } else if (arrList.get(position).equals("MY WALLET")) {
            if( AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE, false, MyDrawerBaseActivity.this)) {
                Intent mIntent = new Intent(MyDrawerBaseActivity.this, ActivityMyWallet.class);
                startActivity(mIntent);
            }else{
                Intent mIntent = new Intent(MyDrawerBaseActivity.this,ActivityLogin.class);
                mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER,true);
                startActivity(mIntent);
                //ToastUtils.showToast(MyDrawerBaseActivity.this,"You need to login/register First");

            }


        } else if (arrList.get(position).equals("MY ORDER")) {
            if( AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE, false, MyDrawerBaseActivity.this)) {
                Intent mIntent = new Intent(MyDrawerBaseActivity.this, ActivityMyOrder.class);
                startActivity(mIntent);
            }else{
                Intent mIntent = new Intent(MyDrawerBaseActivity.this,ActivityLogin.class);
                mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER,true);
                startActivity(mIntent);
                //ToastUtils.showToast(MyDrawerBaseActivity.this,"You need to login/register First");

            }
        }else if (arrList.get(position).equals("TERMS & CONDITION")) {
            Intent mIntent = new Intent(MyDrawerBaseActivity.this, ActivityTermsConditions.class);
            startActivity(mIntent);

        }else if (arrList.get(position).equals("LOGOUT")||arrList.get(position).equals("LOGIN/REGISTER") ){
            if(arrList.get(position).equals("LOGOUT")) {
                AppSharedPreference.putBoolean(PrefConstants.IS_LOGIN_TRUE, false, MyDrawerBaseActivity.this);
                AppSharedPreference.putString(PrefConstants.USER_NAME, "", MyDrawerBaseActivity.this);
                AppSharedPreference.putString(PrefConstants.USER_PHONE, "", MyDrawerBaseActivity.this);
                AppSharedPreference.putString(PrefConstants.USER_ID, "", MyDrawerBaseActivity.this);
                AppSharedPreference.putInt(PrefConstants.ADDRESS_COUNT, 0, MyDrawerBaseActivity.this);
                AppSharedPreference.putString(PrefConstants.AUTHORIZATION_TOKEN, "" ,  MyDrawerBaseActivity.this);
            }
            Intent mIntent = new Intent(MyDrawerBaseActivity.this,ActivityLogin.class);
            mIntent.putExtra(Constant.IS_COMING_FROM_DRAWER,true);
            startActivity(mIntent);
            finish();

        }


        if (newActivityClass != null) {
            if (this.getClass() != newActivityClass) {
                Intent intent = new Intent(getBaseContext(), newActivityClass);
                if (flag) {
                    flag = false;
                    intent.putExtra("isFromDrawer", true);
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                Intent intent = new Intent(getBaseContext(), newActivityClass);
                if (flag) {
                    flag = false;
                    intent.putExtra("isFromDrawer", true);
                }
                startActivity(intent);
                finish();
            }
        }
        mDrawerLayout.closeDrawer(mDrawerWindow);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        setAdapter();
        switch (id) {
            case android.R.id.home:
                if(mTitle.equalsIgnoreCase("Schedule Booking")){
                    AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", MyDrawerBaseActivity.this);
                    Intent i = new Intent(MyDrawerBaseActivity.this, ActivityHome.class);
                  // set the new task and clear flags
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }
                if(mTitle.equalsIgnoreCase("Order Confirmation")){
                    Intent i = new Intent(MyDrawerBaseActivity.this, ActivityHome.class);
                  // set the new task and clear flags
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }else if(mTitle.equalsIgnoreCase("MakenMake")){
                    Intent i = new Intent(MyDrawerBaseActivity.this, ActivityHome.class);
                    // set the new task and clear flags
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }else if(mTitle.equalsIgnoreCase("Unlimited Plan")||mTitle.equalsIgnoreCase("Fixed Plan")||mTitle.equalsIgnoreCase("Add on Plan")){
                    Intent i = new Intent(MyDrawerBaseActivity.this, ActivityHome.class);
                      // set the new task and clear flags
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                    finish();
                }else{
                    finish();
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

    }


    /**
     * Subclass should over-ride this method to update the UI with response,
     * this base class promises to call this method from UI thread.
     *
     * @param serviceResponse
     */
void setAdapter(){
    ArrayList<NavDrawerItem> drawerItemsList = createDrawerItemsList();
    drawerItemsAdapter = new AdapterNavDrawer(this, drawerItemsList);
     drawerItemsListView = (ListView) findViewById(R.id.list_slidermenu);
    if(drawerItemsListView!=null)
    drawerItemsListView.setAdapter(drawerItemsAdapter);
}

}