package com.mnm.services.model.response.oepnticket;

import java.io.Serializable;

/**
 * Created by saten on 4/22/17.
 */

public class ChildService implements Serializable {
    private String serviceId;
    private String serviceName;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }
}
