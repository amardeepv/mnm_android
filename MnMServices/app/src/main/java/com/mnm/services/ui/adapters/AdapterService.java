package com.mnm.services.ui.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mnm.services.R;
import com.mnm.services.model.response.ServiceModel;
import com.mnm.services.ui.fragment.FragmentService;
import com.mnm.services.utils.GlobalUtils;

import java.util.List;

/**
 * Adapter to manage list of properties
 */
public class AdapterService extends RecyclerView.Adapter<AdapterService.ViewHolder> {
    private List<ServiceModel> mList;
    private Context mContext;
    private View mItemLayoutView;
    private ViewHolder mViewHolder;
    FragmentService serviceFragment;
    private LinearLayoutManager linearLayoutManager;
    private RelativeLayout relayButtons;
    private String serviceTag;

    public AdapterService(Context ctx, List<ServiceModel> itemsData, RecyclerView recyclerView, FragmentService serviceFragment,String serviceTag) {
        this.mList = itemsData;
        this.mContext = ctx;
        this.serviceFragment = serviceFragment;
        this.serviceTag=serviceTag;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                }
            });
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("create","create view holder****");
        mItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_service, null);
        mViewHolder = new ViewHolder(mItemLayoutView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        Log.e("create","bind view holder ++++ "+position);
        final ServiceModel  serviceModel = mList.get(position);
        if(serviceModel.getRateType().contains("Base Rate")||serviceModel.getRateType().equalsIgnoreCase("Fixed")){
            viewHolder.relaybuttons.setVisibility(View.GONE);
            viewHolder.relayCheckBox.setVisibility(View.VISIBLE);
            viewHolder.btnCheckBox.setChecked(serviceModel.isChecked());
        }else{
            viewHolder.relayCheckBox.setVisibility(View.GONE);
            viewHolder.relaybuttons.setVisibility(View.VISIBLE);
            viewHolder.txtCount.setText(""+serviceModel.getQuantity());
        }
        mList.get(position).setServiceNameToDisplay(serviceTag+"->"+ mList.get(position).getServicename());
        if(serviceModel.isSingleAddresrate()){
            viewHolder.txtRate.setText("0");
        }else{
            viewHolder.txtRate.setText(""+serviceModel.getRate());
        }

        viewHolder.txtServiceName.setText(serviceModel.getServicename());
       viewHolder.txtMultiline.setText(serviceModel.getDesc());
        viewHolder.btnCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ServiceModel mItem = mList.get(position);
                if(mItem.isChecked()){
                    mItem.setChecked(false);
                }else{
                    mItem.setChecked(true);
                }
                viewHolder.btnCheckBox.setChecked(mItem.isChecked());
                mItem.setServiceSelected(mItem.isChecked());
                serviceFragment.addAndRemove(mList.get(position));
            }
        });
        viewHolder.imgBtnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServiceModel mItem = mList.get(position);
                mList.get(position).setQuantity(mList.get(position).getQuantity() + 1);
                viewHolder.txtCount.setText("" + mList.get(position).getQuantity());
                mList.get(position).setServiceSelected(true);
                if(serviceModel.isSingleAddresrate()){
                    viewHolder.txtRate.setText("0");

                }else{
                    viewHolder.txtRate.setText(""+serviceModel.getRate());
                }

                serviceFragment.addAndRemove(mList.get(position));

            }
        });

        viewHolder.imgBtnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServiceModel mItem = mList.get(position);
                        if (mItem.getQuantity() >= 1) {
                            mItem.setQuantity(mItem.getQuantity() - 1);
                            viewHolder.txtCount.setText("" + mItem.getQuantity());
                            if (mItem.getQuantity() >= 1) {
                                mList.get(position).setServiceSelected(true);
                            } else {
                                mList.get(position).setServiceSelected(false);
                            }
                        } else {
                            mList.get(position).setServiceSelected(false);
                        }
                if(serviceModel.isSingleAddresrate()){
                    viewHolder.txtRate.setText("0");

                }else{
                    viewHolder.txtRate.setText(""+serviceModel.getRate());
                }
                        serviceFragment.addAndRemove(mList.get(position));


            }
        });

        viewHolder.imgBtnArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showSingleAndMultiline(mList.get(position),viewHolder);

            }
        });
        viewHolder.infoImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GlobalUtils.displayDialog(mContext,"Info",mList.get(position).getNotes());
            }
        });
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgBtnArrow;
        private ImageView imgBtnMinus;
        private ImageView imgBtnPlus;
        private TextView txtCount;
        private TextView txtRate;
      //  private TextView txtSingleLine;
        private TextView txtMultiline;
        private RelativeLayout relaybuttons;
        private RelativeLayout relayCheckBox;
        private TextView txtServiceName;
        private CheckBox btnCheckBox;
        private ImageView infoImg;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            imgBtnArrow = (ImageView) itemLayoutView.findViewById(R.id.img_arrow);
            imgBtnMinus = (ImageView) itemLayoutView.findViewById(R.id.img_minus);
            imgBtnPlus=(ImageView)itemLayoutView.findViewById(R.id.img_plus);
            txtCount=(TextView) itemLayoutView.findViewById(R.id.txt_count);
            txtRate=(TextView) itemLayoutView.findViewById(R.id.txt_amount);
            //txtSingleLine=(TextView) itemLayoutView.findViewById(R.id.txt_single_line);
            txtMultiline=(TextView) itemLayoutView.findViewById(R.id.txt_multiple_line);
            relaybuttons=(RelativeLayout)itemLayoutView.findViewById(R.id.relay_buttons);
            txtServiceName=(TextView)itemLayoutView.findViewById(R.id.txt_service_name);
            relayCheckBox=(RelativeLayout)itemLayoutView.findViewById(R.id.relay_checkbox);
            btnCheckBox=(CheckBox)itemLayoutView.findViewById(R.id.checkbox);
            infoImg=(ImageView)itemLayoutView.findViewById(R.id.info_img);

        }
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

}