package com.mnm.services.model.response.profile;

/**
 * Created by satendra.singh on 04-05-2017.
 */

public class Profileupdateresponse {
    boolean hasError;
    String messageCode;
    String message;

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
