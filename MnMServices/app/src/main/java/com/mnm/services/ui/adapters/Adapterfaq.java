package com.mnm.services.ui.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.mnm.services.R;
import com.mnm.services.model.response.faq.FaqData;
import com.mnm.services.ui.widgets.NonScrollListView;
import java.util.List;

/**
 * Adapter to manage list of properties
 */
public class Adapterfaq extends RecyclerView.Adapter<Adapterfaq.ViewHolder> {
    private List<FaqData> mList;
    private Context mContext;
    private View mItemLayoutView;
    private ViewHolder mViewHolder;
    private LinearLayoutManager linearLayoutManager;

    public Adapterfaq(Context ctx, List<FaqData> itemsData, RecyclerView recyclerView) {
        this.mList = itemsData;
        this.mContext = ctx;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                }
            });
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("create","create view holder****");
        mItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_faq, null);
        mViewHolder = new ViewHolder(mItemLayoutView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        final FaqData  faqData = mList.get(position);
        viewHolder.txtQues.setText(faqData.getQuestion());
        viewHolder.txtAns.setText(faqData.getAnswer());
        if(faqData.getChild_array()!=null&&faqData.getChild_array().size()>0) {
            AdapterFaqPoints adapterFaqPoints=new AdapterFaqPoints(mContext,faqData.getChild_array());
            viewHolder.faqPointlist.setAdapter(adapterFaqPoints);
        }
        expandOrColapaseView(faqData,viewHolder,false);
        viewHolder.imgBtnArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean mboolean;
                if(faqData.isArrowExpand()){
                    mboolean=false;
                  //  expandOrColapaseView(faqData,viewHolder,true);
                }else{
                    mboolean=true;
                  //  expandOrColapaseView(faqData,viewHolder,true);
                }
                for(int i=0;i<mList.size();i++){
                    mList.get(i).setArrowExpand(false);
                }
                mList.get(position).setArrowExpand(mboolean);
                notifyDataSetChanged();

            }
        });

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgBtnArrow;
        private TextView txtQues;
        private TextView txtAns;
        private NonScrollListView faqPointlist;
        RelativeLayout relayData;

        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            faqPointlist=(NonScrollListView)itemLayoutView.findViewById(R.id.faq_point_list);
            imgBtnArrow = (ImageView) itemLayoutView.findViewById(R.id.img_arrow);
            txtQues=(TextView) itemLayoutView.findViewById(R.id.txt_question);
            txtAns=(TextView) itemLayoutView.findViewById(R.id.txt_answer);
            relayData=(RelativeLayout)itemLayoutView.findViewById(R.id.relay_data);
        }
    }



    @Override
    public int getItemCount() {
        return mList.size();
    }

    void expandOrColapaseView(FaqData faqData, ViewHolder viewHolder,boolean isbtnClick ){
        if(!faqData.isArrowExpand()){
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                viewHolder.imgBtnArrow.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.arrow_down) );
            } else {
                viewHolder.imgBtnArrow.setImageDrawable(mContext.getResources().getDrawable(R.drawable.arrow_down));
            }
            viewHolder.relayData.setVisibility(View.GONE);
            if(isbtnClick) {
                faqData.setArrowExpand(true);
            }
        }else{
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                viewHolder.imgBtnArrow.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.arrow_up));
            } else {
                viewHolder.imgBtnArrow.setImageDrawable(mContext.getResources().getDrawable(R.drawable.arrow_up));
            }
            if(isbtnClick) {
                faqData.setArrowExpand(false);
            }
            viewHolder.relayData.setVisibility(View.VISIBLE);
        }
    }

}