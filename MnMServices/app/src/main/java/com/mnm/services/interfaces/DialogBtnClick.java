package com.mnm.services.interfaces;

import com.mnm.services.model.response.address.AddressModel;

/**
 * Created by saten on 4/11/17.
 */

public interface DialogBtnClick {
    public void onDialogBtnClicked(int btnPosition);

}
