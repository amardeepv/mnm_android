package com.mnm.services.model.response;

/**
 * Created by satendra.singh on 07-03-2017.
 */

public class ServiceCategory {
    private Integer serviceGroupId;
    private Integer parentID;
    private String serviceGroupName;

    public Integer getServiceGroupId() {
        return serviceGroupId;
    }

    public void setServiceGroupId(Integer serviceGroupId) {
        this.serviceGroupId = serviceGroupId;
    }

    public Integer getParentID() {
        return parentID;
    }

    public void setParentID(Integer parentID) {
        this.parentID = parentID;
    }

    public String getServiceGroupName() {
        return serviceGroupName;
    }

    public void setServiceGroupName(String serviceGroupName) {
        this.serviceGroupName = serviceGroupName;
    }

}
