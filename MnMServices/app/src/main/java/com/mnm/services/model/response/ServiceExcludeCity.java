package com.mnm.services.model.response;

import java.io.Serializable;

/**
 * Created by saten on 3/18/17.
 */

public class ServiceExcludeCity implements Serializable{
    private Integer createdBy;
    private String createdOn;
    private Integer excludeCityid;
    private Integer cityID;
    private Integer stateId;
    private Integer serviceId;
    private Boolean isexlcude;


    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public Integer getExcludeCityid() {
        return excludeCityid;
    }

    public void setExcludeCityid(Integer excludeCityid) {
        this.excludeCityid = excludeCityid;
    }

    public Integer getCityID() {
        return cityID;
    }

    public void setCityID(Integer cityID) {
        this.cityID = cityID;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public Boolean getIsexlcude() {
        return isexlcude;
    }

    public void setIsexlcude(Boolean isexlcude) {
        this.isexlcude = isexlcude;
    }
}
