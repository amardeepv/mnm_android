package com.mnm.services.model;

import com.mnm.services.model.response.ServiceModel;

import java.util.ArrayList;

/**
 * Created by saten on 5/26/17.
 */

public class SelectedServicesResponse {
    ArrayList<ServiceModel> serviceList;

    public ArrayList<ServiceModel> getServiceList() {
        return serviceList;
    }

    public void setServiceList(ArrayList<ServiceModel> serviceList) {
        this.serviceList = serviceList;
    }
}
