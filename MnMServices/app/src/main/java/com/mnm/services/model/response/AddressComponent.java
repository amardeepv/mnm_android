package com.mnm.services.model.response;

import java.util.List;

/**
 * Created by saten on 6/17/17.
 */

public class AddressComponent {
    public String long_name;
    public String short_name;

    public String getLong_name() {
        return long_name;
    }

    public void setLong_name(String long_name) {
        this.long_name = long_name;
    }

    public String getShort_name() {
        return short_name;
    }

    public void setShort_name(String short_name) {
        this.short_name = short_name;
    }
}
