package com.mnm.services.controller;

import android.app.Activity;
import android.util.Log;
import com.google.gson.Gson;
import com.kelltontech.network.HttpClientConnection;
import com.kelltontech.network.Response;
import com.kelltontech.network.ServiceRequest;
import com.kelltontech.ui.IScreen;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.ArgumentsKeys;
import com.mnm.services.model.request.AddMobileRequest;
import com.mnm.services.model.response.AddMobileResponse;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

public class ControllerCreatePassword extends ControllerBase {
    String mobile;
     String otp;
    String password;

    public ControllerCreatePassword(Activity activity, IScreen screen,String otp,String password,String mobile) {
        super(activity, screen);
        this.otp=otp;
        this.password=password;
        this.mobile=mobile;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kelltontech.controller.BaseController#getData(int,
     * java.lang.Object)
     */
    @Override
    public ServiceRequest getData(int requestType, Object requestData) {
       /* ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setHttpMethod(HttpClientConnection.HTTP_METHOD.GET);
        serviceRequest.setDataType(requestType);
        setHttpsHeadersOneAssistPreLogin(serviceRequest);
        serviceRequest.setRequestData(requestData);
      //  serviceRequest.setPostData(setRequestParameters((AddMobileRequest) requestData));
        serviceRequest.setResponseController(this);
        serviceRequest.setPriority(HttpClientConnection.PRIORITY.HIGH);
        serviceRequest.setUrl(AppSettings.CREATE_PASSWORD_URL+mobile+"&OTP="+otp+"&password="+password);
        HttpClientConnection connection = HttpClientConnection.getInstance();
        connection.addRequest(serviceRequest);
        Log.d("req ConfirmMobile ", serviceRequest.toString());
        return serviceRequest;*/

        ServiceRequest serviceRequest = new ServiceRequest();
        serviceRequest.setHttpMethod(HttpClientConnection.HTTP_METHOD.GET);
        serviceRequest.setDataType(requestType);
        setHttpsHeaders(serviceRequest);
        serviceRequest.setRequestData(requestData);
        serviceRequest.setResponseController(this);
        serviceRequest.setPriority(HttpClientConnection.PRIORITY.HIGH);
        //serviceRequest.setUrl(AppSettings.RFER_AND_EARN+ AppSharedPreference.getString(PrefConstants.USER_ID,"",mContext));
        serviceRequest.setUrl(AppSettings.CREATE_PASSWORD_URL+mobile+"&OTP="+otp+"&password="+password);
        HttpClientConnection connection = HttpClientConnection.getInstance();
        connection.addRequest(serviceRequest);
        Log.d("req ", serviceRequest.toString());
        return serviceRequest;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kelltontech.controller.IController#handleResponse(com.kelltontech
     * .network.Response)
     */
    @Override
    public void handleResponse(Response response) {
        try {
            String responseData = new String(response.getResponseData());
            Log.d(ArgumentsKeys.RESPONSE, responseData);
            AddMobileResponse _loginResponse = new Gson().fromJson(responseData, AddMobileResponse.class);
            response.setResponseObject(_loginResponse);
            sendResponseToScreen(response);
        } catch (Exception e) {
            e.printStackTrace();
            sendResponseToScreen(null);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kelltontech.controller.IController#parseResponse(com.kelltontech.
     * network.Response)
     */
    @Override
    public void parseResponse(Response response) {

    }

    /**
     * this method creates login request
     * <p>
     * param requestData
     * return
     */
    private HttpEntity setRequestParameters(AddMobileRequest requestData) {
        try {
            String loginRequest = new Gson().toJson(requestData);
            Log.d(ArgumentsKeys.REQUEST, loginRequest);
            return new StringEntity(loginRequest);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
