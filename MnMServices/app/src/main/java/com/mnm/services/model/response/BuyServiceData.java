package com.mnm.services.model.response;

import java.util.List;

/**
 * Created by satendra.singh on 06-04-2017.
 */

public class BuyServiceData {


    private List<BuyResult> BuyResult = null;

    public List<com.mnm.services.model.response.BuyResult> getBuyResult() {
        return BuyResult;
    }

    public void setBuyResult(List<com.mnm.services.model.response.BuyResult> buyResult) {
        BuyResult = buyResult;
    }
}