package com.mnm.services.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.gson.Gson;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.StringUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.Constant;
import com.mnm.services.controller.ControllerServiceGroup;
import com.mnm.services.model.SelectedServicesResponse;
import com.mnm.services.model.response.ServiceCategoryModel;
import com.mnm.services.model.response.ServiceGroupResponse;
import com.mnm.services.model.response.ServiceModel;
import com.mnm.services.model.response.oepnticket.ParentService;
import com.mnm.services.model.response.profile.PlanIdServiceid;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.fragment.FragmentService;
import com.mnm.services.ui.tabs.SlidingTabLayout;
import com.mnm.services.utils.GlobalUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class ActivityServiceGroup extends MyDrawerBaseActivity implements View.OnClickListener {
    ServiceCategoryModel categoryModel;
    ProgressBar progressBar;
    public TextView basketCount;
     public  List<ServiceModel> selectedServiceList;
    TextView btnCheckout;
    private boolean isZeroGrpIdHitDone=false;
    int  mCoun=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_category);
        intiUi();
    }

    private void intiUi() {
        findViewById(R.id.root_relay).setOnClickListener(this);
         btnCheckout=(TextView) findViewById(R.id.btn_checkout);
        btnCheckout.setOnClickListener(this);
        progressBar=(ProgressBar)findViewById(R.id.progress_bar);
        basketCount=(TextView)findViewById(R.id.basket_count);
        categoryModel=(ServiceCategoryModel)getIntent().getSerializableExtra(Constant.CATEGORY_MODEL_DATA);
        initCustomActionBarView(ActivityServiceGroup.this,categoryModel.getServiceGroupName(),true);
        selectedServiceList=new ArrayList<>();
        serviceGroupApi(categoryModel.getServiceGroupId());
          mCoun=0;
        if(getDataFromPreferences()!=null) {
            mCoun=getDataFromPreferences().size();
        }
        basketCount.setText(""+mCoun);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityServiceGroup.this, v);
                break;
            case R.id.btn_checkout:
                if(getSelectedList().size()>0||mCoun>0) {
                        Intent mIntent = new Intent(ActivityServiceGroup.this, ActivitySheduleBooking.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(Constant.SERVICE_SELECTED_DATA, (Serializable) getSelectedList());
                        bundle.putBoolean(Constant.IS_SHOW_OFFER_TRUE, categoryModel.getShowOffer());
                        bundle.putString(Constant.SERVICE_NAME, categoryModel.getServiceGroupName());
                        bundle.putSerializable(Constant.TIME_SLOT_DATA,  (Serializable)categoryModel.getStimeSlotList());
                        bundle.putString(Constant.PLAN_ID,getIntent().getStringExtra(Constant.PLAN_ID));
                        bundle.putString(Constant.ADDRESS_ID,getIntent().getStringExtra(Constant.ADDRESS_ID));
                        mIntent.putExtras(bundle);
                        startActivity(mIntent);
                        finish();

                    break;
                }else{
                    ToastUtils.showToast(ActivityServiceGroup.this,"Please select service");
                }
        }
    }
    private void initViewPagerAndTabs(List<ServiceCategoryModel> serviceList) {
        ViewPager  viewPager = (ViewPager) findViewById(R.id.viewPager);
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        for (ServiceCategoryModel obj:serviceList){
            FragmentService serviceFragment=new FragmentService();
            Bundle mBundle=new Bundle();
            mBundle.putSerializable(Constant.SERVICE_MODEL_DATA,obj);
            mBundle.putString(Constant.SERVICE_GRP_NAME,categoryModel.getServiceGroupName());
            if(getIntent().getBooleanExtra(Constant.IS_OPEN_TICKET_TRUE,false)){
                 mBundle.putSerializable(Constant.OPEN_TICKET_SERVICE_DATA,getIntent().getSerializableExtra(Constant.OPEN_TICKET_SERVICE_DATA));
                 mBundle.putBoolean(Constant.IS_OPEN_TICKET_TRUE,true);
            }else{
                mBundle.putBoolean(Constant.IS_OPEN_TICKET_TRUE,false);
            }
            serviceFragment.setArguments(mBundle);
            pagerAdapter.addFragment(serviceFragment, obj.getServiceGroupName());
        }
        viewPager.setOffscreenPageLimit(serviceList.size());
        viewPager.setAdapter(pagerAdapter);
        SlidingTabLayout tabLayout = (SlidingTabLayout) findViewById(R.id.tabLayout);
        if(!categoryModel.getServiceGroupName().equalsIgnoreCase("Car Spa")){
            tabLayout.setDistributeEvenly(true);
        }
        /*tabLayout.setDistributeEvenly(true);*/
        tabLayout.setViewPager(viewPager);
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white_color);          }

               /* @Override
                public int getDividerColor(int position) {
                    return getResources().getColor(R.color.color_primary_red);
                }*/
        });

        tabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });


    }
    class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }
    void serviceGroupApi(int grpId) {
        if (!ConnectivityUtils.isNetworkEnabled(ActivityServiceGroup.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerServiceGroup loginController = new ControllerServiceGroup(this, this,grpId);
            progressBar.setVisibility(View.VISIBLE);
            loginController.getData(AppSettings.SERVICE_GROUP_REQUEST, "");
        }
    }
    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        ActivityServiceGroup.this.runOnUiThread(new Runnable() {
            public void run() {
                progressBar.setVisibility(View.GONE);
                Log.e("Response", response.toString());
                if (response == null) {
                    ToastUtils.showToast(ActivityServiceGroup.this, getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.SERVICE_GROUP_REQUEST:
                            ServiceGroupResponse serviceGroupResponse = (ServiceGroupResponse) response.getResponseObject();
                            //  moveToScreen();
            if(!serviceGroupResponse.getHasError()) {
         if (serviceGroupResponse.getData().size() > 0) {
             if(!serviceGroupResponse.getData().get(0).getServiceGroupName().equalsIgnoreCase("RO"))
               serviceGroupResponse.getData().remove(0);
        initViewPagerAndTabs(serviceGroupResponse.getData());
    } else {
             if(!isZeroGrpIdHitDone) {
                 serviceGroupApi(0);
                 isZeroGrpIdHitDone = true;
             }else{
                 ToastUtils.showToast(ActivityServiceGroup.this,"No Data Found");
                 finish();
             }
        //ToastUtils.showToast(ActivityServiceGroup.this,"No Data Found");
    }
}else{
                ToastUtils.showToast(ActivityServiceGroup.this,""+serviceGroupResponse.getMessage());

            }
                            break;
                        default:
                            break;
                    }
                }}
        });
    }

    public  void addAndRemoveService(ServiceModel serviceModel) {
        int count=0;
        int OpenTicketCount=0;
        if (selectedServiceList != null && selectedServiceList.size() > 0) {
            selectedServiceList.remove(serviceModel);
            selectedServiceList.add(serviceModel);
            for (ServiceModel serviceModell : selectedServiceList) {
                if (serviceModell.isServiceSelected()) {
                    if(serviceModell.getRate()==0){
                        OpenTicketCount++;
                    }
                    count++;
                }
            }

            if (count> 0) {
                int mcountt=count+mCoun;
                basketCount.setText("" + mcountt);
            } else {
                basketCount.setText(""+mCoun);
            }
            if(OpenTicketCount==count){
                if(getIntent().getBooleanExtra(Constant.IS_OPEN_TICKET_TRUE,false)) {
                    btnCheckout.setText("Open ticket");
                }else{
                    btnCheckout.setText(" NEXT ");
                }

            }else {
                btnCheckout.setText(" NEXT ");
            }
        }
    }
    List<ServiceModel> getSelectedList(){
        ArrayList<ServiceModel> serviceList=new ArrayList<>();
        for (ServiceModel serviceModel: selectedServiceList) {
            if(serviceModel.isServiceSelected()){
                serviceList.add(serviceModel);
            }

        }
        return serviceList;
    }

    ArrayList<ServiceModel> getDataFromPreferences(){
        if(!StringUtils.isNullOrEmpty(AppSharedPreference.getString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivityServiceGroup.this))) {
            SelectedServicesResponse selectedServicesResponse = new Gson().fromJson(AppSharedPreference.getString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivityServiceGroup.this), SelectedServicesResponse.class);
            return selectedServicesResponse.getServiceList();
        }else{
            return null;
        }
    }

}