package com.mnm.services.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.kelltontech.ui.IScreen;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.StringUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.Constant;
import com.mnm.services.controller.ControllerAddAddress;
import com.mnm.services.controller.ControllerAddToBasket;
import com.mnm.services.controller.ControllerAddress;
import com.mnm.services.controller.ControllerCoupanPlan;
import com.mnm.services.controller.ControllerSearchLocation;
import com.mnm.services.controller.ControllerServiceTax;
import com.mnm.services.listner.AddressInterface;
import com.mnm.services.model.request.AddAddressModel;
import com.mnm.services.model.request.ApplyCouponRequest;
import com.mnm.services.model.request.SimpleCouponRequestModel;
import com.mnm.services.model.response.AddAddressResponse;
import com.mnm.services.model.response.AddToBasketResponse;
import com.mnm.services.model.response.AddressComponent;
import com.mnm.services.model.response.CoupanResponse;
import com.mnm.services.model.response.LocationSearchResponse;
import com.mnm.services.model.response.Result;
import com.mnm.services.model.response.ServiceTaxResponse;
import com.mnm.services.model.response.TaxModel;
import com.mnm.services.model.response.address.AddressModel;
import com.mnm.services.model.response.address.AddressResponse;
import com.mnm.services.model.response.plan.AddTOBasketPlanRequest;
import com.mnm.services.model.response.plan.PlanBasketList;
import com.mnm.services.model.response.plan.PlanModel;
import com.mnm.services.model.response.plan.PlanService;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.activity.ActivityLogin;
import com.mnm.services.ui.activity.ActivityPayment;
import com.mnm.services.ui.activity.ActivityPlan;
import com.mnm.services.ui.adapters.AdapterPlan;
import com.mnm.services.ui.adapters.AddressAdapter;
import com.mnm.services.ui.widgets.NonScrollListView;
import com.mnm.services.utils.GlobalUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class FragmentAddOn extends FragmentBase implements View.OnClickListener,IScreen,AddressInterface {


    //case 1 when user is not login/register
    private RelativeLayout loginResisterLayout;

    // case 2 when user is login/register but not having address
    private LinearLayout headerAddAddress;
    private RelativeLayout relayAddAddress;
    private EditText addressLineOne;
    AutoCompleteTextView addressLineAutoAddOn;
    private RelativeLayout btnAddNewAddress;
    private TextView txtSelectAddressAddAddress;

    //case 3 user login/register and having address
    private NonScrollListView nonScrollListView;
    Boolean isApiCalled = false;
    ArrayAdapter<String> adapterAddress;

    String addressLineOneText = "";
    String  addressLineTwoText = "";
    AddressAdapter addressAdapter;
    String addressId;


    //plan list UI
    List<PlanService> planServiceArrayList;
    private PlanModel planModel;
    private NonScrollListView listViewPlan;


    String planId;

    AdapterPlan planAdapter = null;
    EditText edtCouponCode;
    TextView basketCount;
    TextView txtAmountPlans;
    TextView txtCouponDiscount;
    TextView txtSavings;
    TextView lblTotalAmountWithTax;
    TextView txtTotalAmountWithTax;
    float planAmt,couponDiscountAmt,taxRate,totalAmtWithTax;
    String cpoupanId="";
    //TODO what is saving % how to calculate it
    float savingAmt=10;
    int basketcount;
    CheckBox termsAndConditionsCheckBox;
    TextView txtPlanDescription,txtNoOfCalls;
    private TextView txtAddAddress;

    private RelativeLayout btnApplyCoupan;
    private boolean isAddonplanApiCalled=false;
    private  String mLatitude="0";
    private String mLongitude="0";
    private String mState="",mCity="",subLocality="";

    ArrayList<String> mAddresslist;
    ArrayList<Result>   locationAddressList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView =  inflater.inflate(R.layout.fragment_addon, container, false);
        basketCount=(TextView)rootView.findViewById(R.id.basket_count);
        termsAndConditionsCheckBox=(CheckBox)rootView.findViewById(R.id.checkbox_terms_n_conditions);
        //  basketCount.setVisibility(View.VISIBLE);
        btnApplyCoupan=(RelativeLayout)rootView.findViewById(R.id.btn_apply_coupon);
        btnApplyCoupan.setOnClickListener(this);
        rootView.findViewById(R.id.btn_checkout).setOnClickListener(this);
        edtCouponCode =(EditText)rootView.findViewById(R.id.edt_coupan_code);
        txtAmountPlans=(TextView)rootView.findViewById(R.id.amount_plan);
        txtCouponDiscount=(TextView)rootView.findViewById(R.id.txt_coupan_discount);
        txtSavings=(TextView)rootView.findViewById(R.id.savings);
        lblTotalAmountWithTax=(TextView)rootView.findViewById(R.id.lbl_total_amount_with_tax);
        txtAddAddress=(TextView)rootView.findViewById(R.id.txt_add_address);
        txtTotalAmountWithTax=(TextView)rootView.findViewById(R.id.total_amount_with_tax);
        txtPlanDescription=(TextView)rootView.findViewById(R.id.txt_plan_decription);
        txtNoOfCalls=(TextView)rootView.findViewById(R.id.txt_no_of_calls);


        //case 1 when user is not login
        loginResisterLayout = (RelativeLayout) rootView.findViewById(R.id.relay_user_option);
        rootView.findViewById(R.id.relay_existing_user).setOnClickListener(this);
        rootView.findViewById(R.id.relay_new_user).setOnClickListener(this);

        //case 2 when user logon/register but not having address
        //show header
        headerAddAddress = (LinearLayout)rootView. findViewById(R.id.linlay_header_add_address);
        btnAddNewAddress = (RelativeLayout) rootView.findViewById(R.id.btn_add_address_new);
        btnAddNewAddress.setOnClickListener(this);
        relayAddAddress = (RelativeLayout)rootView.findViewById(R.id.relay_address);
        txtSelectAddressAddAddress = (TextView) rootView.findViewById(R.id.txt_select_address_add_address);
        addressLineOne = (EditText) rootView.findViewById(R.id.address_line_one);
        addressLineAutoAddOn = (AutoCompleteTextView) rootView.findViewById(R.id.address_line_two);


        //case 3 when user is Login/register and also having address
        nonScrollListView = (NonScrollListView) rootView.findViewById(R.id.non_scroll_listview);

        //Other UI elements
        listViewPlan=(NonScrollListView)rootView.findViewById(R.id.plan_list);
        Bundle bundle = this.getArguments();
        planModel =(PlanModel) bundle.getSerializable(Constant.UNLIMITED_PLAN_MODEL);
       /* addressLineTwo.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    addressLineTwo.showDropDown();

            }
        });
        addressLineTwo.post(new Runnable() {
                                       public void run() {
                                           addressLineTwo.dismissDropDown();
                                       }
                                   });
        addressLineTwo.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                addressLineTwo.showDropDown();
                return false;
            }
        });
        addressLineTwo.setOnTouchListener(new View.OnTouchListener() {

            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View paramView, MotionEvent paramMotionEvent) {
                // TODO Auto-generated method stub
               // addressLineTwo.showDropDown();
               // addressLineTwo.requestFocus();
                return false;
            }
        });*/

        addressLineAutoAddOn.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence cs, int i, int i1, int i2) {
                if (!StringUtils.isNullOrEmpty(cs.toString())) {
                    searchLocationApi(cs.toString());

                    //addressLinetwo.setEnabled(false);
                }
                //addressLineTwo.showDropDown();
                //addressLineTwo.requestFocus();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        addressLineAutoAddOn.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
                Result mResult=locationAddressList.get(pos);
                if(mResult.getGeometry()!=null){
                    mLongitude=mResult.getGeometry().getLocation().getLng();
                    mLatitude=mResult.getGeometry().getLocation().getLat();
                }
                if(mResult.getAddress_components().size()>=4){
                    mState=mResult.getAddress_components().get(3).getLong_name();
                }
                if(mResult.getAddress_components().size()>=2){
                    mCity=mResult.getAddress_components().get(1).getLong_name();
                }
                if(mResult.getAddress_components().size()>0){
                    subLocality=mResult.getAddress_components().get(0).getLong_name();
                }
            }
        });
        return rootView;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_checkout:
                if (AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE, false, getActivity())) {
                    if (validation()) {
                        if(addressLineOne.getText().toString().length()!=0&&addressLineAutoAddOn.getText().toString().length()!=0) {
                            callAddAddressApi();
                        }else {
                            callAddToBasketApi();
                        }
                    }
                } else {
                    ToastUtils.showToast(getActivity(), "Please login first");
                }
                break;
            case R.id.btn_apply_coupon:
                if(planAmt>0) {
                    if (edtCouponCode.getText().toString().length() != 0) {
                        if (!StringUtils.isNullOrEmpty(AppSharedPreference.getString(PrefConstants.USER_ID, "", getActivity()))) {
                            if(!StringUtils.isNullOrEmpty(addressId)) {
                                applyCoupanApi();
                            }else{
                                ToastUtils.showToast(getActivity(),"Please select address");
                            }
                        } else {
                            ToastUtils.showToast(getActivity(), getString(R.string.please_login_or_register));
                        }
                    } else {
                        ToastUtils.showToast(getActivity(), getString(R.string.Enter_coupon_code));
                    }
                }else{
                    ToastUtils.showToast(getActivity(),"Please select atleast one plan");
                }
                break;
            case R.id.relay_existing_user:
                Intent mLoginIntent = new Intent(getActivity(), ActivityLogin.class);
                mLoginIntent.putExtra(Constant.LOGIN_TAB_NUMBER,0);
                startActivity(mLoginIntent);
                break;
            case R.id.relay_new_user:
                Intent mRegisterIntent = new Intent(getActivity(), ActivityLogin.class);
                mRegisterIntent.putExtra(Constant.LOGIN_TAB_NUMBER,1);
                startActivity(mRegisterIntent);
                break;
            case R.id.btn_add_address_new:
                if(txtAddAddress.getText().toString().equalsIgnoreCase("Add Address")) {
                    if(addressAdapter!=null&&addressAdapter.getAddressList().size()>0) {
                        txtAddAddress.setText("Close");
                        btnAddNewAddress.setVisibility(View.VISIBLE);
                    }else{
                        btnAddNewAddress.setVisibility(View.GONE);
                        txtAddAddress.setText("Add Address");
                    }
                    showAddressView(4);
                }else{
                    txtAddAddress.setText("Add Address");
                    btnAddNewAddress.setVisibility(View.VISIBLE);
                    showAddressView(3);
                }
                txtAddAddress.setVisibility(View.VISIBLE);
                // showAddressView(4);
                break;
        }
    }


    void applyCoupanApi() {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            ControllerCoupanPlan loginController = new ControllerCoupanPlan(getActivity(), this);
            ApplyCouponRequest coupanRequest = getCouponRequest();
            showProgressDialog(getString(R.string.please_wait));
            loginController.getData(AppSettings.COUPAN_PLAN_EVENT, coupanRequest);
        }
    }

    private ApplyCouponRequest getCouponRequest() {
        ApplyCouponRequest applyCouponRequest= new ApplyCouponRequest();
        ArrayList<SimpleCouponRequestModel> coupanReuestList=new ArrayList<>();
        // for (PlanService planService: planServiceArrayList){
        SimpleCouponRequestModel coupanRequestModel=new SimpleCouponRequestModel();
        coupanRequestModel.setUserID(AppSharedPreference.getString(PrefConstants.USER_ID,"",getActivity()));
        coupanRequestModel.setServiceGroupID(planModel.getPlanID());
        coupanRequestModel.setTotalAmount(planAmt);
        coupanRequestModel.setCouponCode(edtCouponCode.getText().toString());
        coupanRequestModel.setChannel("MA");
        coupanRequestModel.setAddressId(addressId);
        coupanReuestList.add(coupanRequestModel);
        //  }
        applyCouponRequest.setLstCouponRequest(coupanReuestList);
        return applyCouponRequest;
    }

    private  void callAddToBasketApi(){
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            ControllerAddToBasket addToBasketController = new ControllerAddToBasket(getActivity(), this,false);
            AddTOBasketPlanRequest addToBasketRequest=getAddToBasketRequest();
            showProgressDialog(getString(R.string.please_wait));
            addToBasketController.getData(AppSettings.ADD_TO_PLAN_BASKET, addToBasketRequest);
        }
    }


    AddTOBasketPlanRequest getAddToBasketRequest(){
        AddTOBasketPlanRequest addToBasketRequest=new AddTOBasketPlanRequest();
        ArrayList<PlanBasketList> servicestoAdds=new ArrayList<>();
        for(PlanService planService:planServiceArrayList) {
            if (planService.isChecked()) {
                PlanBasketList planBasketList = new PlanBasketList();
                planBasketList.setUserId(AppSharedPreference.getString(PrefConstants.USER_ID, "", getActivity()));

                planBasketList.setPlanDetailId("0");
                planBasketList.setPlanId(""+planModel.getPlanID());
                planBasketList.setFactor(""+planService.getFactor());
                /*PlanDurationModel obj=planDurationModels.get(durationRangBar.getLeftIndex());
                int rangeBarFactor=obj.getFactor();
                float  serviceAmount=planModel.getCost()/planServiceArrayList.size();
                planBasketList.setDurationId(""+((obj.getDurationId()==11111111)?"":obj.getDurationId()));
                planBasketList.setDurationRate(""+((obj.getDurationRate()==11111111)?"":obj.getDurationRate()));
                planBasketList.setPlanAmount(""+planService.getServiceAmount(serviceAmount,rangeBarFactor));*/
                planBasketList.setVisitRequired(""+planModel.getVisitRequired());
                planBasketList.setPlanDesc(planModel.getDescription());
                planBasketList.setCouponId(cpoupanId);
                planBasketList.setCouponDiscountAmt(""+couponDiscountAmt);
                planBasketList.setAddressId(addressId);
                planBasketList.setServiceId(""+planService.getServiceParentId());
                planBasketList.setServiceDate(GlobalUtils.getCurrentDate());
                planBasketList.setCreated(GlobalUtils.getCurrentDate()); ;
                planBasketList.setNoofCalls(""+planService.getNoofCalls());
                servicestoAdds.add(planBasketList);

            }
        }
        addToBasketRequest.setPlanBasketList(servicestoAdds);
        return addToBasketRequest;

    }


    @Override
    public void onResume() {
        super.onResume();
        //call Address Api
        if (AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE, false, getActivity())) {
            if (!isApiCalled) {
                callAddressApi();
                isApiCalled = true;
            }else {
                removeProgressDialog();
            }
        } else {
            showAddressView(1);
            removeProgressDialog();
        }
    }



    private void callAddressApi() {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            //   showProgressDialog(getString(R.string.please_wait));
            ControllerAddress addressController = new ControllerAddress(getActivity(), this);
            addressController.getData(AppSettings.SERVICE_ADDRESS_REQUEST, null);
        }
    }

    @Override
    public void updateUi(final com.kelltontech.network.Response response) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                if (response == null) {
                    //ToastUtils.showToast(getActivity(), getString(R.string.server_is_down_please_try_later));
                } else switch (response.getDataType()) {
                    case AppSettings.SERVICE_APPLY_TAX_EVENT:
                        removeProgressDialog();;
                        ServiceTaxResponse serviceTaxResponse = (ServiceTaxResponse) response.getResponseObject();
                        TaxModel taxModel=serviceTaxResponse.getData();
                        if(!serviceTaxResponse.getHasError()){
                            planAmt=taxModel.getTotalPayableAmount();
                            taxRate=taxModel.getTaxRate();
                            //  totalAmtWithTax=taxModel.getTaxAmount();
                            totalAmtWithTax=taxModel.getBillAfterTax();
                            setUiText();
                        }
                        break;
                    case AppSettings.SERVICE_ADDRESS_REQUEST:
                        removeProgressDialog();
                        AddressResponse addressResponse = (AddressResponse) response.getResponseObject();
                        if (!addressResponse.getHasError()) {
                            if (addressResponse.getData().getLstAddressResult().size() > 0) {
                                addressAdapter = new AddressAdapter(getActivity(), (ArrayList<AddressModel>) addressResponse.getData().getLstAddressResult(), FragmentAddOn.this);           nonScrollListView.setAdapter(addressAdapter);
                                showAddressView(3);
                            } else {
                                showAddressView(2);
                               // ToastUtils.showToast(getActivity(), addressResponse.getMessage());
                            }
                        }
                        break;
                    case AppSettings.COUPAN_PLAN_EVENT:
                        //  removeProgressDialog();
                        CoupanResponse coupanResponse = (CoupanResponse) response.getResponseObject();
                        if(!coupanResponse.isHasError()){
                            btnApplyCoupan.setOnClickListener(null);
                            btnApplyCoupan.setEnabled(false);
                            double totalTaxableAmount=0;
                            /*"couponDiscount": 735,
                                    "discountedAmount": 6615,*/
                            couponDiscountAmt=(float)coupanResponse.getData().getLstCouponResult().get(0).getCouponDiscount();
                            planAmt=(float)coupanResponse.getData().getLstCouponResult().get(0).getDiscountedAmount();
                            cpoupanId=""+coupanResponse.getData().getLstCouponResult().get(0).getCouponId();
                            ToastUtils.showToast(getActivity(),coupanResponse.getMessage());
                            callTaxApi(planAmt,false);
                        }else {
                            removeProgressDialog();
                            //  callServiceTaxApi(""+getTotalAmount());
                            ToastUtils.showToast(getActivity(),coupanResponse.getMessage());
                        }
                        break;
                    case AppSettings.SERVICE_ADD_ADDRESS_EVENT:
                        removeProgressDialog();
                        AddAddressResponse addAddressResponse = (AddAddressResponse) response.getResponseObject();
                        if (!addAddressResponse.isHasError()) {
                            addressId = addAddressResponse.getData().getId();
                            callAddToBasketApi();
                        } else {
                            //TODO current add address api giving response we do not have our service in given state and city
                            ToastUtils.showToast(getActivity(), addAddressResponse.getMessage());
                            // moveToNextScreen();
                        }
                        break;
                    case AppSettings.ADD_TO_PLAN_BASKET:
                        removeProgressDialog();
                        AddToBasketResponse addToBasketResponse = (AddToBasketResponse) response.getResponseObject();
                        if(!addToBasketResponse.isHasError()){
                            ToastUtils.showToast(getActivity(),addToBasketResponse.getMessage());
                            moveToNextScreen(addToBasketResponse.getData().getReferenceId());
                        }else{
                            ToastUtils.showToast(getActivity(),addToBasketResponse.getMessage());
                        }
                        break;
                    case AppSettings.LOCATION_SEARCH_REQUEST:
                        //  addressLinetwo.setEnabled(true);
                        LocationSearchResponse  locationSearchResponse= (LocationSearchResponse) response.getResponseObject();
                        ArrayList<String>   mAddresslist=new ArrayList<String>();
                        locationAddressList=new ArrayList<Result>();
                            /*if(locationSearchResponse.getPredictions().size()>0) {
                                for(Prediction obj:locationSearchResponse.getPredictions()){
                                    mAddresslist.add(obj.getDescription());
                                }

                            }else{
                                mAddresslist.clear();
                            }*/

                        if (locationSearchResponse.getResults()!=null&&locationSearchResponse.getResults().size()>0&&locationSearchResponse.getResults().get(0).getAddress_components().size()>0) {
                            locationAddressList= (ArrayList<Result>) locationSearchResponse.getResults();
                            for (Result result:locationSearchResponse.getResults()){
                                mAddresslist.add(result.getFormatted_address());
                            }

                        }else{
                            mAddresslist.clear();
                            locationAddressList.clear();
                        }
                        adapterAddress  = new ArrayAdapter<String>(getActivity(), android.R.layout.select_dialog_item, mAddresslist);
                        addressLineAutoAddOn.setThreshold(1);//will start working from first character
                        addressLineAutoAddOn.setAdapter(adapterAddress);//setting the adapter data into the AutoCompleteTextView
                        if(!StringUtils.isNullOrEmpty(addressLineAutoAddOn.getText().toString())&&addressLineAutoAddOn.getText().toString().length()<14&&addressLineAutoAddOn.getText().toString().length()>=1) {
                            addressLineAutoAddOn.showDropDown();
                            addressLineAutoAddOn.requestFocus();
                        }
                        break;
                    default:
                        break;
                }
            }
        });
    }

    /*void showAddressView(int mcase) {
        //case 1 when user is not login/register
        addressLineOneText = "";
        addressLineTwoText = "";
        if (mcase == 1) {
            loginResisterLayout.setVisibility(View.VISIBLE);
            headerAddAddress.setVisibility(View.GONE);
            relayAddAddress.setVisibility(View.GONE);
            btnAddNewAddress.setVisibility(View.GONE);
        } else if (mcase == 2) {
            // case 2 when user is login/register but not having address
            loginResisterLayout.setVisibility(View.GONE);
            btnAddNewAddress.setVisibility(View.GONE);
            headerAddAddress.setVisibility(View.VISIBLE);
            relayAddAddress.setVisibility(View.VISIBLE);

        } else if (mcase == 3) {
            //case 3 user login/register and having address
            loginResisterLayout.setVisibility(View.GONE);
            headerAddAddress.setVisibility(View.VISIBLE);
            nonScrollListView.setVisibility(View.VISIBLE);
            btnAddNewAddress.setVisibility(View.VISIBLE);

        } else if (mcase == 4) {
            loginResisterLayout.setVisibility(View.GONE);
            nonScrollListView.setVisibility(View.GONE);
            btnAddNewAddress.setVisibility(View.GONE);
            headerAddAddress.setVisibility(View.VISIBLE);
            relayAddAddress.setVisibility(View.VISIBLE);


        }

    }*/

    void showAddressView(int mcase) {
        //case 1 when user is not login/register
        addressLineOneText = "";
        addressLineTwoText = "";
        if (mcase == 1) {
            loginResisterLayout.setVisibility(View.VISIBLE);
            headerAddAddress.setVisibility(View.GONE);
            relayAddAddress.setVisibility(View.GONE);
        } else if (mcase == 2) {
            // case 2 when user is login/register but not having address
            loginResisterLayout.setVisibility(View.GONE);
            headerAddAddress.setVisibility(View.VISIBLE);
            relayAddAddress.setVisibility(View.VISIBLE);

        } else if (mcase == 3) {
            //case 3 user login/register and having address
            loginResisterLayout.setVisibility(View.GONE);
            headerAddAddress.setVisibility(View.VISIBLE);
            nonScrollListView.setVisibility(View.VISIBLE);
            relayAddAddress.setVisibility(View.GONE);

        } else if (mcase == 4) {
            loginResisterLayout.setVisibility(View.GONE);
            nonScrollListView.setVisibility(View.GONE);
            headerAddAddress.setVisibility(View.VISIBLE);
            relayAddAddress.setVisibility(View.VISIBLE);


        }

    }

    @Override
    public void setAddress(AddressModel addAddressModel) {
        addressLineOneText = addAddressModel.getLine1();
        addressLineTwoText = addAddressModel.getLine2();
        addressId=""+addAddressModel.getId();
    }


    private  void callTaxApi(float totalAmount,boolean isTrue){
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            if(isTrue) {
                ((ActivityPlan) getActivity()).isShowProgressDialog = false;
                showProgressDialog("Please wait...");
            }
            ControllerServiceTax serviceTaxController = new ControllerServiceTax(getActivity(), this, "" + totalAmount);

            serviceTaxController.getData(AppSettings.SERVICE_APPLY_TAX_EVENT,null);
        }
    }

    void setUiText(){
        txtAmountPlans.setText(getString(R.string.rupees_symbol) + " "+planAmt);
        txtCouponDiscount.setText(getString(R.string.rupees_symbol) + " "+couponDiscountAmt);
        txtSavings.setText(getString(R.string.rupees_symbol) + " "+savingAmt);
        lblTotalAmountWithTax.setText("Total Amount (+Tax@"+taxRate+"%)");
        txtTotalAmountWithTax.setText(getString(R.string.rupees_symbol) + " "+totalAmtWithTax);
        if(basketcount==0){
            basketCount.setText("0");
        }else {
            // basketCount.setVisibility(View.VISIBLE);
            basketCount.setText("" + basketcount);
        }
    }




    boolean validation() {
        boolean isValid = true;
        if(planAmt<=0){
            isValid=false;
            ToastUtils.showToast(getActivity(),"Please select plan");
        }else if (!addressValidation()) {
            ToastUtils.showToast(getActivity(), "Please Add/Select Address");
            isValid = false;
        }else if(!termsAndConditionsCheckBox.isChecked()){
            ToastUtils.showToast(getActivity(),"Please accept terms and conditions");
            isValid = false;
        }
        return isValid;
    }

    boolean addressValidation() {
        boolean isValid = false;
        if (!StringUtils.isNullOrEmpty(addressLineOneText)&&addressLineOneText.length() != 0 &&!StringUtils.isNullOrEmpty(addressLineTwoText) &&addressLineTwoText.length() != 0) {
            isValid = true;
        }
        if (addressLineOne.getText().toString().length() != 0&&addressLineAutoAddOn.getText().toString().length()!=0) {
            addressLineOneText=addressLineOne.getText().toString();
            addressLineTwoText=addressLineAutoAddOn.getText().toString();
            isValid = true;
        }
        return isValid;
    }

    private void callAddAddressApi() {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            ControllerAddAddress addAddressController = new ControllerAddAddress(getActivity(), this);
            showProgressDialog("Please wait...");
            AddAddressModel addAddressRequest = getAddAddressRequest();
            addAddressController.getData(AppSettings.SERVICE_ADD_ADDRESS_EVENT, addAddressRequest);
        }
    }

    private AddAddressModel getAddAddressRequest() {
        AddAddressModel addAddressModel = new AddAddressModel();
        addAddressModel.setUserId(AppSharedPreference.getString(PrefConstants.USER_ID,"",getActivity()));
        addAddressModel.setLine1(addressLineOne.getText().toString());
        addAddressModel.setLine2(addressLineAutoAddOn.getText().toString());
        addAddressModel.setLatitude(mLatitude);
        addAddressModel.setLongitude(mLongitude);
        addAddressModel.setCity(mCity);
        addAddressModel.setArea(subLocality);
        addAddressModel.setState(mState);
        addAddressModel.setId("0");
        return addAddressModel;
    }




    void searchLocationApi(String searchTxt) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            ControllerSearchLocation searchLocationController = new ControllerSearchLocation(getActivity(), this, searchTxt);
            searchLocationController.getData(AppSettings.LOCATION_SEARCH_REQUEST, "");
        }
    }

    void moveToNextScreen(String addToBasketRefenceId){
        List<PlanService> mPlanList=new ArrayList<>();
        for(PlanService planService:planServiceArrayList){
            if(planService.isChecked()){
                mPlanList.add(planService);
            }
        }
        Intent mIntent=new Intent(getActivity(),ActivityPayment.class);
        mIntent.putExtra(Constant.TAX_RATE,""+taxRate);
        mIntent.putExtra(Constant.TAX_AMOUNT,""+(totalAmtWithTax-planAmt));
        mIntent.putExtra(Constant.TOTAL_BILL_AFTER_AMOUNT,""+totalAmtWithTax);
        mIntent.putExtra(Constant.SERVICE_NAME,""+planModel.getPlanName());
        mIntent.putExtra(Constant.IS_PLAN,true);
        mIntent.putExtra(Constant.ADD_TO_BASKET_REFERENCE_ID,addToBasketRefenceId);
        mIntent.putExtra(Constant.SERVICE_SELECTED_DATA, (Serializable) mPlanList);
        startActivity(mIntent);
    }
    public void callApi(){
        Log.e("CallApi ","AddOn");
        if(!isAddonplanApiCalled) {
            isAddonplanApiCalled = true;
            if (planModel != null && planModel.getPlanParentServiceList() != null && planModel.getPlanParentServiceList().size() > 0) {
                planServiceArrayList = planModel.getPlanParentServiceList();
                planAdapter = new AdapterPlan(getActivity(), planServiceArrayList,false);
                listViewPlan.setAdapter(planAdapter);
                planId = "" + planModel.getPlanID();
                planAmt = planModel.getCost();
                basketcount = planServiceArrayList.size();
                int noOfCalls = 0;
                for (PlanService planService : planModel.getPlanParentServiceList()) {
                    noOfCalls = noOfCalls + Integer.parseInt(planService.getNoofCalls());
                }
                txtNoOfCalls.setText("" + noOfCalls);
                txtPlanDescription.setText(planModel.getDescription());
                callTaxApi(planAmt, ((ActivityPlan) getActivity()).isShowProgressDialog);
            }
        }

    }

}



