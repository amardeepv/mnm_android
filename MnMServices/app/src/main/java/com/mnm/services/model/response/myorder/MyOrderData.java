package com.mnm.services.model.response.myorder;

import java.util.List;

/**
 * Created by saten on 4/25/17.
 */

public class MyOrderData {

    private List<ServiceData> ServiceDetaillist = null;

    public List<ServiceData> getServiceDetaillist() {
        return ServiceDetaillist;
    }

    public void setServiceDetaillist(List<ServiceData> serviceDetaillist) {
        ServiceDetaillist = serviceDetaillist;
    }
}
