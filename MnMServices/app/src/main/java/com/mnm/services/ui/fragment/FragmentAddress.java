package com.mnm.services.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.kelltontech.ui.IScreen;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.controller.ControllerAddAddress;
import com.mnm.services.controller.ControllerAddress;
import com.mnm.services.controller.ControllerDeleteAddress;
import com.mnm.services.model.request.AddAddressModel;
import com.mnm.services.model.response.AddAddressResponse;
import com.mnm.services.model.response.AddressDeleteResponse;
import com.mnm.services.model.response.ServiceCategoryModel;
import com.mnm.services.model.response.address.AddressResponse;
import com.mnm.services.model.response.oepnticket.ChildService;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.adapters.AdapterAddress;
import com.mnm.services.ui.adapters.AdapterService;
import java.util.List;


public class FragmentAddress extends Fragment  implements View.OnClickListener,IScreen{
    private LinearLayoutManager mLayoutManager;
    private ServiceCategoryModel categoryModel;
    private RecyclerView listViewService;
    private AdapterService serviceAdapter;
    private ProgressBar progressBar;
    private TextView lblNoDataFound;
    List<ChildService> childServices = null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview =  inflater.inflate(R.layout.fragment_service, container, false);
        listViewService=(RecyclerView)rootview.findViewById(R.id.list_view_service);
        listViewService.setLayoutManager(new LinearLayoutManager(getContext()));
        progressBar=(ProgressBar)rootview.findViewById(R.id.progrees_bar);
        lblNoDataFound=(TextView)rootview.findViewById(R.id.lbl_no_data_found);
        lblNoDataFound.setVisibility(View.GONE);
        callAddressApi();
        return rootview;
    }
    private void callAddressApi() {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            ControllerAddress addressController = new ControllerAddress(getActivity(), this);
            progressBar.setVisibility(View.VISIBLE);
            addressController.getData(AppSettings.SERVICE_ADDRESS_REQUEST, null);
        }
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Log.e("Response", response.toString());
                progressBar.setVisibility(View.GONE);
                if (response == null) {
                    ToastUtils.showToast(getActivity(), getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.SERVICE_ADDRESS_REQUEST:
                            progressBar.setVisibility(View.GONE);
                            AddressResponse addressResponse = (AddressResponse) response.getResponseObject();
                            if(!addressResponse.getHasError()) {
                                if(addressResponse.getData().getLstAddressResult().size()>0) {
                                    lblNoDataFound.setVisibility(View.GONE);
                                    listViewService.setVisibility(View.VISIBLE);
                                    AdapterAddress adapterAddress=new AdapterAddress(getActivity(),addressResponse.getData().getLstAddressResult(),listViewService,FragmentAddress.this);
                                    listViewService.setAdapter(adapterAddress);
                                }else{
                                    lblNoDataFound.setVisibility(View.VISIBLE);
                                    listViewService.setVisibility(View.GONE);
                                }
                            }else{

                            }
                            break;
                        case AppSettings.EDIT_ADDRESS_EVENT:
                            progressBar.setVisibility(View.GONE);
                            AddAddressResponse addAddressResponse=(AddAddressResponse) response.getResponseObject();
                            if(!addAddressResponse.isHasError()){
                                ToastUtils.showToast(getActivity(),""+addAddressResponse.getMessage());
                                callAddressApi();
                            }else{
                                ToastUtils.showToast(getActivity(),""+addAddressResponse.getMessage());
                            }
                            break;
                        case AppSettings.DELETE_ADDRESS_EVENT:
                            progressBar.setVisibility(View.GONE);
                            AddressDeleteResponse addressDeleteResponse=(AddressDeleteResponse) response.getResponseObject();
                            if(!addressDeleteResponse.isHasError()){
                                ToastUtils.showToast(getActivity(),""+addressDeleteResponse.getMessage());
                                callAddressApi();
                            }else{
                                ToastUtils.showToast(getActivity(),""+addressDeleteResponse.getMessage());
                            }
                            break;
                        default:
                            break;
                    }
                }}
        });
    }



    public void deleteAddress(String addressId){
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            ControllerDeleteAddress deleteAddress = new ControllerDeleteAddress(getActivity(), this,addressId);
            progressBar.setVisibility(View.VISIBLE);
            deleteAddress.getData(AppSettings.DELETE_ADDRESS_EVENT, null);
        }
    }

    public void editAddress(String addressid,String line1,String line2,String line3){
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            ControllerAddAddress addAddressController = new ControllerAddAddress(getActivity(), this);
            AddAddressModel addAddressModel=getAddAddressRequest(addressid,line1,line2,line3);
            progressBar.setVisibility(View.VISIBLE);
            addAddressController.getData(AppSettings.EDIT_ADDRESS_EVENT, addAddressModel);
        }
    }

    private AddAddressModel getAddAddressRequest(String addressid,String line1,String line2,String line3) {
        AddAddressModel addAddressModel = new AddAddressModel();
        addAddressModel.setUserId(AppSharedPreference.getString(PrefConstants.USER_ID,"",getActivity()));
        addAddressModel.setLine1(line1);
        addAddressModel.setLine2(line2);
        addAddressModel.setLatitude("0");
        addAddressModel.setLongitude("0");
        addAddressModel.setCity(line1);
        addAddressModel.setArea(line3);
        addAddressModel.setState(line2);
        addAddressModel.setId(addressid);
        return addAddressModel;
    }

}



