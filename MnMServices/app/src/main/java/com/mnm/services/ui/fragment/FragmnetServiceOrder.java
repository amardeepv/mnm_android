package com.mnm.services.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.kelltontech.ui.IScreen;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.Constant;
import com.mnm.services.controller.ControllerAddFeedback;
import com.mnm.services.controller.ControllerSearchLocation;
import com.mnm.services.controller.ControllerServiceOrder;
import com.mnm.services.listner.AddFeedBackListner;
import com.mnm.services.model.request.AddFeedbcakRequest;
import com.mnm.services.model.request.BuyServiceRequest;
import com.mnm.services.model.response.AddAddressResponse;
import com.mnm.services.model.response.ServiceCategoryModel;
import com.mnm.services.model.response.myorder.MyOrderResponse;
import com.mnm.services.model.response.oepnticket.ChildService;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.activity.ActivityPayment;
import com.mnm.services.ui.activity.ActivitySheduleBooking;
import com.mnm.services.ui.adapters.AdapterMyServiceOrder;

import java.util.List;


public class FragmnetServiceOrder extends Fragment  implements View.OnClickListener,IScreen,AddFeedBackListner {
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private TextView lblNoDataFound;
    AddFeedBackListner addFeedBackListner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootview =  inflater.inflate(R.layout.fragment_service, container, false);
        recyclerView=(RecyclerView)rootview.findViewById(R.id.list_view_service);
        lblNoDataFound=(TextView)rootview.findViewById(R.id.lbl_no_data_found) ;
        lblNoDataFound.setVisibility(View.GONE);
        addFeedBackListner=this;
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        progressBar=(ProgressBar)rootview.findViewById(R.id.progrees_bar);
        callApi();
        return rootview;
    }
    void callApi(){
        if(this.getArguments().getInt(Constant.ORDER_TYPE)==Constant.CURRENT_ORDER){
            OrderApi(Constant.CURRENT_ORDER);
        }else if(this.getArguments().getInt(Constant.ORDER_TYPE)==Constant.PAST_ORDER){
            OrderApi(Constant.PAST_ORDER);
        }
    }

    void OrderApi(int OrderType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            ControllerServiceOrder controllerMyOrder = new ControllerServiceOrder(getActivity(), this,OrderType);
            progressBar.setVisibility(View.VISIBLE);
            controllerMyOrder.getData(AppSettings.SERVICE_ORDER_EVENT, "");
        }
    }

    void addFeedBackApi(AddFeedbcakRequest addFeedbcakRequest) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), getResources().getString(R.string.network_error));
        } else {
            progressBar.setVisibility(View.VISIBLE);
            ControllerAddFeedback controllerAddFeedback = new ControllerAddFeedback(getActivity(), this);
            controllerAddFeedback.getData(AppSettings.ADD_FEEDBACK, addFeedbcakRequest);
        }
    }
    @Override
    public void onClick(View view) {

    }

    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Log.e("Response", response.toString());
                progressBar.setVisibility(View.GONE);
                if (response == null) {
                    ToastUtils.showToast(getActivity(), getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.SERVICE_ORDER_EVENT:
                            MyOrderResponse myOrderResponse = (MyOrderResponse) response.getResponseObject();
                            if(!myOrderResponse.getHasError()) {
                                if(myOrderResponse.getData().getServiceDetaillist().size()>0) {
                                    lblNoDataFound.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                    AdapterMyServiceOrder adapter=new AdapterMyServiceOrder(getActivity(),myOrderResponse.getData().getServiceDetaillist(),recyclerView,addFeedBackListner);
                                    recyclerView.setAdapter(adapter);

                                }else{
                                    lblNoDataFound.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.GONE);
                                }
                            }else{

                            }
                            break;
                        case AppSettings.ADD_FEEDBACK:
                            AddAddressResponse addAddressResponse = (AddAddressResponse) response.getResponseObject();
                            if(!addAddressResponse.isHasError()) {
                               ToastUtils.showToast(getActivity(),""+addAddressResponse.getMessage());
                                callApi();
                            }else{
                                ToastUtils.showToast(getActivity(),""+addAddressResponse.getMessage());
                            }
                            break;
                           default:
                            break;
                    }
                }}
        });
    }

    @Override
    public void addFeedBack(AddFeedbcakRequest addFeedbcakRequest) {
        addFeedBackApi(addFeedbcakRequest);
    }



}



