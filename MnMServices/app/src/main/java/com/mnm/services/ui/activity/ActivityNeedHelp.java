package com.mnm.services.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.mnm.services.R;
import com.mnm.services.constants.Constant;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.utils.GlobalUtils;


public class ActivityNeedHelp extends MyDrawerBaseActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_need_help);
        findViewById(R.id.root_relay).setOnClickListener(this);
        initCustomActionBarView(ActivityNeedHelp.this,"Need Help",true);
        findViewById(R.id.linlay_contact_us).setOnClickListener(this);
        findViewById(R.id.linlay_create_password).setOnClickListener(this);
        AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivityNeedHelp.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityNeedHelp.this, v);
                break;
            case R.id.linlay_contact_us:
                Intent mContactIntent=new Intent(ActivityNeedHelp.this,ActivityContactUs.class);
                startActivity(mContactIntent);
                break;
            case R.id.linlay_create_password:
                Intent mIntent=new Intent(ActivityNeedHelp.this,ActivityConfirmMobile.class);
                mIntent.putExtra(Constant.IS_COMING_FROM_CREATE_PASSWORD,true);
                startActivity(mIntent);
                break;
        }
    }
}