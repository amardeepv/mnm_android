package com.mnm.services.model.response.plan;

import java.io.Serializable;

/**
 * Created by saten on 4/10/17.
 */

public class PlanService implements Serializable{
    private String NoofCalls;
    private String ServiceGroupName;
    private int ServiceParentId;
    private float factor;
    private String rateType;
    private float additionalrate;
    private boolean isChecked=true;
    private boolean isCheckVisible;
    private String mNumberOfCalls;

    public String getmNumberOfCalls() {
        String numberOdCallss=getNoofCalls();
        if(getNoofCalls().equalsIgnoreCase("Unlimited")){
            numberOdCallss="99";

        }else{
            numberOdCallss=getNoofCalls();
        }
        return numberOdCallss;
    }

    public void setmNumberOfCalls(String mNumberOfCalls) {
        this.mNumberOfCalls = mNumberOfCalls;
    }

    public String getNoofCalls() {
        return NoofCalls;
    }

    public void setNoofCalls(String noofCalls) {
        NoofCalls = noofCalls;
    }

    public boolean isCheckVisible() {
        return isCheckVisible;
    }

    public void setCheckVisible(boolean checkVisible) {
        isCheckVisible = checkVisible;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public float getAdditionalrate() {
        return additionalrate;
    }

    public void setAdditionalrate(float additionalrate) {
        this.additionalrate = additionalrate;
    }

    public String getServiceGroupName() {
        return ServiceGroupName;
    }

    public void setServiceGroupName(String serviceGroupName) {
        ServiceGroupName = serviceGroupName;
    }

    public int getServiceParentId() {
        return ServiceParentId;
    }

    public void setServiceParentId(int serviceParentId) {
        ServiceParentId = serviceParentId;
    }

    public float getFactor() {
        return factor;
    }

    public void setFactor(float factor) {
        this.factor = factor;
    }
    public  float  getServiceAmount(float serviceCost){
        float amount = 0;
        if(isChecked) {
            amount = serviceCost + ((getFactor() * serviceCost / 100));
        }
        return amount;
    }

}
