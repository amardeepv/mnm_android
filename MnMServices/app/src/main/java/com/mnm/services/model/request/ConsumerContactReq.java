package com.mnm.services.model.request;

/**
 * Created by satendra.singh on 04-05-2017.
 */

public class ConsumerContactReq {
    private String alterNateContactNumber;

    public String getAlterNateContactNumber() {
        return alterNateContactNumber;
    }

    public void setAlterNateContactNumber(String alterNateContactNumber) {
        this.alterNateContactNumber = alterNateContactNumber;
    }
}
