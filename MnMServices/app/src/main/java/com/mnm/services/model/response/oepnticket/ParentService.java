package com.mnm.services.model.response.oepnticket;

import java.io.Serializable;
import java.util.List;

/**
 * Created by saten on 4/22/17.
 */

public class ParentService implements Serializable {

    private Integer serviceId;

    private String serviceName;

    private Integer totalCalls;

    private Integer usedCalls;

    private Integer remainingCalls;

    private List<ChildService> services = null;

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Integer getTotalCalls() {
        return totalCalls;
    }

    public void setTotalCalls(Integer totalCalls) {
        this.totalCalls = totalCalls;
    }

    public Integer getUsedCalls() {
        return usedCalls;
    }

    public void setUsedCalls(Integer usedCalls) {
        this.usedCalls = usedCalls;
    }

    public Integer getRemainingCalls() {
        return remainingCalls;
    }

    public void setRemainingCalls(Integer remainingCalls) {
        this.remainingCalls = remainingCalls;
    }

    public List<ChildService> getServices() {
        return services;
    }

    public void setServices(List<ChildService> services) {
        this.services = services;
    }
}
