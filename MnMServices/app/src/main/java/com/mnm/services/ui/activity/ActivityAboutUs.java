package com.mnm.services.ui.activity;

import android.os.Bundle;
import android.view.View;

import com.mnm.services.R;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.utils.GlobalUtils;


public class ActivityAboutUs extends MyDrawerBaseActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        findViewById(R.id.root_relay).setOnClickListener(this);
       initCustomActionBarView(ActivityAboutUs.this,"About Us",true);
        AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivityAboutUs.this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityAboutUs.this, v);
                break;
        }
    }
}