package com.mnm.services.ui.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.mnm.services.R;
import com.mnm.services.constants.Constant;
import com.mnm.services.model.response.oepnticket.Plan;
import com.mnm.services.ui.activity.ActivityOpenTicketPlansServices;
import java.util.List;
/**
 * Adapter to manage list of properties
 */
public class AdapterOpenTicket extends RecyclerView.Adapter<AdapterOpenTicket.ViewHolder> {
    private List<Plan> planList;
    private Context mContext;
    private View mItemLayoutView;
    private ViewHolder mViewHolder;
    private LinearLayoutManager linearLayoutManager;
    private String addresssId;


    public AdapterOpenTicket(Context ctx, List<Plan> planList, RecyclerView recyclerView,String addressId) {
        this.planList = planList;
        this.mContext = ctx;
        this.addresssId=addressId;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                }
            });
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("create","create view holder****");
        mItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_open_ticket, null);
        mViewHolder = new ViewHolder(mItemLayoutView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        final Plan  mPlan = planList.get(position);
        viewHolder.txtServiceName.setText(mPlan.getPlanName());
        viewHolder.txtServiceDescription.setText("wolf moon tempor, sunt aliqua put");
        viewHolder.imgService.setImageDrawable(getImage(mPlan.getPlanName(),mPlan.getPlanType()));
        viewHolder.mainLinlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                moveToNextScreen(planList.get(position));
            }
        });


    }

    private void moveToNextScreen(Plan plan) {
        Intent mIntnet=new Intent(mContext, ActivityOpenTicketPlansServices.class);
        mIntnet.putExtra(Constant.SELECTED_PLAN_DATA,plan);
        mIntnet.putExtra(Constant.ADDRESS_ID,addresssId);
        mContext.startActivity(mIntnet);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgService;
        private TextView txtServiceName;
        private TextView txtServiceDescription;
        private LinearLayout mainLinlay;
        public ViewHolder(View itemLayoutView) {
            super(itemLayoutView);
            imgService = (ImageView) itemLayoutView.findViewById(R.id.img_service);
            txtServiceDescription=(TextView) itemLayoutView.findViewById(R.id.txt_service_description);
            txtServiceName=(TextView)itemLayoutView.findViewById(R.id.txt_service_name);
            mainLinlay=(LinearLayout)itemLayoutView.findViewById(R.id.main_linlay);
        }
    }

    @Override
    public int getItemCount() {
        return planList.size();
    }

    Drawable getImage(String serviceNamee,String planType){
        // Log.e("serviceNamee ",serviceNamee);
        String serviceName=serviceNamee.toLowerCase();
        // Log.e("serviceName ",serviceName);
        Drawable icon=null;
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(planType.equalsIgnoreCase("U")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_unlimited);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_unlimited);
            }
        }else if(planType.equalsIgnoreCase("F")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_fixed_lock);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_fixed_lock);
            }
        }else if(planType.equalsIgnoreCase("A")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_addon);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_addon);
            }
        }
        else if(serviceName.contains("electrical")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_electrical);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_electrical);
            }
        } else if(serviceName.contains("plumbing")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_plumbing);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_plumbing);
            }

        }else if(serviceName.contains("carpentry")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_carpentry);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_carpentry);
            }

        }else if(serviceName.contains("pest control")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_pest_control);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_pest_control);
            }

        }else if(serviceName.contains("ac repair")||serviceName.contains("ac")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_acrepair);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_acrepair);
            }

        }else if(serviceName.contains("water tank cleaning")||serviceName.contains("watertank Cleaning")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_water_tank);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_water_tank);
            }

        }else if(serviceName.contains("security services")||serviceName.contains("security")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_security);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_security);
            }

        }else if(serviceName.contains("gardening")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_gardening);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_gardening);
            }

        }else if(serviceName.contains("cctv")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_cctv);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_cctv);
            }

        }else if(serviceName.contains("ro")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_ro);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_ro);
            }

        }else if(serviceName.contains("home cleaning")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_home_cleaning);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_home_cleaning);
            }

        }else if(serviceName.contains("home appliances")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_home_appliances);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_home_appliances);
            }

        }else if(serviceName.contains("upholstry cleaning")||serviceName.contains("Upholstery")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_uphole_history);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_uphole_history);
            }

        }else if(serviceName.contains("interior design")||serviceName.contains("interior")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_interier_design);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_interier_design);
            }

        }else if(serviceName.contains("Car Spa")||serviceName.contains("car")){
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_car_cleaning);
            } else {
                icon=mContext.getResources().getDrawable(R.mipmap.ic_car_cleaning);
            }

        }


        return icon;
    }

}