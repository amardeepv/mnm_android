package com.mnm.services.model.response;

import com.mnm.services.model.request.AutoCouponRequest;

import java.util.List;

/**
 * Created by saten on 4/6/17.
 */

public class DefaultCoupanRequest {
    private List<AutoCouponRequest> lstAutoCouponRequest = null;

    public List<AutoCouponRequest> getLstAutoCouponRequest() {
        return lstAutoCouponRequest;
    }

    public void setLstAutoCouponRequest(List<AutoCouponRequest> lstAutoCouponRequest) {
        this.lstAutoCouponRequest = lstAutoCouponRequest;
    }
}
