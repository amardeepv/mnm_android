package com.mnm.services.model.response;

import com.mnm.services.model.request.AddAddressModel;

import java.util.List;

/**
 * Created by satendra.singh on 05-04-2017.
 */

public class AddAddressResponse {
    private boolean hasError;
    private String messageCode;
    private String message;
    private AddAddressModel data;
    private List<String> messages = null;

    public boolean isHasError() {
        return hasError;
    }

    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AddAddressModel getData() {
        return data;
    }

    public void setData(AddAddressModel data) {
        this.data = data;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}
