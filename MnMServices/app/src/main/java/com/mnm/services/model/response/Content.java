package com.mnm.services.model.response;

/**
 * Created by saten on 2/11/17.
 */

public class Content {
    private Integer UserId;
    private String UserName;

    public Integer getUserId() {
        return UserId;
    }

    public void setUserId(Integer userId) {
        UserId = userId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }
}
