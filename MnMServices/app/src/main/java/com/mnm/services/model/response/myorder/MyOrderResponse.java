package com.mnm.services.model.response.myorder;

import java.util.List;

/**
 * Created by saten on 4/25/17.
 */

public class MyOrderResponse {

    private Boolean hasError;
    private String messageCode;
    private String message;
    private MyOrderData data;
    private List<Object> messages = null;

    public Boolean getHasError() {
        return hasError;
    }

    public void setHasError(Boolean hasError) {
        this.hasError = hasError;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MyOrderData getData() {
        return data;
    }

    public void setData(MyOrderData data) {
        this.data = data;
    }

    public List<Object> getMessages() {
        return messages;
    }

    public void setMessages(List<Object> messages) {
        this.messages = messages;
    }
}
