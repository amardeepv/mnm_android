package com.mnm.services.model.response.plan;

/**
 * Created by saten on 4/15/17.
 */

public class PlanBasketList {

    private String planDetailId;
    private String userId;
    private String serviceId;
    private String planId;
    private String durationId;
    private String factor;
    private String durationRate;
    private String planAmount;
    private String visitRequired;
    private String planDesc;
    private String couponId;
    private String couponDiscountAmt;
    private String addressId;
    private String serviceDate;
    private String created;
    private String noofCalls;

    public String getPlanDetailId() {
        return planDetailId;
    }

    public void setPlanDetailId(String planDetailId) {
        this.planDetailId = planDetailId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getDurationId() {
        return durationId;
    }

    public void setDurationId(String durationId) {
        this.durationId = durationId;
    }

    public String getFactor() {
        return factor;
    }

    public void setFactor(String factor) {
        this.factor = factor;
    }

    public String getDurationRate() {
        return durationRate;
    }

    public void setDurationRate(String durationRate) {
        this.durationRate = durationRate;
    }

    public String getPlanAmount() {
        return planAmount;
    }

    public void setPlanAmount(String planAmount) {
        this.planAmount = planAmount;
    }

    public String getVisitRequired() {
        return visitRequired;
    }

    public void setVisitRequired(String visitRequired) {
        this.visitRequired = visitRequired;
    }

    public String getPlanDesc() {
        return planDesc;
    }

    public void setPlanDesc(String planDesc) {
        this.planDesc = planDesc;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponDiscountAmt() {
        return couponDiscountAmt;
    }

    public void setCouponDiscountAmt(String couponDiscountAmt) {
        this.couponDiscountAmt = couponDiscountAmt;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(String serviceDate) {
        this.serviceDate = serviceDate;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getNoofCalls() {
        return noofCalls;
    }

    public void setNoofCalls(String noofCalls) {
        this.noofCalls = noofCalls;
    }
}
