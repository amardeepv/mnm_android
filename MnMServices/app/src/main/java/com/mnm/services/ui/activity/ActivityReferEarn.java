package com.mnm.services.ui.activity;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.controller.ControllerReferAndEarn;
import com.mnm.services.model.response.ReferAndEarnResponse;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.utils.GlobalUtils;


public class ActivityReferEarn extends MyDrawerBaseActivity implements View.OnClickListener {
private TextView referralCode;
    private TextView txtPrice;
    String shareMessage="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_and_earn);
        findViewById(R.id.root_relay).setOnClickListener(this);
        initCustomActionBarView(ActivityReferEarn.this,"Refer And Earn",true);
        referralCode=(TextView)findViewById(R.id.referral_code);
        txtPrice=(TextView)findViewById(R.id.txt_price);
        AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivityReferEarn.this);
     /*   findViewById(R.id.whatssapp).setOnClickListener(this);
        findViewById(R.id.fb).setOnClickListener(this);
        findViewById(R.id.twitter).setOnClickListener(this);
        findViewById(R.id.mail).setOnClickListener(this);
        findViewById(R.id.more).setOnClickListener(this);*/
        findViewById(R.id.btn_share).setOnClickListener(this);
        referralCodeApi();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityReferEarn.this, v);
                break;
           /* case R.id.whatssapp:
                GlobalUtils.shareApp(ActivityReferEarn.this,shareMessage);
                break;
            case R.id.fb:
                GlobalUtils.shareApp(ActivityReferEarn.this,shareMessage);
                break;
            case R.id.twitter:
                GlobalUtils.shareApp(ActivityReferEarn.this,shareMessage);
                break;
            case R.id.mail:
                GlobalUtils.shareApp(ActivityReferEarn.this,shareMessage);
                break;
            case R.id.more:
                GlobalUtils.shareApp(ActivityReferEarn.this,shareMessage);
                break;*/
            case R.id.btn_share:
                GlobalUtils.shareApp(ActivityReferEarn.this,shareMessage);
            default:
                break;
        }
    }
    private void referralCodeApi(){
        if (!ConnectivityUtils.isNetworkEnabled(ActivityReferEarn.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerReferAndEarn controllerReferAndEarn = new ControllerReferAndEarn(this, this);
            showProgressDialog("Please wait...");
            controllerReferAndEarn.getData(AppSettings.REFER_AND_EARN_EVENT, null);
        }
    }





    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
       removeProgressDialog();
        ActivityReferEarn.this.runOnUiThread(new Runnable() {
            public void run() {
                Log.e("Response", response.toString());
                if (response == null) {
                    ToastUtils.showToast(ActivityReferEarn.this, getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.REFER_AND_EARN_EVENT: {
                            ReferAndEarnResponse referAndEarnResponse = (ReferAndEarnResponse) response.getResponseObject();
                            if (!referAndEarnResponse.getHasError()) {
                                referralCode.setText(referAndEarnResponse.getData().getReferalCode());
                                txtPrice.setText("100 MNM Money");
                                shareMessage=referAndEarnResponse.getData().getReferalMessage();
                            } else {
                                ToastUtils.showToast(ActivityReferEarn.this, "" + referAndEarnResponse.getMessages());
                            }
                        }
                        break;

                        default:
                            break;
                    }

                }
            }
        });
    }
}