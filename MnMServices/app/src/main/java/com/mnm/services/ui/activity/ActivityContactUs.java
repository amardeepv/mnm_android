package com.mnm.services.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.mnm.services.R;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.utils.GlobalUtils;


public class ActivityContactUs extends MyDrawerBaseActivity implements View.OnClickListener {

    private  TextView txtPhoneOne;
    private  TextView txtPhoneTwo;
    private  TextView txtEmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        findViewById(R.id.root_relay).setOnClickListener(this);
        initCustomActionBarView(ActivityContactUs.this,"Contact Us",true);
        txtPhoneOne=(TextView) findViewById(R.id.txt_phone_one);
        txtPhoneOne.setOnClickListener(this);
        txtPhoneTwo=(TextView) findViewById(R.id.txt_phone_two);
        txtPhoneTwo.setOnClickListener(this);
        txtEmail=(TextView) findViewById(R.id.txt_email);
        txtEmail.setOnClickListener(this);
        AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivityContactUs.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityContactUs.this, v);
                break;
            case R.id.txt_phone_one:
                GlobalUtils.call(ActivityContactUs.this,txtPhoneOne.getText().toString());
                break;
            case R.id.txt_phone_two:
                GlobalUtils.call(ActivityContactUs.this,txtPhoneTwo.getText().toString());
                break;
            case R.id.txt_email:
                GlobalUtils.sendEmail(ActivityContactUs.this,txtEmail.getText().toString());
                break;
        }
    }
}