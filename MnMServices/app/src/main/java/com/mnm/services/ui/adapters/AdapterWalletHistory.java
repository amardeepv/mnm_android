package com.mnm.services.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kelltontech.utils.StringUtils;
import com.mnm.services.R;
import com.mnm.services.model.response.mywallet.WalletHistory;
import com.mnm.services.utils.GlobalUtils;

import java.util.List;

public class AdapterWalletHistory extends BaseAdapter {
    private Context context;
    List<WalletHistory> WalletHistory;

    public AdapterWalletHistory(Context context, List<WalletHistory> WalletHistory) {
        this.context = context;
        this.WalletHistory = WalletHistory;
    }

    @Override
    public int getCount() {
        return WalletHistory.size();
    }

    @Override
    public Object getItem(int position) {
        return WalletHistory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_wallet_history, null);
            holder = new ViewHolder();
            holder.txtDate=(TextView) convertView.findViewById(R.id.history_date);
            holder.txtAction=(TextView) convertView.findViewById(R.id.action);
            holder.txtAmt=(TextView) convertView.findViewById(R.id.amt);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final WalletHistory walletHistory=WalletHistory.get(position);
        String mDate=walletHistory.getDate();
        String mNewDate=mDate.substring(0,mDate.indexOf("T"));
        String myDate=GlobalUtils.getwalletDateString(mNewDate);
        Log.e("mydate ",myDate);
        holder.txtDate.setText(myDate);
        holder.txtAction.setText(""+walletHistory.getDescription());
        if(walletHistory.getDescription().contains("Credit")){
            holder.txtAmt.setTextColor(context.getResources().getColor(R.color.green_color));
        }else {
            holder.txtAmt.setTextColor(Color.RED);
        }
        holder.txtAmt.setText(context.getString(R.string.rupees_symbol)+" "+walletHistory.getAmount());
        return convertView;
    }

    class ViewHolder {
        private TextView txtDate;
        private TextView txtAction;
        private TextView txtAmt;
    }

}
