package com.mnm.services.ui.adapters;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;
import android.widget.TextView;
import com.mnm.services.R;
import com.mnm.services.listner.AddressInterface;
import com.mnm.services.listner.LocationInterface;
import com.mnm.services.model.response.Result;
import com.mnm.services.ui.activity.ActivityPlan;
import java.util.ArrayList;

public class AdapterLocation extends BaseAdapter {
    private Context context;
    private ArrayList<Result> addressList;
    ActivityPlan planActivity;
    LocationInterface addressInterface;


    public AdapterLocation(Context context, ArrayList<Result> addressList, LocationInterface addressInterface) {
        this.context = context;
        this.addressList = addressList;
        this.addressInterface=addressInterface;
        this.planActivity=planActivity;
    }

    @Override
    public int getCount() {
        return addressList.size();
    }

    @Override
    public Object getItem(int position) {
        return addressList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_location, null);
            holder = new ViewHolder();
            holder.txtLocationTxt=(TextView) convertView.findViewById(R.id.txt_location);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final Result addressModel=addressList.get(position);


        holder.txtLocationTxt.setText(addressModel.getFormatted_address());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addressInterface.setLocation(addressList.get(position));

            }
        });
        return convertView;
    }

    class ViewHolder {
        private TextView txtLocationTxt;
    }


    public ArrayList<Result>  getAddressList(){
        return addressList;
    }
    public  void setLocationList(ArrayList<Result>  locationList){
         addressList=locationList;
        notifyDataSetChanged();
    }
}
