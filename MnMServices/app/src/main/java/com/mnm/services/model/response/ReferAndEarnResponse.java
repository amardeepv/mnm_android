package com.mnm.services.model.response;

import java.util.List;

/**
 * Created by saten on 5/9/17.
 */

public class ReferAndEarnResponse {

    private Boolean hasError;

    private String messageCode;

    private String message;

    private ReferAndEarn data;

    private List<String> messages = null;

    public Boolean getHasError() {
        return hasError;
    }

    public void setHasError(Boolean hasError) {
        this.hasError = hasError;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ReferAndEarn getData() {
        return data;
    }

    public void setData(ReferAndEarn data) {
        this.data = data;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}
