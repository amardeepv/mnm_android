package com.mnm.services.reciever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;

import com.kelltontech.persistence.SharedPrefsUtils;
import com.mnm.services.constants.Constant;

import java.util.regex.Pattern;


/**
 * Created by amardeepvijay on 23/8/15.
 */
public class IncomingSmsReceiver extends BroadcastReceiver {

    // Get the object of SmsManager
    final SmsManager sms = SmsManager.getDefault();
    String message;
    public static String otp;
    Pattern p6 = Pattern.compile("(|^)\\d{6}");

    public void onReceive(Context context, Intent intent) {
        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

                    if (message != null) {
                        String otpString = message.substring(message.indexOf("OTP") + 4, message.indexOf("OTP") + 9);
                        //  otp = message.substring(3,8);
                        SharedPrefsUtils.setSharedPrefString(context, Constant.PREF_FILE_NAME, Constant.PREF_OTP, otpString);
                        context.sendBroadcast(new Intent("INTERNET_LOST"));
                    }

                   /* if (message.contains("Password(OTP)") && message.contains("Paytm")) {
                        ActivityVerifyMobile.otpRecieved(message.substring(0, 6));
                    } else {
                        return;
                    }*/
                } // end for loop
            } // bundle is null
        } catch (Exception e) {
        }
    }
}