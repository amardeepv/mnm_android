package com.mnm.services.ui.adapters;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.mnm.services.R;
import com.mnm.services.model.response.plan.PlanService;
import com.mnm.services.ui.activity.ActivityPlan;
import com.mnm.services.ui.fragment.FragmentFixedPlan;
import java.util.List;

public class AdapterPlan extends BaseAdapter {
    private Context context;
    private List<PlanService> addressList;
    ActivityPlan planActivity;
    FragmentFixedPlan unlimitedPlanFragment;
    boolean isFixedPlan=false;
int numberOfcallS=0;

    public AdapterPlan(Context context, List<PlanService> addressList,boolean isFixedPlan) {
        this.context = context;
        this.addressList = addressList;
        this.isFixedPlan=isFixedPlan;
        this.unlimitedPlanFragment=unlimitedPlanFragment;
        this.planActivity=planActivity;
        if(isFixedPlan) {
            for (PlanService planService : addressList) {
                numberOfcallS = numberOfcallS + Integer.parseInt(planService.getNoofCalls());
            }
        }
    }

    @Override
    public int getCount() {
        return addressList.size();
    }

    @Override
    public Object getItem(int position) {
        return addressList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_fixed_addon_plan, null);
            holder = new ViewHolder();
            holder.txtPlanName=(TextView) convertView.findViewById(R.id.txt_plan_name);
            holder.txtNoofCalls=(TextView)convertView.findViewById(R.id.txt_unlimited);
            holder.mview=(View)convertView.findViewById(R.id.mvieww);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final PlanService addressModel=addressList.get(position);
        holder.txtPlanName.setText(addressModel.getServiceGroupName());
        String numberOfcalls="";
        if(addressModel.getmNumberOfCalls().equalsIgnoreCase("99")){
            numberOfcalls="Unlimited"  ;
        }else{
            numberOfcalls=""+addressModel.getmNumberOfCalls();
        }

        holder.txtNoofCalls.setText(numberOfcalls);

        if(isFixedPlan){
            holder.mview.setVisibility(View.GONE);
            holder.txtNoofCalls.setVisibility(View.GONE);
            if(position==(addressList.size()/2)){
                holder.txtNoofCalls.setText(""+numberOfcallS+" Calls");
                holder.txtNoofCalls.setVisibility(View.VISIBLE);
            }
            if(position==addressList.size()){
                holder.mview.setVisibility(View.VISIBLE);
            }
        }else{
            holder.txtNoofCalls.setVisibility(View.VISIBLE);
            holder.mview.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    class ViewHolder {
        private TextView txtPlanName;
        private TextView txtNoofCalls;
        private View mview;

    }
}
