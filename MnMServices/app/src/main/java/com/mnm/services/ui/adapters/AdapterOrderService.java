package com.mnm.services.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.mnm.services.R;
import com.mnm.services.model.response.ServiceModel;
import java.util.ArrayList;
import java.util.List;

public class AdapterOrderService extends BaseAdapter {
    private Context context;
    List<ServiceModel> serviceList;

    public AdapterOrderService(Context context, ArrayList<ServiceModel> serviceList) {
        this.context = context;
        this.serviceList = serviceList;
    }

    @Override
    public int getCount() {
        return serviceList.size();
    }

    @Override
    public Object getItem(int position) {
        return serviceList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_order_history, null);
            holder = new ViewHolder();
            holder.txtServiceName=(TextView) convertView.findViewById(R.id.service_name);
            holder.txtServicePrice=(TextView) convertView.findViewById(R.id.service_price);
            holder.txtQuantity=(TextView)convertView.findViewById(R.id.txt_service_quantity);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final ServiceModel serviceModel=serviceList.get(position);
        holder.txtServiceName.setText(serviceModel.getServiceNameToDisplay());
        holder.txtServicePrice.setText(context.getString(R.string.rupees_symbol)+serviceModel.getRate());
        int quatity=0;
        if(serviceModel.getQuantity()==0){
            quatity=1;
        }else{
            quatity=serviceModel.getQuantity();
        }
        holder.txtQuantity.setText(""+quatity);


        return convertView;
    }

    class ViewHolder {
        private TextView txtServiceName;
        private TextView txtServicePrice;
        private TextView txtQuantity;
    }


}
