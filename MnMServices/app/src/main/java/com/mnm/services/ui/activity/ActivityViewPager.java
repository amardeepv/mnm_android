package com.mnm.services.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.mnm.services.R;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.adapters.AdapterBanner;
import com.mnm.services.ui.widgets.CirclePageIndicator;

public class ActivityViewPager extends AppCompatActivity implements View.OnClickListener {
    private TextView btnGetStarted;
    private CirclePageIndicator pageIndecator;
    TextView textview;
    ImageView icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);
        ViewPager bannerView = (ViewPager) findViewById(R.id.pager);
        btnGetStarted = (TextView) findViewById(R.id.txtview_get_started);
        AdapterBanner bannerAdapter = new AdapterBanner(ActivityViewPager.this);
        bannerView.setAdapter(bannerAdapter);
        pageIndecator = (CirclePageIndicator) findViewById(R.id.indicator);
        textview = (TextView) findViewById(R.id.textview);
        icon = (ImageView) findViewById(R.id.icon);
        icon.bringToFront();

        pageIndecator.setRadius(10);
        pageIndecator.setViewPager(bannerView);
        btnGetStarted.setOnClickListener(this);
        int value = bannerView.getCurrentItem();
        pageIndecator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                if (position == 4) {
                    HideSkipAndDots(position);
                } else {
                    ShowSKipAndDots(position);
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub
            }
        });
    }


    public void HideSkipAndDots(int position) {
        btnGetStarted.setVisibility(View.GONE);
        pageIndecator.setVisibility(View.GONE);
    }

    public void ShowSKipAndDots(int position) {
        if (position == 0) {
            textview.setText("Our security verified, skilled handymen are ready to serve you with any service you may require; be it electrical, plumbing, home appliance repair, car cleaning or anything else..");
        } else if (position == 1) {
            textview.setText("Enjoy the ease of payment Go cashless with our \"MnM wallet\" or pay with your Debit or Credit card. If not, you can always pay in cash.");
        } else if (position == 2) {
            textview.setText("Share us with your loved ones and show that you care. \n" +
                    "While we share our benefits(earn.....)with you to show we care ");
        } else if (position == 3) {
            textview.setText("MakenMake will make it right for you, avail home care services without hassle.");
        }
        btnGetStarted.setVisibility(View.VISIBLE);
        pageIndecator.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtview_get_started:
                AppSharedPreference.putBoolean(PrefConstants.IS_GET_STARTED_CLICKED, true, ActivityViewPager.this);
                ActivityViewPager.this.startActivity(new Intent(ActivityViewPager.this, ActivityHome.class));
                finish();
                break;
            default:
                break;
        }
    }
}
