package com.mnm.services.model.response.plan;

/**
 * Created by saten on 4/12/17.
 */

public class PlanDurationModel {
    private int durationId;
    private int servicePlanId;
    private Integer durainPeriod;
    private int factor;
    private float durationRate;

    public PlanDurationModel(int durationId, int servicePlanId, Integer durainPeriod, int factor, float durationRate) {
        this.durationId = durationId;
        this.servicePlanId = servicePlanId;
        this.durainPeriod = durainPeriod;
        this.factor = factor;
        this.durationRate = durationRate;
    }

    public int getDurationId() {
        return durationId;
    }

    public void setDurationId(int durationId) {
        this.durationId = durationId;
    }

    public int getServicePlanId() {
        return servicePlanId;
    }

    public void setServicePlanId(int servicePlanId) {
        this.servicePlanId = servicePlanId;
    }

    public Integer getDurainPeriod() {
        return durainPeriod;
    }

    public void setDurainPeriod(Integer durainPeriod) {
        this.durainPeriod = durainPeriod;
    }

    public int getFactor() {
        return factor;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }

    public float getDurationRate() {
        return durationRate;
    }

    public void setDurationRate(float durationRate) {
        this.durationRate = durationRate;
    }


}
