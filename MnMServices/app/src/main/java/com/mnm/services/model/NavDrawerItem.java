package com.mnm.services.model;

public class NavDrawerItem {

    public boolean isOnFront;
    private String title;
    private int icon;
    private int newIcon;
    private int imageType;
    private String count = "0";
    // boolean to set visiblity of the counter
    private boolean isCounterVisible = false;

    public NavDrawerItem() {
    }

    public NavDrawerItem(String title, int icon, int newIcon) {
        this.title = title;
        this.icon = icon;
        this.newIcon = newIcon;
    }

    public NavDrawerItem(String title, int icon, int newIcon, boolean isCounterVisible, String count) {
        this.title = title;
        this.icon = icon;
        this.newIcon = newIcon;
        this.isCounterVisible = isCounterVisible;
        this.count = count;
    }

    public int getNewIcon() {
        return newIcon;
    }

    public void setNewIcon(int newIcon) {
        this.newIcon = newIcon;
    }

    public int getImageType() {
        return imageType;
    }

    public void setImageType(int imageType) {
        this.imageType = imageType;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIcon() {
        return this.icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getCount() {
        return this.count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public boolean getCounterVisibility() {
        return this.isCounterVisible;
    }

    public void setCounterVisibility(boolean isCounterVisible) {
        this.isCounterVisible = isCounterVisible;
    }
}
