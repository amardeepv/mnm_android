package com.mnm.services.model.response.profile;

/**
 * Created by satendra.singh on 02-05-2017.
 */

public class Customer {
    private String userType;
    private String dob;
    private String gender;
    private String panNumber;
    private String phoneNumber;
    private String currentAddress;
    private Integer currentCountry;
    private Integer currentState;
    private Integer currentDistrict;
    private Integer currentCity;
    private String referralCode;
    private Integer referredBy;
    private Object AddressId;

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }

    public Integer getCurrentCountry() {
        return currentCountry;
    }

    public void setCurrentCountry(Integer currentCountry) {
        this.currentCountry = currentCountry;
    }

    public Integer getCurrentState() {
        return currentState;
    }

    public void setCurrentState(Integer currentState) {
        this.currentState = currentState;
    }

    public Integer getCurrentDistrict() {
        return currentDistrict;
    }

    public void setCurrentDistrict(Integer currentDistrict) {
        this.currentDistrict = currentDistrict;
    }

    public Integer getCurrentCity() {
        return currentCity;
    }

    public void setCurrentCity(Integer currentCity) {
        this.currentCity = currentCity;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public Integer getReferredBy() {
        return referredBy;
    }

    public void setReferredBy(Integer referredBy) {
        this.referredBy = referredBy;
    }

    public Object getAddressId() {
        return AddressId;
    }

    public void setAddressId(Object addressId) {
        AddressId = addressId;
    }
}
