package com.mnm.services.constants;

public interface Constant {
    String USER_ID="user_id";
    String MOBILE_NUMBER="mobile_number";
    public String PREF_FILE_NAME = "MnMServices";
    public String PREF_OTP = "otp";
    String categoryFileName = "category.json";
    long ONE_HOUR=60*60*1000;
    long FOUR_DAYS= 96*ONE_HOUR;
    String CATEGORY_MODEL_DATA="category_model_data";
    String SERVICE_MODEL_DATA="service_model_data";
    String SERVICE_SELECTED_DATA="service_selected_data";
    String IS_SHOW_OFFER_TRUE="is_show_offer_true";
    String DATE_AND_YEAR="date_n_year";
    String TIME_SLOT="time_slot";
    String ADDRESS_ID="adress_id";
    String FEEDBACK="feedback";
    String AMOUNT_PAYBLE="amount_payble";
    String TAX_RATE="tax_rate";
    String TAX_AMOUNT="tax_amount";
    String TOTAL_BILL_AFTER_AMOUNT="total+after_amount";
    String WALLET_AMOUNT="wallet_amount";
    String INVOICE_NUMBER="invoice_number";
    String SERVICE_NAME="service_name";
    String TIME_SLOT_DATA="time_slot_data";
    String IS_PLAN="is_plan";
    String IS_FIXED_PLAN="is_fixed_plan";
    String PLAN_SERVICE_LIST="unlimited_plan_service_list";
    String UNLIMITED_PLAN_MODEL="unlimited_plan_model";
    String PLAN_TYPE="plan_type";
    int PLAN_TYPE_UNLIMITED=1;
    int PLAN_TYPE_FIXED=2;
    int PLAN_TYPE_ADD_ON=3;

    //plan_faq
    String UNLIMITED_PLAN="Unlimited Plan";
    String FIXED_PLAN="Fixed Plan";
    String   ADDON_PLAN="Add on Plan";

    //service faq
    String CAR_SPA_FAQ="Car Spa";
    String HOME_CLEANING_FAQ="HOME_CLEANING_FAQ";
    String UPHOLESTREY_CLEANING_FAQ="UPHOLESTREY_CLEANING_FAQ";
    String AC_SERVICES_FAQ="AC_SERVICES_FAQ";
    String ELECTRICAL_SERVICES_FAQ="ELECTRICAL_SERVICES_FAQ";
    String PLUMBING_SERVICES_FAQ="PLUMBING_SERVICES_FAQ";
    String CARPENTRY_SERVICES_FAQ ="CARPENTRY_SERVICES_FAQ";
    String SECURITY_SERVICE_FAQ="SECURITY_SERVICE_FAQ";
    String PEST_CONTROL_SERVICES_FAQ="PEST_CONTROL_SERVICES_FAQ";
    String WATER_TANK_CLEANING_SERVICE_FAQ="WATER_TANK_CLEANING_SERVICE_FAQ";
    String INTERIOR_DESIGN_FAQ="INTERIOR_DESIGN_FAQ";
    String CCTV_SERVICES_FAQ="CCTV_SERVICES_FAQ";
    String GARDENING_SERVICES_FAQ="GARDENING_SERVICES_FAQ";
    String PAINT_SERVICES_FAQ="PAINT_SERVICES_FAQ";
    String RO_SERVICE_FAQ="RO_SERVICE_FAQ";
    String HOME_APPLIANCE_FAQ="HOME_APPLIANCE_FAQ";

    String QUESTION="question";
    String  ANSWER="answer";
    String  CHILD_ARRAY="child_array";
    String  POINT="point";
    String  SERVICE_GRP_NAME="service_grp_name";

    String  IS_OPEN_TICKET_TRUE="is_open_ticket_true";
    String  OPEN_TICKET_SERVICE_DATA="open_ticket_service_data";
    String PLAN_ID="plan_id";
    String ADD_TO_BASKET_REFERENCE_ID="add_to_basket_reference_id";

    int CURRENT_ORDER=1;
    int PAST_ORDER=2;
    String ORDER_TYPE="order_type";
    String PLAN_NAME="plan_namee";
    String IS_COMING_FROM_CREATE_PASSWORD="is_coming_from_create_password";
    String IS_COMING_FROM_DRAWER="is_coming_from_drawer";
    String LOGIN_TAB_NUMBER="login_tab_number";
    String SELECTED_PLAN_DATA="selected_plan_data";
    String SINGLE_ADDRESS_OPEN_TICKET_DATA="single_address_open_ticker_data";
String IS_OLD_USER="is_old_user";

    int ELECTRICAL_SERVICE=1;
    int PLUMBING_SERVICE=3;
    int AC_SERVICE=7;
    int CARPENTRY_SERVICE=12;
    int PEST_CONTROL_SERVICE=16;
    int WATER_TANK_SERVICE=19;
    int CAR_SPA_SERVICE=23;
    int ROO_SERVICE=28;
    int CCTV_SERVICE=29;
    int PAINT_SERVICE=33;
    int HOME_CLEANING_SERVICE=35;
    int UPHOLESTRY_SERVICE=39;
    int GARDENING_SERVICE=42;
    int HOME_APPLIANCE_SERVICE=44;
    int WOOD_WORK_SERVICE=59;
    int INTERIER_DESIGN_SERVICE=66;
    int SECURITY_SERVICE=1044;

}

