package com.mnm.services.model.request;

import java.util.List;

/**
 * Created by saten on 5/2/17.
 */

public class UpdateConsumerRequest {

    private String userId;

    private String firstName;

    private String lastName;

    private String emailId;

    private String dob;

    private String gender;

    private String mobileNumber;
    private List<ConsumerContactReq> consumerContact = null;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public List<ConsumerContactReq> getConsumerContact() {
        return consumerContact;
    }

    public void setConsumerContact(List<ConsumerContactReq> consumerContact) {
        this.consumerContact = consumerContact;
    }
}
