package com.mnm.services.model.response.address;

import java.util.List;

/**
 * Created by satendra.singh on 31-03-2017.
 */

public class AddressData {
    private List<AddressModel> lstAddressResult = null;

    public List<AddressModel> getLstAddressResult() {
        return lstAddressResult;
    }

    public void setLstAddressResult(List<AddressModel> lstAddressResult) {
        this.lstAddressResult = lstAddressResult;
    }
}
