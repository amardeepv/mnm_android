package com.mnm.services.model.response.myorder;

import java.util.List;

/**
 * Created by saten on 4/25/17.
 */

public class ServiceData {
    private boolean isDescriptioonClicked;
    private boolean isFeedBackClicked;
    private boolean isHistoryClicked;
    private boolean isFeedbackDisplayClicked;
    private String feedBacktxtMesaage;
    private int feedbackRating;

    public String getFeedBacktxtMesaage() {
        return feedBacktxtMesaage;
    }

    public void setFeedBacktxtMesaage(String feedBacktxtMesaage) {
        this.feedBacktxtMesaage = feedBacktxtMesaage;
    }

    public int getFeedbackRating() {
        return feedbackRating;
    }

    public void setFeedbackRating(int feedbackRating) {
        this.feedbackRating = feedbackRating;
    }



    public boolean isDescriptioonClicked() {
        return isDescriptioonClicked;
    }

    public void setDescriptioonClicked(boolean descriptioonClicked) {
        isDescriptioonClicked = descriptioonClicked;
    }

    public boolean isFeedBackClicked() {
        return isFeedBackClicked;
    }

    public void setFeedBackClicked(boolean feedBackClicked) {
        isFeedBackClicked = feedBackClicked;
    }

    public boolean isHistoryClicked() {
        return isHistoryClicked;
    }

    public void setHistoryClicked(boolean historyClicked) {
        isHistoryClicked = historyClicked;
    }

    public boolean isFeedbackDisplayClicked() {
        return isFeedbackDisplayClicked;
    }

    public void setFeedbackDisplayClicked(boolean feedbackDisplayClicked) {
        isFeedbackDisplayClicked = feedbackDisplayClicked;
    }

    private boolean isDetailBtnClicked;
    private String ServiceName;
    private Integer Amount;
    private String ServiceDescription;
    private String Created;
    private Integer OrderID;
    private Integer TicketID;
    private String Expirationdate;
    private String EngineerName;
    private String ticketDescription;
    private String ticketStatus;
    private String paymentStatus;

    public String getServiceParentName() {
        return ServiceParentName;
    }

    public void setServiceParentName(String serviceParentName) {
        ServiceParentName = serviceParentName;
    }

    private String engineerImagePath;
    private String ServiceParentName;

    public boolean isDetailBtnClicked() {
        return isDetailBtnClicked;
    }

    public void setDetailBtnClicked(boolean detailBtnClicked) {
        isDetailBtnClicked = detailBtnClicked;
    }

    private String rating;
    private List<TicketHistory> ticketHistoryList = null;
    private List<FeedbackHistory> feedbackHistoryList = null;

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public Integer getAmount() {
        return Amount;
    }

    public void setAmount(Integer amount) {
        Amount = amount;
    }

    public String getServiceDescription() {
        return ServiceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        ServiceDescription = serviceDescription;
    }

    public String getCreated() {
        return Created;
    }

    public void setCreated(String created) {
        Created = created;
    }

    public Integer getOrderID() {
        return OrderID;
    }

    public void setOrderID(Integer orderID) {
        OrderID = orderID;
    }

    public Integer getTicketID() {
        return TicketID;
    }

    public void setTicketID(Integer ticketID) {
        TicketID = ticketID;
    }

    public String getExpirationdate() {
        return Expirationdate;
    }

    public void setExpirationdate(String expirationdate) {
        Expirationdate = expirationdate;
    }

    public String getEngineerName() {
        return EngineerName;
    }

    public void setEngineerName(String engineerName) {
        EngineerName = engineerName;
    }

    public String getTicketDescription() {
        return ticketDescription;
    }

    public void setTicketDescription(String ticketDescription) {
        this.ticketDescription = ticketDescription;
    }

    public String getTicketStatus() {
        return ticketStatus;
    }

    public void setTicketStatus(String ticketStatus) {
        this.ticketStatus = ticketStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getEngineerImagePath() {
        return engineerImagePath;
    }

    public void setEngineerImagePath(String engineerImagePath) {
        this.engineerImagePath = engineerImagePath;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public List<TicketHistory> getTicketHistoryList() {
        return ticketHistoryList;
    }

    public void setTicketHistoryList(List<TicketHistory> ticketHistoryList) {
        this.ticketHistoryList = ticketHistoryList;
    }

    public List<FeedbackHistory> getFeedbackHistoryList() {
        return feedbackHistoryList;
    }

    public void setFeedbackHistoryList(List<FeedbackHistory> feedbackHistoryList) {
        this.feedbackHistoryList = feedbackHistoryList;
    }
}
