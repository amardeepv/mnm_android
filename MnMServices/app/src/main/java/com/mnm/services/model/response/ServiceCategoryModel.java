package com.mnm.services.model.response;

import java.io.Serializable;
import java.util.List;

/**
 * Created by saten on 3/7/17.
 */

public class ServiceCategoryModel implements Serializable {
    private Integer serviceGroupId;
    private Integer parentID;
    private String serviceGroupName;
    private String description;
    private String videoUrl;
    private Boolean showOffer;
    private List<StimeSlotList> stimeSlotList = null;

    public Integer getServiceGroupId() {
        return serviceGroupId;
    }

    public void setServiceGroupId(Integer serviceGroupId) {
        this.serviceGroupId = serviceGroupId;
    }

    public Integer getParentID() {
        return parentID;
    }

    public void setParentID(Integer parentID) {
        this.parentID = parentID;
    }

    public String getServiceGroupName() {
        return serviceGroupName;
    }

    public void setServiceGroupName(String serviceGroupName) {
        this.serviceGroupName = serviceGroupName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public Boolean getShowOffer() {
        return showOffer;
    }

    public void setShowOffer(Boolean showOffer) {
        this.showOffer = showOffer;
    }

    public List<StimeSlotList> getStimeSlotList() {
        return stimeSlotList;
    }

    public void setStimeSlotList(List<StimeSlotList> stimeSlotList) {
        this.stimeSlotList = stimeSlotList;
    }
}
