package com.mnm.services.ui.activity;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.Constant;
import com.mnm.services.controller.ControllerOpenTicket;
import com.mnm.services.model.response.oepnticket.OpenTicketResponse;
import com.mnm.services.model.response.oepnticket.Plan;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.adapters.AdapterOpenTicket;
import com.mnm.services.utils.GlobalUtils;
import java.util.List;
import static com.mnm.services.constants.Constant.USER_ID;


public class ActivityOpenTicketPlans extends MyDrawerBaseActivity implements View.OnClickListener {
    boolean isOpenTicketApiCalled;
    private RecyclerView recycleView;
    AdapterOpenTicket adapterOpenTicket;
    int mPlanId=0;
    TextView lblNoDataFound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_ticket_plans);
        recycleView=(RecyclerView)findViewById(R.id.recycle_view);
        recycleView.setLayoutManager(new LinearLayoutManager(ActivityOpenTicketPlans.this));
        lblNoDataFound=(TextView)findViewById(R.id.lbl_no_data_found);
        lblNoDataFound.setVisibility(View.GONE);
        findViewById(R.id.root_relay).setOnClickListener(this);
        initCustomActionBarView(ActivityOpenTicketPlans.this, getResources().getString(R.string.purchased_service)+" Plans",true);
        AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivityOpenTicketPlans.this);
        callOpenTicketApi(getIntent().getStringExtra(Constant.ADDRESS_ID));
    }



    void  callOpenTicketApi(String addressId){
        if (!ConnectivityUtils.isNetworkEnabled(ActivityOpenTicketPlans.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerOpenTicket openTicketController = new ControllerOpenTicket(this, this, addressId);
            showProgressDialog("Please wait...");
            openTicketController.getData(AppSettings.OPEN_TICKET_EVENT, null);
        }

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityOpenTicketPlans.this, v);
                break;
        }
    }


    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        ActivityOpenTicketPlans.this.runOnUiThread(new Runnable() {
            public void run() {
                removeProgressDialog();
                Log.e("Response", response.toString());
                if (response == null) {
                    ToastUtils.showToast(ActivityOpenTicketPlans.this, getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.OPEN_TICKET_EVENT: {
                            isOpenTicketApiCalled=true;
                            OpenTicketResponse openTicketResponse = (OpenTicketResponse) response.getResponseObject();
                            if (!openTicketResponse.getHasError()) {
                                if(openTicketResponse.getData()!=null&&openTicketResponse.getData().getPlans()!=null&&openTicketResponse.getData().getPlans().size()>0) {
                                   /* mPlanId= openTicketResponse.getData().getPlans().get(0).getPlanId();*/
                                    setAdapter(openTicketResponse.getData().getPlans());
                                    lblNoDataFound.setVisibility(View.GONE);
                                    recycleView.setVisibility(View.VISIBLE);
                                }else{
                                    lblNoDataFound.setVisibility(View.VISIBLE);
                                    recycleView.setVisibility(View.GONE);
                                }

                            }else{
                                lblNoDataFound.setVisibility(View.VISIBLE);
                                ToastUtils.showToast(ActivityOpenTicketPlans.this,""+openTicketResponse.getMessage());
                            }
                        }
                        break;
                        default:
                            break;
                    }

                }
            }
        });
    }

    void setAdapter(List<Plan> Plans){
        adapterOpenTicket = new AdapterOpenTicket(ActivityOpenTicketPlans.this,Plans,recycleView,getIntent().getStringExtra(Constant.ADDRESS_ID));
        recycleView.setAdapter(adapterOpenTicket);
    }
}