package com.mnm.services.ui.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.Constant;
import com.mnm.services.controller.ControllerPlan;
import com.mnm.services.model.response.plan.PlanModel;
import com.mnm.services.model.response.plan.UnlimitedPlanResponse;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.fragment.FragmentFaq;
import com.mnm.services.ui.fragment.FragmentPlanDescription;
import com.mnm.services.ui.tabs.SlidingTabLayout;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class ActivityPlan extends MyDrawerBaseActivity  {
    PagerAdapter pagerAdapter;
   public boolean isShowProgressDialog=true;
    String planName="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan);
        intiUi(getIntent().getIntExtra(Constant.PLAN_TYPE,0));
        AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivityPlan.this);
    }

    private void intiUi(int planType) {

        if(planType==Constant.PLAN_TYPE_UNLIMITED){
            planName=getString(R.string.unlimited_plan);
        }else if(planType==Constant.PLAN_TYPE_FIXED){
            planName=getString(R.string.fixed_plan);
        }else if(planType==Constant.PLAN_TYPE_ADD_ON){
            planName=getString(R.string.add_on_plan);
        }
        initCustomActionBarView(ActivityPlan.this,planName,true);
        planApi(planType);
    }

    void planApi(int planType) {
        if (!ConnectivityUtils.isNetworkEnabled(ActivityPlan.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            showProgressDialog(getString(R.string.please_wait));
            ControllerPlan loginController = new ControllerPlan(this, this,planType);
            loginController.getData(AppSettings.PLAN_EVENT, "");
        }
    }

    private void initViewPagerAndTabs( List<PlanModel> data) {
        FragmentPlanDescription unlimitedPlanDescriptionFragment=new FragmentPlanDescription();
        Bundle bundle=new Bundle();
        bundle.putSerializable(Constant.PLAN_SERVICE_LIST, (Serializable) data);
        bundle.putInt(Constant.PLAN_TYPE,getIntent().getIntExtra(Constant.PLAN_TYPE,0));
        bundle.putString(Constant.PLAN_NAME,planName);
        unlimitedPlanDescriptionFragment.setArguments(bundle);
        FragmentFaq faqFragment=new FragmentFaq();
        faqFragment.setArguments(bundle);
        ViewPager  viewPager = (ViewPager) findViewById(R.id.viewPager);
        pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        String[] tabs = getResources().getStringArray(R.array.tabs);
        pagerAdapter.addFragment(unlimitedPlanDescriptionFragment, tabs[0]);
        pagerAdapter.addFragment(faqFragment, tabs[1]);
        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(pagerAdapter);
        SlidingTabLayout tabLayout = (SlidingTabLayout) findViewById(R.id.tabLayout);
        tabLayout.setDistributeEvenly(true);
        tabLayout.setViewPager(viewPager);
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white_color);          }
        });

        tabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        ActivityPlan.this.runOnUiThread(new Runnable() {
            public void run() {
                if (response == null) {
                    ToastUtils.showToast(ActivityPlan.this, getString(R.string.server_is_down_please_try_later));
                    removeProgressDialog();
                    finish();
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.PLAN_EVENT:
                            UnlimitedPlanResponse unlimitedPlanResponse = (UnlimitedPlanResponse) response.getResponseObject();
                            if(!unlimitedPlanResponse.getHasError()) {
                                if(unlimitedPlanResponse.getData()!=null&&unlimitedPlanResponse.getData().size()>0) {
                                    initViewPagerAndTabs(unlimitedPlanResponse.getData());

                                }else{
                                    ToastUtils.showToast(ActivityPlan.this,getString(R.string.no_plan_found));
                                    removeProgressDialog();
                                    finish();
                                }
                            }else{
                                ToastUtils.showToast(ActivityPlan.this,unlimitedPlanResponse.getMessage());
                                removeProgressDialog();
                                finish();
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivityPlan.this);
        Intent i = new Intent(ActivityPlan.this, ActivityHome.class);
// set the new task and clear flags
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

}