package com.mnm.services.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.StringUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.Constant;
import com.mnm.services.controller.ControllerAddAddress;
import com.mnm.services.controller.ControllerAddress;
import com.mnm.services.controller.ControllerBuyOpenTicket;
import com.mnm.services.controller.ControllerOpenTicket;
import com.mnm.services.controller.ControllerSearchLocation;
import com.mnm.services.interfaces.DialogBtnClick;
import com.mnm.services.listner.AddressInterface;
import com.mnm.services.listner.LocationInterface;
import com.mnm.services.model.SelectedServicesResponse;
import com.mnm.services.model.request.AddAddressModel;
import com.mnm.services.model.request.openticketrequest.LstOpenRequest;
import com.mnm.services.model.request.openticketrequest.OpenTicketRequest;
import com.mnm.services.model.response.AddAddressResponse;
import com.mnm.services.model.response.AddressComponent;
import com.mnm.services.model.response.Day;
import com.mnm.services.model.response.LocationSearchResponse;
import com.mnm.services.model.response.Result;
import com.mnm.services.model.response.ServiceModel;
import com.mnm.services.model.response.StimeSlotList;
import com.mnm.services.model.response.Week;
import com.mnm.services.model.response.address.AddressModel;
import com.mnm.services.model.response.address.AddressResponse;
import com.mnm.services.model.response.oepnticket.BuyOpenTicketResponse;
import com.mnm.services.model.response.oepnticket.ChildService;
import com.mnm.services.model.response.oepnticket.OpenTicketResponse;
import com.mnm.services.model.response.oepnticket.ParentService;
import com.mnm.services.model.response.oepnticket.Plan;
import com.mnm.services.model.response.profile.PlanIdServiceid;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.adapters.AdapterLocation;
import com.mnm.services.ui.adapters.AddressAdapter;
import com.mnm.services.ui.widgets.NonScrollListView;
import com.mnm.services.utils.GlobalUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.mnm.services.R.id.btn_continue;
import static com.mnm.services.constants.AppSettings.BUY_OPEN_TICKET_EVENT;

public class ActivitySheduleBooking extends MyDrawerBaseActivity implements View.OnClickListener,AddressInterface,DialogBtnClick ,LocationInterface{
    private TextView txv_back, txv_month, txv_up;
    private LinearLayout btnContinue;
    private LinearLayout lnr_main;
    private TextView tvDateTxt, tvDayTxt;
    private Map<Integer, Week> weeksMap = new LinkedHashMap<Integer, Week>();
    private Map<Integer, Day> dayMap = new HashMap<Integer, Day>();
    private Week selectedWeek = null;
    //private Day selectedDay = null;
    private Calendar currentDate = Calendar.getInstance();
    private Calendar startDate = null;
    private Calendar endDate = null;
    private SimpleDateFormat sdfSlotValue = new SimpleDateFormat("dd-mm-yy hh:mm a");


    //time slot UI
    private RelativeLayout relayMorning, relayAfterNoon, relayEvening;
    private TextView lblMorning, lblAfterNoon, lblEvening;
    private ImageView imgMorning, imgAfterNoon, imgEvening;
    private String clickedDate = "";
    private String selectedTimeSlot = "";
    private String selectedAddressline1 = "";
    private String selectedAddressline2 = "";
    private String addressId="";

    private AddressAdapter addressAdapter;

    //case 1 when user is not login/register
    private RelativeLayout relauUserLoginRegisterOption;
    // case 2 when user is login/register but not having address
    private LinearLayout headerAddAddress;
    private RelativeLayout relayAddAddress;
    private EditText addressLineOne; AutoCompleteTextView addressLinetwo;
    private RelativeLayout btnAddNewAddress;
    private TextView txtSelectAddressAddAddress;
    //case 3 user login/register and having address
    private NonScrollListView nonScrollListView;
    Boolean isApiCalled = false;
   ArrayAdapter<String> adapterAddress;
    String mCurrentDate="";
    private HorizontalScrollView scrollViewHorizontal;
    private String mCDate=null;
    private String mSDate=null;
    private String mCurrentDDate;
    private String mSelectedDate;
    Date dateObjCurrent=null;
    private TextView txtAddAddress;
    private EditText txtFeedback;
    private List<StimeSlotList> timeSlotList;
    private RelativeLayout addressMainHeader;
    int selectedMonth;
    String selectedDay;
    String selectedYear;
    private  ArrayList<PlanIdServiceid> alreadyPurchasedServices=new ArrayList<>();
    RelativeLayout addMoreRelay;
    DialogBtnClick dialogBtnClick;
    private  String mLatitude="0";
    private String mLongitude="0";
    private String mState="",mCity="",subLocality="";
    private LocationInterface locationInterface;
    ArrayList<Result>   locationAddressList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shedule_booking);

        //case 1 when user is not login
        relauUserLoginRegisterOption = (RelativeLayout) findViewById(R.id.relay_user_option);
        findViewById(R.id.relay_existing_user).setOnClickListener(this);
        findViewById(R.id.relay_new_user).setOnClickListener(this);
        scrollViewHorizontal=(HorizontalScrollView)findViewById(R.id.scroll_view);
         addMoreRelay=(RelativeLayout)findViewById(R.id.add_more_item);
        addMoreRelay.setOnClickListener(this);
        txtAddAddress=(TextView)findViewById(R.id.ttx_add_address);
        addressMainHeader=(RelativeLayout)findViewById(R.id.addres_main_header);
           txtFeedback=(EditText) findViewById(R.id.feedback_txt);
        dialogBtnClick=this;
        //case 2 when user logon/register but not having address
        //show header
        headerAddAddress = (LinearLayout) findViewById(R.id.linlay_header_add_address);
        btnAddNewAddress = (RelativeLayout) findViewById(R.id.btn_add_address_new);
        btnAddNewAddress.setOnClickListener(this);
        relayAddAddress = (RelativeLayout) findViewById(R.id.relay_address);
        txtSelectAddressAddAddress = (TextView) findViewById(R.id.txt_select_address_add_address);
        addressLineOne = (EditText) findViewById(R.id.address_line_one);
        addressLinetwo = (AutoCompleteTextView) findViewById(R.id.address_line_two);

        //case 3 when user is Login/register and also having address
        nonScrollListView = (NonScrollListView) findViewById(R.id.non_scroll_listview);
       locationInterface=this;

        // Time Slot Ui
        relayMorning = (RelativeLayout) findViewById(R.id.relay_morning);
        relayAfterNoon = (RelativeLayout) findViewById(R.id.relay_afternoon);
        relayEvening = (RelativeLayout) findViewById(R.id.relay_evening);
        imgEvening = (ImageView) findViewById(R.id.img_morning);
        imgAfterNoon = (ImageView) findViewById(R.id.img_afternoon);
        imgEvening = (ImageView) findViewById(R.id.img_evening);
        lblMorning = (TextView) findViewById(R.id.lbl_morning);
        lblAfterNoon = (TextView) findViewById(R.id.lbl_after_noon);
        lblEvening = (TextView) findViewById(R.id.lbl_evening);
        relayMorning.setOnClickListener(this);
        relayAfterNoon.setOnClickListener(this);
        relayEvening.setOnClickListener(this);
        addressId=getIntent().getStringExtra(Constant.ADDRESS_ID);
        initCustomActionBarView(this, "Schedule Booking", true);
        setupMonth(null);
        init();

        addressLinetwo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence cs, int i, int i1, int i2) {
                if (!StringUtils.isNullOrEmpty(cs.toString())) {
                    searchLocationApi(cs.toString());
                    //addressLinetwo.setEnabled(false);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        addressLinetwo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
               Result mResult=locationAddressList.get(pos);
                if(mResult.getGeometry()!=null){
                    mLongitude=mResult.getGeometry().getLocation().getLng();
                    mLatitude=mResult.getGeometry().getLocation().getLat();
                }
                if(mResult.getAddress_components().size()>=4){
                    mState=mResult.getAddress_components().get(3).getLong_name();
                }
                if(mResult.getAddress_components().size()>=2){
                    mCity=mResult.getAddress_components().get(1).getLong_name();
                }
                if(mResult.getAddress_components().size()>0){
                    subLocality=mResult.getAddress_components().get(0).getLong_name();
                }
            }
        });

        //getting current date
        Calendar currenCalender = Calendar.getInstance();
        SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-mm-dd");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-mm-dd");
        mCurrentDate = df.format(currenCalender.getTime());
        try {
            dateObjCurrent= curFormater.parse(mCurrentDate);
            mCDate=dateObjCurrent.getDate()+"/"+dateObjCurrent.getDay()+"/"+dateObjCurrent.getYear();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        storeDataInPreference((ArrayList<ServiceModel>)getIntent().getSerializableExtra(Constant.SERVICE_SELECTED_DATA));
         timeSlotList=(ArrayList<StimeSlotList>)getIntent().getSerializableExtra(Constant.TIME_SLOT_DATA);

    }
    void searchLocationApi(String searchTxt) {
        if (!ConnectivityUtils.isNetworkEnabled(ActivitySheduleBooking.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerSearchLocation searchLocationController = new ControllerSearchLocation(this, this, searchTxt);
            searchLocationController.getData(AppSettings.LOCATION_SEARCH_REQUEST, "");
        }
    }
    private void callAddressApi() {
        if (!ConnectivityUtils.isNetworkEnabled(ActivitySheduleBooking.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerAddress addressController = new ControllerAddress(this, this);
            showProgressDialog("Please wait...");
            addressController.getData(AppSettings.SERVICE_ADDRESS_REQUEST, null);
        }
    }

    private void setupMonth(Calendar monthDate) {
        Calendar mDate = monthDate;
        if (monthDate == null) {
            mDate = Calendar.getInstance();
        }
        weeksMap.clear();
        dayMap.clear();
        startDate = Calendar.getInstance();
        startDate.setTime(mDate.getTime());
        startDate.set(Calendar.DAY_OF_MONTH, 1);
        endDate = Calendar.getInstance();
        endDate.setTime(mDate.getTime());
        endDate.add(Calendar.MONTH, 1);
        endDate.set(Calendar.DAY_OF_MONTH, 1);
        endDate.add(Calendar.DATE, -1);
        int emptyDays = startDate.get(Calendar.DAY_OF_WEEK);
        Week week = new Week(1);
        for (int i = 1; i < emptyDays; i++) {
            week.addDay(new Day());
        }
        weeksMap.put(week.getSequence(), week);
        Calendar dt = (Calendar) startDate.clone();
        Calendar ed = (Calendar) startDate.clone();
        ed.add(Calendar.MONTH, 1);
        for (; dt.before(ed); dt.add(Calendar.DATE, 1)) {
            Day day = new Day((Calendar) dt.clone());
            week.addDay(day);
            dayMap.put(day.getDate().get(Calendar.DAY_OF_MONTH), day);
            if (dt.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                int seq = week.getSequence();
                seq++;
                week = new Week(seq);
                weeksMap.put(seq, week);
            }
        }
    }


    private void init() {
        try {
            Calendar c = Calendar.getInstance();
            System.out.println("Current time => " + c.getTime());
            SimpleDateFormat df = new SimpleDateFormat("yyyy-mm-dd");
            txv_back = (TextView) findViewById(R.id.txv_back);
            txv_month = (TextView) findViewById(R.id.txv_month);
            txv_up = (TextView) findViewById(R.id.txv_up);
            btnContinue = (LinearLayout) findViewById(btn_continue);
            lnr_main = (LinearLayout) findViewById(R.id.lnr_main);
            txv_back.setOnClickListener(this);
            txv_up.setOnClickListener(this);
            btnContinue.setOnClickListener(this);
            lnr_main.removeAllViews();

            //set current month and year on top
            DateFormat formatter3 = new SimpleDateFormat("MMMM yyyy");
            String formattedDate = formatter3.format(startDate.getTime());
            txv_month.setText(formattedDate);
            if (currentDate.before(startDate)) {
                txv_back.setVisibility(View.VISIBLE);
            } else {
                txv_back.setVisibility(View.INVISIBLE);
            }

            for (Week week : weeksMap.values()) {
                //set date according to month
                View child = getLayoutInflater().inflate(R.layout.item_date, null);
                LinearLayout lnr2 = (LinearLayout) child.findViewById(R.id.lnr2);
                lnr2.setWeightSum(14);
                for (Day day : week.getDays()) {
                    View child11 = getLayoutInflater().inflate(R.layout.item_day, null);
                    tvDateTxt = (TextView) child11.findViewById(R.id.txv_date);
                    tvDayTxt = (TextView) child11.findViewById(R.id.txv_day_name);
                    LinearLayout.LayoutParams lParams = (LinearLayout.LayoutParams) tvDateTxt.getLayoutParams();
                    lParams.weight = 2;
                    child11.setLayoutParams(lParams);
                    if (day.getDate() != null) {
                        Log.e("TAgdate ", "" + day.getDate().get(Calendar.DAY_OF_MONTH) + "/" + day.getDate().get(Calendar.MONTH) + "/" + day.getDate().get(Calendar.YEAR));
                        tvDateTxt.setText("" + day.getDate().get(Calendar.DAY_OF_MONTH));
                        String dayLongName = day.getDate().getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
                        ;
                        tvDayTxt.setText(dayLongName);
                        //tvDateTxt.setTag("DATE#" + week.getSequence() + "-" + day.getDate().get(Calendar.DAY_OF_MONTH));
                        if (clickedDate.equalsIgnoreCase(day.getDate().get(Calendar.YEAR)+ "-" + day.getDate().get(Calendar.MONTH)+ "-"+day.getDate().get(Calendar.DAY_OF_MONTH))) {
                            tvDateTxt.setBackgroundColor(getResources().getColor(R.color.sign_in_btn_color));
                            tvDayTxt.setBackgroundColor(getResources().getColor(R.color.sign_in_btn_color));
                            tvDateTxt.setTextColor(getResources().getColor(R.color.white_color));
                            tvDayTxt.setTextColor(getResources().getColor(R.color.white_color));
                        } else {
                            tvDateTxt.setBackgroundColor(getResources().getColor(R.color.white_color));
                            tvDayTxt.setBackgroundColor(getResources().getColor(R.color.white_color));
                            tvDateTxt.setTextColor(getResources().getColor(R.color.colorPrimary));
                            tvDayTxt.setTextColor(getResources().getColor(R.color.colorPrimary));
                        }
                        tvDateTxt.setTag("DATE#" + day.getDate().get(Calendar.YEAR)+ "-" + day.getDate().get(Calendar.MONTH)+ "-"+day.getDate().get(Calendar.DAY_OF_MONTH));
                        tvDateTxt.setOnClickListener(this);
                        lnr2.addView(child11);
                    } else {
                        Log.e("dayis Null ", "dayis");
                        tvDateTxt.setText("");
                        tvDateTxt.setVisibility(View.GONE);
                        tvDayTxt.setVisibility(View.GONE);
                        tvDateTxt.setTag("");
                        tvDayTxt.setText("");
                    }
                    // lnr2.addView(child11);
                }
                lnr_main.addView(child);

            }

            txv_back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //do back condition here
                    if (currentDate.before(startDate)) {
                        Calendar nxt = (Calendar) startDate.clone();
                        nxt.add(Calendar.MONTH, -1);
                        setupMonth(nxt);
                        init();
                    }
                }
            });

            txv_up.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //do up condition here
                    Calendar nxt = (Calendar) startDate.clone();
                    nxt.add(Calendar.MONTH, 1);
                    setupMonth(nxt);
                    init();
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.add_more_item:
                Intent mIntent=new Intent(ActivitySheduleBooking.this,ActivityHome.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mIntent);
                break;
            case R.id.relay_existing_user:
                Intent mLoginIntent = new Intent(ActivitySheduleBooking.this, ActivityLogin.class);
                mLoginIntent.putExtra(Constant.LOGIN_TAB_NUMBER,0);
                startActivity(mLoginIntent);
                break;
            case R.id.relay_new_user:
                Intent mRegisterIntent = new Intent(ActivitySheduleBooking.this, ActivityLogin.class);
                mRegisterIntent.putExtra(Constant.LOGIN_TAB_NUMBER,1);
                startActivity(mRegisterIntent);
                break;
            case R.id.relay_morning:
                if(timeSlotList!=null&&timeSlotList.size()>0) {
                    selectTimeSlot(1);
                }else{
                    ToastUtils.showToast(ActivitySheduleBooking.this,"we do not provide our service/plan in this time slot ");
                }
                break;
            case R.id.relay_afternoon:
                if(timeSlotList!=null&&timeSlotList.size()>1) {
                    selectTimeSlot(2);
                }else{
                    ToastUtils.showToast(ActivitySheduleBooking.this,"we do not provide our service/plan in this time slot ");
                }
                break;
            case R.id.relay_evening:
                if(timeSlotList!=null&&timeSlotList.size()>2) {
                    selectTimeSlot(3);
                }else{
                    ToastUtils.showToast(ActivitySheduleBooking.this,"we do not provide our service/plan in this time slot ");
                }
                break;
            case R.id.btn_add_address_new:
                if(addressAdapter!=null&&addressAdapter.getAddressList()!=null&&addressAdapter.getAddressList().size()>0) {
                    if (txtAddAddress.getText().toString().equalsIgnoreCase("Add Address")) {
                        GlobalUtils.displayDialogNew(ActivitySheduleBooking.this, "Alert", "services will be chargeable as there is no plan on this address, do you want to proceed?", dialogBtnClick);

                    } else {
                        txtAddAddress.setText("Add Address");
                        btnAddNewAddress.setVisibility(View.VISIBLE);
                        showAddressView(3);
                    }
                }else{
                    if(txtAddAddress.getText().toString().equalsIgnoreCase("Add Address")) {
                        if(addressAdapter!=null&&addressAdapter.getAddressList().size()>0) {
                            txtAddAddress.setText("Close");
                            btnAddNewAddress.setVisibility(View.VISIBLE);
                        }else{
                            btnAddNewAddress.setVisibility(View.GONE);
                            txtAddAddress.setText("Add Address");
                        }
                        showAddressView(4);
                    }else{
                        txtAddAddress.setText("Add Address");
                        btnAddNewAddress.setVisibility(View.VISIBLE);
                        showAddressView(3);
                    }
                }

                //relayAddAddress.setVisibility(View.VISIBLE);
                break;
            case R.id.btn_continue:
                if (AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE, false, ActivitySheduleBooking.this)) {
                    if (validation()) {
                        if(addressLineOne.getText().toString().length()!=0&&addressLinetwo.getText().toString().length()!=0) {
                            callAddAddressApi();
                        }else {
                            moveToNextScreen();
                        }
                    }
                } else {
                    ToastUtils.showToast(ActivitySheduleBooking.this, "Please login first");
                }

                break;
        }
        if (v.getTag() != null) {
            Log.e("tag value", v.getTag().toString());
            String eventStr = v.getTag().toString();
            String arr[] = eventStr.split("#");
            if (arr.length == 2) {
                if (arr[0].equalsIgnoreCase("DATE")) {
                    String mClickedDate=arr[1];
                    //2017-03-07
                    try {
                        final Calendar c = Calendar.getInstance();
                        int currentYear = c.get(Calendar.YEAR);
                        int currentmonth = c.get(Calendar.MONTH)+1;
                        int currentDay = c.get(Calendar.DAY_OF_MONTH);
                        getSelectedDate(mClickedDate);
                        mCurrentDate=currentYear+"-"+currentmonth+"-"+currentDay;
                        mSDate=selectedYear+"-"+selectedMonth+"-"+selectedDay;
                        if(Integer.parseInt(selectedYear)>=currentYear&&selectedMonth>=currentmonth){
                            if(selectedMonth==currentmonth&&Integer.parseInt(selectedDay)>=currentDay){
                                if(Integer.parseInt(selectedDay)==currentDay&&getCurrentTime()>17){
                                    ToastUtils.showToast(ActivitySheduleBooking.this,"No Time Slot available for today");
                                }else {
                                    if (!StringUtils.isNullOrEmpty(mSelectedDate) && mSDate.equalsIgnoreCase(mCurrentDate)) {
                                        selectTimeSlot(0);
                                    }
                                    mCurrentDDate = mCurrentDate;
                                    mSelectedDate = mSDate;
                                    clickedDate = arr[1];
                                    init();
                                }
                            }else if(selectedMonth>currentmonth){
                                if(!StringUtils.isNullOrEmpty(mSDate)&&mSDate.equalsIgnoreCase(mCurrentDate)) {
                                    selectTimeSlot(0);
                                }
                                mCurrentDDate=mCurrentDate;
                                mSelectedDate=mSDate;
                                clickedDate = arr[1];
                                init();
                            }else{
                                ToastUtils.showToast(ActivitySheduleBooking.this,"You can not Select date before current date");
                            }


                        } else {

                            ToastUtils.showToast(ActivitySheduleBooking.this,"You can not Select date before current date");

                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }


                    //dateClicked(arr[1]);
                    // Toast.makeText(this,""+v.getTag().toString(),Toast.LENGTH_SHORT).show();
                    //  Toast.makeText(this,""+clickedDate,Toast.LENGTH_SHORT).show();
                }
            }

        }

    }

    /*private void dateClicked(String selectedDayStr) {
        if (selectedDayStr != null || !selectedDayStr.equalsIgnoreCase("")) {
            String[] arr = selectedDayStr.split("-");
            if (arr.length == 2) {
                toggle();
                selectedWeek = weeksMap.get(Integer.parseInt(arr[0]));
                selectedDay = dayMap.get(Integer.parseInt(arr[1]));
                toggle();
                init();
            } else {
                selectedDay = null;
                selectedWeek = null;
                init();
            }
        } else {
            selectedDay = null;
            selectedWeek = null;
            init();
        }
    }


    private void toggle() {
        if (selectedDay != null) {
            selectedDay.toggleActive();
        }
        if (selectedWeek != null) {
            selectedWeek.toggleActive(selectedDay);
        }
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }


    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
*/
    void selectTimeSlot(int position) {
        //set selected and unselected images
        // imgMorning,imgAfterNoon,imgEvening,imgNight;
        relayMorning.setBackgroundColor(getResources().getColor(R.color.white_color));
        relayAfterNoon.setBackgroundColor(getResources().getColor(R.color.white_color));
        relayEvening.setBackgroundColor(getResources().getColor(R.color.white_color));
        lblMorning.setTextColor(getResources().getColor(R.color.colorPrimary));
        lblAfterNoon.setTextColor(getResources().getColor(R.color.colorPrimary));
        lblEvening.setTextColor(getResources().getColor(R.color.colorPrimary));
        if(position==0){
            selectedTimeSlot="";
        }
        if (position == 1) {
            if(timeSlotValidation(1)) {
                selectedTimeSlot =""+timeSlotList.get(0).getServiceTimeSlotId();
                relayMorning.setBackgroundColor(getResources().getColor(R.color.sign_in_btn_color));
                lblMorning.setTextColor(getResources().getColor(R.color.white_color));

            }
        } else if (position == 2) {
            if(timeSlotValidation(2)) {
                selectedTimeSlot = ""+timeSlotList.get(1).getServiceTimeSlotId();
                relayAfterNoon.setBackgroundColor(getResources().getColor(R.color.sign_in_btn_color));
                lblAfterNoon.setTextColor(getResources().getColor(R.color.white_color));
            }
        } else if (position == 3) {
            if(timeSlotValidation(3)) {
                selectedTimeSlot = ""+timeSlotList.get(2).getServiceTimeSlotId();
                relayEvening.setBackgroundColor(getResources().getColor(R.color.sign_in_btn_color));
                lblEvening.setTextColor(getResources().getColor(R.color.white_color));
            }
        }

    }

    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        ActivitySheduleBooking.this.runOnUiThread(new Runnable() {
            public void run() {
                removeProgressDialog();
                Log.e("Response", response.toString());
                if (response == null) {
                    ToastUtils.showToast(ActivitySheduleBooking.this, getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.SERVICE_ADDRESS_REQUEST:
                            AddressResponse addressResponse = (AddressResponse) response.getResponseObject();
                            if (!addressResponse.getHasError()) {
                                if (addressResponse.getData().getLstAddressResult().size() > 0) {
                                    addressAdapter = new AddressAdapter(ActivitySheduleBooking.this, (ArrayList<AddressModel>) addressResponse.getData().getLstAddressResult(),ActivitySheduleBooking.this);
                                    nonScrollListView.setAdapter(addressAdapter);
                                    //   setupAddressData(addressResponse.getData().getLstAddressResult());
                                    showAddressView(3);
                                } else {
                                    showAddressView(2);
                                    //ToastUtils.showToast(ActivitySheduleBooking.this, addressResponse.getMessage());
                                }
                            }
                            break;
                        case AppSettings.SERVICE_ADD_ADDRESS_EVENT:
                            AddAddressResponse addAddressResponse = (AddAddressResponse) response.getResponseObject();
                           if (!addAddressResponse.isHasError()) {
                                addressId=addAddressResponse.getData().getId();
                                moveToNextScreen();
                            }else{
                                //TODO current add address api giving response we do not have our service in given state and city
                                ToastUtils.showToast(ActivitySheduleBooking.this,addAddressResponse.getMessage());
                               // moveToNextScreen();
                            }
                            break;
                        case AppSettings.LOCATION_SEARCH_REQUEST:
                            LocationSearchResponse  locationSearchResponse= (LocationSearchResponse) response.getResponseObject();
                            ArrayList<String>   mAddresslist=new ArrayList<String>();
                            locationAddressList=new ArrayList<Result>();
                            if (locationSearchResponse.getResults()!=null&&locationSearchResponse.getResults().size()>0&&locationSearchResponse.getResults().get(0).getAddress_components().size()>0) {
                                locationAddressList= (ArrayList<Result>) locationSearchResponse.getResults();
                                for (Result result:locationSearchResponse.getResults()){
                                    mAddresslist.add(result.getFormatted_address());
                                }

                            }else{
                               /* mAddresslist.clear();
                                locationAddressList.clear();*/
                            }
                            adapterAddress  = new ArrayAdapter<String>(ActivitySheduleBooking.this, android.R.layout.select_dialog_item, mAddresslist);
                            addressLinetwo.setThreshold(1);//will start working from first character
                            addressLinetwo.setAdapter(adapterAddress);//setting the adapter data into the AutoCompleteTextView
                            if(!StringUtils.isNullOrEmpty(addressLinetwo.getText().toString())&&addressLinetwo.getText().toString().length()<14&&addressLinetwo.getText().toString().length()>=1) {
                                addressLinetwo.showDropDown();
                                addressLinetwo.requestFocus();
                            }
                            break;
                        case BUY_OPEN_TICKET_EVENT:
                            BuyOpenTicketResponse buyOpenTicketResponse= (BuyOpenTicketResponse) response.getResponseObject();
                            if(!buyOpenTicketResponse.getHasError()||buyOpenTicketResponse.getHasError()){
                                ToastUtils.showToast(ActivitySheduleBooking.this,""+buyOpenTicketResponse.getMessage());
                                boolean needToMoveNextScreen=false;
                                ArrayList<ServiceModel>  mSelectedOpenTickts= getDataFromPreferences();
                                for (ServiceModel serviceModel:mSelectedOpenTickts) {
                                    if(serviceModel.getRate()!=0){
                                        needToMoveNextScreen=true;
                                        break;

                                    }
                                }
                                if(needToMoveNextScreen) {
                                    Intent intent = new Intent(ActivitySheduleBooking.this, ActivityOrderSummary.class);
                                    intent.putExtra(Constant.SERVICE_SELECTED_DATA, getDataFromPreferences());
                                    intent.putExtra(Constant.DATE_AND_YEAR, getSelectedDate(clickedDate));
                                    intent.putExtra(Constant.TIME_SLOT, selectedTimeSlot);
                                    intent.putExtra(Constant.FEEDBACK, txtFeedback.getText().toString());
                                    intent.putExtra(Constant.ADDRESS_ID, addressId);
                                    intent.putExtra(Constant.IS_SHOW_OFFER_TRUE, getIntent().getBooleanExtra(Constant.IS_SHOW_OFFER_TRUE, false));
                                    intent.putExtra(Constant.SERVICE_NAME, getIntent().getStringExtra(Constant.SERVICE_NAME));
                                    startActivity(intent);
                                    //finish();
                                }else{
                                    AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivitySheduleBooking.this);
                                   finish();
                                }

                            }
                            else {
                                ToastUtils.showToast(ActivitySheduleBooking.this,""+buyOpenTicketResponse.getMessage());
                                finish();
                            }
                            break;
                        case AppSettings.OPEN_TICKET_EVENT: {
                            OpenTicketResponse openTicketResponse = (OpenTicketResponse) response.getResponseObject();
                            if (!openTicketResponse.getHasError()) {
                                if(openTicketResponse.getData()!=null&&openTicketResponse.getData().getPlans()!=null&&openTicketResponse.getData().getPlans().size()>0) {
                                   /* mPlanId= openTicketResponse.getData().getPlans().get(0).getPlanId();*/
                                    /*setAdapter(openTicketResponse.getData().getPlans());
                                    lblNoDataFound.setVisibility(View.GONE);
                                    recycleView.setVisibility(View.VISIBLE);*/
                                    for(Plan plan:openTicketResponse.getData().getPlans()){
                                        if(plan!=null&&plan.getServiceList()!=null&&plan.getServiceList().size()>0) {
                                            for (ParentService parentService : plan.getServiceList()) {
                                                if(parentService!=null&&parentService.getServices()!=null&&parentService.getServices().size()>0) {
                                                    for (ChildService childService : parentService.getServices()) {
                                                        PlanIdServiceid planIdServiceid = new PlanIdServiceid();
                                                        planIdServiceid.setPlanId(plan.getPlanId());
                                                        planIdServiceid.setServiceId(Integer.parseInt(childService.getServiceId()));
                                                        alreadyPurchasedServices.add(planIdServiceid);
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }else{
                                    /*lblNoDataFound.setVisibility(View.VISIBLE);
                                    recycleView.setVisibility(View.GONE);*/
                                }

                            }else{
                               /* lblNoDataFound.setVisibility(View.VISIBLE);*/
                                //ToastUtils.showToast(ActivitySheduleBooking.this,""+openTicketResponse.getMessage());
                            }
                        }
                        break;
                        default:
                            break;
                    }
                }
            }
        });
    }

    void showAddressView(int mcase) {
        //case 1 when user is not login/register
        selectedAddressline1 = "";
        selectedAddressline2 = "";
        if (mcase == 1) {
            relauUserLoginRegisterOption.setVisibility(View.VISIBLE);
            headerAddAddress.setVisibility(View.GONE);
            relayAddAddress.setVisibility(View.GONE);
        } else if (mcase == 2) {
            // case 2 when user is login/register but not having address
            relauUserLoginRegisterOption.setVisibility(View.GONE);
            headerAddAddress.setVisibility(View.VISIBLE);
            relayAddAddress.setVisibility(View.VISIBLE);

        } else if (mcase == 3) {
            //case 3 user login/register and having address
            relauUserLoginRegisterOption.setVisibility(View.GONE);
            headerAddAddress.setVisibility(View.VISIBLE);
            nonScrollListView.setVisibility(View.VISIBLE);
            relayAddAddress.setVisibility(View.GONE);

        } else if (mcase == 4) {
            relauUserLoginRegisterOption.setVisibility(View.GONE);
            nonScrollListView.setVisibility(View.GONE);
            headerAddAddress.setVisibility(View.VISIBLE);
            relayAddAddress.setVisibility(View.VISIBLE);


        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(StringUtils.isNullOrEmpty(addressId)) {
            addMoreRelay.setVisibility(View.VISIBLE);
            addressMainHeader.setVisibility(View.VISIBLE);
            if (AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE, false, ActivitySheduleBooking.this)) {
                if (!isApiCalled) {
                    callAddressApi();
                    isApiCalled = true;
                }
            } else {
                showAddressView(1);
            }
        }else {
            addMoreRelay.setVisibility(View.GONE);
            addressMainHeader.setVisibility(View.GONE);
        }

    }

    /*public void setAddressFromAdapter(AddressModel addressModel) {
        selectedAddressline1 = addressModel.getLine1();
        selectedAddressline2 = addressModel.getLine2();
        addressId=""+addressModel.getId();

    }
*/
    boolean validation() {
        boolean isValid = true;
        if (clickedDate.length() == 0) {
            ToastUtils.showToast(ActivitySheduleBooking.this, "Please select date");
            isValid = false;
        } else if (selectedTimeSlot.length() == 0) {
            ToastUtils.showToast(ActivitySheduleBooking.this, "Please select Time Slot");
            isValid = false;
        } else if (!addressValidation()) {
            ToastUtils.showToast(ActivitySheduleBooking.this, "Please Add/Select Address");
            isValid = false;
        }
        return isValid;
    }

    boolean addressValidation() {
        boolean isValid = false;
        if (selectedAddressline1!=null&&selectedAddressline1.length() != 0 &&selectedAddressline2!=null&& selectedAddressline2.length() != 0) {
            isValid = true;
        }
        if (addressLineOne.getText().toString().length() != 0&&addressLinetwo.getText().toString().length()!=0) {
            selectedAddressline1=addressLineOne.getText().toString();
            selectedAddressline2=addressLinetwo.getText().toString();
            isValid = true;
        }else if(!StringUtils.isNullOrEmpty(addressId)){
            isValid=true;
        }
        return isValid;

    }

    private void callAddAddressApi() {
        if (!ConnectivityUtils.isNetworkEnabled(ActivitySheduleBooking.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerAddAddress addAddressController = new ControllerAddAddress(this, this);
            showProgressDialog("Please wait...");
            AddAddressModel addAddressRequest = getAddAddressRequest();
            addAddressController.getData(AppSettings.SERVICE_ADD_ADDRESS_EVENT, addAddressRequest);
        }
    }

    private AddAddressModel getAddAddressRequest() {
        AddAddressModel addAddressModel = new AddAddressModel();
        addAddressModel.setUserId(AppSharedPreference.getString(PrefConstants.USER_ID,"",ActivitySheduleBooking.this));
        addAddressModel.setLine1(addressLineOne.getText().toString());
        addAddressModel.setLine2(addressLinetwo.getText().toString());
        addAddressModel.setLatitude(mLatitude);
        addAddressModel.setLongitude(mLongitude);
        addAddressModel.setCity(mCity);
        addAddressModel.setArea(subLocality);
        addAddressModel.setState(mState);
        addAddressModel.setId("0");
        return addAddressModel;
    }



    void moveToNextScreen(){
        ArrayList<ServiceModel>  mSelectedOpenTickts= getDataFromPreferences();
        ArrayList<ServiceModel> openTicketList=new ArrayList<>();
        for (ServiceModel serviceModel:mSelectedOpenTickts) {
            if(serviceModel.getRate()==0){
                openTicketList.add(serviceModel);
            }
        }
        if(openTicketList==null||openTicketList.size()==0){
        Intent intent = new Intent(ActivitySheduleBooking.this, ActivityOrderSummary.class);
        intent.putExtra(Constant.SERVICE_SELECTED_DATA, getDataFromPreferences());
        intent.putExtra(Constant.DATE_AND_YEAR,getSelectedDate(clickedDate));
        intent.putExtra(Constant.TIME_SLOT,selectedTimeSlot);
            intent.putExtra(Constant.FEEDBACK,txtFeedback.getText().toString());
        intent.putExtra(Constant.ADDRESS_ID,addressId);
        intent.putExtra(Constant.IS_SHOW_OFFER_TRUE, getIntent().getBooleanExtra(Constant.IS_SHOW_OFFER_TRUE, false));
        intent.putExtra(Constant.SERVICE_NAME,getIntent().getStringExtra(Constant.SERVICE_NAME));
        startActivity(intent);
        }else{
            callOpenTicketApi(openTicketList);
        }

    }

    @Override
    public void setAddress(AddressModel addAddressModel) {
        selectedAddressline1 = addAddressModel.getLine1();
        selectedAddressline2 = addAddressModel.getLine2();
        addressId=""+addAddressModel.getId();
        getPlansApi(addressId);
    }
    void  getPlansApi(String addressId){
        if (!ConnectivityUtils.isNetworkEnabled(ActivitySheduleBooking.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerOpenTicket openTicketController = new ControllerOpenTicket(this, this, addressId);
            showProgressDialog("Please wait...");
            openTicketController.getData(AppSettings.OPEN_TICKET_EVENT, null);
        }

    }
    void  callOpenTicketApi(ArrayList<ServiceModel> openTicketList){
        if (!ConnectivityUtils.isNetworkEnabled(ActivitySheduleBooking.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerBuyOpenTicket buyOpenTicketController = new ControllerBuyOpenTicket(this, this);
            showProgressDialog("Please wait...");
            OpenTicketRequest openTicketRequest = getOpenTicketRequest(openTicketList);
            buyOpenTicketController.getData(BUY_OPEN_TICKET_EVENT, openTicketRequest);
        }
    }

    OpenTicketRequest getOpenTicketRequest(ArrayList<ServiceModel> openTicketList){

        OpenTicketRequest openTicketRequest=new OpenTicketRequest();
        List<LstOpenRequest> lis=new ArrayList<>();
        for (ServiceModel serviceModel:openTicketList){
            LstOpenRequest openRequest=new LstOpenRequest();
            if(!StringUtils.isNullOrEmpty(getIntent().getStringExtra(Constant.PLAN_ID))){
                openRequest.setPlanId(Integer.parseInt(getIntent().getStringExtra(Constant.PLAN_ID)));
            }else{
                openRequest.setPlanId(serviceModel.getmPlanId());
            }

            openRequest.setServiceId(serviceModel.getServiceid());
           openRequest.setTicketDesc(txtFeedback.getText().toString());
            openRequest.setUserId(AppSharedPreference.getString(PrefConstants.USER_ID,"",ActivitySheduleBooking.this));
          openRequest.setPrefferedDate(getSelectedDate(clickedDate));
           openRequest.setSlotId(selectedTimeSlot);
           openRequest.setTicketType("1"); ;
            openRequest.setAddressId(addressId);
            int quantity=0;
            if(serviceModel.getQuantity()==0){
                quantity=1;

            }else{
                quantity=serviceModel.getQuantity();
            }
          openRequest.setQuantity(quantity);
            lis.add(openRequest);
        }
        openTicketRequest.setLstOpenRequest(lis);

        return openTicketRequest;

    }
    boolean timeSlotValidation(int timeSlot){
        boolean isValid=true;
        if(mCurrentDDate!=null&&mSelectedDate!=null){
            if(mCurrentDDate.equalsIgnoreCase(mSelectedDate)){
            if(timeSlot==1){
                if(getCurrentTime()>=0&&getCurrentTime()<9){
                  isValid=true;
                }else{
                    isValid=false;
                    ToastUtils.showToast(ActivitySheduleBooking.this,"Please select valid time slot");
                }

            }else if(timeSlot==2){
                if(getCurrentTime()>=0&&getCurrentTime()<13){
                    isValid=true;
                }else{
                    isValid=false;
                    ToastUtils.showToast(ActivitySheduleBooking.this,"Please select valid time slot");
                }
            }else if(timeSlot==3){
                if(getCurrentTime()>=0&&getCurrentTime()<17){
                    isValid=true;
                }else{
                    isValid=false;
                    ToastUtils.showToast(ActivitySheduleBooking.this,"Please select valid time slot");
                }

            }
            }
            else{
                isValid=true;
            }
        }else{
           isValid=false;
            ToastUtils.showToast(ActivitySheduleBooking.this,"Please select date before time slot");
        }
        return isValid;
    }
   int getCurrentTime(){
        Calendar c = Calendar.getInstance();
        int seconds = c.get(Calendar.SECOND);
       int hour=c.get(Calendar.HOUR_OF_DAY);
       return hour;
    }
    void scrollView(){
       /* scrollViewHorizontal.setSmoothScrollingEnabled(true);
        Log.e("scrollViewWidth ",""+scrollViewHorizontal.getWidth());
        Calendar c = Calendar.getInstance();
        int date=c.get(Calendar.DATE);
              if(date>=5&&date<10)  {
                  scrollViewHorizontal.scrollTo(160, 0);
              }else if(date>=10&&date<15){
                  scrollViewHorizontal.scrollTo(250, 0);
              }else  if(date>=15&&date<20) {
            scrollViewHorizontal.scrollTo(350, 0);
        }else  if(date>=20&&date<25) {
            scrollViewHorizontal.scrollTo(400, 0);
        }else  if(date>=25&&date<=31) {
            scrollViewHorizontal.scrollTo(500, 0);
        }
*/
       /* scrollViewHorizontal.post(new Runnable() {
            public void run() {
                scrollViewHorizontal.scrollTo(0, 500);
            }
        });*/
    }

    void storeDataInPreference(ArrayList<ServiceModel> serviceList) {
        if(serviceList!=null&&serviceList.size()>0) {
            try {
                if (!StringUtils.isNullOrEmpty(AppSharedPreference.getString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivitySheduleBooking.this))) {
                    SelectedServicesResponse selectedServicesResponse = new Gson().fromJson(AppSharedPreference.getString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivitySheduleBooking.this), SelectedServicesResponse.class);
                    ArrayList<ServiceModel> mServiceList = selectedServicesResponse.getServiceList();
                    mServiceList.addAll(serviceList);
                    selectedServicesResponse.setServiceList(mServiceList);
                    String serviceListResponse = new Gson().toJson(selectedServicesResponse);
                    AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, serviceListResponse, ActivitySheduleBooking.this);

                } else {
                    SelectedServicesResponse selectedServicesResponse1 = new SelectedServicesResponse();
                    selectedServicesResponse1.setServiceList(serviceList);
                    String serviceListResponse = new Gson().toJson(selectedServicesResponse1);
                    AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, serviceListResponse, ActivitySheduleBooking.this);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    ArrayList<ServiceModel> getDataFromPreferences(){
        SelectedServicesResponse selectedServicesResponse = new Gson().fromJson(AppSharedPreference.getString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivitySheduleBooking.this), SelectedServicesResponse.class);
        if(alreadyPurchasedServices!=null&&alreadyPurchasedServices.size()>0){
           for (int i=0;i<alreadyPurchasedServices.size();i++) {
               for (int k=0;k< selectedServicesResponse.getServiceList().size();k++) {
                if(selectedServicesResponse.getServiceList().get(k).getServiceid()==alreadyPurchasedServices.get(i).getServiceId()){
                    selectedServicesResponse.getServiceList().get(k).setRate(0);
                    selectedServicesResponse.getServiceList().get(k).setInitialrate(0);
                    selectedServicesResponse.getServiceList().get(k).setmPlanId(alreadyPurchasedServices.get(i).getPlanId());
                 }
               }
           }
       }
        return  selectedServicesResponse.getServiceList();
        }
      String getSelectedDate(String clickedDate){
        //201--2017-
          selectedYear="";selectedDay="";selectedMonth=0;
         selectedYear=clickedDate.substring(0,4);
        String newStringh=clickedDate.substring(clickedDate.indexOf("-"),clickedDate.length());
        String newwString=newStringh.substring(1,newStringh.length());
        String month=newwString.substring(0,newwString.indexOf("-"));
         selectedDay=newwString.substring(newwString.indexOf("-")+1,newwString.length());
         selectedMonth=Integer.parseInt(month)+1;
        String newwwdate=selectedYear+"-"+selectedMonth+"-"+selectedDay;
        return newwwdate;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivitySheduleBooking.this);
        Intent i = new Intent(ActivitySheduleBooking.this, ActivityHome.class);
// set the new task and clear flags
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    @Override
    public void onDialogBtnClicked(int btnPosition) {
        if(btnPosition==1){
            if(txtAddAddress.getText().toString().equalsIgnoreCase("Add Address")) {
                if(addressAdapter!=null&&addressAdapter.getAddressList().size()>0) {
                    txtAddAddress.setText("Close");
                    btnAddNewAddress.setVisibility(View.VISIBLE);
                }else{
                    btnAddNewAddress.setVisibility(View.GONE);
                    txtAddAddress.setText("Add Address");
                }
                showAddressView(4);
            }else{
                txtAddAddress.setText("Add Address");
                btnAddNewAddress.setVisibility(View.VISIBLE);
                showAddressView(3);
            }
        }else{

        }

    }

    @Override
    public void setLocation(Result addAddressModel) {

    }
}