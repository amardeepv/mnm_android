package com.mnm.services.model.response.plan;

import java.util.List;

/**
 * Created by saten on 4/15/17.
 */

public class AddTOBasketPlanRequest {
    private List<PlanBasketList> planBasketList = null;

    public List<PlanBasketList> getPlanBasketList() {
        return planBasketList;
    }

    public void setPlanBasketList(List<PlanBasketList> planBasketList) {
        this.planBasketList = planBasketList;
    }
}
