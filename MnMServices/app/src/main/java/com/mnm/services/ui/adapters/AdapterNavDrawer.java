package com.mnm.services.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kelltontech.utils.StringUtils;
import com.mnm.services.R;
import com.mnm.services.model.NavDrawerItem;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.activity.MyDrawerBaseActivity;

import java.util.ArrayList;

public class AdapterNavDrawer extends BaseAdapter {

    private Context context;
    private ArrayList<NavDrawerItem> navDrawerItems;

    public AdapterNavDrawer(Context context, ArrayList<NavDrawerItem> navDrawerItems) {
        this.context = context;
        this.navDrawerItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
            holder = new ViewHolder();
            holder.imgIcon = (ImageView) convertView.findViewById(R.id.icon);
            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
            holder.imgIconNew = (ImageView) convertView.findViewById(R.id.icon_new);
            holder.mView=(View)convertView.findViewById(R.id.mview);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.imgIcon.setImageResource(navDrawerItems.get(position).getIcon());

        holder.imgIconNew.setImageResource(navDrawerItems.get(position).getNewIcon());
        holder.txtTitle.setText(navDrawerItems.get(position).getTitle());
        if(position==2||position==5||position==8){
            holder.mView.setVisibility(View.VISIBLE);
        }else{
            holder.mView.setVisibility(View.GONE);
        }
       if(!AppSharedPreference.getBoolean(PrefConstants.IS_LOGIN_TRUE, false, context)){

        }

        return convertView;
    }

  public void  setNavData(ArrayList<NavDrawerItem> arrList){
       navDrawerItems=arrList;
      notifyDataSetChanged();
    }
    class ViewHolder {
        TextView txtTitle;
        ImageView imgIcon;
        ImageView imgIconNew;
        View mView;
    }
}
