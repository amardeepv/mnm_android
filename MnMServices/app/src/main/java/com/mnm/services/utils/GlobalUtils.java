package com.mnm.services.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.kelltontech.utils.StringUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormatSymbols;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;

import static com.mnm.services.constants.Constant.AC_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.ADDON_PLAN;
import static com.mnm.services.constants.Constant.ANSWER;
import static com.mnm.services.constants.Constant.CARPENTRY_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.CAR_SPA_FAQ;
import static com.mnm.services.constants.Constant.CCTV_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.CHILD_ARRAY;
import static com.mnm.services.constants.Constant.ELECTRICAL_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.FIXED_PLAN;
import static com.mnm.services.constants.Constant.GARDENING_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.HOME_APPLIANCE_FAQ;
import static com.mnm.services.constants.Constant.HOME_CLEANING_FAQ;
import static com.mnm.services.constants.Constant.INTERIOR_DESIGN_FAQ;
import static com.mnm.services.constants.Constant.PAINT_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.PEST_CONTROL_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.PLUMBING_SERVICES_FAQ;
import static com.mnm.services.constants.Constant.POINT;
import static com.mnm.services.constants.Constant.QUESTION;
import static com.mnm.services.constants.Constant.RO_SERVICE_FAQ;
import static com.mnm.services.constants.Constant.SECURITY_SERVICE_FAQ;
import static com.mnm.services.constants.Constant.UNLIMITED_PLAN;
import static com.mnm.services.constants.Constant.UPHOLESTREY_CLEANING_FAQ;
import static com.mnm.services.constants.Constant.WATER_TANK_CLEANING_SERVICE_FAQ;


/**
 * Created by saten on 2/12/17.
 */

public class GlobalUtils {

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void saveDataInFile(Context context, String mJsonResponse,String fileName,String prefConstant) {
        try {
            File file=new File(context.getFilesDir().getPath() + "/" + fileName);
            if(file!=null && file.exists()){
                file.delete();
            }
            FileWriter fileWriter = new FileWriter(context.getFilesDir().getPath() + "/" + fileName);
            fileWriter.write(mJsonResponse);
            fileWriter.flush();
            fileWriter.close();
            com.mnm.services.prefrence.AppSharedPreference.putLong(prefConstant,System.currentTimeMillis(),context);
        } catch (IOException e) {
            Log.e("TAG", "Error in Writing: " + e.getLocalizedMessage());
        }
    }

    public static String getJsonData(Context context,String fileName) {
        try {
            File f = new File(context.getFilesDir().getPath() + "/" + fileName);
            //check whether file exists
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (IOException e) {
            Log.e("TAG", "Error in Reading: " + e.getLocalizedMessage());
            return null;
        }
    }


    public static  boolean isValidDateSelected(int numberOfDays,int mSelectedDay,int mSelectedMonth,int mSelectedYear){
        //current date
        final Calendar c = Calendar.getInstance();
        final int mCurrentYear = c.get(Calendar.YEAR);
        final int mCurrentMonth = c.get(Calendar.MONTH);
        final int mCurrentDay = c.get(Calendar.DAY_OF_MONTH);
        Calendar currentCalender = Calendar.getInstance();
        currentCalender.set(mCurrentYear, mCurrentMonth, mCurrentDay);

        //selected date
        Calendar selectedDateCalender = Calendar.getInstance();
        selectedDateCalender.set(mSelectedYear, mSelectedMonth, mSelectedDay);
        long miliSecondFoeCurrentDate = currentCalender.getTimeInMillis();
        long miliSecondForSelectedDate = selectedDateCalender.getTimeInMillis();

        // Calculate the difference in millisecond between two dates
        long diffInMilis = miliSecondFoeCurrentDate - miliSecondForSelectedDate;
        long diffInDays = diffInMilis / (24 * 60 * 60 * 1000);
        if (diffInDays > numberOfDays) {
            return false;
        }else{
            String date = mSelectedMonth + "/" + mSelectedMonth+1+ "/" + mSelectedYear;
            return true;
        }
    }


    public static String getCurrentDate(){
        String dateString;
        Time today = new Time(Time.getCurrentTimezone());
        today.setToNow();
        int month=today.month+1;
        dateString=today.year+"-"+month+"-"+today.monthDay;
        return dateString;
    }

    public static JSONArray getFaq(String serviceName) throws JSONException {
        JSONObject mMainObj=new JSONObject();
        try {
            //main Object deceleration
            JSONArray carSpaFaq =new JSONArray();
            JSONObject ques1=new JSONObject();
            ques1.put(QUESTION,"Is it a doorstep service?");
            ques1.put(ANSWER,"Yes, we provide doorstep service. Our technicians will come at your given address and do the best job");
            carSpaFaq.put(ques1);

            JSONObject ques2=new JSONObject();
            ques2.put(QUESTION,"Do you guarantee removal of all stains from interior?");
            ques2.put(ANSWER,"We will do our best to remove all the stains, even the stubborn one. But in case of risk of material damage, we might not be able to do so.");
            carSpaFaq.put(ques2);

            JSONObject ques3=new JSONObject();
            ques3.put(QUESTION,"Do you clean engines?");
            ques3.put(ANSWER,"No, we do not provide engine cleaning services");
            carSpaFaq.put(ques3);

            JSONObject ques4=new JSONObject();
            ques4.put(QUESTION,"Does your service include buffing, scratch removals, or polishing?");
            ques4.put(ANSWER,"Yes we do Buffing, scratch removals & polishing.");
            carSpaFaq.put(ques4);

            JSONObject ques5=new JSONObject();
            ques5.put(QUESTION,"Does your service include detailing?");
            ques5.put(ANSWER,"No, we not provide detailing");
            carSpaFaq.put(ques5);

            JSONObject ques6=new JSONObject();
            ques6.put(QUESTION,"Is there any pre-requisite for me?");
            ques6.put(ANSWER,"Please make sure that water and electricity is available.");
            carSpaFaq.put(ques6);

            mMainObj.put(CAR_SPA_FAQ,carSpaFaq);

            //Home cleaning FAQ
            JSONArray homeCleaningFaq =new JSONArray();
            JSONObject hques1=new JSONObject();
            hques1.put(QUESTION,"How is deep cleaning different than regular cleaning?");
            hques1.put(ANSWER,"Deep cleaning is an intensive cleaning which includes cleaning of every nook and cranny. Furniture and other appliances are moved whereas required and possible for effective cleaning. \n" +
                    "\n" +
                    "Regular cleaning is more of manual cleaning and is not so thorough ");
            homeCleaningFaq.put(hques1);

            JSONObject hques2=new JSONObject();
            hques2.put(QUESTION,"How long will it take for deep cleaning of whole home?");
            hques2.put(ANSWER,"Time taken depends on the area of the premise and occupancy of the rooms.");
            homeCleaningFaq.put(hques2);

            JSONObject hques3=new JSONObject();
            hques3.put(QUESTION,"What is included in Home Deep Cleaning?");
            hques3.put(ANSWER,"Mechanized scrubbing of the floors, vacuuming of the entire house & upholstery, scrubbing of tiled walls, descaling of taps, dusting & wiping of furniture and cleaning of lofts, fans & fixtures");
            homeCleaningFaq.put(hques3);

            JSONObject hques4=new JSONObject();
            hques4.put(QUESTION,"Can I book home deep cleaning for a portion of my place?");
            hques4.put(ANSWER,"Yes, you can book home deep cleaning for individual rooms or a portion of your place");
            homeCleaningFaq.put(hques4);

            JSONObject hques5=new JSONObject();
            hques5.put(QUESTION,"Is it a doorstep service?");
            hques5.put(ANSWER,"No, you are not required to supply us with any cleaning products, vacuum or any such thing. Unless, you would rather us use your cleaning supplies.");
            homeCleaningFaq.put(hques3);
            mMainObj.put(HOME_CLEANING_FAQ,homeCleaningFaq);


            JSONArray upHoleArray=new JSONArray();
            JSONObject uppques1=new JSONObject();
            uppques1.put(QUESTION,"What all is included in Upholstery cleaning?");
            uppques1.put(ANSWER,"Any type of fabric furniture, leather sofa, carpet, curtains or any other upholstery Item");
            upHoleArray.put(uppques1);

            JSONObject uppques2=new JSONObject();
            uppques2.put(QUESTION,"How much time does it take for Sofa to be dry after cleaning?");
            uppques2.put(ANSWER,"Sofa will be moist for 5 to 6 hours");
            upHoleArray.put(uppques2);

            JSONObject uppques3=new JSONObject();
            uppques3.put(QUESTION,"How often should upholstery item be cleaned?");
            uppques3.put(ANSWER,"Frequency will depend the area where you reside and usability of your place");
            upHoleArray.put(uppques3);


            JSONObject uppques4=new JSONObject();
            uppques4.put(QUESTION,"Do you guarantee removal of all stains or spots?");
            uppques4.put(ANSWER,"We cannot guarantee that all stains or spots will be removed. There are substances which discolor the fabric and age of the Item being cleaned also matters");
            upHoleArray.put(uppques4);

            JSONObject uppques5=new JSONObject();
            uppques5.put(QUESTION,"Do you use any chemicals for upholstery cleaning?");
            uppques5.put(ANSWER,"Yes, we use fabric friendly chemical for cleaning and they are customized as per item being cleaned");
            upHoleArray.put(uppques5);

            mMainObj.put(UPHOLESTREY_CLEANING_FAQ,upHoleArray);



            JSONArray acArray=new JSONArray();
            JSONObject acques1=new JSONObject();
            acques1.put(QUESTION,"Do you provide AMC for AC service?");
            acques1.put(ANSWER,"Yes, we provide AMC for AC service");
            acArray.put(acques1);

            JSONObject acques2=new JSONObject();
            acques2.put(QUESTION,"Do you deal with AC repair issues?");
            acques2.put(ANSWER,"Yes, we provide AC repair service also");
            acArray.put(acques2);
            mMainObj.put(AC_SERVICES_FAQ,acArray);


            JSONArray electricalArray=new JSONArray();
            JSONObject electQues1=new JSONObject();
            electQues1.put(QUESTION,"Is cost of material required for repair included in rates charged by you?");
            electQues1.put(ANSWER,"No, customer is responsible for cost of any material required.");
            electricalArray.put(electQues1);

            JSONObject electQues2=new JSONObject();
            electQues2.put(QUESTION,"What all is included in repair work?");
            electQues2.put(ANSWER,"It includes basic repairing works, it excludes motor rewinding, Fan rewinding, New Installation works. We do these works also but on extra cost.");
            electricalArray.put(electQues2);
            mMainObj.put(ELECTRICAL_SERVICES_FAQ,electricalArray);


            JSONArray plumbingArray=new JSONArray();
            JSONObject plumbQues=new JSONObject();
            plumbQues.put(QUESTION,"Are there any additional changes?");
            plumbQues.put(ANSWER,"Yes, Customer is responsible for cost of any part which needs to be replaced In case water pump is used, customer will be charged Rs 100");
            plumbingArray.put(plumbQues);
            mMainObj.put(PLUMBING_SERVICES_FAQ,plumbingArray);


            JSONArray carpentryArray=new JSONArray();
            JSONObject carpQues1=new JSONObject();
            carpQues1.put(QUESTION,"Is cost of material required for repair included in rates charged by you?");
            carpQues1.put(ANSWER,"No, customer is responsible for cost of any material required.");
            carpentryArray.put(carpQues1);

            JSONObject carpQues2=new JSONObject();
            carpQues2.put(QUESTION,"Do you provide warranty for your work?");
            carpQues2.put(ANSWER,"No, we do not provide any warranty for carpentry work except for any material which comes with warranty");
            carpentryArray.put(carpQues2);


            mMainObj.put(CARPENTRY_SERVICES_FAQ,carpentryArray);


           /* JSONArray woodWordArray=new JSONArray();
            JSONObject woodQues1=new JSONObject();
            woodQues1.put(QUESTION,"Do you create Furniture set only?");
            woodQues1.put(ANSWER,"Along with Furniture set, we deal with creative décor pieces also");
            woodWordArray.put(woodQues1);

            JSONObject woodQues2=new JSONObject();
            woodQues2.put(QUESTION,"Do you work with a specific type of wood?");
            woodQues2.put(ANSWER,"No, we work with any type of wood.");
            woodWordArray.put(woodQues2);

            JSONObject woodQues3=new JSONObject();
            woodQues3.put(QUESTION,"Do you charge an hourly rate?");
            woodQues3.put(ANSWER,"Yes");
            woodWordArray.put(woodQues3);

            JSONObject woodQues4=new JSONObject();
            woodQues4.put(QUESTION,"Is cost of material used included?");
            woodQues4.put(ANSWER,"No, customer is responsible for cost of any material being used");
            woodWordArray.put(woodQues4);

            JSONObject woodQues5=new JSONObject();
            woodQues5.put(QUESTION,"Do you varnish the product?");
            woodQues5.put(ANSWER,"Yes, varnishing of built product is included");
            woodWordArray.put(woodQues5);

            mMainObj.put(WOOD_WORK_SERVICES_FAQ,woodWordArray);*/



            JSONArray pestControlArray=new JSONArray();
            JSONObject pestQues1=new JSONObject();
            pestQues1.put(QUESTION,"Do we have to vacate the place for treatment?");
            pestQues1.put(ANSWER,"No, you do not need to vacate your place");
            pestControlArray.put(pestQues1);

            JSONObject pestQues2=new JSONObject();
            pestQues2.put(QUESTION,"Are your materials safe?");
            pestQues2.put(ANSWER,"2.Are your materials safe?");
            pestControlArray.put(pestQues2);

            JSONObject pestQues3=new JSONObject();
            pestQues3.put(QUESTION,"Do I need regular pest control services?");
            pestQues3.put(ANSWER,"Need at least 3-4 treatments in a year to prevent further generation of larvae/new cockroaches and other pests.");
            pestControlArray.put(pestQues3);

            JSONObject pestQues4=new JSONObject();
            pestQues4.put(QUESTION,"Do you provide AMC?");
            pestQues4.put(ANSWER,"Yes, we provide AMC for pest control services.");
            pestControlArray.put(pestQues4);
            mMainObj.put(PEST_CONTROL_SERVICES_FAQ,pestControlArray);

            JSONArray applianRepairArray=new JSONArray();
            JSONObject apreQues1=new JSONObject();
            apreQues1.put(QUESTION,"Is it a doorstep service?");
            apreQues1.put(ANSWER,"Yes, it’s a doorstep service but in case of some issues, we might take the appliance with us for repair");
            pestControlArray.put(apreQues1);

            JSONObject apreQues2=new JSONObject();
            apreQues2.put(QUESTION,"Do you provide warranty for your work?");
            apreQues2.put(ANSWER,"Yes, we provide warranty of 7 days.");
            pestControlArray.put(apreQues2);

            JSONObject apreQues3=new JSONObject();
            apreQues3.put(QUESTION,"Is cost of material required for repair included in rates charged by you?");
            apreQues3.put(ANSWER," No, customer is responsible for cost of any material required.");
            pestControlArray.put(apreQues3);

            mMainObj.put(HOME_APPLIANCE_FAQ,applianRepairArray);


            JSONArray watesrtanServiceArray=new JSONArray();
            JSONObject waterques1=new JSONObject();
            waterques1.put(QUESTION,"Is cost of material required for repair included in rates charged by you?");
            waterques1.put(ANSWER,"No, customer is responsible for cost of any material required.");
            watesrtanServiceArray.put(waterques1);

            JSONObject waterques2=new JSONObject();
            waterques2.put(QUESTION,"How long does it take to clean the tank?");
            waterques2.put(ANSWER,"Time Taken- 2 to 3 hours for overhead tank cleaning");
            watesrtanServiceArray.put(waterques2);

            JSONObject waterques3=new JSONObject();
            waterques3.put(QUESTION,"Do you use any chemicals to clean and are they harmful?");
            waterques3.put(ANSWER,"We use anti- bacterial spray & they are eco-friendly");
            watesrtanServiceArray.put(waterques3);

            mMainObj.put(WATER_TANK_CLEANING_SERVICE_FAQ,watesrtanServiceArray);

            JSONArray cctvArray=new JSONArray();
            JSONObject cctvQues1=new JSONObject();
            cctvQues1.put(QUESTION,"Is cost of material required included in rates charged by you?");
            cctvQues1.put(ANSWER,"No, customer is responsible for cost of any material required.");
            cctvArray.put(cctvQues1);

            JSONObject cctvQues2=new JSONObject();
            cctvQues2.put(QUESTION,"Do you provide warranty for your work?");
            cctvQues2.put(ANSWER,"Any material that comes with warranty will be included");
            cctvArray.put(cctvQues2);
            mMainObj.put(CCTV_SERVICES_FAQ,cctvArray);



            JSONArray gardArray=new JSONArray();
            JSONObject gardQues1=new JSONObject();
            gardQues1.put(QUESTION,"Do I need to provide anything likes bags, machine or something else?");
            gardQues1.put(ANSWER,"No, the team will bring everything");
            gardArray.put(gardQues1);

            JSONObject gardQues2=new JSONObject();
            gardQues2.put(QUESTION,"Do you plant flowers?");
            gardQues2.put(ANSWER,"Yes, we do. Either you can provide us the exact plants or give us a list of plants with all details. In case, we need to get the plants, send your requirement 2-3 days prior to our visit ");
            gardArray.put(gardQues2);

            JSONObject gardQues3=new JSONObject();
            gardQues3.put(QUESTION,"Can you visit on regular basis?");
            gardQues3.put(ANSWER,"Yes, we can. You can arrange our visit weekly, fortnightly, monthly etc., as per your requirement.");
            gardArray.put(gardQues3);

            mMainObj.put(GARDENING_SERVICES_FAQ,gardArray);

            JSONArray paintArray=new JSONArray();

            JSONObject paintQues1=new JSONObject();
            paintQues1.put(QUESTION,"Is cost of material required included in rates charged by you?");
            paintQues1.put(ANSWER,"No, customer is responsible for cost of any material required.");
            paintArray.put(paintQues1);

            JSONObject paintQues2=new JSONObject();
            paintQues2.put(QUESTION,"Do you provide warranty for your work?");
            paintQues2.put(ANSWER,"NO, this is one time job only.");
            paintArray.put(paintQues2);

            JSONObject paintQues3=new JSONObject();
            paintQues3.put(QUESTION,"WHAT SHOULD I DO TO PREPARE MY HOUSE FOR PAINTING?");
            paintQues3.put(ANSWER,"Cover all the Fixtures, Equipments etc. and provide workable space for workers.");
            paintArray.put(paintQues3);

            JSONObject paintQues4=new JSONObject();
            paintQues4.put(QUESTION,"WHAT TYPE OF PAINT DO YOU USE?");
            paintQues1.put(ANSWER,"We provide all type of Paints as per customer requirements. Generally we use branded Paints like Asian, Burger etc.");
            paintArray.put(paintQues4);

            JSONObject paintQues5=new JSONObject();
            paintQues5.put(QUESTION,"HOW LONG DOES A JOB TAKE?");
            paintQues5.put(ANSWER,"It depends of the area to be painted.");
            paintArray.put(paintQues5);
            mMainObj.put(PAINT_SERVICES_FAQ,paintArray);

            JSONArray roArray=new JSONArray();
            JSONObject roObject1=new JSONObject();
            roObject1.put(QUESTION,"Do you provide AMC for RO service?");
            roObject1.put(ANSWER,"Yes, we provide AMC for RO service");
            roArray.put(roObject1);

            JSONObject roObject2=new JSONObject();
            roObject2.put(QUESTION,"Do you work on specific brands of RO?");
            roObject2.put(ANSWER,"No, we work on any type of RO purifier");
            roArray.put(roObject2);

            JSONObject roObject3=new JSONObject();
            roObject3.put(QUESTION,"Is cost of material required included in rates charged by you?");
            roObject3.put(ANSWER,"No, customer is responsible for cost of any material required.");
            roArray.put(roObject3);

            mMainObj.put(RO_SERVICE_FAQ,roArray);




            JSONArray interArray=new JSONArray();
            JSONObject intQues1=new JSONObject();
            intQues1.put(QUESTION,"Do you provide any warranty for material?");
            intQues1.put(ANSWER,"If any new material bought and used by us comes with warranty, then same will be informed to client.");
            interArray.put(intQues1);

            JSONObject intQuest2=new JSONObject();
            intQuest2.put(QUESTION,"Who are typical users of the home care services?");
            intQuest2.put(ANSWER,"Many of our customers fall into one of the following categories:\n" +
                    "The busy professional who face a paucity of time or lack the interest and expertise required to maintain their homes because of their hectic work schedules\n" +
                    "Senior citizens who would like to hand over the physical demands of caring their home into capable, responsible hands\n" +
                    "Children of aging parents who would like to avail our comprehensive home care plans for their parent’s homes, so that they can rest assured of the quality of work and safety of their loved ones\n" +
                    "Single parent households who may find it difficult to care for their home and its various needs all alone\n" +
                    "Owners of nursing homes who would like to delegate the task of carrying about maintenance, repair and restoration tasks of their beloved institution to a professional body instead of relying on a bunch of amateur service engineers\n" +
                    "Owners of enterprises who may find it daunting to take care of the day-to-day repair and maintenance needs of their office spaces\n" +
                    "Builders, realtors and other home sellers who use our maintenance programs as a sales incentive tool to help their clients differentiate between a regular house and a property that is well maintained by us\n" +
                    "Our home caring plans also make a great gift for your beloved spouse, who can never seem to find the time from their busy work schedules to take care of that home ‘To-Do’ list. Gift them one of our home care services plans today – after all, who wouldn’t be thrilled to wash their hands off home caring chores!");
            interArray.put(intQuest2);

            JSONObject intQus3=new JSONObject();
            intQus3.put(QUESTION,"If you find the need for a major repair at my home, do you perform the repair work?");
            intQus3.put(ANSWER,"No, we will not perform major repairs to your home. We will only point out the major damage or fault that we detect and recommend you get the repair done. We have no financial interest in the repair work that we recommend to avoid conflict of interest and you can be assured that the recommendations are truly needed. Our goal is to act as your trusted advisor on all matters concerning the maintenance of your home. Eliminating financial interest mitigates all motives to recommend unneeded repairs for our financial gain so that our customers gain confidence and trust on us and our advises. We always operate in a manner that is truly in the best interest of our customers.");
            interArray.put(intQus3);

            JSONObject intQus4=new JSONObject();
            intQus4.put(QUESTION,"I own a new home/establishment, why would I need your service?");
            intQus4.put(ANSWER,"Owning a new home/establishment is even more of a reason to take advantage of our services. A new home/establishment is one of the significant investment people do in their lifetime. Our services will help in prolonging the new home feel by ascertaining that all components of your new home/establishment are functioning smoothly. The maintenance needs of a new home/establishment and old home/establishment are almost similar. The difference in their condition is because of deferred and poor maintenance in an older home/establishment.\n" +
                    "");
            interArray.put(intQus4);





            JSONObject intQus5=new JSONObject();
            intQus5.put(QUESTION,"How many service request can I open/raise per call?");
            intQus5.put(ANSWER,"For plan users, it depends on the plan. Otherwise, a customer can raise different tickets for different services. You can do it one call or multiple call, as per your need and convenience.");
            interArray.put(intQus5);

            JSONObject intQus6=new JSONObject();
            intQus6.put(QUESTION,"What much time does it take for the service request to get answered?");
            intQus6.put(ANSWER,"We have a response time mechanism of 3 hours; however, the service delivery depends upon the available resources, call flow etc.");
            interArray.put(intQus6);

            JSONObject intQus73=new JSONObject();
            intQus73.put(QUESTION,"What if I am not sure of my availability at my place?");
            intQus73.put(ANSWER,"We will schedule the service as per your availability.");
            interArray.put(intQus73);

            JSONObject intQus7=new JSONObject();
            intQus7.put(QUESTION,"What if I need the technician urgently?");
            intQus7.put(ANSWER,"We will prioritize your call if it is emergency.");
            interArray.put(intQus7);

            JSONObject intQus8=new JSONObject();
            intQus8.put(QUESTION,"Can I request the technician to shop for material, if required?");
            intQus8.put(ANSWER,"Yes. We will charge a convenience fee for bringing the material @ 50 or Rs.3/km, whichever is maximum.");
            interArray.put(intQus8);

            JSONObject intQus9=new JSONObject();
            intQus9.put(QUESTION,"What if technician comes at my place but due to some emergency, I had to leave and work is not yet done?");
            intQus9.put(ANSWER,"You can reschedule the call for another time.");
            interArray.put(intQus9);

            JSONObject intQus10=new JSONObject();
            intQus10.put(QUESTION,"Do you work on weekend and holidays?");
            intQus10.put(ANSWER,"Yes, except Gazetted holidays.");
            interArray.put(intQus10);

            JSONObject intQus11=new JSONObject();
            intQus11.put(QUESTION,"Is there any service warranty provided?");
            intQus11.put(ANSWER,"7 days’ work warranty limited to repair.");
            interArray.put(intQus11);

            mMainObj.put(INTERIOR_DESIGN_FAQ,interArray);


            JSONArray security=new JSONArray();

            JSONObject secur1=new JSONObject();
            secur1.put(QUESTION,"Do you provide any warranty for material?");
            secur1.put(ANSWER,"If any new material bought and used by us comes with warranty, then same will be informed to client.");
            security.put(secur1);

            JSONObject secur2=new JSONObject();
            secur2.put(QUESTION,"Who are typical users of the home care services?");
            secur2.put(ANSWER,"Many of our customers fall into one of the following categories:\n" +
                    "The busy professional who face a paucity of time or lack the interest and expertise required to maintain their homes because of their hectic work schedules\n" +
                    "Senior citizens who would like to hand over the physical demands of caring their home into capable, responsible hands\n" +
                    "Children of aging parents who would like to avail our comprehensive home care plans for their parent’s homes, so that they can rest assured of the quality of work and safety of their loved ones\n" +
                    "Single parent households who may find it difficult to care for their home and its various needs all alone\n" +
                    "Owners of nursing homes who would like to delegate the task of carrying about maintenance, repair and restoration tasks of their beloved institution to a professional body instead of relying on a bunch of amateur service engineers\n" +
                    "Owners of enterprises who may find it daunting to take care of the day-to-day repair and maintenance needs of their office spaces\n" +
                    "Builders, realtors and other home sellers who use our maintenance programs as a sales incentive tool to help their clients differentiate between a regular house and a property that is well maintained by us\n" +
                    "Our home caring plans also make a great gift for your beloved spouse, who can never seem to find the time from their busy work schedules to take care of that home ‘To-Do’ list. Gift them one of our home care services plans today – after all, who wouldn’t be thrilled to wash their hands off home caring chores!");
            security.put(secur2);

            JSONObject secur3=new JSONObject();
            secur3.put(QUESTION,"If you find the need for a major repair at my home, do you perform the repair work?\n");
            secur3.put(ANSWER,"No, we will not perform major repairs to your home. We will only point out the major damage or fault that we detect and recommend you get the repair done. We have no financial interest in the repair work that we recommend to avoid conflict of interest and you can be assured that the recommendations are truly needed. Our goal is to act as your trusted advisor on all matters concerning the maintenance of your home. Eliminating financial interest mitigates all motives to recommend unneeded repairs for our financial gain so that our customers gain confidence and trust on us and our advises. We always operate in a manner that is truly in the best interest of our custom");
            security.put(secur3);

            JSONObject secur4=new JSONObject();
            secur4.put(QUESTION,"I own a new home/establishment, why would I need your service?\n");
            secur4.put(ANSWER,"Owning a new home/establishment is even more of a reason to take advantage of our services. A new home/establishment is one of the significant investment people do in their lifetime. Our services will help in prolonging the new home feel by ascertaining that all components of your new home/establishment are functioning smoothly. The maintenance needs of a new home/establishment and old home/establishment are almost similar. The difference in their condition is because of deferred and poor maintenance in an older home/establishment.");
            security.put(secur4);

            JSONObject secur5=new JSONObject();
            secur5.put(QUESTION,"How many service request can I open/raise per call?");
            secur5.put(ANSWER,"For plan users, it depends on the plan. Otherwise, a customer can raise different tickets for different services. You can do it one call or multiple call, as per your need and convenience.");
            security.put(secur5);

            JSONObject secur6=new JSONObject();
            secur6.put(QUESTION,"What much time does it take for the service request to get answered?");
            secur6.put(ANSWER,"We have a response time mechanism of 3 hours; however, the service delivery depends upon the available resources, call flow etc.");
            security.put(secur6);

            JSONObject secur7=new JSONObject();
            secur7.put(QUESTION,"What if I am not sure of my availability at my place?");
            secur7.put(ANSWER,"We will schedule the service as per your availability.");
            security.put(secur7);

            JSONObject secur8=new JSONObject();
            secur8.put(QUESTION,"What if I need the technician urgently?");
            secur8.put(ANSWER,"We will prioritize your call if it is emergency.");
            security.put(secur8);

            JSONObject secur9=new JSONObject();
            secur9.put(QUESTION,"Can I request the technician to shop for material, if required?");
            secur9.put(ANSWER,"Yes. We will charge a convenience fee for bringing the material @ 50 or Rs.3/km, whichever is maximum.");
            security.put(secur9);

            JSONObject secur10=new JSONObject();
            secur10.put(QUESTION,"What if technician comes at my place but due to some emergency, I had to leave and work is not yet done?");
            secur10.put(ANSWER,"You can reschedule the call for another time.");
            security.put(secur10);

            JSONObject secur11=new JSONObject();
            secur11.put(QUESTION,"Do you work on weekend and holidays?");
            secur11.put(ANSWER,"Yes, except Gazetted holidays.");
            security.put(secur11);

            JSONObject secur12=new JSONObject();
            secur12.put(QUESTION,"Is there any service warranty provided?");
            secur12.put(ANSWER,"7 days’ work warranty limited to repair.");
            security.put(secur12);


            mMainObj.put(SECURITY_SERVICE_FAQ,security);



            JSONArray appliance=new JSONArray();

            JSONObject ap1=new JSONObject();
            ap1.put(QUESTION,"Is it a doorstep service?");
            ap1.put(ANSWER,"Yes, it’s a doorstep service but in case of some issues, we might take the appliance with us for repair");
            appliance.put(ap1);

            JSONObject ap2=new JSONObject();
            ap2.put(QUESTION,"Is cost of material required for repair included in rates charged by you?");
            ap2.put(ANSWER,"No, customer is responsible for cost of any material required.");
            appliance.put(ap2);

            JSONObject ap3=new JSONObject();
            ap3.put(QUESTION,"Do you provide any warranty for material?");
            ap3.put(ANSWER,"If any new material bought and used by us comes with warranty, then same will be informed to client.");
            appliance.put(ap3);

            JSONObject ap4=new JSONObject();
            ap4.put(QUESTION,"Who are typical users of the home care services?");
            ap4.put(ANSWER,"Many of our customers fall into one of the following categories:\n" +
                    "The busy professional who face a paucity of time or lack the interest and expertise required to maintain their homes because of their hectic work schedules\n" +
                    "Senior citizens who would like to hand over the physical demands of caring their home into capable, responsible hands\n" +
                    "Children of aging parents who would like to avail our comprehensive home care plans for their parent’s homes, so that they can rest assured of the quality of work and safety of their loved ones\n" +
                    "Single parent households who may find it difficult to care for their home and its various needs all alone\n" +
                    "Owners of nursing homes who would like to delegate the task of carrying about maintenance, repair and restoration tasks of their beloved institution to a professional body instead of relying on a bunch of amateur service engineers\n" +
                    "Owners of enterprises who may find it daunting to take care of the day-to-day repair and maintenance needs of their office spaces\n" +
                    "Builders, realtors and other home sellers who use our maintenance programs as a sales incentive tool to help their clients differentiate between a regular house and a property that is well maintained by us\n" +
                    "Our home caring plans also make a great gift for your beloved spouse, who can never seem to find the time from their busy work schedules to take care of that home ‘To-Do’ list. Gift them one of our home care services plans today – after all, who wouldn’t be thrilled to wash their hands off home caring chores!");
            appliance.put(ap4);


            JSONObject ap5=new JSONObject();
            ap5.put(QUESTION,"If you find the need for a major repair at my home, do you perform the repair work?");
            ap5.put(ANSWER,"No, we will not perform major repairs to your home. We will only point out the major damage or fault that we detect and recommend you get the repair done. We have no financial interest in the repair work that we recommend to avoid conflict of interest and you can be assured that the recommendations are truly needed. Our goal is to act as your trusted advisor on all matters concerning the maintenance of your home. Eliminating financial interest mitigates all motives to recommend unneeded repairs for our financial gain so that our customers gain confidence and trust on us and our advises. We always operate in a manner that is truly in the best interest of our customers.");
            appliance.put(ap5);


            JSONObject ap6=new JSONObject();
            ap6.put(QUESTION,"How many service request can I open/raise per call?");
            ap6.put(ANSWER,"For plan users, it depends on the plan. Otherwise, a customer can raise different tickets for different services. You can do it one call or multiple call, as per your need and convenience.");
            appliance.put(ap6);

            JSONObject ap7=new JSONObject();
            ap7.put(QUESTION,"What much time does it take for the service request to get answered?");
            ap7.put(ANSWER,"We have a response time mechanism of 3 hours; however, the service delivery depends upon the available resources, call flow etc.");
            appliance.put(ap7);

            JSONObject ap8=new JSONObject();
            ap8.put(QUESTION,"What if I am not sure of my availability at my place?");
            ap8.put(ANSWER,"We will schedule the service as per your availability.");
            appliance.put(ap8);

            JSONObject ap9=new JSONObject();
            ap9.put(QUESTION,"What if I need the technician urgently?");
            ap9.put(ANSWER,"We will prioritize your call if it is emergency.");
            appliance.put(ap9);


            JSONObject ap10=new JSONObject();
            ap10.put(QUESTION,"Can I request the technician to shop for material, if required?");
            ap10.put(ANSWER,"Yes. We will charge a convenience fee for bringing the material @ 50 or Rs.3/km, whichever is maximum.");
            appliance.put(ap10);

            JSONObject ap11=new JSONObject();
            ap11.put(QUESTION,"What if technician comes at my place but due to some emergency, I had to leave and work is not yet done?");
            ap11.put(ANSWER,"You can reschedule the call for another time.");
            appliance.put(ap11);

            JSONObject ap12=new JSONObject();
            ap12.put(QUESTION,"Do you work on weekend and holidays?");
            ap12.put(ANSWER,"Yes, except Gazetted holidays.");
            appliance.put(ap12);

            JSONObject ap13=new JSONObject();
            ap13.put(QUESTION,"Is there any service warranty provided?");
            ap13.put(ANSWER,"7 days’ work warranty limited to repair.");
            appliance.put(ap13);



            mMainObj.put(HOME_APPLIANCE_FAQ,appliance);


        }catch (Exception e){
            e.printStackTrace();
        }
        return mMainObj.getJSONArray(serviceName);
    }





    public static JSONArray getFaqForPlan(String planName) throws JSONException {

        JSONObject mMainObj=new JSONObject();
        try {
            //main Object deceleration
            JSONArray fixedFaq =new JSONArray();
            JSONObject ques1=new JSONObject();
            ques1.put(QUESTION,"What all services are included in Fixed Call Plan?");
            ques1.put(ANSWER,"Under this plan, the four basic services are offered: Electrical, Plumbing, Carpentry and AC Servicing.");
            fixedFaq.put(ques1);

            JSONObject ques2=new JSONObject();
            ques2.put(QUESTION,"What is the duration of Fixed Call Plan?");
            ques2.put(ANSWER,"What is the duration of Fixed Call Plan?");
            fixedFaq.put(ques2);

            JSONObject ques3=new JSONObject();
            ques3.put(QUESTION,"What is the duration of per visit?");
            ques3.put(ANSWER,"Duration of per visit shall be 60 minutes and 90 minutes.");
            fixedFaq.put(ques3);

            JSONObject ques4=new JSONObject();
            ques4.put(QUESTION,"How any visit shall be taken in one go w.r.t service?");
            ques4.put(ANSWER,"Visit will automatically calculated depending upon the time calculation by Check –in/Check Out process.\n" +
                    "Multiple tickets shall be consumed in one go.");
            fixedFaq.put(ques4);

            JSONObject ques5=new JSONObject();
            ques5.put(QUESTION,"What is the scope of work?");
            ques5.put(ANSWER," Scope of work shall be inside the radius of the premises only and does not cover work that involves governmental or regulatory permissions.");
            fixedFaq.put(ques5);

            JSONObject ques6=new JSONObject();
            ques6.put(QUESTION,"Is there any cap/restriction on the number of visits that are bind in the plan?");
            ques6.put(ANSWER,"No, all the visits can be consumed in a single service or multiple service.");
            fixedFaq.put(ques6);

            JSONObject ques7=new JSONObject();
            ques7.put(QUESTION,"Is there any Refund Policy?");
            ques7.put(ANSWER,"Yes, with the below mentioned conditions:");
            JSONArray childArray=new JSONArray();

            JSONObject point1=new JSONObject();
            point1.put(POINT,"A customer may request a refund within a period of 15 days. Refunds shall be made on a pro-rata basis considering the services already consumed.");
            childArray.put(point1);

            JSONObject point2=new JSONObject();
            point2.put(POINT,"Customer will not be eligible for refunds if 3 calls have been consumed within 15 days of plan activation");
            childArray.put(point2);

            JSONObject point3=new JSONObject();
            point3.put(POINT,"The Company reserves the right to cancel the subscription of a customer at any time during the subscription period, and in that case pro-data amount shall be refunded.");
            childArray.put(point3);

            JSONObject point4=new JSONObject();
            point4.put(POINT,"NOTE: MnM reserves the right to cancel any plan at any given time at its own discretion. In such cases, MnM will calculate and refund the appropriate amount, as decided by it, to the customer.");
            childArray.put(point4);

            JSONObject point5=new JSONObject();
            point5.put(POINT,"The Terms and Conditions for the Commercial Plans shall vary from project to project, and shall be confirmed post evaluation of the work scope.");
            childArray.put(point5);

            ques7.put(CHILD_ARRAY,childArray);
            fixedFaq.put(ques7);
            mMainObj.put(FIXED_PLAN,fixedFaq);



            JSONArray unlimitedFaq =new JSONArray();
            JSONObject Unques1=new JSONObject();
            Unques1.put(QUESTION,"How Many services my Unlimited Plan covers");
            Unques1.put(ANSWER,"Plan covers only these four basic services: Electrical, Plumbing, Carpentry and AC Servicing.");
            unlimitedFaq.put(Unques1);

            JSONObject Unques2=new JSONObject();
            Unques2.put(QUESTION,"Is any inspection will be done before plan start at my Home");
            Unques2.put(ANSWER,"Maintenance will be offered to the customer only after inspection of the AC set/electrical system/plumbing system/furniture, and ensuring that it is in the working/maintainable condition. Make n Make’s responsibility to maintain fixtures, fittings, equipment shall be limited to those in working condition at their maximum efficiency at the time of commencement of contract. In case the concerned fixture, fitting or equipment, though covered under the scope of service, is not in working condition, then it shall be first repaired on chargeable basis and then maintenance under contract shall start.");
            unlimitedFaq.put(Unques2);

            JSONObject Unques3=new JSONObject();
            Unques3.put(QUESTION,"Is there any limitation of repairing?");
            Unques3.put(ANSWER,"The company shall be under no obligation to provide repair / service under this plan if the set/system is not working because of improper use, negligence, overuse, unauthorized alteration, modification or substitution of any part or serial number, defacement, abnormal voltage fluctuation, rat bite, negligence on part of customer, acts of God like floods, lightening, earthquakes etc., or through causes generated by non-ordinary use. If our services are required as a result of the causes stated above, such services shall be charged extra as per the company price list in effect at the time of the repair work.");
            unlimitedFaq.put(Unques3);

            JSONObject Unques4=new JSONObject();
            Unques4.put(QUESTION,"Is AC shifting will be covered?");
            Unques4.put(ANSWER,"Un-installation & re-installation of AC Unit is not covered under the plan in case of change of location of product. Carpentry Service covers merely the maintenance of furniture and does not cover manufacturing of furniture. Electrical and Plumbing Services shall not cover installation of new equipment/fitting.");
            unlimitedFaq.put(Unques4);

            JSONObject Unques5=new JSONObject();
            Unques5.put(QUESTION,"Is AC filter cleaning covered under plan?");
            Unques5.put(ANSWER,"Routine cleaning of the AC filter on a Monthly/fortnightly basis does not fall under the scope of this Unlimited Plan pack.");
            unlimitedFaq.put(Unques5);

            JSONObject Unques6=new JSONObject();
            Unques6.put(QUESTION,"Is all repairs covered under plan?");
            Unques6.put(ANSWER,"Repairs that involve Civil work i.e. mason work, digging, paint, plaster, brick work, labour work etc. shall not be included under the scope of this service plan. MnM Services Pvt. Ltd. can provide you an estimate for such issues.");
            unlimitedFaq.put(Unques6);



            JSONObject Unques7=new JSONObject();
            Unques7.put(QUESTION,"What will be scope of work for the plan?");
            Unques7.put(ANSWER,"The scope of all services offered under the service pack will be restricted to the inside area of the premises only. Outside and government liaisons area and other agency scope will not be included. Broadly, the valid scope is elaborated as follows:");
            JSONArray childArray2=new JSONArray();

            JSONObject pointt1=new JSONObject();
            pointt1.put(POINT,"Electrical: Already installed DB/Switchboard /panels etc. are in scope provided that they are inside the premises only.");
            childArray2.put(pointt1);

            JSONObject pointt2=new JSONObject();
            pointt2.put(POINT,"Plumbing: Water line and Sewerage lines inside the premises only.");
            childArray2.put(pointt2);

            JSONObject pointt3=new JSONObject();
            pointt3.put(POINT,"Air conditioning: Includes Split, Window and Ductable ACs only (VRV AC units are excluded).");
            childArray2.put(pointt3);

            JSONObject pointt4=new JSONObject();
            pointt4.put(POINT,"Carpentry: Includes repairing of the furniture only. Manufacturing/fabrication/modification is not covered.");
            childArray2.put(pointt4);

            JSONObject pointt5=new JSONObject();
            pointt5.put(POINT,"The technician shall rectify the complaints for the existing set-up and will not carry out new installation work. Any new installation work will be intimated during call registration and we shall provide you a quote after the inspection.");
            childArray2.put(pointt5);

            JSONObject pointt6=new JSONObject();
            pointt6.put(POINT,"Some major repairs shall take more time and customer co-operation shall be appreciated.");
            childArray2.put(pointt6);

            JSONObject pointt7=new JSONObject();
            pointt7.put(POINT,"Each service call will be limited to a maximum duration of 90 minutes only");
            childArray2.put(pointt7);

            JSONObject pointt8=new JSONObject();
            pointt8.put(POINT,"In case the work requires further time, the customer will need to raise a fresh service ticket/request for completion on a subsequent day. This does not cover the bulk jobs");
            childArray2.put(pointt8);

            JSONObject pointt9=new JSONObject();
            pointt9.put(POINT,"In case of bulk jobs, which require over 90 minutes of service, the customers shall make a booking at least 48 hours in advance to ensure a smooth handling and completion of the job. ");
            childArray2.put(pointt9);


            Unques7.put(CHILD_ARRAY,childArray2);
            unlimitedFaq.put(Unques7);


            JSONObject Unques8=new JSONObject();
            Unques8.put(QUESTION,"How many sqft are there in 1 YARD?");
            Unques8.put(ANSWER,"How many sqft are there in 1 YARD?");
            unlimitedFaq.put(Unques8);

            JSONObject Unques9=new JSONObject();
            Unques9.put(QUESTION,"Is there any limit of AC’s(Window & Split) under Unlimited plan?");
            Unques9.put(ANSWER,"Maximum of 3, 5 and 7 ACs shall be covered under the unlimited plans for the Rs. 4999, Rs. 5999 and Rs. 6999 respectively. For additional ACs, separate charges as per the prevailing rate card of MnM Services Pvt. Ltd. shall be applicable.");
            unlimitedFaq.put(Unques9);




            JSONObject Unques10=new JSONObject();
            Unques10.put(QUESTION,"What is excluded in Unlimited plans?");
            Unques10.put(ANSWER,"The following jobs stand strictly excluded from the scope of the Service Packs:");
            JSONArray childArray22=new JSONArray();

            JSONObject pointt11=new JSONObject();
            pointt11.put(POINT,"Repair, installation, uninstallation of home appliances such as TV, washing machine, fridge, microwave, juicer-mixer-grinders, dishwashers, chimney etc.");
            childArray22.put(pointt11);

            JSONObject pointt12=new JSONObject();
            pointt12.put(POINT,"RO water system");
            childArray22.put(pointt12);

            JSONObject pointt13=new JSONObject();
            pointt13.put(POINT,"DG sets");
            childArray22.put(pointt13);

            JSONObject pointt14=new JSONObject();
            pointt14.put(POINT,"AC Installation & Uninstallation Works");
            childArray22.put(pointt14);

            JSONObject pointt15=new JSONObject();
            pointt15.put(POINT,"Water Cooler Cleaning");
            childArray22.put(pointt15);

            JSONObject pointt16=new JSONObject();
            pointt16.put(POINT,"Any uninstallation or installation");
            childArray22.put(pointt16);

            JSONObject pointt17=new JSONObject();
            pointt17.put(POINT,"Wooden Flooring");
            childArray22.put(pointt17);

            JSONObject pointt18=new JSONObject();
            pointt18.put(POINT,"Incidental civil work, special handling equipment for heavy and/or bulky products");
            childArray22.put(pointt18);

            JSONObject pointt10=new JSONObject();
            pointt10.put(POINT,"The Unlimited Plan shall get activated within 24 hours of making the payment. In case of payments made through cheque, the Plan will get activated 24 hours after the credit of cheque in MnM’s bank account. ");
            childArray22.put(pointt10);

            Unques10.put(CHILD_ARRAY,childArray22);
            unlimitedFaq.put(Unques10);


            JSONObject Unques11=new JSONObject();
            Unques11.put(QUESTION,"What is the duration of Unlimited Plan?");
            Unques11.put(ANSWER,"Unlimited Plan pack will automatically expire after a tenure of 1 year.");
            unlimitedFaq.put(Unques11);

            mMainObj.put(UNLIMITED_PLAN,unlimitedFaq);




            JSONArray addonPlan =new JSONArray();
            JSONObject Adques1=new JSONObject();
            Adques1.put(QUESTION,"Conversion from sq. ft. to yards?");
            Adques1.put(ANSWER,"1yard= 9 sq. ft.\n" +
                    "     166.6667= 1500 sq. ft.  \n");
            addonPlan.put(Adques1);

            JSONObject adques2=new JSONObject();
            adques2.put(QUESTION,"Please find the Add-On plan details below:");
            adques2.put(ANSWER,"W.r.t Place: Add-On Plan services are limited to the address against which Plan is purchased. Any service booked and used for any other address will be charged as per Company price list.\n" +
                    "W.r.t Services: Plan covers only those services which are included in the plan purchased.\n" +
                    "W.r.t Calls: In Add-On plan, customer can only take as many calls as included in the plan purchased. ");
            addonPlan.put(adques2);

            JSONObject adQues3=new JSONObject();
            adQues3.put(QUESTION,"Does Add-on plan include any basic service?");
            adQues3.put(ANSWER,"No, Add-on plan does not include any basic service, i.e. Electrical, Plumbing, Carpentry & AC service. For basic services, you can consider Add-On Plan or Add-On Call Plan.");
            addonPlan.put(adQues3);

            JSONObject adQues4=new JSONObject();
            adQues4.put(QUESTION,"Is there any limitation of repairing?");
            adQues4.put(ANSWER,"The company shall be under no obligation to provide repair / service under this plan if the set/system is not working because of improper use, negligence, overuse, unauthorized alteration, modification or substitution of any part or serial number, defacement, abnormal voltage fluctuation, rat bite, negligence on part of customer, acts of God like floods, lightening, earthquakes etc., or through causes generated by non-ordinary use. If our services are required as a result of the causes stated above, such services shall be charged extra as per the company price list in effect at the time of the repair work.");
            addonPlan.put(adQues4);

            JSONObject adQues5=new JSONObject();
            adQues5.put(QUESTION,"What will be scope of work?");
            adQues5.put(ANSWER,"The scope of all services offered under the Add-On plan will be restricted to the inside area of the premises only. Outside and government liaisons area and another agency scope will not be included.");
            addonPlan.put(adQues5);





            JSONObject Adques6=new JSONObject();
            Adques6.put(QUESTION,"How much does your service cost?");
            Adques6.put(ANSWER,"Any work outside the scope of Add-On plan, will be charged as per company price list.");
            addonPlan.put(Adques6);



            JSONObject Adques7=new JSONObject();
            Adques7.put(QUESTION,"Is there any Refund Policy?");
            Adques7.put(ANSWER,"Yes, with the below mentioned conditions: • A customer may request a refund within a period of 15 days. Refunds shall be made on a pro-rata basis considering the services already consumed. • Customer will not be eligible for refunds if 3 calls have been consumed within 15 days of plan activation • The Company reserves the right to cancel the subscription of a customer at any time during the subscription period, and in that case pro-data amount shall be refunded.");
            addonPlan.put(Adques7);



            JSONObject Adques8=new JSONObject();
            Adques8.put(QUESTION,"What is the duration of the Plan?");
            Adques8.put(ANSWER,"Add-On plan will be active for 1 year");
            addonPlan.put(Adques8);

            mMainObj.put(ADDON_PLAN,addonPlan);




        }catch (Exception e){
            e.printStackTrace();
        }
        return mMainObj.getJSONArray(planName);
    }



    public static String getMonth(int month) {

        String monthh = new DateFormatSymbols().getMonths()[month - 1];

        return monthh.substring(0, 3);
    }
    public static String giveDate() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        //year + "/" + month + "/" + day
        String ljmDay = "" + mDay;
        String mmonth = "" + mMonth;
        if (mDay < 10) {
            ljmDay = "0" + "" + mDay;
        }
        if (mMonth < 10) {
            mmonth = "0" + mMonth;
        }
        return mYear + "-" + mmonth + "-" + ljmDay;
    }
    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }

    }
    public static void call(Context mContext, String phoneNumber) {
        if (isAirplaneModeOn(mContext)) {
            ToastUtils.showToast(mContext, "To place a call, first turn off airplane mode.");
        } else {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + phoneNumber));
            mContext.startActivity(callIntent);
        }

    }
    public static void sendEmail(Context mContext,String emailId){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",emailId, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        mContext.startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    public static void shareApp(Context mContext,String msg){
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, R.string.share_subject);
        sharingIntent.putExtra(Intent.EXTRA_TEXT, msg);
        Intent intent = Intent.createChooser(sharingIntent, "Share via");
        mContext.startActivity(intent);
    }
    public static String getdate(String mdate){
        if(!StringUtils.isNullOrEmpty(mdate)) {
            String newdate = mdate.substring(0, mdate.indexOf("T"));
           String mStringDate= getDateString(newdate);
            String mString=mStringDate.replaceAll("-", "/");
            return mString;
        }else{
            return "";
        }
    }


    public static  JSONObject getData() {
        JSONObject main = new JSONObject();

        try {
            JSONArray arrayobj1=new JSONArray();
            JSONObject mustObj1=new JSONObject();

            JSONArray arrayaybool1=new JSONArray();
            JSONObject rangeObj=new JSONObject();
            rangeObj.put("range",new JSONObject().put("postdate",new JSONObject().put("gte","2017-05-01 00:00:00").put("lte","2017-05-10 23:59:59")));
            arrayaybool1.put(rangeObj);
            mustObj1.put("bool",new JSONObject().put("should",arrayaybool1));


            JSONObject mustObj2=new JSONObject();
            JSONArray arrayaybool2=new JSONArray();
            JSONObject matchObj=new JSONObject();
            matchObj.put("match",new JSONObject().put("platform","web"));
            arrayaybool2.put(matchObj);



            mustObj2.put("bool",new JSONObject().put("must_not",arrayaybool2));
            arrayobj1.put(mustObj1);
            arrayobj1.put(mustObj2);
            main.put("query",new JSONObject().put("bool",new JSONObject().put("must",arrayobj1)));


        } catch (Exception e) {
            e.printStackTrace();
        }
        return main;
    }
    public static void displayDialog(Context ctx,String title,String msg) {
        new AlertDialog.Builder(ctx).setMessage(msg)
                .setTitle(title)
                .setCancelable(true)
                .setNeutralButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();

                            }
                        })
                .show();
    }
      public static void displayDialogNew(Context ctx, String title, String msg, final com.mnm.services.interfaces.DialogBtnClick dialogBtnClick) {
        new AlertDialog.Builder(ctx).setMessage(msg)
                .setTitle(title)
                .setCancelable(true)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialogBtnClick.onDialogBtnClicked(1);
                                dialog.dismiss();

                            }
                        })
                .setNegativeButton(android.R.string.no,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialogBtnClick.onDialogBtnClicked(2);
                        dialog.dismiss();

                    }
                })
                .show();
    }


    public static String getDateString(String aDate) {
        String mString[]=aDate.split("-");
        String year=mString[0];
        String month=mString[1];
        String day=mString[2];
        String mDate=day+"-"+month+"-"+year;
        return mDate;

    }


    public static String getDatejdjString(String aDate) {
        String mString[]=aDate.split("/");
        String year=mString[2];
        String month=mString[1];
        String day=mString[0];
        String mDate=year+"-"+month+"-"+day;
        return mDate;

    }
    public static String getwalletDateString(String aDate) {
        String mDate="";
        Log.e("aDate ",aDate);
        if (!StringUtils.isNullOrEmpty(aDate)) {
            String mString[] = aDate.split("-");
            String year = mString[0];
            String month = mString[1];
            String day = mString[2];
            mDate = day + "-" + month + "-" + year;

        }
        Log.e("date ",mDate);
        return ""+mDate;
    }

    public static void saveAccessToken(String accessToken,Context mContext){
        com.mnm.services.prefrence.AppSharedPreference.putString(com.mnm.services.prefrence.PrefConstants.AUTHORIZATION_TOKEN, "" + accessToken, mContext);

    }
    }
