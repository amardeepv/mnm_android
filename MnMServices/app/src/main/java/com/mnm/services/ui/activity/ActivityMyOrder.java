package com.mnm.services.ui.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import com.mnm.services.R;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.fragment.FragmentOrder;
import com.mnm.services.ui.fragment.FragmnetPlanOrder;
import com.mnm.services.ui.tabs.SlidingTabLayout;
import com.mnm.services.utils.GlobalUtils;
import java.util.ArrayList;
import java.util.List;


public class ActivityMyOrder extends MyDrawerBaseActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order);
        intiUi();
    }
    private void intiUi() {
        findViewById(R.id.root_relay).setOnClickListener(this);
        initCustomActionBarView(ActivityMyOrder.this,"My Order",true);
        initViewPagerAndTabs();
        AppSharedPreference.putString(PrefConstants.SELECTED_SERVICE_DATA, "", ActivityMyOrder.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityMyOrder.this, v);
                break;
        }
    }
    private void initViewPagerAndTabs() {
        FragmentOrder fragmentOrder=new FragmentOrder();
        FragmnetPlanOrder fragmnetPlanOrder=new FragmnetPlanOrder();
        ViewPager  viewPager = (ViewPager) findViewById(R.id.viewPager);
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        String[] tabs = getResources().getStringArray(R.array.tabsOrder);
        pagerAdapter.addFragment(fragmentOrder, tabs[0]);
        pagerAdapter.addFragment(fragmnetPlanOrder, tabs[1]);
        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(pagerAdapter);
        SlidingTabLayout tabLayout = (SlidingTabLayout) findViewById(R.id.tabLayout);
        tabLayout.setDistributeEvenly(true);
        tabLayout.setViewPager(viewPager);
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white_color);          }

               /* @Override
                public int getDividerColor(int position) {
                    return getResources().getColor(R.color.color_primary_red);
                }*/
        });

        tabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });
    }

    class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }
}