package com.mnm.services.prefrence;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Created by satender.singh on 16/8/16.
 */
public class AppSharedPreference {

    // SharePrefrence Name

    /**
     * @param key
     * @param defValue
     * @param context
     * @return
     */
    public static boolean getBoolean(String key, boolean defValue, Context context) {
        return getSharedPreferences(context).getBoolean(key, defValue);
    }


    public static boolean checkKeyExist(String key, Context context) {
        return getSharedPreferences(context).contains(key);
    }

    /**
     * @param key
     * @param defValue
     * @param context
     * @return
     */
    public static float getFloat(String key, float defValue, Context context) {
        return getSharedPreferences(context).getFloat(key, defValue);
    }

    /**
     * @param key
     * @param defValue
     * @param context
     * @return
     */
    public static int getInt(String key, int defValue, Context context) {
        return getSharedPreferences(context).getInt(key, defValue);
    }

    /**
     * @param key
     * @param defValue
     * @param context
     * @return
     */
    public static long getLong(String key, long defValue, Context context) {
        return getSharedPreferences(context).getLong(key, defValue);
    }

    /**
     * @param key
     * @param defValue
     * @param context
     * @return
     */
    public static String getString(String key, String defValue, Context context) {
        try {
            return getSharedPreferences(context).getString(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
            return defValue;
        }
    }

    /**
     * @param key
     * @param value
     * @param context
     */
    public static void putBoolean(String key, boolean value, Context context) {
        Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    /**
     * @param key
     * @param value
     * @param context
     */
    public static void putFloat(String key, float value, Context context) {
        Editor editor = getSharedPreferences(context).edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    /**
     * @param key
     * @param value
     * @param context
     */
    public static void putInt(String key, int value, Context context) {
        Editor editor = getSharedPreferences(context).edit();
        editor.putInt(key, value);
        editor.commit();
    }

    /**
     * @param key
     * @param value
     * @param context
     */
    public static void putLong(String key, long value, Context context) {
        Editor editor = getSharedPreferences(context).edit();
        editor.putLong(key, value);
        editor.commit();
    }

    /**
     * @param key
     * @param defaultValue
     * @param context
     */
    public static void putString(String key, String defaultValue, Context context) {
        Editor editor = getSharedPreferences(context).edit();
        editor.putString(key, defaultValue);
        editor.commit();
    }

    /**
     * @param context
     * @return
     */
    private static SharedPreferences getSharedPreferences(Context context) {
        SharedPreferences pref = context.getSharedPreferences(PrefConstants.MNM_PREF_NAME, Context.MODE_PRIVATE);
        return pref;
    }


    public static void remove(String key, Context context) {
        Editor editor = getSharedPreferences(context).edit();
        editor.remove(key);
        editor.commit();
    }

}
