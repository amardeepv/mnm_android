package com.mnm.services.model.response;

import com.mnm.services.model.response.Data;
import java.util.List;

/**
 * Created by saten on 2/16/17.
 */

public class SocialLoginResponse {
    private Boolean hasError;
    private String messageCode;
    private String message;
    private Data data;
    private List<String> messages = null;




    public Boolean getHasError() {
        return hasError;
    }

    public void setHasError(Boolean hasError) {
        this.hasError = hasError;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }
}
