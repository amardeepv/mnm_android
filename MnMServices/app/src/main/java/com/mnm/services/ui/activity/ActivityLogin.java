package com.mnm.services.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.StringUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.Constant;
import com.mnm.services.google.GooglePlusUtils;
import com.mnm.services.google.IPlusClient;
import com.mnm.services.ui.fragment.FragmentLogin;
import com.mnm.services.ui.fragment.FragmentRegister;
import com.mnm.services.ui.tabs.SlidingTabLayout;
import com.mnm.services.utils.GlobalUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ActivityLogin extends MyDrawerBaseActivity implements View.OnClickListener, IPlusClient {
    public GooglePlusUtils mGooglePlusUtils;
    private String mUserFullName, mUserEmail, mProvider, mProviderId, mUserProfilePic;
    FragmentLogin fragmentLogin = new FragmentLogin();
    FragmentRegister fragmentRegister = new FragmentRegister();
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGooglePlusUtils = new GooglePlusUtils(this, this);
        setContentView(R.layout.activity_login_register);
        intiUi();
    }

    private void intiUi() {
        findViewById(R.id.root_relay).setOnClickListener(this);
        initCustomActionBarView(ActivityLogin.this, "MakenMake", true);
        initViewPagerAndTabs();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityLogin.this, v);
                break;
        }
    }

    private void initViewPagerAndTabs() {
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        String[] tabs = getResources().getStringArray(R.array.tabs_login_register);
        pagerAdapter.addFragment(fragmentLogin, tabs[0]);
        pagerAdapter.addFragment(fragmentRegister, tabs[1]);
        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(pagerAdapter);
        SlidingTabLayout tabLayout = (SlidingTabLayout) findViewById(R.id.tabLayout);
        tabLayout.setDistributeEvenly(true);
        if (getIntent() != null) {
            viewPager.setCurrentItem(getIntent().getIntExtra(Constant.LOGIN_TAB_NUMBER, 0));
        }

        tabLayout.setViewPager(viewPager);
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {

            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.white_color);
            }

               /* @Override
                public int getDividerColor(int position) {
                    return getResources().getColor(R.color.color_primary_red);
                }*/
        });

        tabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        break;
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void getGooglePlusInfo(GoogleApiClient plusClient) {
        //removeProgressDialog();
        try {
            if (Plus.PeopleApi.getCurrentPerson(plusClient) != null) {
                Person currentPerson = Plus.PeopleApi.getCurrentPerson(plusClient);
                mUserFullName = currentPerson.getDisplayName();
                mProviderId = currentPerson.getId();
                mProvider = "google";
                mUserProfilePic = currentPerson.getImage().getUrl();
                mUserEmail = Plus.AccountApi.getAccountName(plusClient);
                //api call here
                fragmentLogin.socialLoginApi("2", mProvider, mUserFullName, mProviderId, mUserEmail, mUserProfilePic);
            }
        } catch (Exception e) {
            ToastUtils.showToast(this, "Try again later.");
        }
    }

    @Override
    public void onGooglePlusLoginFailed() {

    }

    class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGooglePlusUtils != null)
            mGooglePlusUtils.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGooglePlusUtils != null)
            mGooglePlusUtils.onStop();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == GooglePlusUtils.REQUEST_CODE_SIGN_IN) {
            if (mGooglePlusUtils != null)
                mGooglePlusUtils.onActivityResult(this, requestCode, resultCode, data);
            if (resultCode != -1) {
                removeProgressDialog();
            }

        } else if (resultCode == RESULT_OK) {
            if (callbackManager != null)
                callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void googleLogin() {
       // showProgressDialog("Please wait...");
        if (ConnectivityUtils.isNetworkEnabled(this)) {
            if (mGooglePlusUtils == null) {
                mGooglePlusUtils = new GooglePlusUtils(this, this);
            }
            mGooglePlusUtils.onStart();
            mGooglePlusUtils.googlePlusLogin();
        } else {
            ToastUtils.showToast(this, "Please check your network connection");
        }
    }


    public void fbLogin(){
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        Log.d("Success", "Login");
                        Profile profile = Profile.getCurrentProfile();
                        try {
                            if (profile != null) {
                                mProviderId = profile.getId();
                                mProvider = "facebook";
                                mUserFullName = profile.getName();
                                // first check user exists or not
                                Uri mUse = profile.getProfilePictureUri(100, 100);
                                mUserProfilePic = mUse.toString();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.i("LoginActivity", response.toString());
                                try {
                                    if (!StringUtils.isNullOrEmpty(mProviderId)) {
                                        mProvider = "facebook";
                                        mProviderId = object.getString("id");
                                        mUserFullName = object.getString("first_name");
                                    }
                                    mUserEmail = object.getString("email");
                                    //api call here
                                    Log.e("prodivder ", " 1" + "socialId " + mProviderId + " mUserFullName " + mUserFullName + " mProviderId " + mProviderId + " mUserEmail " + mUserEmail);
                                    fragmentLogin.socialLoginApi("1", mProviderId, mUserFullName, mProviderId, mUserEmail, mUserProfilePic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Toast.makeText(ActivityLogin.this, "Try Again", Toast.LENGTH_LONG).show();
                                }
                                LoginManager.getInstance().logOut();

                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(ActivityLogin.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(ActivityLogin.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }
}