package com.mnm.services.ui.adapters;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kelltontech.utils.StringUtils;
import com.mnm.services.R;
import com.mnm.services.model.response.myorder.TicketHistory;
import com.mnm.services.utils.GlobalUtils;

import java.util.List;

public class AdapterTicketHistory extends BaseAdapter {
    private Context context;
    private List<TicketHistory> listTickethistory;

    public AdapterTicketHistory(Context context,List<TicketHistory> listTickethistory) {
        this.context = context;
        this.listTickethistory = listTickethistory;

    }

    @Override
    public int getCount() {
        return listTickethistory.size();
    }

    @Override
    public Object getItem(int position) {
        return listTickethistory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_ticket_history, null);
            holder = new ViewHolder();
            holder.txtDate = (TextView) convertView.findViewById(R.id.txt_date_ticket_history);
            holder.txtCheckIn=(TextView) convertView.findViewById(R.id.txt_check_in);
            holder.txtCheckOut=(TextView) convertView.findViewById(R.id.txt_check_out);
            holder.txtNextSchedule=(TextView) convertView.findViewById(R.id.txt_next_scheldule);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final TicketHistory ticketHistory=listTickethistory.get(position);
        String serviceDate="";
        if(!StringUtils.isNullOrEmpty(ticketHistory.getServiceDate())){
            String mServiceDate=ticketHistory.getServiceDate();
            if(!StringUtils.isNullOrEmpty(mServiceDate)) {
                serviceDate = mServiceDate.substring(0, mServiceDate.indexOf("T"));
            }
        }
        holder.txtDate.setText(GlobalUtils.getDateString(serviceDate));
        holder.txtCheckIn.setText(StringUtils.isNullOrEmpty(ticketHistory.getCheckIn())?"":ticketHistory.getCheckIn());
        holder.txtCheckOut.setText(StringUtils.isNullOrEmpty(ticketHistory.getCheckOut())?"":ticketHistory.getCheckOut());
        String nextSheduleDate="";
        if(!StringUtils.isNullOrEmpty(ticketHistory.getNextShceduleDate())){
            String mNextSheduleDate=ticketHistory.getNextShceduleDate();
            nextSheduleDate=mNextSheduleDate.substring(0,mNextSheduleDate.indexOf("T"));
        }
       holder.txtNextSchedule.setText(nextSheduleDate);

        return convertView;
    }

    class ViewHolder {
        private TextView txtDate;
        private TextView txtCheckIn;
        private TextView txtCheckOut;
        private TextView txtNextSchedule;
    }


}
