package com.mnm.services.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.android.volley.Response;
import com.kelltontech.utils.ConnectivityUtils;
import com.kelltontech.utils.StringUtils;
import com.kelltontech.utils.ToastUtils;
import com.mnm.services.R;
import com.mnm.services.constants.AppSettings;
import com.mnm.services.constants.Constant;
import com.mnm.services.controller.ControllerAddToBasket;
import com.mnm.services.controller.ControllerCoupan;
import com.mnm.services.controller.ControllerDefaultCoupan;
import com.mnm.services.controller.ControllerServiceTax;
import com.mnm.services.model.request.AddToBasketRequest;
import com.mnm.services.model.request.AutoCouponRequest;
import com.mnm.services.model.request.ApplyCouponRequest;
import com.mnm.services.model.request.ServicestoAdd;
import com.mnm.services.model.request.SimpleCouponRequestModel;
import com.mnm.services.model.response.AddToBasketResponse;
import com.mnm.services.model.response.CoupanResponse;
import com.mnm.services.model.response.CouponResultModel;
import com.mnm.services.model.response.DefaultCoupanRequest;
import com.mnm.services.model.response.ServiceModel;
import com.mnm.services.model.response.ServiceTaxResponse;
import com.mnm.services.model.response.TaxModel;
import com.mnm.services.prefrence.AppSharedPreference;
import com.mnm.services.prefrence.PrefConstants;
import com.mnm.services.ui.adapters.AdapterOrderService;
import com.mnm.services.ui.widgets.NonScrollListView;
import com.mnm.services.utils.GlobalUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ActivityOrderSummary extends MyDrawerBaseActivity implements View.OnClickListener {
    LinearLayout servicLayout;
    NonScrollListView nonScrollListView;
    RelativeLayout btnApplyCoupan;
    private ArrayList<ServiceModel> serviceList;
    private TextView txtAmountSubTotal;
    private TextView lblServiceTax,txtServiceTaxAmount;
    private TextView txtCoupanDiscountAmount;
    private TextView txtGrandTotalAmount;
    private EditText edtCoupanCode;
    LinearLayout coupanLayout;
    CheckBox checkboxTermAndConditions;
    float amountPayble,taxRate,taxAmount,totalBillAfterAmount;
    double walletAmount;
    double totalDiscountAmount=0;
    String addressID="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_summary);
        findViewById(R.id.relay_confirm).setOnClickListener(this);
        servicLayout=(LinearLayout)findViewById(R.id.servic_layout);
        nonScrollListView=(NonScrollListView)findViewById(R.id.lisview_non_scroll);
        btnApplyCoupan=(RelativeLayout)findViewById(R.id.btn_apply_coupan);
        edtCoupanCode=(EditText)findViewById(R.id.edt_coupan_code);
        txtAmountSubTotal=(TextView)findViewById(R.id.amount_sub_total);
        txtServiceTaxAmount=(TextView)findViewById(R.id.amount_service_tax);
        lblServiceTax=(TextView)findViewById(R.id.lbl_service_tax);
        txtGrandTotalAmount=(TextView)findViewById(R.id.grand_total_amount);
        txtCoupanDiscountAmount=(TextView)findViewById(R.id.coupon_discount_amount);
        coupanLayout=(LinearLayout)findViewById(R.id.coupan_linlay);
        checkboxTermAndConditions=(CheckBox)findViewById(R.id.checkbox_term_n_condition);
        btnApplyCoupan.setOnClickListener(this);
        initCustomActionBarView(ActivityOrderSummary.this,"Order Summary",true);
        addressID=getIntent().getStringExtra(Constant.ADDRESS_ID);
        serviceList= (ArrayList<ServiceModel>) getIntent().getSerializableExtra(Constant.SERVICE_SELECTED_DATA);
        if(getIntent().getBooleanExtra(Constant.IS_SHOW_OFFER_TRUE,false)){
            coupanLayout.setVisibility(View.GONE);
            applyDefaultCoupan();
        }else{
            coupanLayout.setVisibility(View.VISIBLE);
            callServiceTaxApi(""+getTotalAmount());
        }
        AdapterOrderService orderServiceAdapter=new AdapterOrderService(ActivityOrderSummary.this,serviceList);
        nonScrollListView.setAdapter(orderServiceAdapter);

    }

    void applyDefaultCoupan(){
        if (!ConnectivityUtils.isNetworkEnabled(ActivityOrderSummary.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerDefaultCoupan defaultCoupanController = new ControllerDefaultCoupan(this, this);
            DefaultCoupanRequest defaultCoupanRequest = getDefaultCoupanRequest();
            showProgressDialog("Please wait...");
            defaultCoupanController.getData(AppSettings.SERVICE_DEFAULT_COUPAN_EVENT, defaultCoupanRequest);
        }

    }
    DefaultCoupanRequest getDefaultCoupanRequest(){
        DefaultCoupanRequest defaultCoupanRequest=new DefaultCoupanRequest();
        List<AutoCouponRequest> lstAutoCouponRequest = new ArrayList<>();
        for (ServiceModel serviceModel:serviceList){
            AutoCouponRequest autoCouponRequest=new AutoCouponRequest();
            autoCouponRequest.setUserID(AppSharedPreference.getString(Constant.USER_ID,"",ActivityOrderSummary.this));
            autoCouponRequest.setServiceGroupID(""+serviceModel.getServiceid());
            double totalAmount;
            if(serviceModel.getQuantity()==0){
                totalAmount=serviceModel.getRate();
            }else{
                totalAmount=serviceModel.getQuantity()*serviceModel.getRate();
            }
            autoCouponRequest.setServiceAmount(totalAmount);
            autoCouponRequest.setChannel("MA");
            lstAutoCouponRequest.add(autoCouponRequest);
        }
        defaultCoupanRequest.setLstAutoCouponRequest(lstAutoCouponRequest);
        return defaultCoupanRequest;
    }
    private  void callServiceTaxApi(String totalAmount){
        if (!ConnectivityUtils.isNetworkEnabled(ActivityOrderSummary.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerServiceTax serviceTaxController = new ControllerServiceTax(this, this,totalAmount);
            showProgressDialog("Please wait...");
            serviceTaxController.getData(AppSettings.SERVICE_APPLY_TAX_EVENT,null);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.root_relay:
                GlobalUtils.hideKeyboardFrom(ActivityOrderSummary.this, v);
                break;
            case R.id.relay_confirm:
                if(checkboxTermAndConditions.isChecked()) {
                    callAddToBasketApi();
                }else {
                    ToastUtils.showToast(ActivityOrderSummary.this,"Please accept the terms and conditions");
                }
                break;
            case R.id.btn_apply_coupan:
                if(edtCoupanCode.getText().toString().length()!=0) {
                    if(!StringUtils.isNullOrEmpty(addressID)) {
                        applyCoupanApi();
                    }else{
                        ToastUtils.showToast(ActivityOrderSummary.this,"Please select address");
                    }
                }else{
                    ToastUtils.showToast(ActivityOrderSummary.this,"Please enter coupan code");
                }
                break;
        }
    }

    @Override
    protected void updateUi(Response response) {
        super.updateUi(response);
    }


    double getTotalAmount(){
        double totalAmount=0;
        if(serviceList!=null&&serviceList.size()>0){
            for(ServiceModel obj:serviceList) {
                totalAmount=totalAmount+obj.getRate();
            }
        }
        return totalAmount;
    }


    void applyCoupanApi() {
        if (!ConnectivityUtils.isNetworkEnabled(ActivityOrderSummary.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerCoupan loginController = new ControllerCoupan(this, this);
            ApplyCouponRequest coupanRequest = getCoupanRequest();
            showProgressDialog("Please wait...");
            loginController.getData(AppSettings.SERVICE_COUPAN_EVENT, coupanRequest);
        }
    }

    private ApplyCouponRequest getCoupanRequest() {
        ApplyCouponRequest coupanRequest= new ApplyCouponRequest();
        ArrayList<SimpleCouponRequestModel> simpleCouponRequestModelArrayList=new ArrayList<>();
        for (ServiceModel serviceModel:serviceList){
            SimpleCouponRequestModel simpleCouponRequestModel=new SimpleCouponRequestModel();
            simpleCouponRequestModel.setCouponCode(edtCoupanCode.getText().toString());
            simpleCouponRequestModel.setServiceGroupID(serviceModel.getServiceid());
            simpleCouponRequestModel.setAddressId(getIntent().getStringExtra(Constant.ADDRESS_ID));
            double totalAmount;
            if(serviceModel.getQuantity()==0){
                totalAmount=serviceModel.getRate();
            }else{
                totalAmount=serviceModel.getQuantity()*serviceModel.getRate();
            }
            simpleCouponRequestModel.setTotalAmount(totalAmount);
            simpleCouponRequestModel.setUserID(AppSharedPreference.getString(Constant.USER_ID,"",ActivityOrderSummary.this));
            simpleCouponRequestModel.setChannel("MA");
            simpleCouponRequestModel.setAddressId(addressID);
            simpleCouponRequestModelArrayList.add(simpleCouponRequestModel);

        }
        coupanRequest.setLstCouponRequest(simpleCouponRequestModelArrayList);
        return coupanRequest;
    }

    @Override
    public void handleUiUpdate(final com.kelltontech.network.Response response) {
        ActivityOrderSummary.this.runOnUiThread(new Runnable() {
            public void run() {
                removeProgressDialog();
                Log.e("Response", response.toString());
                if (response == null) {
                    ToastUtils.showToast(ActivityOrderSummary.this, getString(R.string.server_is_down_please_try_later));
                } else {
                    switch (response.getDataType()) {
                        case AppSettings.SERVICE_COUPAN_EVENT:
                            CoupanResponse coupanResponse = (CoupanResponse) response.getResponseObject();
                            if(!coupanResponse.isHasError()){
                                ToastUtils.showToast(ActivityOrderSummary.this,coupanResponse.getMessage());
                                btnApplyCoupan.setOnClickListener(null);
                                btnApplyCoupan.setEnabled(false);
                                double totalTaxableAmount=0;
                                for (CouponResultModel couponResultModel:coupanResponse.getData().getLstCouponResult()) {
                                    totalDiscountAmount =totalDiscountAmount+couponResultModel.getCouponDiscount();
                                    totalTaxableAmount=totalTaxableAmount+couponResultModel.getDiscountedAmount();
                                }
                                callServiceTaxApi(""+totalTaxableAmount);
                            }else {
                                callServiceTaxApi(""+getTotalAmount());
                                ToastUtils.showToast(ActivityOrderSummary.this,coupanResponse.getMessage());
                            }
                            break;
                        case AppSettings.SERVICE_DEFAULT_COUPAN_EVENT:
                            CoupanResponse mCoupanResponse= (CoupanResponse) response.getResponseObject();
                            if(!mCoupanResponse.isHasError()){
                                double totalTaxableAmount=0;
                                for (CouponResultModel couponResultModel:mCoupanResponse.getData().getLstCouponResult()) {
                                    totalDiscountAmount =totalDiscountAmount+couponResultModel.getCouponDiscount();
                                    totalTaxableAmount=totalTaxableAmount+couponResultModel.getDiscountedAmount();
                                }
                                callServiceTaxApi(""+totalTaxableAmount);
                            }else {
                                callServiceTaxApi(""+getTotalAmount());
                                ToastUtils.showToast(ActivityOrderSummary.this,mCoupanResponse.getMessage());
                            }
                            break;
                        case AppSettings.SERVICE_APPLY_TAX_EVENT:
                            ServiceTaxResponse serviceTaxResponse = (ServiceTaxResponse) response.getResponseObject();
                            TaxModel taxModel=serviceTaxResponse.getData();
                            if(!serviceTaxResponse.getHasError()){
                                amountPayble=taxModel.getTotalPayableAmount();
                                taxRate=taxModel.getTaxRate();
                                taxAmount=taxModel.getTaxAmount();
                                totalBillAfterAmount=taxModel.getBillAfterTax();
                                txtAmountSubTotal.setText("₹ "+amountPayble);
                                lblServiceTax.setText("Service Tax @"+taxRate);
                                txtServiceTaxAmount.setText("₹ "+taxAmount);
                                txtCoupanDiscountAmount.setText("₹ "+totalDiscountAmount);
                                txtGrandTotalAmount.setText("₹ "+totalBillAfterAmount);
                            }
                            break;
                        case AppSettings.SERVICE_ADD_TO_BASKET_EVENT:
                            AddToBasketResponse addToBasketResponse = (AddToBasketResponse) response.getResponseObject();
                            if(!addToBasketResponse.isHasError()){
                                ToastUtils.showToast(ActivityOrderSummary.this,addToBasketResponse.getMessage());

                                moveToNextScreen(addToBasketResponse.getData().getReferenceId());
                            }else{
                                ToastUtils.showToast(ActivityOrderSummary.this,addToBasketResponse.getMessage());
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        });
    }


    private  void callAddToBasketApi(){
        if (!ConnectivityUtils.isNetworkEnabled(ActivityOrderSummary.this)) {
            ToastUtils.showToast(this, getResources().getString(R.string.network_error));
        } else {
            ControllerAddToBasket addToBasketController = new ControllerAddToBasket(this, this,true);
            AddToBasketRequest addToBasketRequest=getAddToBasketRequest();
            showProgressDialog("Adding Service to basket..");
            addToBasketController.getData(AppSettings.SERVICE_ADD_TO_BASKET_EVENT, addToBasketRequest);
        }
    }
    AddToBasketRequest getAddToBasketRequest(){
        AddToBasketRequest addToBasketRequest=new AddToBasketRequest();
        ArrayList<ServicestoAdd> servicestoAdds=new ArrayList<>();
        for(ServiceModel serviceModel:serviceList) {
            if (serviceModel.getRate() != 0) {
                ServicestoAdd servicestoAdd = new ServicestoAdd();
                servicestoAdd.setUserID(AppSharedPreference.getString(PrefConstants.USER_ID, "", ActivityOrderSummary.this));
                servicestoAdd.setServiceID(serviceModel.getServiceid());
                servicestoAdd.setPlan("F");
                servicestoAdd.setServiceDate(getIntent().getStringExtra(Constant.DATE_AND_YEAR));
                servicestoAdd.setAmount("" + serviceModel.getRate());
                servicestoAdd.setDiscount("0");
                servicestoAdd.setSlotId(getIntent().getStringExtra(Constant.TIME_SLOT));


            /*double totalAmount;
            if(serviceModel.getQuantity()==0){
                serviceModel.setQuantity(1);
                totalAmount=serviceModel.getRate();
            }else {
                totalAmount=serviceModel.getQuantity()*serviceModel.getRate();
            }*/
                servicestoAdd.setNoOfCalls("" + serviceModel.getQuantity());
                servicestoAdd.setQuantity((serviceModel.getQuantity()==0)?"1":""+serviceModel.getQuantity());
                servicestoAdd.setTotalAmount("" + serviceModel.getRate());
                servicestoAdd.setStatus("1");
                servicestoAdd.setCreatedBy("1");
                servicestoAdd.setCreated(GlobalUtils.getCurrentDate());
                // servicestoAdd.setSlotId("");
                servicestoAdd.setCouponId("0");
                servicestoAdd.setCouponAmount("0");
                servicestoAdd.setAddressId(getIntent().getStringExtra(Constant.ADDRESS_ID));
                servicestoAdd.setServiceDesc(getIntent().getStringExtra(Constant.FEEDBACK));
                servicestoAdds.add(servicestoAdd);
                servicestoAdd.setServiceDesc(getIntent().getStringExtra(Constant.FEEDBACK));
            }
        }
        addToBasketRequest.setServicestoAdd(servicestoAdds);
        return addToBasketRequest;

    }

    void moveToNextScreen(String addToBasketReferanceId){
        Intent mIntent=new Intent(ActivityOrderSummary.this,ActivityPayment.class);
        mIntent.putExtra(Constant.SERVICE_SELECTED_DATA,  (Serializable)  (List<ServiceModel>) getIntent().getSerializableExtra(Constant.SERVICE_SELECTED_DATA));
        // mIntent.putExtra(Constant.DATE_AND_YEAR,getIntent().getStringExtra(Constant.DATE_AND_YEAR));
        // mIntent.putExtra(Constant.TIME_SLOT,getIntent().getStringExtra(Constant.TIME_SLOT));
        //mIntent.putExtra(Constant.ADDRESS_ID,getIntent().getStringExtra(Constant.ADDRESS_ID));
        //mIntent.putExtra(Constant.AMOUNT_PAYBLE,amountPayble);
        mIntent.putExtra(Constant.TAX_RATE,""+taxRate);
        mIntent.putExtra(Constant.TOTAL_BILL_AFTER_AMOUNT,""+totalBillAfterAmount);
        mIntent.putExtra(Constant.WALLET_AMOUNT,""+walletAmount);
        mIntent.putExtra(Constant.TAX_AMOUNT,""+(totalBillAfterAmount-amountPayble));
        mIntent.putExtra(Constant.SERVICE_NAME,getIntent().getStringExtra(Constant.SERVICE_NAME));
        mIntent.putExtra(Constant.TIME_SLOT,getIntent().getStringExtra(Constant.TIME_SLOT));
        mIntent.putExtra(Constant.IS_PLAN,false);
        mIntent.putExtra(Constant.FEEDBACK,getIntent().getStringExtra(Constant.FEEDBACK));
        mIntent.putExtra(Constant.ADD_TO_BASKET_REFERENCE_ID,addToBasketReferanceId);
        startActivity(mIntent);
    }
}