package com.mnm.services.ui.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import com.kelltontech.network.Response;
import com.kelltontech.ui.IScreen;

/**
 * Created by amit.m on 21-12-2016.
 */

public class FragmentBase extends Fragment implements IScreen {
    private ProgressDialog mProgressDialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return null;
    }
    public void showProgressDialog(String bodyText) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setOnKeyListener(new Dialog.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    return keyCode == KeyEvent.KEYCODE_CAMERA || keyCode == KeyEvent.KEYCODE_SEARCH;
                }
            });
        }

        mProgressDialog.setMessage(bodyText);
        //play store crash MOb-612
        if (!getActivity().isFinishing()) {
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        }


    }

    public void removeProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
    @Override
    public final void handleUiUpdate(final Response response) {
        if (getActivity().isFinishing()) {
            return;
        }
        try {
            updateUi(response);
        } catch (Exception e) {
            Log.i(getClass().getSimpleName(), "updateUi()", e);
        }
    }

    /**
     * Subclass should over-ride this method to update the UI with response
     * <p/>
     * param response
     */
    protected void updateUi(Response response) {

    }

}
