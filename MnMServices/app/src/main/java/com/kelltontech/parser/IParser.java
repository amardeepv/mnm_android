package com.kelltontech.parser;

public interface IParser {

    Object serialize(byte[] response) throws Exception;

}

