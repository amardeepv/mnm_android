package com.kelltontech.ui;


/**
 * @author anoop.singh
 */
public interface CommonEventHandler {

    /**
     * param eventType
     * param eventData
     */
    void onEvent(int eventType, Object eventData);
}

