package com.kelltontech.utils.facebook.listener;

/**
 * Post on Wall Listener
 *
 * @author monish.agarwal
 */
public interface FacebookPostListener {
    void doAfterPostOnWall(boolean status);
}
