package com.kelltontech.utils.facebook.model;

/**
 * @author monish.agarwal
 */
public class FriendList extends Base {

    Picture picture;

    public FriendList() {

    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }


}
