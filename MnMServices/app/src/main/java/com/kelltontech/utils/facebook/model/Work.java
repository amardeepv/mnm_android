package com.kelltontech.utils.facebook.model;

/**
 * @author monish.agarwal
 */
public class Work {

    Base position;
    Base employer;
    Base location;

    /**
     * @return the position
     */
    public Base getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(Base position) {
        this.position = position;
    }

    /**
     * @return the employer
     */
    public Base getEmployer() {
        return employer;
    }

    /**
     * @param employer the employer to set
     */
    public void setEmployer(Base employer) {
        this.employer = employer;
    }

    /**
     * @return the location
     */
    public Base getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(Base location) {
        this.location = location;
    }


}
