package com.kelltontech.utils.facebook.listener;


import com.kelltontech.utils.facebook.model.FriendList;

import java.util.List;

/**
 * Get user Friend List Listener
 *
 * @author monish.agarwal
 */
public interface FacebookFriendListListener {
    void doAfterFriendList(List<FriendList> friendLists);
}
