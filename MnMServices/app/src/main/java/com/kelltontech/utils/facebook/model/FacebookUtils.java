package com.kelltontech.utils.facebook.model;//package com.kelltontech.utils.facebook.model;
//
//import android.annotation.SuppressLint;
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.os.Bundle;
//import android.util.Log;
//
//import com.facebook.FacebookAuthorizationException;
//import com.facebook.FacebookOperationCanceledException;
//import com.facebook.FacebookRequestError;
//import com.facebook.HttpMethod;
//import com.facebook.Request;
//import com.facebook.Request.GraphUserCallback;
//import com.facebook.Request.GraphUserListCallback;
//import com.facebook.RequestAsyncTask;
//import com.facebook.Response;
//import com.facebook.Session;
//import com.facebook.SessionState;
//import com.facebook.model.GraphUser;
//import com.facebook.widget.FacebookDialog;
//import com.google.gson.Gson;
//import com.google.gson.reflect.TypeToken;
//import com.kelltontech.utils.facebook.listener.FacebookFriendListListener;
//import com.kelltontech.utils.facebook.listener.FacebookLoginListener;
//import com.kelltontech.utils.facebook.listener.FacebookPostListener;
//import com.oneassist.R;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.Arrays;
//import java.util.Collection;
//import java.util.List;
//
///**
// * @author monish.agarwal created: 15-oct-2013
// */
//public class FacebookUtils {
//    private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
//    public static FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
//        @Override
//        public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
//            Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
//        }
//
//        @Override
//        public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
//            Log.d("HelloFacebook", "Success!");
//        }
//    };
//    static UserInfo mUserInfo;
//    static List<FriendList> mFriendList;
//    @SuppressLint("StaticFieldLeak")
//    private static Activity mActivity;
//    private static PendingAction pendingAction = PendingAction.NONE;
//    public static Session.StatusCallback callback = new Session.StatusCallback() {
//        @Override
//        public void call(Session session, SessionState state, Exception exception) {
//            onSessionStateChange(session, state, exception);
//        }
//    };
//
//    private static boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
//        for (String string : subset) {
//            if (!superset.contains(string)) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//    public static void postOnWall(Activity pActivity, final FacebookPostListener facebookPostListener) {
//        mActivity = pActivity;
//
//        Session session = Session.getActiveSession();
//        if (session.getState().isOpened()) {
//            if (session != null) {
//
//                // Check for publish permissions
//                List<String> permissions = session.getPermissions();
//                if (!isSubsetOf(PERMISSIONS, permissions)) {
//
//                    Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(mActivity, PERMISSIONS);
//                    session.requestNewPublishPermissions(newPermissionsRequest);
//                    return;
//                }
//
//                Bundle postParams = new Bundle();
//                postParams.putString("name", "Facebook SDK for Android");
//                postParams.putString("caption", "Build great social apps and get more installs.");
//                postParams.putString("description", "The Facebook SDK for Android makes it easier and faster to develop Facebook integrated Android apps.");
//                postParams.putString("link", "https://developers.facebook.com/android");
//                postParams.putString("picture", "https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png");
//
//                Request.Callback callback = new Request.Callback() {
//                    public void onCompleted(Response response) {
//                        JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();
//                        String postId = null;
//                        try {
//                            postId = graphResponse.getString("id");
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        FacebookRequestError error = response.getError();
//
//                        if (error != null) {
//                            facebookPostListener.doAfterPostOnWall(true);
//
//                        } else {
//                            facebookPostListener.doAfterPostOnWall(false);
//                        }
//
//                    }
//                };
//
//                Request request = new Request(session, "me/feed", postParams, HttpMethod.POST, callback);
//
//                RequestAsyncTask task = new RequestAsyncTask(request);
//                task.execute();
//            }
//        } else {
//            facebookLogin(mActivity, new FacebookLoginListener() {
//
//                @Override
//                public void doAfterLogin(UserInfo userInfo) {
//
//                    postOnWall(mActivity, facebookPostListener);
//
//                }
//            });
//        }
//
//    }
//
//    public static void getFriendList(Activity pActivity, final FacebookFriendListListener facebookFriendListListener) {
//        mActivity = pActivity;
//
//        Session activeSession = Session.getActiveSession();
//        if (activeSession.getState().isOpened()) {
//            Request friendRequest = Request.newMyFriendsRequest(activeSession, new GraphUserListCallback() {
//
//                @Override
//                public void onCompleted(List<GraphUser> users, Response response) {
//
//                    System.out.println(response.getGraphObject().getProperty("data"));
//
//                    Gson gson = new Gson();
//
//                    List<FriendList> friendLists = gson.fromJson(response.getGraphObject().getProperty("data").toString(), new TypeToken<List<FriendList>>() {
//                    }.getType());
//                    facebookFriendListListener.doAfterFriendList(friendLists);
//                    System.out.println(friendLists.get(0).getName());
//
//                }
//            });
//            Bundle params = new Bundle();
//            params.putString("fields", "id,name,picture");
//            friendRequest.setParameters(params);
//            friendRequest.executeAsync();
//
//        } else {
//            facebookLogin(mActivity, new FacebookLoginListener() {
//
//                @Override
//                public void doAfterLogin(UserInfo userInfo) {
//
//                    getFriendList(mActivity, facebookFriendListListener);
//
//                }
//            });
//        }
//
//    }
//
//	/*
//	 * public static void loginFacebook(Activity pActivity, final
//	 * FacebookLoginListener facebookListener) {
//	 *
//	 * mActivity = pActivity;
//	 *
//	 * Session.openActiveSession(pActivity, true, new Session.StatusCallback() {
//	 *
//	 * @Override public void call(Session session, SessionState state, Exception
//	 * exception) { // TODO Auto-generated method stub if (session.isOpened()) {
//	 *
//	 * Request me = Request.newMeRequest(session, new GraphUserCallback() {
//	 *
//	 * @Override public void onCompleted(GraphUser user, Response response) { //
//	 * TODO Auto-generated method stub if (user != null) {
//	 *
//	 * System.out.println(user .getInnerJSONObject());
//	 *
//	 * Gson gson = new Gson();
//	 *
//	 * UserInfo userInfo = gson .fromJson( user.getInnerJSONObject()
//	 * .toString(), UserInfo.class);
//	 *
//	 * facebookListener .doAfterLogin(userInfo);
//	 *
//	 * } } }); Bundle params = me.getParameters(); params.putString("fields",
//	 * "email,name"); me.setParameters(params); me.executeAsync();
//	 *
//	 *
//	 *
//	 * Request.newMeRequest(session, new GraphUserCallback() {
//	 *
//	 * @Override public void onCompleted(GraphUser user, Response response) { //
//	 * TODO Auto-generated method stub if (user != null) {
//	 *
//	 * System.out.println(user .getInnerJSONObject());
//	 *
//	 * Gson gson = new Gson();
//	 *
//	 * UserInfo userInfo = gson .fromJson( user.getInnerJSONObject()
//	 * .toString(), UserInfo.class);
//	 *
//	 * facebookListener .doAfterLogin(userInfo);
//	 *
//	 * } } }).executeAsync();
//	 *
//	 * } } });
//	 *
//	 * }
//	 */
//
//    public static void facebookLogin(Activity context, final FacebookLoginListener iFacebookUser) {
//
//        Session.OpenRequest openRequest = new Session.OpenRequest(context);
//        openRequest.setPermissions(Arrays.asList("email"));
//        openRequest.setCallback(new Session.StatusCallback() {
//
//            @Override
//            public void call(Session session, SessionState state, Exception exception) {
//                if (session.isOpened()) {
//
//                    // make request to the /me API
//                    Request request = Request.newMeRequest(session, new GraphUserCallback() {
//                        @Override
//                        public void onCompleted(GraphUser user, Response response) {
//                            if (user != null) {
//
//								/*
//                                 * System.out.println(user
//								 * .getInnerJSONObject());
//								 */
//
//                                Gson gson = new Gson();
//
//                                UserInfo userInfo = gson.fromJson(user.getInnerJSONObject().toString(), UserInfo.class);
//                                userInfo.setAccessToken(Session.getActiveSession().getAccessToken());
//                                iFacebookUser.doAfterLogin(userInfo);
//
//                            }
//                        }
//                    });
//                    request.executeAsync();
//                }
//            }
//        });
//        Session session = new Session(context);
//        Session.setActiveSession(session);
//        session.openForRead(openRequest);
//    }
//
//    public static void loginFacebook(Activity pActivity, final FacebookLoginListener facebookListener) {
//
//        mActivity = pActivity;
//
//        Session.openActiveSession(pActivity, true, new Session.StatusCallback() {
//
//            @Override
//            public void call(Session session, SessionState state, Exception exception) {
//                // TODO Auto-generated method stub
//                if (session.isOpened()) {
//                    Request.newMeRequest(session, new GraphUserCallback() {
//
//                        @Override
//                        public void onCompleted(GraphUser user, Response response) {
//                            // TODO Auto-generated method stub
//                            if (user != null) {
//
//                                System.out.println(user.getInnerJSONObject());
//                                Log.d("JSONObject()-toString()", user.getInnerJSONObject().toString());
//								/*
//								 * Log.d("JSONObject()", user
//								 * .getInnerJSONObject());
//								 */
//                                Gson gson = new Gson();
//
//                                UserInfo userInfo = gson.fromJson(user.getInnerJSONObject().toString(), UserInfo.class);
//
//                                facebookListener.doAfterLogin(userInfo);
//
//                            }
//                        }
//                    }).executeAsync();
//
//                }
//            }
//        });
//
//    }
//
//    public static void logout() {
//
//        Session session = Session.getActiveSession();
//        if (session != null)
//            session.closeAndClearTokenInformation();
//
//    }
//
//    private static void onSessionStateChange(Session session, SessionState state, Exception exception) {
//        if (pendingAction != PendingAction.NONE && (exception instanceof FacebookOperationCanceledException || exception instanceof FacebookAuthorizationException)) {
//            new AlertDialog.Builder(mActivity).setTitle(R.string.cancelled).setMessage(R.string.permission_not_granted).setPositiveButton(R.string.ok, null).show();
//            pendingAction = PendingAction.NONE;
//        } else if (state == SessionState.OPENED_TOKEN_UPDATED) {
//
//        }
//        updateUI();
//    }
//
//    public static void updateUI() {// to update UI at time of login and
//        // logout....................
//        Session session = Session.getActiveSession();
//
//    }
//
//    private enum PendingAction {
//        NONE, POST_PHOTO, POST_STATUS_UPDATE
//    }
//}
