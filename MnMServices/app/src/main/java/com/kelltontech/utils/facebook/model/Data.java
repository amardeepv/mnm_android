package com.kelltontech.utils.facebook.model;

/**
 * @author monish.agarwal
 */
public class Data {

    String is_silhouette;
    String url;

    /**
     * @return the is_silhouette
     */
    public String getIs_silhouette() {
        return is_silhouette;
    }

    /**
     * @param is_silhouette the is_silhouette to set
     */
    public void setIs_silhouette(String is_silhouette) {
        this.is_silhouette = is_silhouette;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }


}
