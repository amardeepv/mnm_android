package com.kelltontech.utils.facebook.model;

/**
 * @author monish.agarwal
 */
public class Education {
    String type;
    Base school;

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the school
     */
    public Base getSchool() {
        return school;
    }

    /**
     * @param school the school to set
     */
    public void setSchool(Base school) {
        this.school = school;
    }


}
