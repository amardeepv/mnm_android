package com.kelltontech.utils.facebook.model;

/**
 * @author monish.agarwal
 */
public class Picture {
    Data data;

    /**
     * @return the data
     */
    public Data getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(Data data) {
        this.data = data;
    }


}
